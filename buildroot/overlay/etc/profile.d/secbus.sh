#
# SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
# Copyright (C) - Telecom Paris
# Contacts: contact-secbus@telecom-paris.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

alias ll="ls -algs"
export PS1='\u@secbus> '
if [ ! -d /sdcard ]; then
	mkdir /sdcard
fi
if [ ! -r /sdcard/player ]; then
	mount /dev/mmcblk0p1 /sdcard
fi
cat <<!

   _____           ____
  / ___/___  _____/ __ )__  _______
  \\__ \\/ _ \\/ ___/ __  / / / / ___/
 ___/ /  __/ /__/ /_/ / /_/ (__  )
/____/\\___/\\___/_____/\\__,_/____/
(C) Telecom Paris
https://secbus.telecom-paris.fr/

username / password
  root       secbus
!
