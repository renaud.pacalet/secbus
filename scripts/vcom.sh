#/usr/bin/env bash
#
# SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
# Copyright (C) - Telecom Paris
# Contacts: contact-secbus@telecom-paris.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

dir=$(dirname $(realpath $0))
vhdl=$(dirname $dir)/vhdl
while read line; do
	line="${line%%#*}"
	line="${line#"${line%%[![:space:]]*}"}"
    line="${line%"${line##*[![:space:]]}"}"
	if [ -n "$line" ]; then
		lib="${line##* }"
		vhd="$vhdl/${line%% *}"
		if [ ! -d .$lib ]; then
			vlib .$lib
			vmap $lib .$lib
		fi
		vcom -2008 -work $lib $vhd
	fi
done < $dir/vhdl.lst
	
# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
