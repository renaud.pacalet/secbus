#!/usr/bin/env ash
#
# SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
# Copyright (C) - Telecom Paris
# Contacts: contact-secbus@telecom-paris.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#


HSM_IO_GPI_ADDR=0x81000078

cfg=$( devmem $HSM_IO_GPI_ADDR 32 )
cfg=$(( cfg + 0))

devmem $HSM_IO_GPI_ADDR 32 0x00000000

case $cfg in
	0)  pdir=G4K_NN;;
	1)  pdir=G4K_ON;;
	2)  pdir=G4K_NM;;
	3)  pdir=G4K_OM;;
	4)  pdir=G4K_CN;;
	5)  pdir=G4K_NT;;
	6)  pdir=G4K_CT;;
	7)  pdir=G64K_NN;;
	8)  pdir=G64K_ON;;
	9)  pdir=G64K_NM;;
	10) pdir=G64K_OM;;
	11) pdir=G64K_CN;;
	12) pdir=G64K_NT;;
	13) pdir=G64K_CT;;
	14) pdir=G1M_NN;;
	15) pdir=G1M_ON;;
	16) pdir=G1M_NM;;
	17) pdir=G1M_OM;;
	18) pdir=G1M_CN;;
	19) pdir=G1M_NT;;
	20) pdir=G1M_CT;;
	21) pdir=G16M_NN;;
	22) pdir=G16M_ON;;
	23) pdir=G16M_NM;;
	24) pdir=G16M_OM;;
	25) pdir=G16M_CN;;
	26) pdir=G16M_NT;;
	27) pdir=G16M_CT;;
	*)  pdir=HSM_DISABLED;;
esac

echo "Configuration: $pdir"
pdir=/root/perf/$pdir

if [ $# -ne 0 -a $# -ne 1 ]; then
	echo "usage: perf.sh [dir]"
	echo "  dir: output directory (default $pdir)"
	exit 1
fi

mkdir -p $pdir
echo "dmesg > $pdir/dmesg.txt"
dmesg > $pdir/dmesg.txt
devmem $HSM_IO_GPI_ADDR 32 0x01010101
echo "dhrystone 10000000 | tee $pdir/dhrystone_10000000.txt"
dhrystone 10000000 | tee $pdir/dhrystone_10000000.txt
devmem $HSM_IO_GPI_ADDR 32 0x02020202
echo "whetstone 10000 | tee $pdir/whetstone_10000.txt"
whetstone 10000 | tee $pdir/whetstone_10000.txt
devmem $HSM_IO_GPI_ADDR 32 0x04040404
echo "ramspeed -b 1 -g 1 | tee $pdir/ramspeed_b1_g1.txt"
ramspeed -b 1 -g 1 | tee $pdir/ramspeed_b1_g1.txt
devmem $HSM_IO_GPI_ADDR 32 0x08080808
echo "ramspeed -b 2 -g 1 | tee $pdir/ramspeed_b2_g1.txt"
ramspeed -b 2 -g 1 | tee $pdir/ramspeed_b2_g1.txt
devmem $HSM_IO_GPI_ADDR 32 0x10101010
echo "ramsmp -b 1 -g 1 | tee $pdir/ramsmp_b1_g1.txt"
ramsmp -b 1 -g 1 | tee $pdir/ramsmp_b1_g1.txt
devmem $HSM_IO_GPI_ADDR 32 0x20202020
echo "ramsmp -b 2 -g 1 | tee $pdir/ramsmp_b2_g1.txt"
ramsmp -b 2 -g 1 | tee $pdir/ramsmp_b2_g1.txt
devmem $HSM_IO_GPI_ADDR 32 0x40404040

cp -r $pdir /sdcard
sync
echo "Results stored in /sdcard."
echo "Do not forget to properly unmount /sdcard before power off:"
echo "root@secbus> umount /sdcard"
echo "root@secbus> poweroff"
devmem $HSM_IO_GPI_ADDR 32 0xffffffff
