#
# SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
# Copyright (C) - Telecom Paris
# Contacts: contact-secbus@telecom-paris.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

set expected_version 2018.3
set v [version -short]
if { $v != $expected_version } {
	puts "
WARNING: Vivado version ($v) is not the expected one ($expected_version).
WARNING: Trying anyway but be warned that things may go wrong."
}

set board [get_board_parts em.avnet.com:zed:part0:1.3]
set part xc7z020clg484-1
set chipscope 0
set vendor www.telecom-paris.fr
set library SecBus
set frequency_mhz 100

proc usage {} {
  puts "
usage: vivado -mode batch -source <script> [-tclargs <frequency_mhz> <w/o chipscope>]
  <script>:        TCL script
  <frequency_mhz>: target clock frequency in MHz
  <w/o chipscope>: 0/1 flag (default: 0), if set inserts in-circuit debugging modules"
  exit -1
}

if { $argc == 2 } {
  set frequency_mhz [lindex $argv 0]
  set chipscope [lindex $argv 1]
  if { $chipscope != 0 && $chipscope != 1 } {
    puts "invalid parameter: $chipscope"
    usage
  }
} elseif { $argc != 0 } {
  usage
}

set script [file normalize [info script]]
set scriptdir [file dirname $script]
set src "[file dirname $scriptdir]/vhdl"
regsub {\..*} [file tail $script] "" design

puts "*********************************************"
puts "Summary of build parameters"
puts "*********************************************"
puts "Board: $board"
puts "Part: $part"
puts "Source directory: $src"
puts "Design name: $design"
puts "Frequency: $frequency_mhz MHz"
puts "W/O chipscope: $chipscope"
puts "*********************************************"

#############
# Create IP #
#############
set_part $part
set_property board_part $board [current_project]
set fp [open "$scriptdir/vhdl.lst" r]
set file_data [read $fp]
close $fp
set data [split $file_data "\n"]
foreach line $data {
	if { [regexp {^([^#[:space:]]+) +([^#[:space:]]+)$} $line match vhd lib] } {
		read_vhdl -library $lib $src/$vhd
	}
}
ipx::package_project -import_files -root_dir $design -vendor $vendor -library $library -force $design
close_project

############################
## Create top level design #
############################
set_part $part
set_property board_part $board [current_project]
set_property ip_repo_paths [list ./$design] [current_fileset]
update_ip_catalog
create_bd_design $design
set demo [create_bd_cell -type ip -vlnv $vendor:$library:demo demo]
set cct [create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat cct]
set ix [create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect ix]
set_property -dict [list CONFIG.NUM_SI {1} CONFIG.NUM_MI {2}] $ix
set uart [create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite uart]
set_property -dict [list CONFIG.PARITY {No_Parity}] $uart
set_property -dict [list CONFIG.C_BAUDRATE {115200}] $uart
set_property -dict [list CONFIG.C_DATA_BITS {8}] $uart
set ps7 [create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7 ps7]
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" } $ps7
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ $frequency_mhz] $ps7
set_property -dict [list CONFIG.PCW_USE_M_AXI_GP0 {1}] $ps7
set_property -dict [list CONFIG.PCW_USE_M_AXI_GP1 {1}] $ps7
set_property -dict [list CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {1}] $ps7
set_property -dict [list CONFIG.PCW_M_AXI_GP1_ENABLE_STATIC_REMAP {1}] $ps7
set_property -dict [list CONFIG.PCW_USE_S_AXI_HP0 {1}] $ps7
set_property -dict [list CONFIG.PCW_USE_S_AXI_HP1 {1}] $ps7
set_property -dict [list CONFIG.PCW_S_AXI_HP0_DATA_WIDTH {32}] $ps7
set_property -dict [list CONFIG.PCW_S_AXI_HP1_DATA_WIDTH {32}] $ps7
set_property -dict [list CONFIG.PCW_USE_FABRIC_INTERRUPT {1}] $ps7
set_property -dict [list CONFIG.PCW_CORE0_FIQ_INTR {0}] $ps7
set_property -dict [list CONFIG.PCW_IRQ_F2P_INTR {1}] $ps7

# Interconnections
# Primary IOs
create_bd_port -dir O -from 7 -to 0 gpo
connect_bd_net [get_bd_pins demo/gpo] [get_bd_ports gpo]
create_bd_port -dir I -from 7 -to 0 gpi
connect_bd_net [get_bd_pins demo/gpi] [get_bd_ports gpi]
create_bd_port -dir I srst
connect_bd_net [get_bd_pins demo/srst] [get_bd_ports srst]
create_bd_port -dir I btn
connect_bd_net [get_bd_pins demo/btn] [get_bd_ports btn]
create_bd_port -dir I -type data rx
connect_bd_net [get_bd_ports rx] [get_bd_pins uart/rx]
create_bd_port -dir O -type data tx
connect_bd_net [get_bd_ports tx] [get_bd_pins uart/tx]

# ps7 - demo
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list Master /ps7/M_AXI_GP1 Clk Auto]  [get_bd_intf_pins demo/asb_ctrl_axi]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list Master /ps7/M_AXI_GP0 Clk Auto]  [get_bd_intf_pins demo/asb_cpu_axi]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list Master /demo/asb_mem_axi Clk Auto]  [get_bd_intf_pins ps7/S_AXI_HP0]
connect_bd_net [get_bd_pins demo/irq_c] [get_bd_pins cct/In0]
connect_bd_net [get_bd_pins demo/irq_e] [get_bd_pins cct/In1]
connect_bd_net [get_bd_pins cct/dout] [get_bd_pins ps7/IRQ_F2P]

# uart - demo
connect_bd_net [get_bd_pins uart/rx] [get_bd_pins demo/rx]
connect_bd_net [get_bd_pins uart/tx] [get_bd_pins demo/tx]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list Master /demo/u2m_uart_axi Clk Auto]  [get_bd_intf_pins uart/S_AXI]

# demo - ix
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins ix/S00_AXI] [get_bd_intf_pins demo/u2m_mem_axi]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins ix/M01_AXI] [get_bd_intf_pins demo/am_ctrl_axi]

# ix - ps7
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins ix/M00_AXI] [get_bd_intf_pins ps7/S_AXI_HP1]
connect_bd_net [get_bd_pins ps7/S_AXI_HP1_ACLK] [get_bd_pins ps7/FCLK_CLK0]
connect_bd_net [get_bd_pins ix/ACLK] [get_bd_pins ps7/FCLK_CLK0]
connect_bd_net [get_bd_pins ix/S00_ACLK] [get_bd_pins ps7/FCLK_CLK0]
connect_bd_net [get_bd_pins ix/M00_ACLK] [get_bd_pins ps7/FCLK_CLK0]
connect_bd_net [get_bd_pins ix/M01_ACLK] [get_bd_pins ps7/FCLK_CLK0]
connect_bd_net [get_bd_pins ix/ARESETN] [get_bd_pins rst_ps7_*/interconnect_aresetn]
connect_bd_net [get_bd_pins ix/S00_ARESETN] [get_bd_pins rst_ps7_*/peripheral_aresetn]
connect_bd_net [get_bd_pins ix/M00_ARESETN] [get_bd_pins rst_ps7_*/peripheral_aresetn]
connect_bd_net [get_bd_pins ix/M01_ARESETN] [get_bd_pins rst_ps7_*/peripheral_aresetn]

# Addresses ranges
# PS.GP0_AXI
set_property range 512M [get_bd_addr_segs ps7/Data/SEG_demo_reg01]
set_property offset 0x40000000 [get_bd_addr_segs ps7/Data/SEG_demo_reg01]
# PS.GP1_AXI
set_property range 1M [get_bd_addr_segs {ps7/Data/SEG_demo_reg0}]
set_property offset 0x81000000 [get_bd_addr_segs {ps7/Data/SEG_demo_reg0}]
# demo.ASB HP0_AXI
set_property range 512M [get_bd_addr_segs [list demo/asb_mem_axi/SEG_ps7_HP0_DDR_LOWOCM]]
set_property offset 0x00000000 [get_bd_addr_segs [list demo/asb_mem_axi/SEG_ps7_HP0_DDR_LOWOCM]]
# demo.U2M UART
set_property range 4K [get_bd_addr_segs [list demo/u2m_uart_axi/SEG_uart_Reg]]
set_property offset 0x000 [get_bd_addr_segs [list demo/u2m_uart_axi/SEG_uart_Reg]]
# demo.U2M HP1_AXI
assign_bd_address [get_bd_addr_segs {ps7/S_AXI_HP1/HP1_DDR_LOWOCM }]
set_property range 512M [get_bd_addr_segs [list demo/u2m_mem_axi/SEG_ps7_HP1_DDR_LOWOCM]]
set_property offset 0x00000000 [get_bd_addr_segs [list demo/u2m_mem_axi/SEG_ps7_HP1_DDR_LOWOCM]]
# demo.U2M, demo.AM
assign_bd_address [get_bd_addr_segs [list demo/am_ctrl_axi/reg0]]
set_property range 8K [get_bd_addr_segs [list demo/u2m_mem_axi/SEG_demo_reg0]]
set_property offset 0x20000000 [get_bd_addr_segs [list demo/u2m_mem_axi/SEG_demo_reg0]]

# In-circuit debugging
if { $chipscope == 1 } {
	set_property HDL_ATTRIBUTE.MARK_DEBUG true [get_bd_intf_nets {ps7_M_AXI_GP0}]
	set_property HDL_ATTRIBUTE.MARK_DEBUG true [get_bd_intf_nets {demo_asb_mem_axi}]
}

# Synthesis flow
validate_bd_design
save_bd_design
generate_target all [get_files $design.bd]
make_wrapper -top [get_files $design.bd] -import -force
write_hwdef -force -file ${design}.hwdef
synth_design -top ${design}_wrapper

# In-circuit debugging
if { $chipscope == 1 } {
	puts "Instanciating ILA debug cores"
	set dc [ vv_init_debug_core u_ila_0 [list top_i/ps7_FCLK_CLK0 ] ]
	set nets { ARID ARADDR ARLEN ARSIZE ARBURST ARLOCK ARCACHE ARPROT
		ARVALID RREADY AWID AWADDR AWLEN AWSIZE AWBURST AWLOCK AWCACHE
		AWPROT AWVALID WID WDATA WSTRB WLAST WVALID BREADY ARREADY RID
		RDATA RRESP RLAST RVALID AWREADY WREADY BID BVALID BRESP }
	set nets_basename "top_i/ps7_M_AXI_GP0_"
	set bnets {}
	foreach n $nets {
		lappend bnets ${nets_basename}${n}
	}
	vv_add_debug_core_probes $dc $bnets
	set dc [ vv_init_debug_core u_ila_1 [list top_i/demo_asb_mem_axi ] ]
	set nets { ARID ARADDR ARLEN ARSIZE ARBURST ARLOCK ARCACHE ARPROT
		ARVALID RREADY AWID AWADDR AWLEN AWSIZE AWBURST AWLOCK AWCACHE
		AWPROT AWVALID WID WDATA WSTRB WLAST WVALID BREADY ARREADY RID
		RDATA RRESP RLAST RVALID AWREADY WREADY BID BVALID BRESP }
	set nets_basename "top_i/demo_asb_mem_axi"
	set bnets {}
	foreach n $nets {
		lappend bnets ${nets_basename}${n}
	}
	vv_add_debug_core_probes $dc $bnets
}

# IOs
array set ios {
	"gpi[0]"	{ "F22"  "LVCMOS25" }
	"gpi[1]"	{ "G22"  "LVCMOS25" }
	"gpi[2]"	{ "H22"  "LVCMOS25" }
	"gpi[3]"	{ "F21"  "LVCMOS25" }
	"gpi[4]"	{ "H19"  "LVCMOS25" }
	"gpi[5]"	{ "H18"  "LVCMOS25" }
	"gpi[6]"	{ "H17"  "LVCMOS25" }
	"gpi[7]"	{ "M15"  "LVCMOS25" }
	"gpo[0]"	{ "T22"  "LVCMOS33" }
	"gpo[1]"	{ "T21"  "LVCMOS33" }
	"gpo[2]"	{ "U22"  "LVCMOS33" }
	"gpo[3]"	{ "U21"  "LVCMOS33" }
	"gpo[4]"	{ "V22"  "LVCMOS33" }
	"gpo[5]"	{ "W22"  "LVCMOS33" }
	"gpo[6]"	{ "U19"  "LVCMOS33" }
	"gpo[7]"	{ "U14"  "LVCMOS33" }
	"srst"   	{ "P16"  "LVCMOS25" }
	"btn"    	{ "T18"  "LVCMOS25" }
	"tx"		{ "AA11" "LVCMOS33" }
	"rx"		{ "Y10"  "LVCMOS33" }
}
foreach io [ array names ios ] {
	set pin [ lindex $ios($io) 0 ]
	set std [ lindex $ios($io) 1 ]
	set_property package_pin $pin [get_ports $io]
	set_property iostandard $std [get_ports [list $io]]
}

# Clocks and timing
set clock [get_clocks]
set_false_path -from $clock -to [get_ports {gpo[*] tx}]
set_false_path -from [get_ports {gpi[*] rx srst btn}] -to $clock

# Implementation
opt_design
place_design
route_design

# Reports
report_utilization -file $design.utilization.rpt
report_timing_summary -file $design.timing.rpt

# Bitstream
write_bitstream -force $design
write_sysdef -force -bitfile $design.bit -hwdef $design.hwdef $design.sysdef

# Messages
puts ""
puts "*********************************************"
puts "\[VIVADO\]: done"
puts "*********************************************"
puts "Summary of build parameters"
puts "*********************************************"
puts "Board: $board"
puts "Part: $part"
puts "Source directory: $src"
puts "Design name: $design"
puts "Frequency: $frequency_mhz MHz"
puts "*********************************************"
puts "  bitstream in $design.bit"
puts "  hardware definition file in $design.hwdef"
puts "  system definition file in $design.sysdef"
puts "  resource utilization report in $design.utilization.rpt"
puts "  timing report in $design.timing.rpt"
puts "*********************************************"

# Quit
# quit

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
