#
# SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
# Copyright (C) - Telecom Paris
# Contacts: contact-secbus@telecom-paris.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

set expected_version 2018.3
set v [version -short]
if { $v != $expected_version } {
	puts "
WARNING: Vivado version ($v) is not the expected one ($expected_version).
WARNING: Trying anyway but be warned that things may go wrong."
}

puts $argc
if { $argc != 3 } {
	puts "usage: hsi -source dts.hsi.tcl -tclargs <hardware-description-file> <path-to-device-tree-xlnx> <output-directory>"
} else {
	set hdf [lindex $argv 0]
	set xdts [lindex $argv 1]
	set ldts [lindex $argv 2]
	open_hw_design $hdf
	set_repo_path $xdts
	create_sw_design device-tree -os device_tree -proc ps7_cortexa9_0
	generate_target -dir $ldts
}
