#
# SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
# Copyright (C) - Telecom Paris
# Contacts: contact-secbus@telecom-paris.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

set expected_version 2018.3
set v [version -short]
if { $v != $expected_version } {
	puts "
WARNING: Vivado version ($v) is not the expected one ($expected_version).
WARNING: Trying anyway but be warned that things may go wrong."
}

if { $argc != 2 } {
	puts "usage: hsi -source fsbl.hsi.tcl -tclargs <hardware-description-file> <output-directory>"
} else {
	set hdf [lindex $argv 0]
	set dir [lindex $argv 1]
	set design [ open_hw_design ${hdf} ]
	generate_app -hw $design -os standalone -proc ps7_cortexa9_0 -app zynq_fsbl -sw fsbl -dir ${dir}
}
