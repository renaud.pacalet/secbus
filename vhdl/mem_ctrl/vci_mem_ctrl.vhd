--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- vci_mem_ctrl.vhd

-- @todo : assert mem_rq.id != 0

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

entity vci_mem_ctrl is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       p_srcid                 : in  srcid_t;

       -- Data from Mt or Ms (ctrl/cache)
       rd_mac_data             : in  data_vector(1 downto 0);     

       -- Data from reg bank
       rd_reg                  : in  data_register_read_array(REG_MAX - 1 downto 0);     
      
       -- Data to reg Bank
       wr_reg                  : out data_register_write_array(REG_MAX - 1 downto 0); 

       -- Request from Mem_Arbiter
       rd_mem_rq               : in  memory_ctrl_cmd_out; 
       wr_mem_rq               : out memory_ctrl_cmd_in;  -- cmdack, rspval
 
       -- Req to  Vci_Merge
       p_vci_ini_out           : out vci_i2t;
       p_vci_ini_in            : in  vci_t2i
);

end vci_mem_ctrl;

architecture rtl of vci_mem_ctrl is
type fsm_state is (IDLE, CMD_RD, CMD_WR, RSP_RD, RSP_RD_LAST, RSP_WR);

signal s_state, r_state               : fsm_state;
signal s_write_cnt   , r_write_cnt    : unsigned(plen_size - CELL_BITS - 1 downto 0); 
signal s_write_idx   , r_write_idx    : unsigned(plen_size - CELL_BITS - 1 downto 0); 
signal s_wrd_index   , r_wrd_index    : natural range 0 to SIZE_LINE_WORDS; -- - 1;
signal s_addr        , r_addr         : unsigned(addr_size - 1 downto 0);
signal s_plen        , r_plen         : unsigned(plen_size - 1 downto 0);
signal s_id          , r_id           : unsigned(REG_MAX   - 1 downto 0);
signal s_direct_write, r_direct_write : std_ulogic;
signal s_mac_data    , r_mac_data     : data_vector(1 downto 0);     
signal s_cpt_reached                  : std_ulogic;
--#----------------
begin

s_cpt_reached <= '1' when r_write_idx = r_write_cnt else
                 '0';


pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state        <= IDLE;
              r_write_cnt    <= (others => '0');
              r_write_idx    <= (others => '0');
              r_addr         <= (others => '0');
              r_wrd_index    <= 0;
              r_plen         <= (others => '0');
              r_id           <= (others => '0');
              r_mac_data     <= (others => (others => '0'));
              r_direct_write <= '0';

        elsif ce = '1' then
              r_state        <= s_state;
              r_write_cnt    <= s_write_cnt;
              r_write_idx    <= s_write_idx;
              r_addr         <= s_addr;
              r_wrd_index    <= s_wrd_index; 
              r_plen         <= s_plen;
              r_id           <= s_id; 
              r_mac_data     <= s_mac_data;
              r_direct_write <= s_direct_write;
        end if;
end if;
end process;

pTransition : process(r_state, rd_mem_rq, p_vci_ini_in, r_wrd_index, r_write_cnt, r_write_idx, 
                      r_addr, r_plen, r_id, r_direct_write, rd_mac_data, r_mac_data)

begin

s_wrd_index    <= r_wrd_index;
s_write_cnt    <= r_write_cnt;
s_write_idx    <= r_write_idx;
s_state        <= r_state;
s_addr         <= r_addr;
s_plen         <= r_plen;
s_id           <= r_id;
s_mac_data     <= r_mac_data;
s_direct_write <= r_direct_write;

case r_state is
       when IDLE =>
            if rd_mem_rq.cmdval = '1' then
                s_wrd_index    <= word_index(rd_mem_rq.start_address);
                s_addr         <= unsigned(rd_mem_rq.start_address);
                s_plen         <= unsigned(rd_mem_rq.plen);
                s_id           <= unsigned(rd_mem_rq.id);
                s_direct_write <= rd_mem_rq.direct_write;

                if rd_mem_rq.cmd = READ then 
                      s_state <= CMD_RD;
                elsif rd_mem_rq.cmd = WRITE then
                      if rd_mem_rq.direct_write = '1' then
                         s_mac_data <= rd_mac_data;
                      end if;
                      -- word counter : plen expressed in bytes => divide by cellsize 
                      s_write_cnt <= unsigned(rd_mem_rq.plen(rd_mem_rq.plen'left downto CELL_BITS)) - 1;
                      s_write_idx <= (others => '0'); 
                      s_state     <= CMD_WR;
                end if;
            end if;
                  
       when CMD_RD =>
            if p_vci_ini_in.cmdack = '1' then
                s_state <= RSP_RD;
            end if;                 

       when CMD_WR =>

            if (p_vci_ini_in.cmdack = '1')  then 

                s_addr <= r_addr + cell_size;

                if (r_write_idx = r_write_cnt) then
                    s_state     <= RSP_WR;
                else
                    if r_direct_write = '0' then
                        s_wrd_index <= r_wrd_index + 1;
                    end if;

                    s_write_idx <= r_write_idx + 1;
                    s_state     <= CMD_WR;
                end if; 
            end if;
 
       when RSP_RD =>
            if (p_vci_ini_in.rspval = '1') then
                if (p_vci_ini_in.reop = '1') then 
                    s_state <= RSP_RD_LAST;
                else
                    s_wrd_index <= r_wrd_index + 1;
                end if;
            end if;

       when RSP_RD_LAST =>
               s_state <= IDLE;

       when RSP_WR =>
               if (p_vci_ini_in.rspval = '1') and (p_vci_ini_in.reop = '1') then 
                   s_state <= IDLE;
               end if;

end case;

end process;

pGeneration : process(r_state, r_addr, r_plen, p_srcid, s_cpt_reached, r_mac_data, rd_reg, r_direct_write, p_vci_ini_in,
                      r_id, r_wrd_index, r_write_idx )

begin

p_vci_ini_out    <= vci_i2t_none;

wr_mem_rq.cmdack <= '0';
wr_mem_rq.rspval <= '0';

for i in wr_reg'range loop          
       wr_reg(i).enable <= '0';
       wr_reg(i).flush  <= '0';
       wr_reg(i).addr   <= (others => '0');
       wr_reg(i).data   <= (others => (others => '0'));
       wr_reg(i).be     <= (others => (others => '0'));
end loop;

case r_state is
       when IDLE =>                  
            wr_mem_rq.cmdack     <= '1';                                          

       when CMD_RD =>

            p_vci_ini_out.cmdval <= '1';
            p_vci_ini_out.addr   <= std_ulogic_vector(r_addr);
            p_vci_ini_out.be     <= (others => '1');
            p_vci_ini_out.plen   <= std_ulogic_vector(r_plen);
            p_vci_ini_out.cmd    <= VCI_READ;
            p_vci_ini_out.eop    <= '1';
            p_vci_ini_out.contig <= '1';
            p_vci_ini_out.srcid  <= p_srcid;                    -- axi ARID 
            p_vci_ini_out.trdid  <= (others => '0');            
            p_vci_ini_out.pktid  <= (1 => '1', others => '0');  -- axi ARSIZE = 0b010 => 4 bytes to transfer

       when CMD_WR =>

            p_vci_ini_out.cmdval <= '1';
            p_vci_ini_out.addr   <= std_ulogic_vector(r_addr);
            p_vci_ini_out.plen   <= std_ulogic_vector(r_plen);  
            p_vci_ini_out.cmd    <= VCI_WRITE;
            p_vci_ini_out.eop    <= s_cpt_reached;
            p_vci_ini_out.contig <= '1';
            p_vci_ini_out.srcid  <= p_srcid;                    -- axi AWID
            p_vci_ini_out.trdid  <= (others => '0');            -- axi3 WID
            p_vci_ini_out.pktid  <= (1 => '1', others => '0');  -- axi AWSIZE = 0b010 => 4 bytes to transfer

            if (r_direct_write = '1') then
                  p_vci_ini_out.wdata <= r_mac_data(to_integer(r_write_idx)); 
                  p_vci_ini_out.be    <= (others => '1'); 
            else 
                  p_vci_ini_out.wdata <= rd_reg(ctz(r_id)).data(r_wrd_index);
                  p_vci_ini_out.be    <= rd_reg(ctz(r_id)).be(r_wrd_index); 
            end if;

       when RSP_RD =>
            p_vci_ini_out.rspack <= '1';
            -- we can write in more than one register
            for i in r_id'range loop 
                  if r_id(i) = '1' then
                     wr_reg(i).enable            <= '1';
                     wr_reg(i).addr              <= std_ulogic_vector(r_addr);
                     wr_reg(i).data(r_wrd_index) <= p_vci_ini_in.rdata;
                     wr_reg(i).be(r_wrd_index)   <= (others => '1');
                  end if;
            end loop;

       when RSP_RD_LAST =>
            wr_mem_rq.rspval           <= '1';

       when RSP_WR =>
             p_vci_ini_out.rspack      <= '1';
             wr_mem_rq.rspval          <= p_vci_ini_in.rspval and p_vci_ini_in.reop;  
end case;

end process;

end rtl;


