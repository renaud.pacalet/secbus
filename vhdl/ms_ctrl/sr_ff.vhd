--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- ms_ctrl.vhd
-- Description : A zero/One flip-flop used as a a shared signal       
--                 between 2 fsms                                       

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sr_ff   is

port (
        clk      : in  std_ulogic;
        srstn    : in  std_ulogic;
        ce       : in  std_ulogic;
	set      : in  std_ulogic;
	rst      : in  std_ulogic;
	state    : out std_ulogic);
end sr_ff;

architecture archi of sr_ff is

signal r_state, s_state : std_logic;
 
begin

s_state <= '0' when  (r_state  = '1' and rst    = '1') or (r_state  = '0' and set = '0') else
           '1' when  (r_state  = '1' and rst    = '0') or (r_state  = '0' and set = '1') else
           r_state;
------
process(clk)
begin

if rising_edge(clk) then
    if srstn = '0' then 
           r_state <= '0';
    elsif ce = '1' then
        r_state <= s_state;
    end if;
end if;

end process;
-------
state <= r_state;

end archi;


