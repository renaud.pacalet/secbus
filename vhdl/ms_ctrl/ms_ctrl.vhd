--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- ms_ctrl.vhd
--
-- Registers : 
--     R4 : from Memory Ctrl
--     R0, R1, R2 : Data Set from Security Ctrl to compute CBC MAC (Crypot_Engine_Int)
--                  with Address as first CBC Block
--     CHECK  : The computed CBC_MAC is compared to the stored CBC MAC (Cache|Memory)
--              Raise an interrupt if mismatch
--     UPDATE : The computed CBC MAC is written into Cache/Memory.
--
-- Ms generic cache : set associative , ram distributed, write Back                        
-- Ms Cache cmd =>  000 : nop
--                  100 : read 
--                  010 : write dir
--                  001 : write_data (write Dir and write Data not exclusive).

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

library des_lib;
use des_lib.des_pkg.all;

library caches_lib;
use caches_lib.all;

entity ms_ctrl is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       p_cache                 : in  std_ulogic;

       -- Request from Security Ctrl
       rd_ms_cmd               : in  integrity_ctrl_cmd;
       wr_ms_ack               : out integrity_ctrl_ack;

       -- Request to crypto_engine_int
       wr_crypto_cmd           : out crypto_cmd_out;  
       wr_crypto_data          : out data_vector(11 downto 0);       -- 6 MAC Sets 
 
       rd_crypto_cmd           : in  crypto_cmd_in;     -- cmdack, rspval
       rd_crypto_data          : in  data_vector(1 downto 0);    

       -- Data from Memory    
       rd_reg_4                : in  data_register_read;  

       -- from Security Ctrl
       rd_reg_0                : in  data_register_read;  
       rd_reg_1                : in  data_register_read;  
       rd_reg_2                : in  data_register_read;  

       -- Security context from Security context controller
       rd_ctx_register         : in  ctx_register_read;
     
       -- Request to Mem Ctrl
       wr_mem_rq               : out memory_ctrl_cmd_out; --  
       rd_mem_rq               : in  memory_ctrl_cmd_in;  -- cmdack, rspval

       wr_mem_data             : out direct_data;                      
 
       -- Req to IRQ Handler
       wr_irq                  : out irq_out;
       rd_irq_ack              : in std_ulogic

);

end ms_ctrl;

architecture rtl of ms_ctrl is

type fsm_cry_state is (IDLE            ,
                       CRYPTO_CMD_READ , CRYPTO_RSP_READ,
                       CRYPTO_CMD_WRITE, CRYPTO_RSP_WRITE,
                       MAC_COMP        , IRQ_SEND 
                      );

type fsm_mem_state is (IDLE            ,
                       CACHE_READ_HIT  , CACHE_WRITE_HIT , CACHE_WRITE_DATA ,
                       MEM_CMD_READ    , MEM_RSP_READ    ,
                       MEM_CMD_WRITE   , MEM_RSP_WRITE   , 
                       MEM_CMD_WB      , MEM_RSP_WB       
                      );

Signal s_cry_state   , r_cry_state          : fsm_cry_state;
Signal s_mem_state   , r_mem_state          : fsm_mem_state;
signal s_keyI        , r_keyI               : desx_key; 
signal s_set_addr    , r_set_addr           : addr_t;                            
signal s_mac_addr    , r_mac_addr           : addr_t;                            

-- ms cache signals
signal s_cache_cmd                          : std_ulogic_vector(2 downto 0); 
signal s_cache_way_in                       : std_ulogic_vector(MS_CACHE_WAYS - 1 downto 0);
signal s_cache_dirty_in                     : std_ulogic;
signal s_cache_addr_in                      : addr_t;
signal s_cache_data_in                      : data_vector(1 downto 0);
signal s_cache_way_out                      : std_ulogic_vector(MS_CACHE_WAYS - 1 downto 0);
signal s_cache_addr_out                     : addr_t;
signal s_cache_data_out                     : data_vector(1 downto 0); 
signal s_cache_hit                          : std_ulogic;
signal s_cache_dirty_out                    : std_ulogic;
signal s_ways         , r_ways              : std_ulogic_vector(MS_CACHE_WAYS - 1 downto 0);
signal s_vict_data    , r_vict_data         : data_vector(1 downto 0); 
signal s_vict_addr    , r_vict_addr         : addr_t; 
signal s_dirty        , r_dirty             : std_ulogic;

signal s_mem_mac      , r_mem_mac           : data_vector(1 downto 0);
signal s_cry_mac      , r_cry_mac           : data_vector(1 downto 0);

signal s_mem_cmd_read_set                   : std_ulogic;
signal s_mem_cmd_read_reset                 : std_ulogic;
signal s_mem_cmd_read                       : std_ulogic;

signal s_mem_cmd_write_set                  : std_ulogic;
signal s_mem_cmd_write_reset                : std_ulogic;
signal s_mem_cmd_write                      : std_ulogic;

signal s_hit           , r_hit              : std_ulogic;

signal s_wr_cry_data   , r_wr_cry_data      : data_vector(11 downto 0);       -- 6 MAC Sets 
------
begin

sr_ff_rd_inst : entity work.sr_ff 
port map(
    clk      => clk,
    srstn    => srstn,
    ce       => ce,
	set      => s_mem_cmd_read_set,
	rst      => s_mem_cmd_read_reset,
	state    => s_mem_cmd_read
);

sr_ff_wr_inst : entity work.sr_ff 
port map(
    clk      => clk,
    srstn    => srstn,
    ce       => ce,
	set      => s_mem_cmd_write_set,
	rst      => s_mem_cmd_write_reset,
	state    => s_mem_cmd_write
);

ms_cache_inst : entity caches_lib.ms_cache 
generic map(
        WAYS            => MS_CACHE_WAYS, 
        SETS            => MS_CACHE_SETS,
        BLOCKS          => MS_CACHE_BLKS
)
port map(
        clk             => clk, 
        srstn           => srstn, 
        ce              => ce,
        
        cmd             => s_cache_cmd, 
                        
        way_in          => s_cache_way_in,
        addr_in         => s_cache_addr_in,
        data_in         => s_cache_data_in,
        dirty_in        => s_cache_dirty_in,
        
        way_out         => s_cache_way_out,
        addr_out        => s_cache_addr_out,
        data_out        => s_cache_data_out,
        dirty_out       => s_cache_dirty_out,
        hit             => s_cache_hit    
     
);

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_cry_state         <= IDLE;
              r_mem_state         <= IDLE;
              r_keyI              <= key_null;
              r_set_addr          <= (others => '0');
              r_mac_addr          <= (others => '0');
              r_vict_data         <= (others => (others => '0'));
              r_vict_addr         <= (others => '0');     
              r_ways              <= (others => '0'); 
              r_mem_mac           <= (others => (others => '0')); 
              r_cry_mac           <= (others => (others => '0')); 
              r_dirty             <= '0';
              r_hit               <= '0';
              r_wr_cry_data       <= (others => (others => '0')); 

        elsif ce = '1' then
              r_cry_state         <= s_cry_state; 
              r_mem_state         <= s_mem_state; 
              r_keyI              <= s_keyI;      
              r_set_addr          <= s_set_addr;  
              r_mac_addr          <= s_mac_addr;  
              r_vict_data         <= s_vict_data; 
              r_vict_addr         <= s_vict_addr; 
              r_ways              <= s_ways;      
              r_mem_mac           <= s_mem_mac;   
              r_cry_mac           <= s_cry_mac;   
              r_dirty             <= s_dirty;     
              r_hit               <= s_hit; 
              r_wr_cry_data       <= s_wr_cry_data;
        end if;
end if;
end process;

---------------------------
-- Mac Set Controller FSM
-- Computes CBC MAC and sends a request to Memory FSM
pMs_Transition : process(r_cry_state  , s_mem_cmd_read , rd_ms_cmd    , rd_ctx_register, 
                         rd_crypto_cmd, rd_crypto_data , r_set_addr   , r_mac_addr     , 
                         r_keyI       , r_mem_mac      , r_cry_mac    , 
                         rd_reg_0     , rd_reg_1       , rd_reg_2     ,
                         rd_irq_ack   , s_mem_cmd_write, r_wr_cry_data)

variable vsrc : natural;
begin

s_cry_state     <= r_cry_state;
s_set_addr      <= r_set_addr;
s_mac_addr      <= r_mac_addr;
s_keyI          <= r_keyI;
s_cry_mac       <= r_cry_mac;

s_mem_cmd_read_set  <= '0';
s_mem_cmd_write_set <= '0';             
 
s_wr_cry_data   <= r_wr_cry_data;

case r_cry_state is
     when IDLE =>
           if rd_ms_cmd.enable = '1' and s_mem_cmd_write = '0' then -- valid request and no pending write
                s_keyI      <= sp_getKeyI(rd_ctx_register.sp);
                s_set_addr  <= rd_ms_cmd.addr;
                s_mac_addr  <= ms_getMacAddress(rd_ms_cmd.addr, rd_ctx_register.m_pspe); 

                vsrc         := to_integer(unsigned(rd_ms_cmd.src));
                s_wr_cry_data <= (others => (others => '0'));
                
                case vsrc is
                      when 0 =>
                           s_wr_cry_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_0.data;
                      when 1 =>
                           s_wr_cry_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_1.data;
                      when 2 =>
                           s_wr_cry_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_2.data;
                      when others =>
                           s_wr_cry_data(SIZE_LINE_WORDS - 1 downto 0) <= (others => (others => '0')); 
                end case;

                if rd_ms_cmd.cmd = CHECK then
                    s_cry_state         <= CRYPTO_CMD_READ;
                    s_mem_cmd_read_set  <= '1'; -- launch memory/cache mac read request 
                elsif rd_ms_cmd.cmd = UPDATE then
                    s_cry_state <= CRYPTO_CMD_WRITE;
                -- else  -- @todo : Bad command => Error 
                end if;
 
             end if; -- enable

       when CRYPTO_CMD_READ =>
            if rd_crypto_cmd.cmdack = '1' then
                 s_cry_state <= CRYPTO_RSP_READ;
            else
                 s_cry_state <= CRYPTO_CMD_READ;
            end if;         

       when CRYPTO_RSP_READ =>
            if rd_crypto_cmd.rspval = '1' then
                s_cry_mac     <= rd_crypto_data;
                s_cry_state   <= MAC_COMP;
            else
                 s_cry_state  <= CRYPTO_RSP_READ;
            end if;     

       when CRYPTO_CMD_WRITE =>
            if rd_crypto_cmd.cmdack = '1' then
                 s_cry_state <= CRYPTO_RSP_WRITE;
            end if;
       
       when CRYPTO_RSP_WRITE =>
            if rd_crypto_cmd.rspval = '1' then
                s_cry_mac           <= rd_crypto_data;
                s_mem_cmd_write_set <= '1';    -- launch memory/cache mac write request           
                s_cry_state         <= IDLE;
            end if;

       when MAC_COMP =>
            if s_mem_cmd_read = '0' then     -- Read Data valid
               if r_mem_mac = r_cry_mac then 
                  s_cry_state <= IDLE;
               else
                  s_cry_state <= IRQ_SEND;
               end if;
            end if;

       when IRQ_SEND =>  
           --if rd_irq_ack = '1' then 
           --   s_cry_state <= IDLE;
           --end if; 
end case;
end process;
------------------
-- Memory/Cache Fsm
-- CHECK : Fetches the Reference MAC in Memory/Cache then
--         returns it to Mac Computing Fsm
-- UPDATE : Writes the computed MAC into Memory/Cache
pMem_Transition : process(r_mem_state      , r_hit           , 
                          s_mem_cmd_read   , s_mem_cmd_write , 
                          p_cache          , s_cache_hit     ,  
                          s_cache_addr_out , s_cache_data_out, 
                          s_cache_dirty_out, s_cache_way_out ,
                          r_ways           , r_dirty         ,
                          r_vict_addr      , r_vict_data     , 
                          r_mac_addr       , r_mem_mac       , r_cry_mac    , 
                          rd_mem_rq        , rd_reg_4)

variable v_mac_data : data_vector(MS_CACHE_BLKS - 1 downto 0);

begin

s_mem_state      <= r_mem_state;
s_mem_mac        <= r_mem_mac;

s_vict_addr      <= r_vict_addr;
s_vict_data      <= r_vict_data;
s_ways           <= r_ways;
s_hit            <= r_hit;
s_dirty          <= r_dirty;

s_cache_cmd      <= (others => '0');
s_cache_way_in   <= (others => '0');
s_cache_addr_in  <= (others => '0');
s_cache_data_in  <= (others => (others => '0'));
s_cache_dirty_in <= '0';

s_mem_cmd_read_reset  <= '0';
s_mem_cmd_write_reset <= '0'; 

case r_mem_state is
     when IDLE =>
        if s_mem_cmd_read = '1' then
              if p_cache = '1' then
                 s_cache_addr_in <= r_mac_addr;
                 s_cache_cmd(2)  <= '1';         -- Read
                 s_mem_state     <= CACHE_READ_HIT;
              else
                 s_mem_state <= MEM_CMD_READ;
              end if;
        elsif s_mem_cmd_write = '1' then
              if p_cache = '1' then
                 s_cache_addr_in <= r_mac_addr;
                 s_cache_cmd(2)  <= '1';         -- Read
                 s_mem_state     <= CACHE_WRITE_HIT;
              else
                 s_mem_state <= MEM_CMD_WRITE;
              end if;

        else
              s_mem_state <= IDLE;
        end if;

    when CACHE_READ_HIT =>
        if s_cache_hit = '1' then
           s_mem_cmd_read_reset <= '1';
           s_mem_mac            <= s_cache_data_out;
           s_mem_state          <= IDLE;
        else -- 
           s_vict_addr <= s_cache_addr_out;
           s_vict_data <= s_cache_data_out;
           s_dirty     <= s_cache_dirty_out;
           s_ways      <= s_cache_way_out;

           s_mem_state <= MEM_CMD_READ;
        end if;

    when CACHE_WRITE_HIT =>
         s_hit       <= s_cache_hit;
         s_vict_addr <= s_cache_addr_out;
         s_vict_data <= s_cache_data_out;
         s_dirty     <= s_cache_dirty_out;
         s_ways      <= s_cache_way_out;
 
         s_mem_state <= CACHE_WRITE_DATA;
        
    when CACHE_WRITE_DATA  =>
        s_cache_cmd(1)        <= not r_hit;          -- write dir
        s_cache_cmd(0)        <= '1';                -- write Data
        s_cache_addr_in       <= r_mac_addr;
        s_cache_dirty_in      <= '1';
        s_cache_data_in       <= r_cry_mac;
        s_cache_way_in        <= r_ways;

        if r_hit = '0' and r_dirty = '1' then
             s_mem_state <= MEM_CMD_WB;
        else
             s_mem_state           <= IDLE;
             s_mem_cmd_write_reset <= '1';
        end if;
           
    when MEM_CMD_READ =>  
        if rd_mem_rq.cmdack = '1' then            
           s_mem_state <= MEM_RSP_READ;
        end if;

   when MEM_RSP_READ =>  
          v_mac_data := (others => (others => '0'));

          if rd_mem_rq.rspval = '1' then
             v_mac_data(0)        := rd_reg_4.data(word_index(r_mac_addr));
             v_mac_data(1)        := rd_reg_4.data(word_index(r_mac_addr) + 1);

             s_mem_mac            <= v_mac_data;

             if p_cache = '1' then
                s_cache_addr_in   <= r_mac_addr;
                s_cache_cmd(1)    <= '1';         -- write Dir
                s_cache_cmd(0)    <= '1';         -- write Data
                s_cache_dirty_in  <= '0';
                s_cache_data_in   <= v_mac_data;
                s_cache_way_in    <= r_ways;

                if r_dirty = '1' then
                   s_mem_state <= MEM_CMD_WB;
                else
                   s_mem_state          <= IDLE;
                   s_mem_cmd_read_reset <= '1'; 
                end if;

             else
                s_mem_state          <= IDLE;
                s_mem_cmd_read_reset <= '1'; 
             end if; -- p_cache
          end if; --rspval

    when MEM_CMD_WRITE =>  
          if rd_mem_rq.cmdack = '1' then            
              s_mem_state <= MEM_RSP_WRITE;
          end if;

     when MEM_RSP_WRITE => 
            if rd_mem_rq.rspval = '1' then
               s_mem_state           <= IDLE;
               s_mem_cmd_write_reset <= '1';
            end if; --rspval

     when MEM_CMD_WB    =>  
            if rd_mem_rq.cmdack = '1' then            
               s_mem_state <= MEM_RSP_WB;
            end if;

     when MEM_RSP_WB    =>   
            if rd_mem_rq.rspval = '1' then
               s_mem_state           <= IDLE;
               s_mem_cmd_write_reset <= '1';
               s_mem_cmd_read_reset  <= '1'; 
            end if; --rspval

end case;
end process;

-----------------
pMs_Generation : process(r_cry_state, r_keyI, r_set_addr, s_mem_cmd_write, r_wr_cry_data) 

begin

wr_crypto_cmd.cmdval      <= '0';  
wr_crypto_cmd.mode        <= CRY_MAC;
wr_crypto_cmd.key         <= key_null;
wr_crypto_cmd.iv          <= (others => '0');  -- not used
wr_crypto_cmd.size        <= (others => '0');      
wr_crypto_cmd.cipher      <= '0';
wr_crypto_cmd.id_src      <= (others => '0');
wr_crypto_cmd.id_dest     <= (others => '0');
wr_crypto_cmd.direct_data <= '0';                                    
wr_crypto_cmd.no_init     <= '0'; 
wr_crypto_cmd.address     <= (others => '0');
wr_crypto_cmd.data_ready  <= '0'; 
wr_crypto_data            <= (others => (others => '0'));

wr_ms_ack.cack     <= '0';
wr_ms_ack.iack     <= '0';
wr_ms_ack.uack     <= '0';
wr_ms_ack.alap_ack <= '0';

wr_irq.irq         <= '0';
wr_irq.addr        <= (others => '0');
wr_irq.typ         <= NONE;
wr_irq.op          <= LOAD;

case r_cry_state is
       when IDLE =>  
            wr_ms_ack.cack <= not s_mem_cmd_write; --'1';
            wr_ms_ack.uack <= not s_mem_cmd_write; --'1';

       when CRYPTO_CMD_READ | CRYPTO_CMD_WRITE =>

            wr_crypto_cmd.cmdval    <= '1';  
            wr_crypto_cmd.mode      <= CRY_MAC;
            wr_crypto_cmd.key       <= r_keyI;
            wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));      
            wr_crypto_cmd.address   <= std_ulogic_vector(r_set_addr);
            wr_crypto_data          <= r_wr_cry_data;

       when CRYPTO_RSP_READ =>
            null;

       when CRYPTO_RSP_WRITE =>
            null;

       when MAC_COMP =>
            null;

       when IRQ_SEND =>
            wr_irq.irq     <= '1';
            wr_irq.addr    <= r_set_addr; 
            wr_irq.typ     <= MAC;
            wr_irq.op      <= LOAD;
end case;

end process;
--------
pMem_Generation : process(r_mem_state, r_mac_addr, r_cry_mac, r_vict_addr, r_vict_data) 

begin

wr_mem_rq.cmdval        <= '0';
wr_mem_rq.cmd           <= READ;
wr_mem_rq.start_address <= (others => '0');
wr_mem_rq.plen          <= (others => '0');
wr_mem_rq.id            <= (others => '0'); 
wr_mem_rq.direct_write  <= '0';
 
wr_mem_data.enable      <= '0'; 
wr_mem_data.data        <= (others => (others => '0')); 

case r_mem_state is
       when IDLE =>  
            null;

       when CACHE_READ_HIT =>
            null;

       when CACHE_WRITE_HIT =>
            null;

       when CACHE_WRITE_DATA =>
            null;

       when MEM_CMD_READ =>
            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= READ;
            wr_mem_rq.start_address <= r_mac_addr; 
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(MS_CACHE_BLKS*CELL_SIZE, PLEN_SIZE));
            wr_mem_rq.id            <= (4 => '1', others => '0'); 

       when MEM_RSP_READ =>
            null;

       when MEM_CMD_WRITE =>

            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= WRITE;
            wr_mem_rq.start_address <= r_mac_addr;  
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(MS_CACHE_BLKS*CELL_SIZE, PLEN_SIZE));
            wr_mem_rq.id            <= (others => '0'); 
            wr_mem_rq.direct_write  <= '1';
 
            wr_mem_data.enable      <= '1';         
            wr_mem_data.data        <= r_cry_mac;

       when MEM_RSP_WRITE => 
            null;

       when MEM_CMD_WB =>
            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= WRITE;
            wr_mem_rq.start_address <= std_ulogic_vector(r_vict_addr); 
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(MS_CACHE_BLKS*CELL_SIZE, PLEN_SIZE));
            wr_mem_rq.id            <= (others => '0'); 
            wr_mem_rq.direct_write  <= '1';
 
            wr_mem_data.enable      <= '1';         
            wr_mem_data.data        <= r_vict_data;  -- victim data

       when MEM_RSP_WB =>
            null;
end case;

end process;

end rtl;


