--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- cryptoInt.vhd
--
-- CBC (Cipher Block Chaining) : Read Write pages
-- CTR (Counter)               : Read Only  pages
-- @todo : 
--         Block Cipher parameter  L2N, to be defined
--         Next : implement slave pages to store IV randomly generated.
--                One IV must be generated for Each Line to Encrypt.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

library des_lib;
use des_lib.des_pkg.all;

library bc_lib;
use bc_lib.all;

entity cryptoInt is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- Req From Ms/MT Ctrl  
       rd_crypto_cmd           : in  crypto_cmd_out;  
       rd_crypto_data          : in  data_vector(11 downto 0);   -- 6 MAC Sets 
 
       wr_crypto_cmd           : out crypto_cmd_in;              -- cmdack, rspval
       wr_crypto_data          : out data_vector(1 downto 0)    
);


end cryptoInt;

architecture rtl of cryptoInt is 

type fsm_state is (IDLE, 
                   CBC_FIRST, CBC_CIPHER,  
                   BC_CIPHER_WAIT);  

signal s_state  , r_state  : fsm_state;
signal s_bc_dsi            : std_ulogic; 
signal s_bc_first          : std_ulogic;
signal s_bc_di             : des_word_64;
signal s_bc_key            : desx_key;
signal s_bc_iv             : des_word_64;
signal s_bc_mode           : encryption_mode;
signal s_bc_do             : des_word_64;
signal s_bc_busy           : std_ulogic;

signal s_rsp    , r_rsp    : std_ulogic;
signal s_noinit , r_noinit : std_ulogic;

signal s_key  , r_key      : desx_key;
signal s_idx  , r_idx      : natural range 0 to 12; 
signal s_do   , r_do       : des_word_64;
signal s_di   , r_di       : des_word_64;
signal s_iv   , r_iv       : des_word_64;
signal s_size , r_size     : natural range 0 to 12;      -- up to 12 word_32 = 6 Mac Set Blocks     

signal s_data , r_data     : data_vector(11 downto 0);   -- 6 MAC Sets 
------------------
begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state       <= IDLE;
              r_key         <= key_null;
              r_rsp         <= '0';
              r_size        <= 0;
              r_idx         <= 0;
              r_do          <= (others => '0');
              r_di          <= (others => '0');
              r_iv          <= (others => '0');
              r_noinit      <= '0';
              r_data        <= (others => (others => '0'));

        elsif ce = '1' then
              r_state       <= s_state  ;
              r_key         <= s_key    ;
              r_rsp         <= s_rsp    ;
              r_size        <= s_size   ;
              r_idx         <= s_idx    ;
              r_do          <= s_do     ;
              r_di          <= s_di     ;
              r_iv          <= s_iv     ;
              r_noinit      <= s_noinit ;
              r_data        <= s_data   ;
        end if;
end if;
end process;

bc_inst : entity bc_lib.bc(rtl) 
generic map(
       l2n    => BC_RND_BITS                          
)
port map(
       clk    => clk,           
       srstn  => srstn,            
       ce     => ce,            
       first  => s_bc_first,            
       dsi    => s_bc_dsi,            
       iv     => s_bc_iv,            
       di     => s_bc_di,            
       key    => s_bc_key,            
       mode   => s_bc_mode,            
       busy   => s_bc_busy,            
       do     => s_bc_do            
);

pTransition : process(r_state, r_size,
                      s_bc_busy, s_bc_do, r_rsp, r_iv, r_noinit, 
                      r_key, r_idx, r_do, r_di, r_data,
                      rd_crypto_cmd, rd_crypto_data)
begin

s_state        <= r_state;
s_key          <= r_key;
s_idx          <= r_idx;
s_do           <= r_do;
s_di           <= r_di;
s_size         <= r_size;
s_data         <= r_data;

s_bc_dsi       <= '0';
s_bc_first     <= '0';
s_bc_di        <= (others => '0');
s_bc_key       <= key_null;
s_bc_iv        <= (others => '0');
s_bc_mode      <= DECRYPT;
s_rsp          <= r_rsp;
s_iv           <= r_iv;
s_noinit       <= r_noinit;

case r_state is
       when IDLE =>
                  if rd_crypto_cmd.cmdval = '1' then --and rd_crypto_cmd.mode = CRY_MAC 
                     s_rsp      <= '0'; 
                     s_key      <= rd_crypto_cmd.key; 
                     s_size     <= to_integer(unsigned(rd_crypto_cmd.size)); 
                     s_data     <= rd_crypto_data;
                     s_iv       <= rd_crypto_cmd.iv;
                     s_noinit   <= rd_crypto_cmd.no_init;
                     s_idx      <= 0;
 
                     if  rd_crypto_cmd.no_init = '1' then
                         s_di  <= rd_crypto_data(1) & rd_crypto_data(0);
                         s_idx <= 2;
                     else
                         s_di  <= std_ulogic_vector(resize(unsigned(rd_crypto_cmd.address), s_bc_di'length));
                         if to_integer(unsigned(rd_crypto_cmd.size)) = 2 then 
                            s_idx <= 2; 
                         end if; 
                     end if;
                     s_state  <= CBC_FIRST;               
                  else
                     s_state    <= IDLE;
                  end if;

        when CBC_FIRST =>               
                 s_bc_first <= '1';
                 s_bc_dsi   <= '1';
                 s_bc_iv    <= r_iv;
                 s_bc_di    <= r_di;
                 s_bc_key   <= r_key;
                 s_bc_mode  <= ENCRYPT;

                 s_state    <= BC_CIPHER_WAIT;

       when CBC_CIPHER =>               
                 s_bc_first <= '0';
                 s_bc_dsi   <= '1';
                 s_bc_di    <= r_di;                                             
                 s_bc_key   <= r_key;
                 s_bc_mode  <= ENCRYPT;
                 s_idx      <= r_idx + 2;
                 s_state    <= BC_CIPHER_WAIT;

       when BC_CIPHER_WAIT =>
                 if s_bc_busy = '0' then
                    if r_idx = r_size then
                       s_rsp   <= '1';
                       s_state <= IDLE;               
                    else 
                       s_di    <= r_data(r_idx + 1) & r_data(r_idx);
                       s_state <= CBC_CIPHER;
                    end if;
                    s_do    <= s_bc_do;
                 end if; 

end case;

end process;

pGeneration : process(r_state)
begin

wr_crypto_cmd.cmdack <= '0';

case r_state is
        when IDLE =>
             wr_crypto_cmd.cmdack <= '1';

        when others =>
             null;
end case;

end process;

wr_crypto_cmd.rspval           <= r_rsp;
wr_crypto_data(0)(31 downto 0) <= r_do(33 to 64);
wr_crypto_data(1)(31 downto 0) <= r_do(1  to 32);

end rtl;


