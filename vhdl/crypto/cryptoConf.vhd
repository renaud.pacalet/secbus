--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- cryptoConf.vhd
--
-- CBC (Cipher Block Chaining) : Read Write pages
-- CTR (Counter)               : Read Only  pages
-- Registers : R0 => Cleartext  Data
--             R1 => Ciphertext Data
-- @todo : 
--         Block Cipher parameter  L2N, to be defined
--         Next : implement slave pages to store IV randomly generated.
--                One IV must be generated for Each Line to Encrypt.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

library des_lib;
use des_lib.des_pkg.all;

library bc_lib;
use bc_lib.all;

entity cryptoConf is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- Data from reg bank
       rd_reg_0                : in  data_register_read;  
       rd_reg_1                : in  data_register_read;  
      
       -- Data to reg Bank
       wr_reg_0                : out data_register_write; 
       wr_reg_1                : out data_register_write; 

       -- request from Security_Ctrl
       rd_crypto_cmd           : in  crypto_cmd_out;  
       wr_crypto_cmd           : out crypto_cmd_in        -- cmdack, rspval
);

end cryptoConf;

architecture rtl of cryptoConf is 

type fsm_state is (IDLE,  
                  BC_CBC_WAIT, CBC_REQ , CBC_LAST, 
                  BC_CTR_WAIT, CTR_ADDR, CTR_DATA,
                  RSP);

signal s_state, r_state            : fsm_state;
signal s_bc_dsi                    : std_ulogic; 
signal s_bc_first                  : std_ulogic;
signal s_bc_di                     : des_word_64;
signal s_bc_key                    : desx_key;
signal s_bc_iv                     : des_word_64;
signal s_bc_mode                   : encryption_mode;
signal s_bc_do                     : des_word_64;
signal s_bc_busy                   : std_ulogic;

signal s_key        , r_key        : desx_key;
signal s_addr       , r_addr       : addr_t;
signal s_addr_cpt   , r_addr_cpt   : unsigned(63 downto 0);
signal s_idx        , r_idx        : natural range 0 to SIZE_LINE_WORDS;                          
signal s_idx_prev   , r_idx_prev   : natural range 0 to SIZE_LINE_WORDS;                          
signal s_do         , r_do         : des_word_64;
signal s_di         , r_di         : des_word_64;
signal s_write      , r_write      : std_ulogic; 
signal s_cipher     , r_cipher     : std_ulogic; 
signal s_ctr_addr   , r_ctr_addr   : data_vector(SIZE_LINE_WORDS - 1 downto 0);
------------------
begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state       <= IDLE;
              r_key         <= key_null;
              r_addr        <= (others => '0');
              r_addr_cpt    <= (others => '0');
              r_idx         <= 0;
              r_idx_prev    <= 0;
              r_do          <= (others => '0');
              r_di          <= (others => '0');
              r_write       <= '0';             
              r_cipher      <= '0';             
              r_ctr_addr    <= (others => (others => '0'));

        elsif ce = '1' then
              r_state       <= s_state     ;
              r_key         <= s_key       ;
              r_addr        <= s_addr      ;
              r_idx         <= s_idx       ;
              r_idx_prev    <= s_idx_prev  ;
              r_do          <= s_do        ;
              r_di          <= s_di        ;
              r_write       <= s_write     ;
              r_addr_cpt    <= s_addr_cpt  ;
              r_cipher      <= s_cipher    ;
              r_ctr_addr    <= s_ctr_addr  ;
        end if;
end if;
end process;

bc_inst : entity bc_lib.bc(rtl) 
generic map(
       l2n    => BC_RND_BITS                
)
port map(
       clk    => clk,           
       srstn  => srstn,            
       ce     => ce,            
       first  => s_bc_first,            
       dsi    => s_bc_dsi,            
       iv     => s_bc_iv,            
       di     => s_bc_di,            
       key    => s_bc_key,            
       mode   => s_bc_mode,            
       busy   => s_bc_busy,            
       do     => s_bc_do            
);

pTransition : process(r_state     , r_write    , r_cipher   ,
                      r_addr      , r_addr_cpt ,
                      rd_reg_0    , rd_reg_1   , 
                      s_bc_busy   , s_bc_do    ,
                      r_key       , r_idx      , r_idx_prev  ,  
                      r_do        , r_di       , 
                      r_ctr_addr  , 
                      rd_crypto_cmd)
begin

s_state        <= r_state;
s_key          <= r_key;
s_idx          <= r_idx;
s_idx_prev     <= r_idx_prev;
s_do           <= r_do;
s_addr         <= r_addr;
s_addr_cpt     <= r_addr_cpt;
s_di           <= r_di;
s_write        <= r_write;
s_cipher       <= r_cipher;

s_bc_dsi       <= '0';
s_bc_first     <= '0';
s_bc_di        <= (others => '0');
s_bc_key       <= key_null;
s_bc_iv        <= (others => '0');
s_bc_mode      <= DECRYPT;

s_ctr_addr     <= r_ctr_addr;

case r_state is
       when IDLE =>
                  if rd_crypto_cmd.cmdval = '1' then 
                     s_addr     <= rd_crypto_cmd.address;
                     s_addr_cpt <= resize(unsigned(rd_crypto_cmd.address), s_addr_cpt'length);
                     s_key      <= rd_crypto_cmd.key; 
 
                     s_idx      <= 0;
                     s_write    <= '0';
                     s_cipher   <= rd_crypto_cmd.cipher;

                     s_bc_di    <= std_ulogic_vector(resize(unsigned(rd_crypto_cmd.address), s_bc_di'length));
                     s_bc_key   <= rd_crypto_cmd.key;
                     s_bc_mode  <= ENCRYPT;

                     if rd_crypto_cmd.mode = CRY_CBC then
                         s_bc_first <= '1';
                         s_bc_dsi   <= '1';
                         s_bc_iv    <= rd_crypto_cmd.iv;
                         s_state    <= BC_CBC_WAIT; 
                     else -- CRY_CTR
                         if  rd_crypto_cmd.data_ready = '0' then
                            s_bc_first <= '1';
                            s_bc_dsi   <= '1';
                            s_bc_iv <= (others => '0');
                            s_state <= BC_CTR_WAIT;
                            s_ctr_addr <= (others => (others => '0'));
                         else
                            s_state <= CTR_DATA;
                         end if;   
                     end if; 
                  else
                     s_state <= IDLE;
                  end if;
        -- CBC 
        when BC_CBC_WAIT =>
                 if s_bc_busy = '0' then
                     if r_idx = SIZE_LINE_WORDS then
                         s_state <= CBC_LAST; 
                     else
                         if r_cipher = '1' then
                             s_di <= rd_reg_0.data(r_idx + 1) & rd_reg_0.data(r_idx);
                         else
                             s_di <= rd_reg_1.data(r_idx + 1) & rd_reg_1.data(r_idx);
                         end if;
                         s_state <= CBC_REQ;
                     end if;
                     s_do    <= s_bc_do;
                 else
                     s_state <= BC_CBC_WAIT;
                 end if; 

       when CBC_REQ =>               

                 s_write    <= '1';                 
                 s_bc_first <= not(r_cipher or r_write);
                 s_bc_iv    <= r_do;
                 s_bc_dsi   <= '1';
                 s_bc_di    <= r_di;                                             
                 s_bc_key   <= r_key;

                 if r_cipher = '1' then
                     s_bc_mode  <= ENCRYPT;
                 else
                     s_bc_mode  <= DECRYPT;
                 end if;

                 s_idx      <= r_idx + 2;
                 s_idx_prev <= r_idx;

                 s_state   <= BC_CBC_WAIT;

         when CBC_LAST =>               
                 s_state   <= RSP;
        -- CTR
        when BC_CTR_WAIT =>
                 if s_bc_busy = '0' then
                     s_do       <= s_bc_do;
                     s_addr_cpt <= r_addr_cpt + 8;
                     s_state    <= CTR_ADDR;
                 else
                     s_state <= BC_CTR_WAIT;
                 end if; 
 
        when CTR_ADDR =>              
                 s_ctr_addr(r_idx)(31 downto 0)   <= r_do(33 to 64);
                 s_ctr_addr(r_idx+1)(31 downto 0) <= r_do(1  to 32);

                 if r_idx = (SIZE_LINE_WORDS - 2) then
                     s_state <= RSP;
                 else
                     s_bc_first <= '1';
                     s_bc_dsi   <= '1';
                     s_bc_iv    <= (others => '0');
                     s_bc_di    <= std_ulogic_vector(r_addr_cpt);                                           
                     s_bc_key   <= r_key;
                     s_bc_mode  <= ENCRYPT;

                     s_idx      <= r_idx  + 2;
                     s_state    <= BC_CTR_WAIT;
                 end if;

        when CTR_DATA =>
                 s_state <= RSP;

        when RSP =>
                 s_state <= IDLE;

end case;

end process;

pGeneration : process(r_state, r_addr, rd_reg_0, rd_reg_1, r_do, r_idx, r_idx_prev, r_write, r_cipher, r_ctr_addr)
begin

wr_crypto_cmd.cmdack <= '0';
wr_crypto_cmd.rspval <= '0';

wr_reg_1.enable      <= '0';
wr_reg_1.flush       <= '0';

wr_reg_0.enable      <= '0';
wr_reg_0.flush       <= '0';

wr_reg_0.addr        <= (others => '0');
wr_reg_0.data        <= (others => (others => '0'));
wr_reg_0.be          <= (others => (others => '0'));

wr_reg_1.addr        <= (others => '0');
wr_reg_1.data        <= (others => (others => '0'));
wr_reg_1.be          <= (others => (others => '0'));

case r_state is
        when IDLE =>
             wr_crypto_cmd.cmdack <= '1';

        when BC_CBC_WAIT =>
             null;

        when CBC_REQ =>
             if r_cipher = '1' then
                  wr_reg_1.enable                          <= r_write; 
                  wr_reg_1.addr                            <= r_addr;
                  wr_reg_1.data(r_idx_prev)(31 downto 0)   <= r_do(33 to 64);
                  wr_reg_1.data(r_idx_prev+1)(31 downto 0) <= r_do(1  to 32);
                  wr_reg_1.be(r_idx_prev+1)                <= (others => '1');
                  wr_reg_1.be(r_idx_prev)                  <= (others => '1');
             else 
                  wr_reg_0.enable                          <= r_write; 
                  wr_reg_0.addr                            <= r_addr;
                  wr_reg_0.data(r_idx_prev)(31 downto 0)   <= r_do(33 to 64);
                  wr_reg_0.data(r_idx_prev+1)(31 downto 0) <= r_do(1  to 32);
                  wr_reg_0.be(r_idx_prev+1)                <= (others => '1');
                  wr_reg_0.be(r_idx_prev)                  <= (others => '1');
             end if;


        when CBC_LAST =>
             if r_cipher = '1' then
                  wr_reg_1.enable                          <= '1';      
                  wr_reg_1.addr                            <= r_addr;
                  wr_reg_1.data(r_idx_prev)(31 downto 0)   <= r_do(33 to 64);
                  wr_reg_1.data(r_idx_prev+1)(31 downto 0) <= r_do(1  to 32);
                  wr_reg_1.be(r_idx_prev+1)                <= (others => '1');
                  wr_reg_1.be(r_idx_prev)                  <= (others => '1');
             else 
                  wr_reg_0.enable                          <= '1';       
                  wr_reg_0.addr                            <= r_addr;
                  wr_reg_0.data(r_idx_prev)(31 downto 0)   <= r_do(33 to 64);
                  wr_reg_0.data(r_idx_prev+1)(31 downto 0) <= r_do(1  to 32);
                  wr_reg_0.be(r_idx_prev+1)                <= (others => '1');
                  wr_reg_0.be(r_idx_prev)                  <= (others => '1');
             end if;
        --
        when BC_CTR_WAIT =>
             null;

        when CTR_ADDR =>
             null;

        when CTR_DATA =>
             if r_cipher = '1' then
                  wr_reg_1.enable      <= '1';
                  wr_reg_1.addr        <= r_addr;
                  for i in r_ctr_addr'range loop
                      wr_reg_1.data(i)  <= rd_reg_0.data(i) xor r_ctr_addr(i);
                      wr_reg_1.be(i)    <= (others => '1');
                  end loop;    
             else
                  wr_reg_0.enable       <= '1';
                  wr_reg_0.addr         <= r_addr;
                  for i in r_ctr_addr'range loop
                      wr_reg_0.data(i)  <= rd_reg_1.data(i) xor r_ctr_addr(i);
                      wr_reg_0.be(i)    <= (others => '1');
                  end loop;    

             end if;

        when RSP =>
             wr_crypto_cmd.rspval <= '1';
end case;

end process;

end rtl;


