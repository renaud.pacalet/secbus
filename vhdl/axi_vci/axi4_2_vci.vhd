--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- axi4_2_vci.vhd 

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

library axi_lib;
use axi_lib.axi_pkg.all;

entity axi4_2_vci is
port (
       clk          : in  std_ulogic;
       srstn        : in  std_ulogic;
       ce           : in  std_ulogic;

       -- Axi4 in
       p_axi_in     : in  axi_gp_m2s; 
       p_axi_out    : out axi_gp_s2m; 

       -- Vci out 
       p_vci_out    : out vci_i2t;
       p_vci_in     : in  vci_t2i

);
end axi4_2_vci;

architecture rtl of axi4_2_vci is
type fsm_state is (IDLE, 
                   RD_ACK_WAIT, RD_RSP_BURST,  
                   WR_ADDR_ACK, WR_BURST    , WR_RSP_WAIT);

constant plen_zero : unsigned(PLEN_SIZE - 1 downto 0) := (others => '0');

signal s_state      , r_state      : fsm_state;
signal s_addr       , r_addr       : unsigned(ADDR_SIZE - 1 downto 0);
signal s_full_plen  , r_full_plen  : unsigned(PLEN_SIZE - 1 downto 0);
signal s_chunk_plen , r_chunk_plen : unsigned(PLEN_SIZE - 1 downto 0);
signal s_srcid      , r_srcid      : srcid_t;
signal s_pktid      , r_pktid      : pktid_t;

signal s_contig     , r_contig     : std_ulogic;
signal s_cons       , r_cons       : std_ulogic;
signal s_wrap       , r_wrap       : std_ulogic;
signal s_eop                       : std_ulogic;
signal s_first      , r_first      : std_ulogic;
signal s_last                      : std_ulogic;

begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state      <= IDLE;
              r_addr       <= (others => '0'); 
              r_full_plen  <= (others => '0'); 
              r_chunk_plen <= (others => '0'); 
              r_srcid      <= (others => '0'); 
              r_pktid      <= (others => '0'); 
              r_contig     <= '0'; 
              r_cons       <= '0'; 
              r_wrap       <= '0'; 
              r_first      <= '0'; 

        elsif ce = '1' then
              r_state      <= s_state;
              r_addr       <= s_addr ; 
              r_full_plen  <= s_full_plen ; 
              r_chunk_plen <= s_chunk_plen ; 
              r_srcid      <= s_srcid; 
              r_pktid      <= s_pktid; 
              r_contig     <= s_contig;
              r_cons       <= s_cons;  
              r_wrap       <= s_wrap;  
              r_first      <= s_first;  
        end if;
end if;
end process;

--
s_last <= '1' when r_full_plen = plen_zero else
          '0';

s_eop  <= p_vci_in.reop and (not r_contig or s_last);

--
pTransition : process(r_state , p_axi_in, p_vci_in,
                      r_addr  ,  
                      r_srcid , r_pktid , 
                      r_contig, r_cons  , r_wrap,
                      r_full_plen  , r_chunk_plen, r_first  
                     )

variable v_burst            : std_ulogic_vector(axi_b - 1 downto 0);
variable v_plen             : unsigned(PLEN_SIZE - 1 downto 0);
variable v_upper_bound_addr : unsigned(ADDR_SIZE - 1 downto 0);
variable v_last_addr        : unsigned(ADDR_SIZE - 1 downto 0);
begin

s_state        <= r_state;
s_addr         <= r_addr;
s_full_plen    <= r_full_plen;
s_chunk_plen   <= r_chunk_plen; 
s_srcid        <= r_srcid;
s_pktid        <= r_pktid;
s_contig       <= r_contig;
s_cons         <= r_cons;
s_wrap         <= r_wrap;
s_first        <= r_first;

case r_state is
     when IDLE =>
          if p_axi_in.awvalid = '1' then
               s_addr   <= unsigned(p_axi_in.awaddr);
               s_srcid  <= p_axi_in.awid;
               -- plen expressed in bytes
               s_full_plen(s_full_plen'left downto CELL_BITS) <= unsigned(p_axi_in.awlen) + 1; -- 2**p_axi_in.arsize*p_axi_in.arlen
               s_full_plen(1 downto 0) <= (others => '0');
               s_pktid  <= '0' & p_axi_in.awsize;

               v_burst := p_axi_in.awburst;

               s_contig <= '0';
               s_cons   <= '0';
               s_wrap   <= '0';

               case v_burst is
                    when axi_burst_fixed => 
                         s_cons   <= '1'; 
                    when axi_burst_incr =>    
                         s_contig <= '1'; 
                    when axi_burst_wrap =>
                         s_wrap   <= '1';
                    when others => -- reserved
                         null;
               end case;

               s_state  <= WR_ADDR_ACK;

          elsif p_axi_in.arvalid = '1' then
               s_first  <= '1'; 
               s_addr   <= unsigned(p_axi_in.araddr);
               s_srcid  <= p_axi_in.arid;
               -- plen expressed in bytes
               v_plen(v_plen'left downto CELL_BITS) := unsigned(p_axi_in.arlen) + 1; -- 2**p_axi_in.arsize*p_axi_in.arlen
               v_plen(1 downto 0) := (others => '0');

               s_full_plen        <= v_plen;
               s_chunk_plen       <= v_plen;                                                           
   
               v_burst := p_axi_in.arburst;

               s_pktid  <= '0' & p_axi_in.arsize;

               s_contig <= '0';
               s_cons   <= '0';
               s_wrap   <= '0';

               v_last_addr        := (others => '0');
               v_upper_bound_addr := (others => '0');

               case v_burst is
                    when axi_burst_fixed => 
                         s_cons   <= '1'; 

                    when axi_burst_incr =>   
                         v_last_addr        := unsigned(p_axi_in.araddr) + unsigned(p_axi_in.arlen)*CELL_SIZE;         
                         v_upper_bound_addr := unsigned(line_aligned(p_axi_in.araddr)) + SIZE_LINE_BYTES;

                         if(v_last_addr >= v_upper_bound_addr) then
                            s_chunk_plen <= to_unsigned(SIZE_LINE_BYTES - (word_index(p_axi_in.araddr) * CELL_SIZE), PLEN_SIZE);
                         end if;  
                         s_contig <= '1';

                    when axi_burst_wrap =>
                         s_wrap   <= '1';

                    when others => -- reserved
                         null;
               end case;

               s_state  <= RD_ACK_WAIT;

          end if; -- arvalid or awvalid

     when RD_ACK_WAIT =>
          if p_vci_in.cmdack = '1' then
                 s_first            <= '0';
                 s_full_plen        <= r_full_plen - r_chunk_plen;                    
                 s_state            <= RD_RSP_BURST; 
          end if;
    
     when WR_ADDR_ACK =>
          s_state <= WR_BURST;
 
     when WR_BURST =>
          if p_axi_in.wvalid = '1' and p_vci_in.cmdack = '1' then
                 if p_axi_in.wlast = '1' then
                    s_state <= WR_RSP_WAIT;
                 else
                    s_addr  <= r_addr + axi_gp_m; -- CELL_SIZE;  -- burst = INCR/WRAP, SIZE = 2 (4 bytes)
                    s_state <= WR_BURST;
                 end if;
          end if;

     when RD_RSP_BURST =>
          if (p_vci_in.rspval = '1' and p_vci_in.reop = '1' and p_axi_in.rready = '1') then
                 if r_contig = '1' then -- axi burst incr

                   if r_full_plen = plen_zero then
                       s_state <= IDLE;
                   else
                       s_addr  <= r_addr + r_chunk_plen;
                       s_state <= RD_ACK_WAIT;

                       if r_full_plen > SIZE_LINE_BYTES then
                           s_chunk_plen <= to_unsigned(SIZE_LINE_BYTES, PLEN_SIZE);
                       else
                           s_chunk_plen <= r_full_plen;
                       end if; 
                    end if; 
 
                 else 
                    s_state <= IDLE;
                 end if;  
          end if;  

     when WR_RSP_WAIT =>
          if p_vci_in.rspval = '1' and p_axi_in.bready = '1' then -- and p_vci_in.reop = '1'
                 s_state <= IDLE;
          end if;
end case;

end process;
----
pGeneration : process(r_state , 
                      p_axi_in, p_vci_in     , 
                      r_addr  , r_chunk_plen , r_full_plen , 
                      r_srcid , r_pktid      , s_eop       ,
                      r_contig, r_cons       , r_wrap      , r_first 
                     )
begin

p_vci_out <= vci_i2t_none;
-- s2m
p_axi_out <= axi_gp_s2m_none; 

case r_state is 
         when IDLE =>
            null;

         when RD_ACK_WAIT => 
            p_axi_out.arready  <= r_first and p_vci_in.cmdack;

            p_vci_out.cmdval   <= '1';
            p_vci_out.addr     <= std_ulogic_vector(r_addr);             
            p_vci_out.be       <= (others => '1');
            p_vci_out.plen     <= std_ulogic_vector(r_chunk_plen);
            p_vci_out.cmd      <= VCI_READ;
            p_vci_out.eop      <= '1';
            p_vci_out.contig   <= r_contig;
            p_vci_out.cons     <= r_cons;
            p_vci_out.wrap     <= r_wrap;
            p_vci_out.srcid    <= r_srcid;            
            p_vci_out.pktid    <= r_pktid;                                                              

         when RD_RSP_BURST =>         
            p_vci_out.rspack   <= p_axi_in.rready;

            p_axi_out.rvalid   <= p_vci_in.rspval; 
            p_axi_out.rdata    <= p_vci_in.rdata;
            p_axi_out.rlast    <= s_eop;           --p_vci_in.reop;
            p_axi_out.rid      <= p_vci_in.rsrcid; 
            p_axi_out.rresp    <= p_vci_in.rerror; 

         when WR_ADDR_ACK =>
            p_axi_out.awready  <= '1';
 
         when WR_BURST =>
            p_axi_out.wready   <= p_vci_in.cmdack;

            p_vci_out.cmdval   <= p_axi_in.wvalid;
            p_vci_out.addr     <= std_ulogic_vector(r_addr);                                                 
            p_vci_out.plen     <= std_ulogic_vector(r_full_plen);
            p_vci_out.cmd      <= VCI_WRITE;
            p_vci_out.wdata    <= p_axi_in.wdata;
            p_vci_out.be       <= p_axi_in.wstrb;
            p_vci_out.eop      <= p_axi_in.wlast;
            p_vci_out.contig   <= r_contig;
            p_vci_out.cons     <= r_cons;
            p_vci_out.wrap     <= r_wrap;
            p_vci_out.srcid    <= r_srcid;            
            p_vci_out.trdid    <= p_axi_in.wid;                                                         
            p_vci_out.pktid    <= r_pktid;                                                              

         when WR_RSP_WAIT =>
            p_vci_out.rspack   <= p_axi_in.bready;

            p_axi_out.bvalid   <= p_vci_in.rspval; 
            p_axi_out.bid      <= p_vci_in.rsrcid; 
            p_axi_out.bresp    <= p_vci_in.rerror;             
end case;

end process;

end rtl;

