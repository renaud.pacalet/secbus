--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- vci_2_axi4.vhd 

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

library axi_lib;
use axi_lib.axi_pkg.all;

entity vci_2_axi4 is
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- Axi4 out 
       p_axi_in              : in  axi_gp_s2m; 
       p_axi_out             : out axi_gp_m2s; 

       -- Vci in   
       p_vci_out             : out vci_t2i;
       p_vci_in              : in  vci_i2t

);
end vci_2_axi4;

architecture rtl of vci_2_axi4 is
type fsm_state is (IDLE       , 
                   RD_ACK_WAIT, RD_RSP_BURST,  
                   WR_ADDR_ACK, WR_BURST    , WR_RSP_WAIT
                  );

signal s_state , r_state : fsm_state;
signal s_addr  , r_addr  : addr_t;                            
signal s_len   , r_len   : unsigned(axi_l - 1 downto 0);
signal s_burst , r_burst : std_ulogic_vector(axi_b - 1 downto 0);
signal s_size  , r_size  : std_ulogic_vector(axi_s - 1 downto 0); 
signal s_srcid , r_srcid : srcid_t;

begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state <= IDLE;
              r_addr  <= (others => '0'); 
              r_len   <= (others => '0'); 
              r_size  <= (others => '0'); 
              r_srcid <= (others => '0'); 
              r_burst <= (others => '0'); 
        elsif ce = '1' then
              r_state <= s_state;
              r_addr  <= s_addr ; 
              r_len   <= s_len ; 
              r_size  <= s_size; 
              r_burst <= s_burst; 
              r_srcid <= s_srcid; 
        end if;
end if;
end process;

pTransition : process(r_state, p_axi_in, p_vci_in,
                      r_addr, r_len, r_size, r_srcid, r_burst 
                     )
begin

s_state <= r_state;
s_addr  <= r_addr;
s_len   <= r_len;
s_size  <= r_size;
s_burst <= r_burst; 
s_srcid <= r_srcid;

case r_state is
     when IDLE =>
          if p_vci_in.cmdval = '1' then
                 s_addr  <= p_vci_in.addr;
                 s_srcid <= p_vci_in.srcid;
                 s_size  <= p_vci_in.pktid(r_size'range);
                 s_len   <= resize((unsigned(p_vci_in.plen(p_vci_in.plen'left downto CELL_BITS)) - 1), AXI_L); -- 2**p_axi_in.arsize*p_axi_in.arlen
 
                 s_burst <= (others => '0');                
                 --assert (p_vci_in.contig and p_vci_in.cons and p_vci_in.wrap) = '0'
                 --report "Burst Type RE-Encoding impossible due to non exclusive values"
                 --severity failure;
 
                 if p_vci_in.contig = '1' then
                    s_burst <= axi_burst_incr;
                 elsif p_vci_in.wrap = '1' then
                    s_burst <= axi_burst_wrap;
                 elsif p_vci_in.cons = '1' then 
                    s_burst <= axi_burst_fixed; 
                 end if;

                 if p_vci_in.cmd = VCI_READ then
                     s_state <= RD_ACK_WAIT;
                 elsif p_vci_in.cmd = VCI_WRITE then
                     s_state <= WR_ADDR_ACK;
                 --else
                 -- pop entire vci burst then return error
                 end if;
          end if; -- cmdval

     when RD_ACK_WAIT =>

          if p_axi_in.arready = '1' then 
                 s_state <= RD_RSP_BURST; 
          end if;
    
     when WR_ADDR_ACK =>
          if p_axi_in.awready = '1' then
              s_state <= WR_BURST;
          end if;

     when WR_BURST =>

          if (p_vci_in.cmdval = '1' and p_vci_in.eop = '1' and p_axi_in.wready = '1') then 
              s_state <= WR_RSP_WAIT;
          end if;

     when RD_RSP_BURST =>
          if (p_axi_in.rvalid = '1' and p_axi_in.rlast = '1' and p_vci_in.rspack = '1') then
              s_state <= IDLE;
          end if;  

     when WR_RSP_WAIT =>
          if p_axi_in.bvalid = '1' and p_vci_in.rspack = '1' then -- no need to test reop
              s_state <= IDLE;
          end if;

end case;

end process;

pGeneration : process(r_state , 
                      p_axi_in, p_vci_in, 
                      r_addr  , r_len   , r_srcid, 
                      r_size  , r_burst  
                     )
begin

p_vci_out <= vci_t2i_none;
-- m2s
p_axi_out <= axi_gp_m2s_none;   

case r_state is 
         when IDLE =>
             null;

         when RD_ACK_WAIT => 
            p_vci_out.cmdack    <= p_axi_in.arready;

            p_axi_out.arvalid  <= '1';  
            p_axi_out.araddr   <= r_addr;
            p_axi_out.arid     <= r_srcid;
            p_axi_out.arlen    <= std_ulogic_vector(r_len);
            p_axi_out.arsize   <= r_size;
            p_axi_out.arburst  <= r_burst;   

         when RD_RSP_BURST =>         
            p_axi_out.rready  <=  p_vci_in.rspack ; 
                             
            p_vci_out.rspval   <= p_axi_in.rvalid; 
            p_vci_out.rdata    <= p_axi_in.rdata ; 
            p_vci_out.reop     <= p_axi_in.rlast ; 
            p_vci_out.rsrcid   <= p_axi_in.rid   ; 
            p_vci_out.rerror   <= p_axi_in.rresp ; 

         when WR_ADDR_ACK =>
            p_axi_out.awvalid <= '1';
            p_axi_out.awaddr  <= r_addr;                              
            p_axi_out.awid    <= r_srcid;                             
            p_axi_out.awlen   <= std_ulogic_vector(r_len);
            p_axi_out.awsize  <= r_size;                                                                                               
            p_axi_out.awburst <= r_burst;   

         when WR_BURST =>
            p_vci_out.cmdack  <=  p_axi_in.wready;
                                                
            p_axi_out.wvalid  <=  p_vci_in.cmdval;
            p_axi_out.wid     <=  p_vci_in.trdid; 
            p_axi_out.wdata   <=  p_vci_in.wdata; 
            p_axi_out.wstrb   <=  p_vci_in.be;     
            p_axi_out.wlast   <=  p_vci_in.eop;   
 
         when WR_RSP_WAIT =>
            p_axi_out.bready  <=  p_vci_in.rspack ; 
                             
            p_vci_out.rspval  <=  p_axi_in.bvalid; 
            p_vci_out.rdata   <=  (others => '0'); 
            p_vci_out.reop    <=  '1'            ; 
            p_vci_out.rsrcid  <=  p_axi_in.bid   ; 
            p_vci_out.rerror  <=  p_axi_in.bresp ; 
end case;

end process;

end rtl;

