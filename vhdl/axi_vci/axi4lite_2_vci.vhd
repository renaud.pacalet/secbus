--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- axi4lite_2_vci.vhd 

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

library axi_lib;
use axi_lib.axi_pkg.all;

entity axi4lite_2_vci is
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- Axi4 in
       p_axi_in              : in  axilite_gp_m2s; 
       p_axi_out             : out axilite_gp_s2m; 

       -- Vci out 
       p_vci_out             : out vci_i2t;
       p_vci_in              : in  vci_t2i

);
end axi4lite_2_vci;

architecture rtl of axi4lite_2_vci is
type fsm_state is (IDLE       , 
                   RD_ACK_WAIT, RD_RSP_DT, 
                   WR_DT    , WR_RSP_WAIT);

signal s_state , r_state : fsm_state;
signal s_addr  , r_addr  : unsigned(ADDR_SIZE - 1 downto 0);

signal s_contig, r_contig : std_ulogic;
signal s_cons  , r_cons   : std_ulogic;
signal s_wrap  , r_wrap   : std_ulogic;

begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state  <= IDLE;
              r_addr   <= (others => '0'); 
              r_contig <= '0'; 
              r_cons   <= '0'; 
              r_wrap   <= '0'; 

        elsif ce = '1' then
              r_state  <= s_state;
              r_addr   <= s_addr ; 
              r_contig <= s_contig;
              r_cons   <= s_cons;  
              r_wrap   <= s_wrap;  
        end if;
end if;
end process;

pTransition : process(r_state, 
                      p_axi_in, p_vci_in, r_addr  ,    
                      r_contig, r_cons  , r_wrap
                     )
begin

s_state  <= r_state;
s_addr   <= r_addr;
s_contig <= r_contig;
s_cons   <= r_cons;
s_wrap   <= r_wrap;

case r_state is
     when IDLE =>
          if p_axi_in.arvalid = '1' then
               s_addr   <= unsigned(p_axi_in.araddr);

               s_state  <= RD_ACK_WAIT;
          elsif p_axi_in.awvalid = '1' and p_axi_in.wvalid  = '1' then
               s_addr   <= unsigned(p_axi_in.awaddr);
               s_state  <= WR_DT;
          end if;

     when RD_ACK_WAIT =>

          if p_vci_in.cmdack = '1' then 
                 s_state <= RD_RSP_DT; 
          end if;
    
     when WR_DT =>

          if p_vci_in.cmdack = '1' then
                 s_state <= WR_RSP_WAIT;
          end if;

     when RD_RSP_DT =>
          if p_vci_in.rspval = '1' and p_axi_in.rready = '1' then
                  s_state <= IDLE;
          end if;  

     when WR_RSP_WAIT =>
          if p_vci_in.rspval = '1' and p_axi_in.bready = '1' then 
                 s_state <= IDLE;
          end if;
end case;

end process;
-----
pGeneration : process(r_state, p_axi_in, p_vci_in, r_addr)
begin

p_vci_out <= vci_i2t_none;
p_axi_out <= axilite_gp_s2m_none; 

case r_state is 
         when IDLE =>
            null;

         when RD_ACK_WAIT => 
            p_axi_out.arready  <= p_vci_in.cmdack;

            p_vci_out.cmdval   <= '1';
            p_vci_out.addr     <= std_ulogic_vector(r_addr);             
            p_vci_out.be       <= (others => '1');
            p_vci_out.plen     <= std_ulogic_vector(to_unsigned(4, PLEN_SIZE));
            p_vci_out.cmd      <= VCI_READ;
            p_vci_out.eop      <= '1';
            p_vci_out.contig   <= '1';
            p_vci_out.cons     <= '0';       
            p_vci_out.wrap     <= '0';     

         when RD_RSP_DT =>         
            p_vci_out.rspack   <= p_axi_in.rready;

            p_axi_out.rvalid   <= p_vci_in.rspval; 
            p_axi_out.rdata    <= p_vci_in.rdata;
            p_axi_out.rresp    <= p_vci_in.rerror; 

         when WR_DT =>
            p_axi_out.wready   <= p_vci_in.cmdack;
            p_axi_out.awready  <= '1';

            p_vci_out.cmdval   <= p_axi_in.wvalid;
            p_vci_out.addr     <= std_ulogic_vector(r_addr);                                                 
            p_vci_out.plen     <= std_ulogic_vector(to_unsigned(4, PLEN_SIZE));
            p_vci_out.cmd      <= VCI_WRITE;
            p_vci_out.wdata    <= p_axi_in.wdata;
            p_vci_out.be       <= p_axi_in.wstrb;
            p_vci_out.eop      <= '1';               
            p_vci_out.contig   <= '1';         
            p_vci_out.cons     <= '0';       
            p_vci_out.wrap     <= '0';     

         when WR_RSP_WAIT =>
            p_vci_out.rspack   <= p_axi_in.bready;

            p_axi_out.bvalid   <= p_vci_in.rspval; 
            p_axi_out.bresp    <= p_vci_in.rerror;             
end case;

end process;

end rtl;

