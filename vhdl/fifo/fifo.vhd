--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity fifo is
Generic (
	constant DATA_WIDTH  : positive := ADDR_SIZE;
	constant FIFO_DEPTH  : positive := LAST_HASH_ARITY      
);
Port ( 
	clk	    : in  std_ulogic;
	rstn	: in  std_ulogic;
	ce   	: in  std_ulogic;
	init 	: in  std_ulogic;
	write	: in  std_ulogic;
	wdata	: in  std_ulogic_vector (DATA_WIDTH - 1 downto 0);
	read	: in  std_ulogic;
	rdata	: out std_ulogic_vector (DATA_WIDTH - 1 downto 0);
	empty	: out std_ulogic;
	full	: out std_ulogic
);
end fifo;

architecture rtl of fifo is

  type fifo_memory is array (0 to FIFO_DEPTH - 1) of std_ulogic_vector(DATA_WIDTH - 1 downto 0);
  signal Memory : fifo_memory;
  signal WrPtr  : natural range 0 to FIFO_DEPTH - 1;
  signal RdPtr  : natural range 0 to FIFO_DEPTH - 1;

begin

	-- Memory Pointer Process
	fifo_proc : process (clk)
	variable Looped : boolean;
	begin
		if rising_edge(clk) then
			if rstn = '0' then
				WrPtr <= 0;
				RdPtr <= 0;
				
				Looped := false;
				
				full  <= '0';
				empty <= '1';
			elsif ce = '1' then

 		        if init = '1' then
		        	WrPtr <= 0;
		        	RdPtr <= 0;
		        	
		        	Looped := false;
		        	
		        	full  <= '0';
		        	empty <= '1';
                end if;

				if (read = '1') then
					if ((Looped = true) or (WrPtr /= RdPtr)) then
						-- Update data output
						rdata <= Memory(RdPtr);
						
						-- Update RdPtr pointer as needed
						if (RdPtr = FIFO_DEPTH - 1) then
							RdPtr <= 0;
							
							Looped := false;
						else
							RdPtr <= RdPtr + 1;
						end if;
						
						
					end if;
				end if;
				
				if (write = '1') then
					if ((Looped = false) or (WrPtr /= RdPtr)) then
						-- Write Data to Memory
						Memory(WrPtr) <= wdata;
						
						-- Increment WrPtr pointer as needed
						if (WrPtr = FIFO_DEPTH - 1) then
							WrPtr <= 0;
							
							Looped := true;
						else
							WrPtr <= WrPtr + 1;
						end if;
					end if;
				end if;
				
				-- Update empty and full flags
				if (WrPtr = RdPtr) then
					if Looped then
						full <= '1';
					else
						empty <= '1';
					end if;
				else
					empty	<= '0';
					full	<= '0';
				end if;
			end if; -- rstn
		end if; -- rising_edge(clk)
	end process;
		
end rtl;
