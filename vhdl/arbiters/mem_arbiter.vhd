--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mem_arbiter.vhd
--
-- sources  security_ctrl/mt_cache_ctrl/ms_ctrl
-- dest     vci_mem_ctrl

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity mem_arbiter is
generic(
       NRQ                   : positive := 4
);
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- 
       rd_mem_rq_a           : in  memory_ctrl_cmd_out_array(NRQ - 1 downto 0);
       wr_mem_rq_a           : out memory_ctrl_cmd_in_array(NRQ  - 1 downto 0);

       -- Request to vci_mem_ctrl 
       wr_mem_rq             : out memory_ctrl_cmd_out;  
       rd_mem_rq             : in  memory_ctrl_cmd_in   -- cmdack, rspval
);
end mem_arbiter;

architecture rtl of mem_arbiter is
type fsm_state is (IDLE, MEM_CMD, MEM_RSP);

signal s_state, r_state : fsm_state;
signal s_idx  , r_idx   : natural range 0 to NRQ - 1;

begin
----
pTrans : process(r_state, r_idx, rd_mem_rq_a, rd_mem_rq)
begin

s_state  <= r_state;
s_idx    <= r_idx;

case r_state is
     when IDLE =>
         s_idx <= 0;
         for i in 0 to (NRQ - 1) loop
               if rd_mem_rq_a(i).cmdval = '1' then
                  s_idx   <= i;
                  s_state <= MEM_CMD;
                  exit;
               end if;
         end loop;

     when MEM_CMD =>
         if rd_mem_rq.cmdack = '1' then
            s_state <= MEM_RSP;
         end if;
     when MEM_RSP =>
         if rd_mem_rq.rspval = '1' then
              s_state <= IDLE;
         end if;
end case;

end process;
---

pGen : process(r_state, r_idx, rd_mem_rq, rd_mem_rq_a)

begin

for i in 0 to (NRQ - 1) loop
    wr_mem_rq_a(i).cmdack <= '0';
    wr_mem_rq_a(i).rspval <= '0';
end loop;

wr_mem_rq.cmdval        <= '0';
wr_mem_rq.cmd           <= READ;
wr_mem_rq.start_address <= (others => '0');
wr_mem_rq.plen          <= (others => '0');
wr_mem_rq.id            <= (others => '0'); 
wr_mem_rq.direct_write  <= '0'; 

case r_state is 
         when IDLE =>
              null;

         when MEM_CMD =>
              wr_mem_rq_a(r_idx).cmdack <= rd_mem_rq.cmdack;

              wr_mem_rq.cmdval        <= rd_mem_rq_a(r_idx).cmdval;       
              wr_mem_rq.cmd           <= rd_mem_rq_a(r_idx).cmd;          
              wr_mem_rq.start_address <= rd_mem_rq_a(r_idx).start_address;
              wr_mem_rq.plen          <= rd_mem_rq_a(r_idx).plen;         
              wr_mem_rq.id            <= rd_mem_rq_a(r_idx).id;           
              wr_mem_rq.direct_write  <= rd_mem_rq_a(r_idx).direct_write; 
 
         when MEM_RSP => 
              wr_mem_rq_a(r_idx).rspval <= rd_mem_rq.rspval;
end case;

end process;

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state <= IDLE;
              r_idx   <= 0;
     
        elsif ce = '1' then
              r_state <= s_state;
              r_idx   <= s_idx;
        end if;
end if;
end process;

end rtl;

