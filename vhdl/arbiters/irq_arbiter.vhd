--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- irq_arbiter.vhd
--
-- sources  security_ctx_ctrl/io_input_ctrl/mt_ctrl/ms_ctrl
-- dest     vci_io_target

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity irq_arbiter is
generic(
       NRQ                   : positive := 4
);
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- 
       rd_irq_a              : in  irq_out_array(NRQ - 1 downto 0);
       wr_irq_ack_a          : out std_ulogic_vector(NRQ  - 1 downto 0);

       -- to vci_io_target
       wr_irq                : out irq_out;  
       rd_irq_ack            : in  std_ulogic
);
end irq_arbiter;

architecture rtl of irq_arbiter is
type fsm_state is (IDLE, IRQ_SEND);

signal s_state, r_state : fsm_state;
signal s_idx  , r_idx   : natural range 0 to NRQ - 1;

begin
----
pTrans : process(r_state, r_idx, rd_irq_a, rd_irq_ack)
begin

s_state  <= r_state;
s_idx    <= r_idx;

case r_state is
     when IDLE =>
         s_idx <= 0;
         for i in 0 to (NRQ - 1) loop
               if rd_irq_a(i).irq = '1' then
                  s_idx   <= i;
                  s_state <= IRQ_SEND;
                  exit;
               end if;
         end loop;

     when IRQ_SEND =>
         if rd_irq_ack = '1' then
            s_state <= IDLE;
         end if;

end case;

end process;
---

pGen : process(r_state, r_idx, rd_irq_ack, rd_irq_a)

begin

wr_irq_ack_a        <= (others => '0');

wr_irq.irq   <= '0';
wr_irq.addr  <= (others => '0');
wr_irq.typ   <= NONE;
wr_irq.op    <= LOAD;

case r_state is 
         when IDLE =>
            null;

         when IRQ_SEND =>
              wr_irq_ack_a(r_idx) <= rd_irq_ack;

              wr_irq.irq          <= rd_irq_a(r_idx).irq; 
              wr_irq.addr         <= rd_irq_a(r_idx).addr;
              wr_irq.typ          <= rd_irq_a(r_idx).typ; 
              wr_irq.op           <= rd_irq_a(r_idx).op;  
 
end case;

end process;

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state <= IDLE;
              r_idx   <= 0;
     
        elsif ce = '1' then
              r_state <= s_state;
              r_idx   <= s_idx;
        end if;
end if;
end process;

end rtl;

