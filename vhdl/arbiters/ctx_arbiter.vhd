--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- ctx_arbiter.vhd
--
-- sources  mt_ctrl/io_input_ctrl/vci_input_ctrl
-- dest     security_ctx_ctrl

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity ctx_arbiter is
generic(
       NRQ                   : positive := 4
);
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- 
       rd_ctx_cmd_a          : in  security_ctx_cmd_out_array(NRQ - 1 downto 0);
       wr_ctx_cmd_a          : out security_ctx_cmd_in_array(NRQ  - 1 downto 0);

       -- Request to Security context ctrl 
       wr_ctx_cmd            : out security_ctx_cmd_out;  
       rd_ctx_cmd            : in  security_ctx_cmd_in   -- cmdack, rspval
);
end ctx_arbiter;

architecture rtl of ctx_arbiter is
type fsm_state is (IDLE, CTX_CMD, CTX_RSP);

signal s_state, r_state : fsm_state;
signal s_idx  , r_idx   : natural range 0 to NRQ - 1;

begin
----
pTrans : process(r_state, r_idx, rd_ctx_cmd_a, rd_ctx_cmd)
begin

s_state  <= r_state;
s_idx    <= r_idx;

case r_state is
     when IDLE =>
         s_idx <= 0;
         for i in 0 to (NRQ - 1) loop
               if rd_ctx_cmd_a(i).enable = '1' then
                  s_idx   <= i;
                  s_state <= CTX_CMD;
                  exit;
               end if;
         end loop;

     when CTX_CMD =>
         if rd_ctx_cmd.cmdack = '1' then
            s_state <= CTX_RSP;
         end if;

     when CTX_RSP =>
         if rd_ctx_cmd.rspval = '1' then
              s_state <= IDLE;
         end if;
end case;

end process;
---

pGen : process(r_state, r_idx, rd_ctx_cmd, rd_ctx_cmd_a)

begin

for i in 0 to (NRQ - 1) loop
    wr_ctx_cmd_a(i).cmdack <= '0';
    wr_ctx_cmd_a(i).rspval <= '0';
end loop;

wr_ctx_cmd.enable  <= '0'; 
wr_ctx_cmd.cmd     <= READ;
wr_ctx_cmd.mt      <= '0';
wr_ctx_cmd.address <= (others => '0');

case r_state is 
         when IDLE =>
              wr_ctx_cmd_a(r_idx).rspval <= rd_ctx_cmd.rspval;

         when CTX_CMD =>
              wr_ctx_cmd_a(r_idx).cmdack <= rd_ctx_cmd.cmdack;

              wr_ctx_cmd.enable        <= rd_ctx_cmd_a(r_idx).enable;       
              wr_ctx_cmd.cmd           <= rd_ctx_cmd_a(r_idx).cmd;          
              wr_ctx_cmd.address       <= rd_ctx_cmd_a(r_idx).address;
              wr_ctx_cmd.mt            <= rd_ctx_cmd_a(r_idx).mt;         
 
         when CTX_RSP => 
              wr_ctx_cmd_a(r_idx).rspval <= rd_ctx_cmd.rspval;
end case;

end process;

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state <= IDLE;
              r_idx   <= 0;
     
        elsif ce = '1' then
              r_state <= s_state;
              r_idx   <= s_idx;
        end if;
end if;
end process;

end rtl;

