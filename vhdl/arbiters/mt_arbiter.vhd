--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mt_arbiter.vhd
--
-- sources  io_input_ctrl/security_ctrl
-- dest     mt_ctrl

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;
use global_lib.utils.all;

entity mt_arbiter is
generic(
       NRQ                  : positive := 4
);
port (
       clk                  : in  std_ulogic;
       srstn                : in  std_ulogic;
       ce                   : in  std_ulogic;

       -- 
       rd_mt_cmd_a          : in  integrity_ctrl_cmd_array(NRQ - 1 downto 0);

       -- Request to MT ctrl 
       wr_mt_cmd            : out integrity_ctrl_cmd  
);
end mt_arbiter;

architecture rtl of mt_arbiter is

begin
----
pGen : process(rd_mt_cmd_a)

begin

wr_mt_cmd.enable <= '0';
wr_mt_cmd.addr   <= (others => '0');
wr_mt_cmd.cmd    <= CHECK;
wr_mt_cmd.src    <= (others => '0');  

for i in 0 to (NRQ - 1) loop
    if rd_mt_cmd_a(i).enable = '1' then
         wr_mt_cmd <= rd_mt_cmd_a(i);      
         exit; 
    end if;
end loop;

end process;

end rtl;
