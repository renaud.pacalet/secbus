--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- reg_arbiter.vhd

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity reg_arbiter is
generic(
       NRQ                   : positive := 4
);
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- 
       rd_register_write_a   : in  data_register_write_array(NRQ - 1 downto 0);

       wr_register_write     : out data_register_write
);
end reg_arbiter;

architecture rtl of reg_arbiter is

begin
----
pGen : process(rd_register_write_a)

begin

wr_register_write.enable <= '0';
wr_register_write.flush  <= '0';
wr_register_write.addr   <= (others => '0');
wr_register_write.data   <= (others => (others => '0'));
wr_register_write.be     <= (others => (others => '0'));

for k in 0 to (NRQ - 1) loop
    if rd_register_write_a(k).enable = '1' then
          wr_register_write <= rd_register_write_a(k);
          exit;
    end if;
end loop;

end process;
---

end rtl;

