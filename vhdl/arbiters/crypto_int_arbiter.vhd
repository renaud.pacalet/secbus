--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- crypto_int_arbiter.vhd
--
-- sources  mt_ctrl/ms_ctrl
-- dest     crypto_engine_int

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity crypto_int_arbiter is
generic(
       NRQ                   : positive := 2
);
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- 
       rd_crypto_cmd_a       : in  crypto_cmd_out_array(NRQ - 1 downto 0);
       wr_crypto_cmd_a       : out crypto_cmd_in_array(NRQ  - 1 downto 0);  -- cmdack, rspval
       
       rd_crypto_data_ms     : in  data_vector(11 downto 0); -- Data set = 6 blocks 
       rd_crypto_data_mt     : in  data_vector(11 downto 0);
    
       -- Request to Crypto_int       
       wr_crypto_cmd         : out crypto_cmd_out;  
       rd_crypto_cmd         : in  crypto_cmd_in;  -- cmdack, rspval

       wr_crypto_data        : out data_vector(11 downto 0)
);
end crypto_int_arbiter;

architecture rtl of crypto_int_arbiter is
type fsm_state is (IDLE, CRY_INT_CMD, CRY_INT_RSP);

signal s_state, r_state  : fsm_state;
signal s_idx  , r_idx    : natural range 0 to NRQ - 1;

begin
----
pTrans : process(r_state, r_idx, rd_crypto_cmd_a, rd_crypto_cmd)
begin

s_state  <= r_state;
s_idx    <= r_idx;

case r_state is
     when IDLE =>
         s_idx <= 0;
         for i in 0 to (NRQ - 1) loop
               if rd_crypto_cmd_a(i).cmdval = '1' then
                  s_idx   <= i;
                  s_state <= CRY_INT_CMD;
                  exit;
               end if;
         end loop;

     when CRY_INT_CMD =>
         if rd_crypto_cmd.cmdack = '1' then
            s_state <= CRY_INT_RSP;
         end if;
     when CRY_INT_RSP =>
         if rd_crypto_cmd.rspval = '1' then
              s_state <= IDLE;
         end if;
end case;

end process;
---

pGenCmd : process(r_state, r_idx, rd_crypto_cmd, rd_crypto_cmd_a, rd_crypto_data_ms, rd_crypto_data_mt)

begin

for i in 0 to (NRQ - 1) loop
  wr_crypto_cmd_a(i).cmdack <= '0';
  wr_crypto_cmd_a(i).rspval <= '0';
end loop;

wr_crypto_data            <= (others => (others => '0'));

wr_crypto_cmd.cmdval      <= '0';  
wr_crypto_cmd.address     <= (others => '0');
wr_crypto_cmd.cipher      <= '0';
wr_crypto_cmd.direct_data <= '0';  
wr_crypto_cmd.no_init     <= '0'; 
wr_crypto_cmd.key         <= key_null;
wr_crypto_cmd.iv          <= (others => '0');
wr_crypto_cmd.size        <= (others => '0');      
wr_crypto_cmd.id_src      <= (others => '0');
wr_crypto_cmd.id_dest     <= (others => '0');
wr_crypto_cmd.mode        <= CRY_CTR;
wr_crypto_cmd.data_ready  <= '0';       

case r_state is 
         when IDLE =>
              wr_crypto_cmd_a(r_idx).rspval <= rd_crypto_cmd.rspval;

         when CRY_INT_CMD =>
              wr_crypto_cmd_a(r_idx).cmdack <= rd_crypto_cmd.cmdack;

              wr_crypto_cmd.cmdval          <= rd_crypto_cmd_a(r_idx).cmdval; 
              wr_crypto_cmd.address         <= rd_crypto_cmd_a(r_idx).address;
              wr_crypto_cmd.cipher          <= rd_crypto_cmd_a(r_idx).cipher; 
              wr_crypto_cmd.direct_data     <= rd_crypto_cmd_a(r_idx).direct_data;
              wr_crypto_cmd.no_init         <= rd_crypto_cmd_a(r_idx).no_init;
              wr_crypto_cmd.key             <= rd_crypto_cmd_a(r_idx).key;    
              wr_crypto_cmd.iv              <= rd_crypto_cmd_a(r_idx).iv;    
              wr_crypto_cmd.size            <= rd_crypto_cmd_a(r_idx).size;   
              wr_crypto_cmd.mode            <= rd_crypto_cmd_a(r_idx).mode;   
     
              if r_idx = 0 then
                 wr_crypto_data             <= rd_crypto_data_mt;
              else
                 wr_crypto_data             <= rd_crypto_data_ms;
              end if; 
                 
         when CRY_INT_RSP =>
              wr_crypto_cmd_a(r_idx).rspval <= rd_crypto_cmd.rspval;
end case;

end process;

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state <= IDLE;
              r_idx   <= 0;
     
        elsif ce = '1' then
              r_state <= s_state;
              r_idx   <= s_idx;
        end if;
end if;
end process;

end rtl;

