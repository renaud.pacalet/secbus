--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- direct_data_arbiter.vhd
--
-- sources  mt_ctrl/ms_ctrl
-- dest     vci_mem_ctrl 

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity direct_data_arbiter is
generic(
       NRQ                  : positive := 4
);
port (
       clk                  : in  std_ulogic;
       srstn                : in  std_ulogic;
       ce                   : in  std_ulogic;

       -- 
       rd_direct_data_a     : in  direct_data_array(NRQ - 1 downto 0);

       wr_direct_data       : out data_vector(1 downto 0)
       
);
end direct_data_arbiter;

architecture rtl of direct_data_arbiter is
signal s_data_out : data_vector(1 downto 0); 

begin
----

pWriteMem : process(rd_direct_data_a)
begin

s_data_out <= (others => (others => '0'));
 
for i in 0 to (NRQ - 1) loop
      if rd_direct_data_a(i).enable = '1' then
         s_data_out <= rd_direct_data_a(i).data;
         exit;
      end if;
end loop;

end process;
---
wr_direct_data <= s_data_out;

end rtl;

