--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--


library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;
use global_lib.utils.all;

library axi_lib;
use axi_lib.axi_pkg.all;

library vci_secbus_lib;
use vci_secbus_lib.all;

entity axi_secbus_bridge is
  generic(
    s1_axi_address_and_mask: std_ulogic_vector(31 downto 0) := X"3fffffff"; -- And mask of S1_AXI addresses
    s1_axi_address_or_mask:  std_ulogic_vector(31 downto 0) := X"00000000"; -- Or mask of S1_AXI addresses
    s0_axi_address_width:    natural := 20  -- Number of significant bits in S0_AXI addresses (20 bits => 1MB address space)
  );
  port(
    aclk:       in std_logic;
    aresetn:    in std_logic;
    srst:       in std_logic; -- Soft reset
    --------------------------------
    -- AXI lite slave port s0_axi --
    --------------------------------
    -- Inputs (master to slave) --
    ------------------------------
    -- Read address channel
    s0_axi_araddr:  in  std_logic_vector(s0_axi_address_width - 1 downto 0);
    s0_axi_arprot:  in  std_logic_vector(2 downto 0);
    s0_axi_arvalid: in  std_logic;
    -- Read data channel
    s0_axi_rready:  in  std_logic;
    -- Write address channel
    s0_axi_awaddr:  in  std_logic_vector(s0_axi_address_width - 1 downto 0);
    s0_axi_awprot:  in  std_logic_vector(2 downto 0);
    s0_axi_awvalid: in  std_logic;
    -- Write data channel
    s0_axi_wdata:   in  std_logic_vector(31 downto 0);
    s0_axi_wstrb:   in  std_logic_vector(3 downto 0);
    s0_axi_wvalid:  in  std_logic;
    -- Write response channel
    s0_axi_bready:  in  std_logic;
    -------------------------------
    -- Outputs (slave to master) --
    -------------------------------
    -- Read address channel
    s0_axi_arready: out std_logic;
    -- Read data channel
    s0_axi_rdata:   out std_logic_vector(31 downto 0);
    s0_axi_rresp:   out std_logic_vector(1 downto 0);
    s0_axi_rvalid:  out std_logic;
    -- Write address channel
    s0_axi_awready: out std_logic;
    -- Write data channel
    s0_axi_wready:  out std_logic;
    -- Write response channel
    s0_axi_bvalid:  out std_logic;
    s0_axi_bresp:   out std_logic_vector(1 downto 0);

    ---------------------------
    -- AXI slave port s1_axi --
    ------------------------------
    -- Inputs (master to slave) --
    ------------------------------
    -- Read address channel
    s1_axi_arid:    in  std_logic_vector(5 downto 0);
    s1_axi_araddr:  in  std_logic_vector(31 downto 0);
    s1_axi_arlen:   in  std_logic_vector(3 downto 0);
    s1_axi_arsize:  in  std_logic_vector(2 downto 0);
    s1_axi_arburst: in  std_logic_vector(1 downto 0);
    s1_axi_arlock:  in  std_logic_vector(1 downto 0);
    s1_axi_arcache: in  std_logic_vector(3 downto 0);
    s1_axi_arprot:  in  std_logic_vector(2 downto 0);
    s1_axi_arvalid: in  std_logic;
    -- Read data channel
    s1_axi_rready:  in  std_logic;
    -- Write address channel
    s1_axi_awid:    in  std_logic_vector(5 downto 0);
    s1_axi_awaddr:  in  std_logic_vector(31 downto 0);
    s1_axi_awlen:   in  std_logic_vector(3 downto 0);
    s1_axi_awsize:  in  std_logic_vector(2 downto 0);
    s1_axi_awburst: in  std_logic_vector(1 downto 0);
    s1_axi_awlock:  in  std_logic_vector(1 downto 0);
    s1_axi_awcache: in  std_logic_vector(3 downto 0);
    s1_axi_awprot:  in  std_logic_vector(2 downto 0);
    s1_axi_awvalid: in  std_logic;
    -- Write data channel
    s1_axi_wid:     in  std_logic_vector(5 downto 0);
    s1_axi_wdata:   in  std_logic_vector(31 downto 0);
    s1_axi_wstrb:   in  std_logic_vector(3 downto 0);
    s1_axi_wlast:   in  std_logic;
    s1_axi_wvalid:  in  std_logic;
    -- Write response channel
    s1_axi_bready:  in  std_logic;
    -------------------------------
    -- Outputs (slave to master) --
    -------------------------------
    -- Read address channel
    s1_axi_arready: out std_logic;
    -- Read data channel
    s1_axi_rid:     out std_logic_vector(5 downto 0);
    s1_axi_rdata:   out std_logic_vector(31 downto 0);
    s1_axi_rresp:   out std_logic_vector(1 downto 0);
    s1_axi_rlast:   out std_logic;
    s1_axi_rvalid:  out std_logic;
    -- Write address channel
    s1_axi_awready: out std_logic;
    -- Write data channel
    s1_axi_wready:  out std_logic;
    -- Write response channel
    s1_axi_bvalid:  out std_logic;
    s1_axi_bid:     out std_logic_vector(5 downto 0);
    s1_axi_bresp:   out std_logic_vector(1 downto 0);

    ---------------------------
    -- AXI master port m_axi --
    ---------------------------
    -------------------------------
    -- Outputs (slave to master) --
    -------------------------------
    -- Read address channel
    m_axi_arid:    out std_logic_vector(5 downto 0);
    m_axi_araddr:  out std_logic_vector(31 downto 0);
    m_axi_arlen:   out std_logic_vector(3 downto 0);
    m_axi_arsize:  out std_logic_vector(2 downto 0);
    m_axi_arburst: out std_logic_vector(1 downto 0);
    m_axi_arlock:  out std_logic_vector(1 downto 0);
    m_axi_arcache: out std_logic_vector(3 downto 0);
    m_axi_arprot:  out std_logic_vector(2 downto 0);
    m_axi_arvalid: out std_logic;
    -- Read data channel
    m_axi_rready:  out std_logic;
    -- Write address channel
    m_axi_awid:    out std_logic_vector(5 downto 0);
    m_axi_awaddr:  out std_logic_vector(31 downto 0);
    m_axi_awlen:   out std_logic_vector(3 downto 0);
    m_axi_awsize:  out std_logic_vector(2 downto 0);
    m_axi_awburst: out std_logic_vector(1 downto 0);
    m_axi_awlock:  out std_logic_vector(1 downto 0);
    m_axi_awcache: out std_logic_vector(3 downto 0);
    m_axi_awprot:  out std_logic_vector(2 downto 0);
    m_axi_awvalid: out std_logic;
    -- Write data channel
    m_axi_wid:     out std_logic_vector(5 downto 0);
    m_axi_wdata:   out std_logic_vector(31 downto 0);
    m_axi_wstrb:   out std_logic_vector(3 downto 0);
    m_axi_wlast:   out std_logic;
    m_axi_wvalid:  out std_logic;
    -- Write response channel
    m_axi_bready:  out std_logic;
    ------------------------------
    -- Inputs (slave to master) --
    ------------------------------
    -- Read address channel
    m_axi_arready: in  std_logic;
    -- Read data channel
    m_axi_rid:     in  std_logic_vector(5 downto 0);
    m_axi_rdata:   in  std_logic_vector(31 downto 0);
    m_axi_rresp:   in  std_logic_vector(1 downto 0);
    m_axi_rlast:   in  std_logic;
    m_axi_rvalid:  in  std_logic;
    -- Write address channel
    m_axi_awready: in  std_logic;
    -- Write data channel
    m_axi_wready:  in  std_logic;
    -- Write response channel
    m_axi_bvalid:  in  std_logic;
    m_axi_bid:     in  std_logic_vector(5 downto 0);
    m_axi_bresp:   in  std_logic_vector(1 downto 0);

    -- Interrupts
    irq_c:      out std_logic;
    irq_e:      out std_logic;
    -- GPIO (debug)
    gpi:        in  std_logic_vector(7 downto 0);
    gpo:        out std_logic_vector(7 downto 0)
  );
end entity axi_secbus_bridge;

architecture rtl of axi_secbus_bridge is

  -- Debug registers
  constant num_registers: natural := 6; -- Number of debug registers
  constant l2nr: natural := log2_up(num_registers); -- Log2 of num_registers (rounded towards infinity)

  constant   gpir_idx: natural range 0 to num_registers - 1 := 0;              -- 0x0  : General purpose input register
  constant aw_cnt_idx: natural range 0 to num_registers - 1 := gpir_idx + 1;   -- 0x4  : Debugging, counts # of write address transactions
  constant ar_cnt_idx: natural range 0 to num_registers - 1 := aw_cnt_idx + 1; -- 0x8  : Debugging, counts # of read address transactions
  constant  w_cnt_idx: natural range 0 to num_registers - 1 := ar_cnt_idx + 1; -- 0x10 : Debugging, counts # of write data transactions
  constant  r_cnt_idx: natural range 0 to num_registers - 1 := w_cnt_idx + 1;  -- 0x14 : Debugging, counts # of read data transactions
  constant  b_cnt_idx: natural range 0 to num_registers - 1 := r_cnt_idx + 1;  -- 0x18 : Debugging, counts write responses transactions

  subtype reg_type is unsigned(7 downto 0);
  type reg_array is array(0 to num_registers - 1) of reg_type;
  signal regs: reg_array;

 signal s0_axi_m2s    : axilite_gp_m2s;
 signal s0_axi_s2m    : axilite_gp_s2m;
 signal s1_axi_m2s    : axi_gp_m2s;
 signal s1_axi_s2m    : axi_gp_s2m;
 signal m_axi_m2s     : axi_gp_m2s;
 signal m_axi_s2m     : axi_gp_s2m;
 signal s_irq_c       : std_ulogic;
 signal s_irq_e       : std_ulogic;

  signal aw:        std_ulogic;
  signal ar:        std_ulogic;
  signal w:         std_ulogic;
  signal r:         std_ulogic;
  signal b:         std_ulogic;

  signal srst_sync:     std_ulogic;
  signal resetn_local: std_ulogic;

  -- Registered IRQs. irq_e_r is sticky
  signal irq_c_r, irq_e_r: std_ulogic;

  signal sb_gpo: std_ulogic_vector(7 downto 0);

begin

  resetn_local <= (not srst_sync) and aresetn;

  process(aclk)
    variable tmp: std_ulogic_vector(1 downto 0);
  begin
    if rising_edge(aclk) then
      srst_sync <= tmp(0);
      tmp := srst & tmp(1);
    end if;
  end process;

  irq_c <= s_irq_c;
  irq_e <= s_irq_e;

  i_axi_secbus_wrapper : entity vci_secbus_lib.axi_secbus_wrapper
  port map(
    clk           => aclk,
    srstn         => aresetn,
    ce            => '1',
    p_axi_ini_in  => s1_axi_m2s,
    p_axi_ini_out => s1_axi_s2m,
    p_axi_io_in   => s0_axi_m2s,
    p_axi_io_out  => s0_axi_s2m,
    p_axi_tgt_out => m_axi_m2s,
    p_axi_tgt_in  => m_axi_s2m,
    p_irq_c       => s_irq_c,
    p_irq_e       => s_irq_e,
    gpi           => std_ulogic_vector(gpi),
    gpo           => sb_gpo
  );

  regs_pr: process(aclk)
  begin
    if rising_edge(aclk) then 
      if resetn_local = '0' then
        regs <= (others => (others => '0'));
      else
        regs(gpir_idx) <= unsigned(gpi); -- General purpose inputs
        -- s1_axi transaction counters
        if s1_axi_m2s.arvalid = '1' and s1_axi_s2m.arready = '1' then
          regs(ar_cnt_idx) <= regs(ar_cnt_idx) + 1;
        end if;
        if s1_axi_s2m.rvalid = '1' and s1_axi_m2s.rready = '1' then
          regs(r_cnt_idx) <= regs(r_cnt_idx) + 1;
        end if;
        if s1_axi_m2s.awvalid = '1' and s1_axi_s2m.awready = '1' then
          regs(aw_cnt_idx) <= regs(aw_cnt_idx) + 1;
        end if;
        if s1_axi_m2s.wvalid = '1' and s1_axi_s2m.wready = '1' then
          regs(w_cnt_idx) <= regs(w_cnt_idx) + 1;
        end if;
        if s1_axi_s2m.bvalid = '1' and s1_axi_m2s.bready = '1' then
          regs(b_cnt_idx) <= regs(b_cnt_idx) + 1;
        end if;
      end if;
    end if;
  end process regs_pr;

  aw <= or_reduce(regs(gpir_idx) and regs(aw_cnt_idx));
  ar <= or_reduce(regs(gpir_idx) and regs(ar_cnt_idx));
  w  <= or_reduce(regs(gpir_idx) and regs(w_cnt_idx));
  r  <= or_reduce(regs(gpir_idx) and regs(r_cnt_idx));
  b  <= or_reduce(regs(gpir_idx) and regs(b_cnt_idx));

  -- Interrupt request lines
  process(aclk)
  begin
    if rising_edge(aclk) then
      if resetn_local = '0' then
        irq_c_r <= '0';
        irq_e_r <= '0';
      else
        irq_c_r <= s_irq_c;
        irq_e_r <= irq_e_r or s_irq_e;
      end if;
    end if;
  end process;

  with regs(gpir_idx) select
    gpo <= std_logic_vector(sb_gpo) when X"00",
           aw & w & ar & r & b & '0' & irq_c_r & irq_e_r when others;

  s1_axi_m2s.arid    <= std_ulogic_vector(s1_axi_arid);
  s1_axi_m2s.araddr  <= (std_ulogic_vector(s1_axi_araddr) and s1_axi_address_and_mask) or s1_axi_address_or_mask;
  s1_axi_m2s.arlen   <= std_ulogic_vector(s1_axi_arlen);
  s1_axi_m2s.arsize  <= std_ulogic_vector(s1_axi_arsize);
  s1_axi_m2s.arburst <= std_ulogic_vector(s1_axi_arburst);
  s1_axi_m2s.arlock  <= std_ulogic_vector(s1_axi_arlock);
  s1_axi_m2s.arcache <= std_ulogic_vector(s1_axi_arcache);
  s1_axi_m2s.arprot  <= std_ulogic_vector(s1_axi_arprot);
  s1_axi_m2s.arvalid <= s1_axi_arvalid;

  s1_axi_m2s.rready  <= s1_axi_rready;

  s1_axi_m2s.awid    <= std_ulogic_vector(s1_axi_awid);
  s1_axi_m2s.awaddr  <= (std_ulogic_vector(s1_axi_awaddr) and s1_axi_address_and_mask) or s1_axi_address_or_mask;
  s1_axi_m2s.awlen   <= std_ulogic_vector(s1_axi_awlen);
  s1_axi_m2s.awsize  <= std_ulogic_vector(s1_axi_awsize);
  s1_axi_m2s.awburst <= std_ulogic_vector(s1_axi_awburst);
  s1_axi_m2s.awlock  <= std_ulogic_vector(s1_axi_awlock);
  s1_axi_m2s.awcache <= std_ulogic_vector(s1_axi_awcache);
  s1_axi_m2s.awprot  <= std_ulogic_vector(s1_axi_awprot);
  s1_axi_m2s.awvalid <= s1_axi_awvalid;

  s1_axi_m2s.wid     <= std_ulogic_vector(s1_axi_wid);
  s1_axi_m2s.wdata   <= std_ulogic_vector(s1_axi_wdata);
  s1_axi_m2s.wstrb   <= std_ulogic_vector(s1_axi_wstrb);
  s1_axi_m2s.wlast   <= s1_axi_wlast;
  s1_axi_m2s.wvalid  <= s1_axi_wvalid;

  s1_axi_m2s.bready  <= s1_axi_bready;

  s1_axi_arready     <= s1_axi_s2m.arready;

  s1_axi_rid         <= std_logic_vector(s1_axi_s2m.rid);
  s1_axi_rdata       <= std_logic_vector(s1_axi_s2m.rdata);
  s1_axi_rresp       <= std_logic_vector(s1_axi_s2m.rresp);
  s1_axi_rlast       <= s1_axi_s2m.rlast;
  s1_axi_rvalid      <= s1_axi_s2m.rvalid;

  s1_axi_awready     <= s1_axi_s2m.awready;

  s1_axi_wready      <= s1_axi_s2m.wready;

  s1_axi_bvalid      <= s1_axi_s2m.bvalid;
  s1_axi_bid         <= std_logic_vector(s1_axi_s2m.bid);
  s1_axi_bresp       <= std_logic_vector(s1_axi_s2m.bresp);

  process(s0_axi_araddr)
  begin
    s0_axi_m2s.araddr <= (others => '0');
    s0_axi_m2s.araddr(s0_axi_address_width - 1 downto 0) <= std_ulogic_vector(s0_axi_araddr);
  end process;
  s0_axi_m2s.arprot  <= std_ulogic_vector(s0_axi_arprot);
  s0_axi_m2s.arvalid <= s0_axi_arvalid;

  s0_axi_m2s.rready  <= s0_axi_rready;

  process(s0_axi_awaddr)
  begin
    s0_axi_m2s.awaddr <= (others => '0');
    s0_axi_m2s.awaddr(s0_axi_address_width - 1 downto 0) <= std_ulogic_vector(s0_axi_awaddr);
  end process;
  s0_axi_m2s.awprot  <= std_ulogic_vector(s0_axi_awprot);
  s0_axi_m2s.awvalid <= s0_axi_awvalid;

  s0_axi_m2s.wdata   <= std_ulogic_vector(s0_axi_wdata);
  s0_axi_m2s.wstrb   <= std_ulogic_vector(s0_axi_wstrb);
  s0_axi_m2s.wvalid  <= s0_axi_wvalid;

  s0_axi_m2s.bready  <= s0_axi_bready;

  s0_axi_arready     <= s0_axi_s2m.arready;

  s0_axi_rdata       <= std_logic_vector(s0_axi_s2m.rdata);
  s0_axi_rresp       <= std_logic_vector(s0_axi_s2m.rresp);
  s0_axi_rvalid      <= s0_axi_s2m.rvalid;

  s0_axi_awready     <= s0_axi_s2m.awready;

  s0_axi_wready      <= s0_axi_s2m.wready;

  s0_axi_bvalid      <= s0_axi_s2m.bvalid;
  s0_axi_bresp       <= std_logic_vector(s0_axi_s2m.bresp);

  m_axi_arid         <= std_logic_vector(m_axi_m2s.arid);
  m_axi_araddr       <= std_logic_vector(m_axi_m2s.araddr);
  m_axi_arlen        <= std_logic_vector(m_axi_m2s.arlen);
  m_axi_arsize       <= std_logic_vector(m_axi_m2s.arsize);
  m_axi_arburst      <= std_logic_vector(m_axi_m2s.arburst);
  m_axi_arlock       <= std_logic_vector(m_axi_m2s.arlock);
  m_axi_arcache      <= std_logic_vector(m_axi_m2s.arcache);
  m_axi_arprot       <= std_logic_vector(m_axi_m2s.arprot);
  m_axi_arvalid      <= m_axi_m2s.arvalid;

  m_axi_rready       <= m_axi_m2s.rready;

  m_axi_awid         <= std_logic_vector(m_axi_m2s.awid);
  m_axi_awaddr       <= std_logic_vector(m_axi_m2s.awaddr);
  m_axi_awlen        <= std_logic_vector(m_axi_m2s.awlen);
  m_axi_awsize       <= std_logic_vector(m_axi_m2s.awsize);
  m_axi_awburst      <= std_logic_vector(m_axi_m2s.awburst);
  m_axi_awlock       <= std_logic_vector(m_axi_m2s.awlock);
  m_axi_awcache      <= std_logic_vector(m_axi_m2s.awcache);
  m_axi_awprot       <= std_logic_vector(m_axi_m2s.awprot);
  m_axi_awvalid      <= m_axi_m2s.awvalid;

  m_axi_wid          <= std_logic_vector(m_axi_m2s.wid);
  m_axi_wdata        <= std_logic_vector(m_axi_m2s.wdata);
  m_axi_wstrb        <= std_logic_vector(m_axi_m2s.wstrb);
  m_axi_wlast        <= m_axi_m2s.wlast;
  m_axi_wvalid       <= m_axi_m2s.wvalid;

  m_axi_bready       <= m_axi_m2s.bready;

  m_axi_s2m.arready  <= m_axi_arready;

  m_axi_s2m.rid      <= std_ulogic_vector(m_axi_rid);
  m_axi_s2m.rdata    <= std_ulogic_vector(m_axi_rdata);
  m_axi_s2m.rresp    <= std_ulogic_vector(m_axi_rresp);
  m_axi_s2m.rlast    <= m_axi_rlast;
  m_axi_s2m.rvalid   <= m_axi_rvalid;

  m_axi_s2m.awready  <= m_axi_awready;

  m_axi_s2m.wready   <= m_axi_wready;

  m_axi_s2m.bvalid   <= m_axi_bvalid;
  m_axi_s2m.bid      <= std_ulogic_vector(m_axi_bid);
  m_axi_s2m.bresp    <= std_ulogic_vector(m_axi_bresp);

end architecture rtl;
