--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- Declarations for Xilinx Zynq AXI3 and AXI3 lite PS-PL interfaces 

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

package axi_pkg is

  -------------------------------------------
  -- AXI3 definitions for Xilinx Zynq cores
  -------------------------------------------

  -------------------------------------------
  -- Common to all AXI3 interfaces
  -------------------------------------------
  constant axi_l: positive := 4;  -- len bit width
  constant axi_b: positive := 2;  -- burst bit width
  constant axi_a: positive := 32; -- address bit width
  constant axi_o: positive := 2;  -- lock bit width
  constant axi_p: positive := 3;  -- prot bit width
  constant axi_c: positive := 4;  -- cache bit width
  constant axi_r: positive := 2;  -- resp bit width
  constant axi_s: positive := 3;  -- size bit width

  constant axi_resp_okay:   std_ulogic_vector(axi_r - 1 downto 0) := "00";
  constant axi_resp_exokay: std_ulogic_vector(axi_r - 1 downto 0) := "01";
  constant axi_resp_slverr: std_ulogic_vector(axi_r - 1 downto 0) := "10";
  constant axi_resp_decerr: std_ulogic_vector(axi_r - 1 downto 0) := "11";

  constant axi_burst_fixed: std_ulogic_vector(axi_b - 1 downto 0) := "00";
  constant axi_burst_incr:  std_ulogic_vector(axi_b - 1 downto 0) := "01";
  constant axi_burst_wrap:  std_ulogic_vector(axi_b - 1 downto 0) := "10";
  constant axi_burst_res:   std_ulogic_vector(axi_b - 1 downto 0) := "11";

  constant axi_lock_normal:    std_ulogic_vector(axi_o - 1 downto 0) := "00";
  constant axi_lock_exclusive: std_ulogic_vector(axi_o - 1 downto 0) := "01";
  constant axi_lock_locked:    std_ulogic_vector(axi_o - 1 downto 0) := "10";
  constant axi_lock_res:       std_ulogic_vector(axi_o - 1 downto 0) := "11";

  -------------------------------------------
  -- AXI3 definitions for 32 bits data and 6 bits IDs
  -- Can be used for non-lite AXI_GP{0,1} ports or
  -- AXI_HP{0,1,2,3} ports in 32 bits mode
  -------------------------------------------
  constant axi_gp_i: positive := 6;  -- id  bit width
  constant axi_gp_m: positive := 4;  -- strb bit width
  constant axi_gp_d: positive := 8 * axi_gp_m; -- data bit width
  constant axi_gp_size: std_ulogic_vector(axi_s - 1 downto 0) := std_ulogic_vector(to_unsigned(log2_up(axi_gp_m), axi_s));

  -- AXI_ports. M2S: Master to slave. S2M: Slave to master
  -- One record per direction
  type axi_gp_m2s is record
    -- Read address channel
    arid:    std_ulogic_vector(axi_gp_i - 1 downto 0);
    araddr:  std_ulogic_vector(axi_a - 1 downto 0);
    arlen:   std_ulogic_vector(axi_l - 1 downto 0);
    arsize:  std_ulogic_vector(axi_s - 1 downto 0);
    arburst: std_ulogic_vector(axi_b - 1 downto 0);
    arlock:  std_ulogic_vector(axi_o - 1 downto 0);
    arcache: std_ulogic_vector(axi_c - 1 downto 0);
    arprot:  std_ulogic_vector(axi_p - 1 downto 0);
    arvalid: std_ulogic;
    -- Read data channel
    rready:  std_ulogic;
    -- Write address channel
    awid:    std_ulogic_vector(axi_gp_i - 1 downto 0);
    awaddr:  std_ulogic_vector(axi_a - 1 downto 0);
    awlen:   std_ulogic_vector(axi_l - 1 downto 0);
    awsize:  std_ulogic_vector(axi_s - 1 downto 0);
    awburst: std_ulogic_vector(axi_b - 1 downto 0);
    awlock:  std_ulogic_vector(axi_o - 1 downto 0);
    awcache: std_ulogic_vector(axi_c - 1 downto 0);
    awprot:  std_ulogic_vector(axi_p - 1 downto 0);
    awvalid: std_ulogic;
    -- Write data channel
    wid:     std_ulogic_vector(axi_gp_i - 1 downto 0);
    wdata:   std_ulogic_vector(axi_gp_d - 1 downto 0);
    wstrb:   std_ulogic_vector(axi_gp_m - 1 downto 0);
    wlast:   std_ulogic;
    wvalid:  std_ulogic;
    -- Write response channel
    bready:  std_ulogic;
  end record;

  constant axi_gp_m2s_none: axi_gp_m2s := ( arvalid | rready | awvalid | wvalid | bready => '0', arsize => axi_gp_size, arburst => axi_burst_incr,
    arlock => axi_lock_normal, awsize => axi_gp_size, awburst => axi_burst_incr, awlock => axi_lock_normal, wlast => '1', others => (others => '0'));

  type axi_gp_s2m is record
    -- Read address channel
    arready: std_ulogic;
    -- Read data channel
    rid:     std_ulogic_vector(axi_gp_i - 1 downto 0);
    rdata:   std_ulogic_vector(axi_gp_d - 1 downto 0);
    rresp:   std_ulogic_vector(axi_r - 1 downto 0);
    rlast:   std_ulogic;
    rvalid:  std_ulogic;
    -- Write address channel
    awready: std_ulogic;
    -- Write data channel
    wready:  std_ulogic;
    -- Write response channel
    bid:     std_ulogic_vector(axi_gp_i - 1 downto 0);
    bvalid:  std_ulogic;
    bresp:   std_ulogic_vector(axi_r - 1 downto 0);
  end record;

  constant axi_gp_s2m_none: axi_gp_s2m := ( arready | rvalid | awready | wready | bvalid => '0', rlast => '1', rresp => axi_resp_okay,
    bresp => axi_resp_okay, others => (others => '0'));

  -- One record per channel
  -- Address channels
  type axi_gp_m2s_a is record
    id    : std_ulogic_vector(axi_gp_i - 1 downto 0);
    addr  : std_ulogic_vector(axi_a - 1 downto 0);      
    len   : std_ulogic_vector(axi_l - 1 downto 0);
    size  : std_ulogic_vector(axi_s - 1 downto 0);
    burst : std_ulogic_vector(axi_b - 1 downto 0);
    lock  : std_ulogic_vector(axi_o - 1 downto 0);
    cache : std_ulogic_vector(axi_c - 1 downto 0);
    prot  : std_ulogic_vector(axi_p - 1 downto 0);
    valid : std_ulogic;      
  end record;

  constant axi_gp_m2s_a_none : axi_gp_m2s_a := 
    (id    => (others => '0'), 
     addr  => (others => '0'), 
     len   => (others => '0'), 
     size  => axi_gp_size,
     burst => axi_burst_incr,
     lock  => axi_lock_normal,
     cache => (others => '0'), 
     prot  => (others => '0'), 
     valid => '0');

  -- Data channels
  type axi_gp_s2m_r is record
    id      : std_ulogic_vector(axi_gp_i - 1 downto 0);
    data    : std_ulogic_vector(axi_gp_d - 1 downto 0);
    resp    : std_ulogic_vector(axi_r - 1 downto 0);
    last    : std_ulogic;
    valid   : std_ulogic;      
  end record;

  constant axi_gp_s2m_r_none : axi_gp_s2m_r := 
    (id    => (others => '0'), 
     data  => (others => '0'), 
     resp  => axi_resp_okay,
     last  => '1', 
     valid => '0');
 
  type axi_gp_m2s_w is record
    id      : std_ulogic_vector(axi_gp_i - 1 downto 0);
    data    : std_ulogic_vector(axi_gp_d - 1 downto 0);
    strb    : std_ulogic_vector(axi_gp_m - 1 downto 0);
    last    : std_ulogic;
    valid   : std_ulogic;      
  end record;
  
  constant axi_gp_m2s_w_none : axi_gp_m2s_w := 
    (id    => (others => '0'), 
     data  => (others => '0'), 
     strb  => (others => '0'), 
     last  => '1', 
     valid => '0');

  -- Response channels
  type axi_gp_s2m_b is record
    id      : std_ulogic_vector(axi_gp_i - 1 downto 0);
    resp    : std_ulogic_vector(axi_r - 1 downto 0);
    valid   : std_ulogic;      
  end record;

  constant axi_gp_s2m_b_none : axi_gp_s2m_b :=
    (id    => (others => '0'), 
     resp  => axi_resp_okay,
     valid => '0');
 
  -- All M2S channels
  type axi_gp_m2s_all is record --Master to slave
    ar:     axi_gp_m2s_a; --Read address channel
    aw:     axi_gp_m2s_a; --Write address channel
    rready: std_ulogic;   --Read data channel
    w:      axi_gp_m2s_w; --Write data channel
    bready: std_ulogic;   --Write response channel
  end record;
 
  constant axi_gp_m2s_all_none : axi_gp_m2s_all := 
    (ar      => axi_gp_m2s_a_none,
     aw      => axi_gp_m2s_a_none,
     rready  => '0',
     w       => axi_gp_m2s_w_none,
     bready  => '0');
 
  -- All S2M channels
  type axi_gp_s2m_all is record --Slave to master 
    arready: std_ulogic;   --Read address channel
    awready: std_ulogic;   --Write address channel
    r:       axi_gp_s2m_r; --Read data channel
    wready:  std_ulogic;   --Write data channel
    b:       axi_gp_s2m_b; --Write response channel
  end record;
    
  constant axi_gp_s2m_all_none : axi_gp_s2m_all := 
    (arready => '0',
     awready => '0',
     r       => axi_gp_s2m_r_none,
     wready  => '0',
     b       => axi_gp_s2m_b_none);

  -- Conversions between single and multiple records types
  function axi_gp_m2s_to_axi_gp_m2s_ra(a: axi_gp_m2s) return axi_gp_m2s_a;
  function axi_gp_m2s_to_axi_gp_m2s_wa(a: axi_gp_m2s) return axi_gp_m2s_a;
  function axi_gp_m2s_to_axi_gp_m2s_w(a: axi_gp_m2s) return axi_gp_m2s_w;
  function axi_gp_s2m_to_axi_gp_s2m_r(a: axi_gp_s2m) return axi_gp_s2m_r;
  function axi_gp_s2m_to_axi_gp_s2m_b(a: axi_gp_s2m) return axi_gp_s2m_b;
  function axi_gp_m2s_to_axi_gp_m2s(
    ar:      axi_gp_m2s_a;
    aw:      axi_gp_m2s_a;
    rready:  std_ulogic;
    w:       axi_gp_m2s_w;
    bready:  std_ulogic
  ) return axi_gp_m2s;
  function axi_gp_s2m_to_axi_gp_s2m(
    arready: std_ulogic;
    awready: std_ulogic;
    r:       axi_gp_s2m_r;
    wready:  std_ulogic;
    b:       axi_gp_s2m_b
  ) return axi_gp_s2m;

  -------------------------------------------
  -- AXI lite definitions for 32 bits data
  -- Can be used for AXI_GP{0,1} ports in lite mode
  -------------------------------------------

  -- AXI_GP ports
  type axilite_gp_m2s is record
    -- Read address channel
    araddr:  std_ulogic_vector(axi_a - 1 downto 0);
    arprot:  std_ulogic_vector(axi_p - 1 downto 0);
    arvalid: std_ulogic;
    -- Read data channel
    rready:  std_ulogic;
    -- Write address channel
    awaddr:  std_ulogic_vector(axi_a - 1 downto 0);
    awprot:  std_ulogic_vector(axi_p - 1 downto 0);
    awvalid: std_ulogic;
    -- Write data channel
    wdata:   std_ulogic_vector(axi_gp_d - 1 downto 0);
    wstrb:   std_ulogic_vector(axi_gp_m - 1 downto 0);
    wvalid:  std_ulogic;
    -- Write response channel
    bready:  std_ulogic;
  end record;

  constant axilite_gp_m2s_none: axilite_gp_m2s := ( arvalid | rready | awvalid | wvalid | bready => '0', others => (others => '0'));

  type axilite_gp_s2m is record
    -- Read address channel
    arready: std_ulogic;
    -- Read data channel
    rdata:   std_ulogic_vector(axi_gp_d - 1 downto 0);
    rresp:   std_ulogic_vector(axi_r - 1 downto 0);
    rvalid:  std_ulogic;
    -- Write address channel
    awready: std_ulogic;
    -- Write data channel
    wready:  std_ulogic;
    -- Write response channel
    bvalid:  std_ulogic;
    bresp:   std_ulogic_vector(axi_r - 1 downto 0);
  end record;

  constant axilite_gp_s2m_none: axilite_gp_s2m := ( arready | rvalid | awready | wready | bvalid => '0', others => (others => '0'));

  -- One record per channel
  -- Address channels
  type axilite_gp_m2s_a is record
    addr  : std_ulogic_vector(axi_a - 1 downto 0);      
    prot  : std_ulogic_vector(axi_p - 1 downto 0);
    valid : std_ulogic;      
  end record;

  constant axilite_gp_m2s_a_none : axilite_gp_m2s_a := 
    (addr  => (others => '0'), 
     prot  => (others => '0'), 
     valid => '0');

  -- Data channels
  type axilite_gp_s2m_r is record
    data    : std_ulogic_vector(axi_gp_d - 1 downto 0);
    resp    : std_ulogic_vector(axi_r - 1 downto 0);
    valid   : std_ulogic;      
  end record;

  constant axilite_gp_s2m_r_none : axilite_gp_s2m_r := 
    (data  => (others => '0'), 
     resp  => axi_resp_okay,
     valid => '0');
 
  type axilite_gp_m2s_w is record
    data    : std_ulogic_vector(axi_gp_d - 1 downto 0);
    strb    : std_ulogic_vector(axi_gp_m - 1 downto 0);
    valid   : std_ulogic;      
  end record;
  
  constant axilite_gp_m2s_w_none : axilite_gp_m2s_w := 
    (data  => (others => '0'), 
     strb  => (others => '0'), 
     valid => '0');

  -- Response channels
  type axilite_gp_s2m_b is record
    resp    : std_ulogic_vector(axi_r - 1 downto 0);
    valid   : std_ulogic;      
  end record;

  constant axilite_gp_s2m_b_none : axilite_gp_s2m_b :=
    (resp  => axi_resp_okay,
     valid => '0');
 
  -- All M2S channels
  type axilite_gp_m2s_all is record --Master to slave
    ar:     axilite_gp_m2s_a; --Read address channel
    aw:     axilite_gp_m2s_a; --Write address channel
    rready: std_ulogic;       --Read data channel
    w:      axilite_gp_m2s_w; --Write data channel
    bready: std_ulogic;       --Write response channel
  end record;
 
  constant axilite_gp_m2s_all_none : axilite_gp_m2s_all := 
    (ar      => axilite_gp_m2s_a_none,
     aw      => axilite_gp_m2s_a_none,
     rready  => '0',
     w       => axilite_gp_m2s_w_none,
     bready  => '0');
 
  -- All S2M channels
  type axilite_gp_s2m_all is record --Slave to master 
    arready: std_ulogic;       --Read address channel
    awready: std_ulogic;       --Write address channel
    r:       axilite_gp_s2m_r; --Read data channel
    wready:  std_ulogic;       --Write data channel
    b:       axilite_gp_s2m_b; --Write response channel
  end record;
    
  constant axilite_gp_s2m_all_none : axilite_gp_s2m_all := 
    (arready => '0',
     awready => '0',
     r       => axilite_gp_s2m_r_none,
     wready  => '0',
     b       => axilite_gp_s2m_b_none);

  -- Conversions between single and multiple records types
  function axilite_gp_m2s_to_axilite_gp_m2s_ra(a: axilite_gp_m2s) return axilite_gp_m2s_a;
  function axilite_gp_m2s_to_axilite_gp_m2s_wa(a: axilite_gp_m2s) return axilite_gp_m2s_a;
  function axilite_gp_m2s_to_axilite_gp_m2s_w(a: axilite_gp_m2s) return axilite_gp_m2s_w;
  function axilite_gp_s2m_to_axilite_gp_s2m_r(a: axilite_gp_s2m) return axilite_gp_s2m_r;
  function axilite_gp_s2m_to_axilite_gp_s2m_b(a: axilite_gp_s2m) return axilite_gp_s2m_b;
  function axilite_gp_m2s_to_axilite_gp_m2s(
    ar:      axilite_gp_m2s_a;
    aw:      axilite_gp_m2s_a;
    rready:  std_ulogic;
    w:       axilite_gp_m2s_w;
    bready:  std_ulogic
  ) return axilite_gp_m2s;
  function axilite_gp_s2m_to_axilite_gp_s2m(
    arready: std_ulogic;
    awready: std_ulogic;
    r:       axilite_gp_s2m_r;
    wready:  std_ulogic;
    b:       axilite_gp_s2m_b
  ) return axilite_gp_s2m;

  -------------------------------------------
  -- AXI definitions for 64 bits data and 3 bits IDs
  -- Can be used for AXI ACP port
  -------------------------------------------

  constant axi_acp_i: positive := 3;  -- id  bit width
  constant axi_acp_u: positive := 5;  -- user bit width
  constant axi_acp_m: positive := 8;  -- strb bit width
  constant axi_acp_d: positive := 8 * axi_acp_m; -- data bit width

  -- AXI_ACP ports
  type axi_acp_m2s is record
    -- Read address channel
    arid:    std_ulogic_vector(axi_acp_i - 1 downto 0);
    araddr:  std_ulogic_vector(axi_a - 1 downto 0);
    arlen:   std_ulogic_vector(axi_l - 1 downto 0);
    arsize:  std_ulogic_vector(axi_s - 1 downto 0);
    arburst: std_ulogic_vector(axi_b - 1 downto 0);
    arlock:  std_ulogic_vector(axi_o - 1 downto 0);
    arcache: std_ulogic_vector(axi_c - 1 downto 0);
    arprot:  std_ulogic_vector(axi_p - 1 downto 0);
    aruser:  std_ulogic_vector(axi_acp_u - 1 downto 0);
    arvalid: std_ulogic;
    -- Read data channel
    rready:  std_ulogic;
    -- Write address channel
    awid:    std_ulogic_vector(axi_acp_i - 1 downto 0);
    awaddr:  std_ulogic_vector(axi_a - 1 downto 0);
    awlen:   std_ulogic_vector(axi_l - 1 downto 0);
    awsize:  std_ulogic_vector(axi_s - 1 downto 0);
    awburst: std_ulogic_vector(axi_b - 1 downto 0);
    awlock:  std_ulogic_vector(axi_o - 1 downto 0);
    awcache: std_ulogic_vector(axi_c - 1 downto 0);
    awprot:  std_ulogic_vector(axi_p - 1 downto 0);
    awuser:  std_ulogic_vector(axi_acp_u - 1 downto 0);
    awvalid: std_ulogic;
    -- Write data channel
    wid:     std_ulogic_vector(axi_acp_i - 1 downto 0);
    wdata:   std_ulogic_vector(axi_acp_d - 1 downto 0);
    wstrb:   std_ulogic_vector(axi_acp_m - 1 downto 0);
    wlast:   std_ulogic;
    wvalid:  std_ulogic;
    -- Write response channel
    bready:  std_ulogic;
  end record;

  constant axi_acp_m2s_none: axi_gp_m2s := ( arvalid | rready | awvalid | wlast | wvalid | bready => '0', others => (others => '0'));

  type axi_acp_s2m is record
    -- Read address channel
    arready: std_ulogic;
    -- Read data channel
    rid:     std_ulogic_vector(axi_acp_i - 1 downto 0);
    rdata:   std_ulogic_vector(axi_acp_d - 1 downto 0);
    rresp:   std_ulogic_vector(axi_r - 1 downto 0);
    rlast:   std_ulogic;
    rvalid:  std_ulogic;
    -- Write address channel
    awready: std_ulogic;
    -- Write data channel
    wready:  std_ulogic;
    -- Write response channel
    bid:     std_ulogic_vector(axi_acp_i - 1 downto 0);
    bvalid:  std_ulogic;
    bresp:   std_ulogic_vector(axi_r - 1 downto 0);
  end record;

  constant axi_acp_s2m_none: axi_gp_s2m := ( arready | rlast | rvalid | awready | wready | bvalid => '0', others => (others => '0'));

end package axi_pkg;

package body axi_pkg is

  function axi_gp_m2s_to_axi_gp_m2s_ra(a: axi_gp_m2s) return axi_gp_m2s_a is
  begin
    return (
      id    => a.arid,
      addr  => a.araddr,
      len   => a.arlen,
      size  => a.arsize,
      burst => a.arburst,
      lock  => a.arlock,
      cache => a.arcache,
      prot  => a.arprot,
      valid => a.arvalid
    );
  end function axi_gp_m2s_to_axi_gp_m2s_ra;

  function axi_gp_m2s_to_axi_gp_m2s_wa(a: axi_gp_m2s) return axi_gp_m2s_a is
  begin
    return (
      id    => a.awid,
      addr  => a.awaddr,
      len   => a.awlen,
      size  => a.awsize,
      burst => a.awburst,
      lock  => a.awlock,
      cache => a.awcache,
      prot  => a.awprot,
      valid => a.awvalid
    );
  end function axi_gp_m2s_to_axi_gp_m2s_wa;

  function axi_gp_m2s_to_axi_gp_m2s_w(a: axi_gp_m2s) return axi_gp_m2s_w is
  begin
    return (
      id    => a.wid,
      data  => a.wdata,
      strb  => a.wstrb,
      last  => a.wlast,
      valid => a.wvalid
    );
  end function axi_gp_m2s_to_axi_gp_m2s_w;

  function axi_gp_s2m_to_axi_gp_s2m_r(a: axi_gp_s2m) return axi_gp_s2m_r is
  begin
    return (
      id    => a.rid,
      data  => a.rdata,
      resp  => a.rresp,
      last  => a.rlast,
      valid => a.rvalid
    );
  end function axi_gp_s2m_to_axi_gp_s2m_r;

  function axi_gp_s2m_to_axi_gp_s2m_b(a: axi_gp_s2m) return axi_gp_s2m_b is
  begin
    return (
      id    => a.bid,
      resp  => a.bresp,
      valid => a.bvalid
    );
  end function axi_gp_s2m_to_axi_gp_s2m_b;

  function axi_gp_m2s_to_axi_gp_m2s(
    ar:      axi_gp_m2s_a;
    aw:      axi_gp_m2s_a;
    rready:  std_ulogic;
    w:       axi_gp_m2s_w;
    bready:  std_ulogic
  ) return axi_gp_m2s is
  begin
    return (
      arid    => ar.id,
      araddr  => ar.addr,
      arlen   => ar.len,
      arsize  => ar.size,
      arburst => ar.burst,
      arlock  => ar.lock,
      arcache => ar.cache,
      arprot  => ar.prot,
      arvalid => ar.valid,
      rready  => rready,
      awid    => aw.id,
      awaddr  => aw.addr,
      awlen   => aw.len,
      awsize  => aw.size,
      awburst => aw.burst,
      awlock  => aw.lock,
      awcache => aw.cache,
      awprot  => aw.prot,
      awvalid => aw.valid,
      wid     => w.id,
      wdata   => w.data,
      wstrb   => w.strb,
      wlast   => w.last,
      wvalid  => w.valid,
      bready  => bready
    );
  end function axi_gp_m2s_to_axi_gp_m2s;

  function axi_gp_s2m_to_axi_gp_s2m(
    arready: std_ulogic;
    awready: std_ulogic;
    r:       axi_gp_s2m_r;
    wready:  std_ulogic;
    b:       axi_gp_s2m_b
  ) return axi_gp_s2m is
  begin
    return (
      arready => arready,
      rid     => r.id,
      rdata   => r.data,
      rresp   => r.resp,
      rlast   => r.last,
      rvalid  => r.valid,
      awready => awready,
      wready  => wready,
      bid     => b.id,
      bvalid  => b.valid,
      bresp   => b.resp
    );
  end function axi_gp_s2m_to_axi_gp_s2m;

  function axilite_gp_m2s_to_axilite_gp_m2s_ra(a: axilite_gp_m2s) return axilite_gp_m2s_a is
  begin
    return (
      addr  => a.araddr,
      prot  => a.arprot,
      valid => a.arvalid
    );
  end function axilite_gp_m2s_to_axilite_gp_m2s_ra;

  function axilite_gp_m2s_to_axilite_gp_m2s_wa(a: axilite_gp_m2s) return axilite_gp_m2s_a is
  begin
    return (
      addr  => a.awaddr,
      prot  => a.awprot,
      valid => a.awvalid
    );
  end function axilite_gp_m2s_to_axilite_gp_m2s_wa;

  function axilite_gp_m2s_to_axilite_gp_m2s_w(a: axilite_gp_m2s) return axilite_gp_m2s_w is
  begin
    return (
      data  => a.wdata,
      strb  => a.wstrb,
      valid => a.wvalid
    );
  end function axilite_gp_m2s_to_axilite_gp_m2s_w;

  function axilite_gp_s2m_to_axilite_gp_s2m_r(a: axilite_gp_s2m) return axilite_gp_s2m_r is
  begin
    return (
      data  => a.rdata,
      resp  => a.rresp,
      valid => a.rvalid
    );
  end function axilite_gp_s2m_to_axilite_gp_s2m_r;

  function axilite_gp_s2m_to_axilite_gp_s2m_b(a: axilite_gp_s2m) return axilite_gp_s2m_b is
  begin
    return (
      resp  => a.bresp,
      valid => a.bvalid
    );
  end function axilite_gp_s2m_to_axilite_gp_s2m_b;

  function axilite_gp_m2s_to_axilite_gp_m2s(
    ar:      axilite_gp_m2s_a;
    aw:      axilite_gp_m2s_a;
    rready:  std_ulogic;
    w:       axilite_gp_m2s_w;
    bready:  std_ulogic
  ) return axilite_gp_m2s is
  begin
    return (
      araddr  => ar.addr,
      arprot  => ar.prot,
      arvalid => ar.valid,
      rready  => rready,
      awaddr  => aw.addr,
      awprot  => aw.prot,
      awvalid => aw.valid,
      wdata   => w.data,
      wstrb   => w.strb,
      wvalid  => w.valid,
      bready  => bready
    );
  end function axilite_gp_m2s_to_axilite_gp_m2s;

  function axilite_gp_s2m_to_axilite_gp_s2m(
    arready: std_ulogic;
    awready: std_ulogic;
    r:       axilite_gp_s2m_r;
    wready:  std_ulogic;
    b:       axilite_gp_s2m_b
  ) return axilite_gp_s2m is
  begin
    return (
      arready => arready,
      rdata   => r.data,
      rresp   => r.resp,
      rvalid  => r.valid,
      awready => awready,
      wready  => wready,
      bvalid  => b.valid,
      bresp   => b.resp
    );
  end function axilite_gp_s2m_to_axilite_gp_s2m;

end package body axi_pkg;
