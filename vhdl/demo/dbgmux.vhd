--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- DBGMUX multiplexes N B-bits sources, indexed from 0 to N-1, and sends the selected one to its destination B-bits output. The selection uses a press button
-- (BTN). At reset source #0 is selected. When BTN is pressed, the index of the selected source is incremented (and wraps around N) and sent to the output. When
-- BTN is released the new selected source is sent to the output. Note: BTN is re-synchronized but not debounced.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dbgmux is
  port(
    aclk:    in  std_logic; -- Master clock
    aresetn: in  std_logic; -- Master reset
    srst:    in  std_logic; -- Soft reset
    btn:     in  std_logic; -- Press button
    src0:    in  std_logic_vector(7 downto 0);
    src1:    in  std_logic_vector(7 downto 0);
    src2:    in  std_logic_vector(7 downto 0);
    src3:    in  std_logic_vector(7 downto 0);
    dst:     out std_logic_vector(7 downto 0)
  );
  constant n: positive := 4;
  constant b: positive := 8;
end entity dbgmux;

architecture rtl of dbgmux is

  subtype word is std_logic_vector(b - 1 downto 0);
  type wordarray is array(natural range 0 to n - 1) of word;

  signal idx:        unsigned(b - 1 downto 0);
  signal btn_resync: std_logic_vector(2 downto 0);
  signal srcs:       wordarray;

begin

  srcs <= (src0, src1, src2, src3);

  process(btn_resync, idx, srcs)
  begin
    dst <= std_logic_vector(idx);
    if btn_resync(1 downto 0) /= "11" then
      for i in 0 to n - 1 loop
        if idx = i then
          dst <= srcs(i);
        end if;
      end loop;
    end if;
  end process;

  process(aclk)
  begin
    if rising_edge(aclk) then
      if aresetn = '0' or srst = '1' then
        idx        <= (others => '0');
        btn_resync <= (others => '0');
      else
        if btn_resync(1 downto 0) = "10" then
          if idx = n - 1 then
            idx <= (others => '0');
          else
            idx <= idx + 1;
          end if;
        end if;
        btn_resync <= btn & btn_resync(2 downto 1);
      end if;
    end if;
  end process;

end architecture rtl;

