--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--


library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;

library axi_lib;
use axi_lib.axi_pkg.all;

entity axi_monitor is
  generic(
    cpu_axi_address_high:   natural := 29;
    ctrl_axi_address_width: natural := 13  -- Number of significant bits in ctrl_axi addresses
  );
  port(
    aclk:       in std_logic;
    aresetn:    in std_logic;
    srst:       in std_logic; -- Soft reset
    --------------------------------
    -- AXI lite slave port ctrl_axi --
    --------------------------------
    -- Inputs (master to slave) --
    ------------------------------
    -- Read address channel
    ctrl_axi_araddr:  in  std_logic_vector(ctrl_axi_address_width - 1 downto 0);
    ctrl_axi_arprot:  in  std_logic_vector(2 downto 0);
    ctrl_axi_arvalid: in  std_logic;
    -- Read data channel
    ctrl_axi_rready:  in  std_logic;
    -- Write address channel
    ctrl_axi_awaddr:  in  std_logic_vector(ctrl_axi_address_width - 1 downto 0);
    ctrl_axi_awprot:  in  std_logic_vector(2 downto 0);
    ctrl_axi_awvalid: in  std_logic;
    -- Write data channel
    ctrl_axi_wdata:   in  std_logic_vector(31 downto 0);
    ctrl_axi_wstrb:   in  std_logic_vector(3 downto 0);
    ctrl_axi_wvalid:  in  std_logic;
    -- Write response channel
    ctrl_axi_bready:  in  std_logic;
    -------------------------------
    -- Outputs (slave to master) --
    -------------------------------
    -- Read address channel
    ctrl_axi_arready: out std_logic;
    -- Read data channel
    ctrl_axi_rdata:   out std_logic_vector(31 downto 0);
    ctrl_axi_rresp:   out std_logic_vector(1 downto 0);
    ctrl_axi_rvalid:  out std_logic;
    -- Write address channel
    ctrl_axi_awready: out std_logic;
    -- Write data channel
    ctrl_axi_wready:  out std_logic;
    -- Write response channel
    ctrl_axi_bvalid:  out std_logic;
    ctrl_axi_bresp:   out std_logic_vector(1 downto 0);

    ---------------------------
    -- AXI slave port cpu_axi --
    ------------------------------
    -- Inputs (master to slave) --
    ------------------------------
    -- Read address channel
    cpu_axi_araddr:  in  std_logic_vector(31 downto 0);
    cpu_axi_arvalid: in  std_logic;
    -- Write address channel
    cpu_axi_awaddr:  in  std_logic_vector(31 downto 0);
    cpu_axi_awvalid: in  std_logic;
    -------------------------------
    -- Outputs (slave to master) --
    -------------------------------
    -- Read address channel
    cpu_axi_arready: in  std_logic;
    -- Write address channel
    cpu_axi_awready: in  std_logic;
    -- GPIO (debug)
    gpo:             out std_logic_vector(7 downto 0)
  );
end entity axi_monitor;

architecture rtl of axi_monitor is

  -- Part of CPU AXI addresses to monitor is cpu_axi_address_high downto cpu_axi_address_low, that is:
  --   cpu_axi_address_high - cpu_axi_address_low + 1 bits
  --   2**(cpu_axi_address_high - cpu_axi_address_low + 1) different addresses
  --   2 bits per monitored addresses (one for read one for write)
  --   mem capacity = 2**(cpu_axi_address_high - cpu_axi_address_low + 2) bits =
  --     2**(cpu_axi_address_high - cpu_axi_address_low - 1) bytes =
  --     2**(cpu_axi_address_high - cpu_axi_address_low - 3) 32-bits words =
  --     2**ctrl_axi_address_width
  --   So:  ctrl_axi_address_width = cpu_axi_address_high - cpu_axi_address_low - 1
  --   And: cpu_axi_address_low = cpu_axi_address_high - ctrl_axi_address_width - 1
  constant cpu_axi_address_low: natural := cpu_axi_address_high - ctrl_axi_address_width - 1;

  -- Synchronized version of srst (warning: there is no debouncer, one may be needed)
  signal srst_sync:     std_ulogic;

  -- Local reset (combination of srst and aresetn)
  signal resetn_local: std_ulogic;

  -- Bit-width of mem address bus (addressing units = 32-bits words)
  constant mem_address_width: natural := ctrl_axi_address_width - 2;

  signal mem_raddr: std_ulogic_vector(mem_address_width - 1 downto 0);
  signal mem_rdata: std_ulogic_vector(31 downto 0);
  signal mem_ren: std_ulogic;

begin

  resetn_local <= (not srst_sync) and aresetn;

  -- srst re-synchronizer
  process(aclk)
    variable tmp: std_ulogic_vector(1 downto 0);
  begin
    if rising_edge(aclk) then
      srst_sync <= tmp(0);
      tmp := srst & tmp(1);
    end if;
  end process;

  ctrl_axi_rresp <= std_logic_vector(axi_resp_okay);
  ctrl_axi_bresp <= std_logic_vector(axi_resp_slverr);

  -- Read process on CTRL AXI interface
  process(aclk)
    type state_t is (idle, pipe1, pipe2, pipe3, pipe4);
    variable state: state_t;
  begin
    if rising_edge(aclk) then
      if resetn_local = '0' then
        state := idle;
        ctrl_axi_arready <= '1';
        ctrl_axi_rvalid <= '0';
        ctrl_axi_rdata <= (others => '0');
        mem_ren <= '0';
      else
        case state is
          when idle =>
            if ctrl_axi_arvalid = '1' then
              ctrl_axi_arready <= '0';
              mem_raddr <= std_ulogic_vector(ctrl_axi_araddr(ctrl_axi_address_width - 1 downto 2));
              mem_ren <= '1';
              state := pipe1;
            end if;
          when pipe1 =>
            mem_ren <= '0';
            state := pipe2;
          when pipe2 =>
            state := pipe3;
          when pipe3 =>
            ctrl_axi_rvalid <= '1';
            ctrl_axi_rdata <= std_logic_vector(mem_rdata);
            state := pipe4;
          when pipe4 =>
            if ctrl_axi_rready = '1' then
              ctrl_axi_rvalid <= '0';
              ctrl_axi_arready <= '1';
              state := idle;
            end if;
        end case;
      end if;
    end if;
  end process;

  -- Write process on CTRL AXI interface
  process(aclk)
    type state_t is (idle, pipe1, pipe2);
    variable state: state_t;
  begin
    if rising_edge(aclk) then
      if resetn_local = '0' then
        state := idle;
        ctrl_axi_awready <= '1';
        ctrl_axi_wready <= '0';
        ctrl_axi_bvalid <= '0';
      else
        case state is
          when idle =>
            if ctrl_axi_awvalid = '1' then
              ctrl_axi_awready <= '0';
              ctrl_axi_wready <= '1';
              state := pipe1;
            end if;
          when pipe1 =>
            if ctrl_axi_wvalid = '1' then
              ctrl_axi_wready <= '0';
              ctrl_axi_bvalid <= '1';
              state := pipe2;
            end if;
          when pipe2 =>
            if ctrl_axi_bready = '1' then
              ctrl_axi_bvalid <= '0';
              ctrl_axi_awready <= '1';
              state := idle;
            end if;
        end case;
      end if;
    end if;
  end process;

  process(aclk)
    subtype word is std_ulogic_vector(31 downto 0);
    type word_array is array(natural range <>) of word;
    variable addr1: natural range 0 to 2**mem_address_width - 1;
    variable en1: boolean;
    variable memr: word_array(0 to 2**(mem_address_width - 1) - 1);
    variable memw: word_array(0 to 2**(mem_address_width - 1) - 1);
    variable init: natural range 0 to 2**(mem_address_width - 1);
    variable cpu_addr: natural range 0 to 2**(cpu_axi_address_high - cpu_axi_address_low + 1) - 1;
  begin
    if rising_edge(aclk) then
      if resetn_local = '0' then
        en1 := false;
        init := 0;
        gpo <= (others => '0');
      elsif init < 2**(mem_address_width - 1) then
        memr(init) := (others => '0');
        memw(init) := (others => '0');
        init := init + 1;
      else
        gpo <= (others => '1');
        -- Read
        if en1 then
          if addr1 < 2**(mem_address_width - 1) then
            mem_rdata <= memr(addr1);
          else
            mem_rdata <= memw(addr1 - 2**(mem_address_width - 1));
          end if;
        end if;
        if mem_ren = '1' then
          en1 := true;
          addr1 := to_integer(unsigned(mem_raddr));
        else
          en1 := false;
        end if;
        -- Write
        if cpu_axi_arvalid = '1' and cpu_axi_arready = '1' then
          cpu_addr := to_integer(unsigned(cpu_axi_araddr(cpu_axi_address_high downto cpu_axi_address_low)));
          memr(cpu_addr / 32)(cpu_addr mod 32) := '1';
        end if;
        if cpu_axi_awvalid = '1' and cpu_axi_awready = '1' then
          cpu_addr := to_integer(unsigned(cpu_axi_awaddr(cpu_axi_address_high downto cpu_axi_address_low)));
          memw(cpu_addr / 32)(cpu_addr mod 32) := '1';
        end if;
      end if;
    end if;
  end process;

end architecture rtl;
