--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--


library ieee;
use ieee.std_logic_1164.all;

library global_lib;

library axi_secbus_bridge_lib;

library uart2maxilite_lib;
use uart2maxilite_lib.uart2maxilite_pkg.all;

entity demo is
  generic(
    asb_cpu_axi_address_and_mask: std_ulogic_vector(31 downto 0) := X"3fffffff"; -- And mask of asb_cpu_axi addresses
    asb_cpu_axi_address_or_mask:  std_ulogic_vector(31 downto 0) := X"00000000"; -- Or mask of asb_cpu_axi addresses
    asb_ctrl_axi_address_width:   natural := 20; -- Number of significant bits in asb_ctrl_axi addresses (20 bits => 1MB address space)
    am_cpu_axi_address_high:      natural := 29; -- Most significant bit of GP0_AXI addresses that AXI monitor shall monitor
    am_ctrl_axi_address_width:    natural := 13  -- Number of significant bits in am_ctrl_axi addresses
  );
  port(
    -----------------
    -- System I/Os --
    -----------------
    aclk:       in  std_logic; -- Master clock
    aresetn:    in  std_logic; -- Active low synchronous system reset
    srst:       in  std_logic; -- Active high synchronous soft reset (centre press button)

    ------------------------
    -- Interrupt requests --
    ------------------------
    irq_c:      out std_logic; -- End of atomic operation interrupt request
    irq_e:      out std_logic; -- Integrity violation interrupt request

    ---------------
    -- Debugging --
    ---------------
    btn:        in  std_logic; -- Debugging output selector (north press button)
    gpi:        in  std_logic_vector(7 downto 0); -- General purpose inputs (slide switches)
    gpo:        out std_logic_vector(7 downto 0); -- General purpose outputs (LEDs)
    rx:         in  std_logic; -- UART RX line
    tx:         in  std_logic; -- UART TX line

    ----------------------------------
    -- AXI SecBus Bridge (ASB) I/Os --
    ----------------------------------
    -- AXI lite slave port (control, GP1_AXI)
    --   Inputs (master to slave)
    --     Read address channel
    asb_ctrl_axi_araddr:  in  std_logic_vector(asb_ctrl_axi_address_width - 1 downto 0);
    asb_ctrl_axi_arprot:  in  std_logic_vector(2 downto 0);
    asb_ctrl_axi_arvalid: in  std_logic;
    --     Read data channel
    asb_ctrl_axi_rready:  in  std_logic;
    --     Write address channel
    asb_ctrl_axi_awaddr:  in  std_logic_vector(asb_ctrl_axi_address_width - 1 downto 0);
    asb_ctrl_axi_awprot:  in  std_logic_vector(2 downto 0);
    asb_ctrl_axi_awvalid: in  std_logic;
    --     Write data channel
    asb_ctrl_axi_wdata:   in  std_logic_vector(31 downto 0);
    asb_ctrl_axi_wstrb:   in  std_logic_vector(3 downto 0);
    asb_ctrl_axi_wvalid:  in  std_logic;
    --     Write response channel
    asb_ctrl_axi_bready:  in  std_logic;
    --   Outputs (slave to master)
    --     Read address channel
    asb_ctrl_axi_arready: out std_logic;
    --     Read data channel
    asb_ctrl_axi_rdata:   out std_logic_vector(31 downto 0);
    asb_ctrl_axi_rresp:   out std_logic_vector(1 downto 0);
    asb_ctrl_axi_rvalid:  out std_logic;
    --     Write address channel
    asb_ctrl_axi_awready: out std_logic;
    --     Write data channel
    asb_ctrl_axi_wready:  out std_logic;
    --     Write response channel
    asb_ctrl_axi_bvalid:  out std_logic;
    asb_ctrl_axi_bresp:   out std_logic_vector(1 downto 0);

    -- AXI slave port (memory access, GP0_AXI)
    --   Inputs (master to slave)
    --     Read address channel
    asb_cpu_axi_arid:    in  std_logic_vector(5 downto 0);
    asb_cpu_axi_araddr:  in  std_logic_vector(31 downto 0);
    asb_cpu_axi_arlen:   in  std_logic_vector(3 downto 0);
    asb_cpu_axi_arsize:  in  std_logic_vector(2 downto 0);
    asb_cpu_axi_arburst: in  std_logic_vector(1 downto 0);
    asb_cpu_axi_arlock:  in  std_logic_vector(1 downto 0);
    asb_cpu_axi_arcache: in  std_logic_vector(3 downto 0);
    asb_cpu_axi_arprot:  in  std_logic_vector(2 downto 0);
    asb_cpu_axi_arvalid: in  std_logic;
    --     Read data channel
    asb_cpu_axi_rready:  in  std_logic;
    --     Write address channel
    asb_cpu_axi_awid:    in  std_logic_vector(5 downto 0);
    asb_cpu_axi_awaddr:  in  std_logic_vector(31 downto 0);
    asb_cpu_axi_awlen:   in  std_logic_vector(3 downto 0);
    asb_cpu_axi_awsize:  in  std_logic_vector(2 downto 0);
    asb_cpu_axi_awburst: in  std_logic_vector(1 downto 0);
    asb_cpu_axi_awlock:  in  std_logic_vector(1 downto 0);
    asb_cpu_axi_awcache: in  std_logic_vector(3 downto 0);
    asb_cpu_axi_awprot:  in  std_logic_vector(2 downto 0);
    asb_cpu_axi_awvalid: in  std_logic;
    --     Write data channel
    asb_cpu_axi_wid:     in  std_logic_vector(5 downto 0);
    asb_cpu_axi_wdata:   in  std_logic_vector(31 downto 0);
    asb_cpu_axi_wstrb:   in  std_logic_vector(3 downto 0);
    asb_cpu_axi_wlast:   in  std_logic;
    asb_cpu_axi_wvalid:  in  std_logic;
    --     Write response channel
    asb_cpu_axi_bready:  in  std_logic;
    --   Outputs (slave to master)
    --     Read address channel
    asb_cpu_axi_arready: out std_logic;
    --     Read data channel
    asb_cpu_axi_rid:     out std_logic_vector(5 downto 0);
    asb_cpu_axi_rdata:   out std_logic_vector(31 downto 0);
    asb_cpu_axi_rresp:   out std_logic_vector(1 downto 0);
    asb_cpu_axi_rlast:   out std_logic;
    asb_cpu_axi_rvalid:  out std_logic;
    --     Write address channel
    asb_cpu_axi_awready: out std_logic;
    --     Write data channel
    asb_cpu_axi_wready:  out std_logic;
    --     Write response channel
    asb_cpu_axi_bvalid:  out std_logic;
    asb_cpu_axi_bid:     out std_logic_vector(5 downto 0);
    asb_cpu_axi_bresp:   out std_logic_vector(1 downto 0);

    -- AXI master port (memory access, HP0_AXI)
    --   Outputs (slave to master)
    --     Read address channel
    asb_mem_axi_arid:    out std_logic_vector(5 downto 0);
    asb_mem_axi_araddr:  out std_logic_vector(31 downto 0);
    asb_mem_axi_arlen:   out std_logic_vector(3 downto 0);
    asb_mem_axi_arsize:  out std_logic_vector(2 downto 0);
    asb_mem_axi_arburst: out std_logic_vector(1 downto 0);
    asb_mem_axi_arlock:  out std_logic_vector(1 downto 0);
    asb_mem_axi_arcache: out std_logic_vector(3 downto 0);
    asb_mem_axi_arprot:  out std_logic_vector(2 downto 0);
    asb_mem_axi_arvalid: out std_logic;
    --     Read data channel
    asb_mem_axi_rready:  out std_logic;
    --     Write address channel
    asb_mem_axi_awid:    out std_logic_vector(5 downto 0);
    asb_mem_axi_awaddr:  out std_logic_vector(31 downto 0);
    asb_mem_axi_awlen:   out std_logic_vector(3 downto 0);
    asb_mem_axi_awsize:  out std_logic_vector(2 downto 0);
    asb_mem_axi_awburst: out std_logic_vector(1 downto 0);
    asb_mem_axi_awlock:  out std_logic_vector(1 downto 0);
    asb_mem_axi_awcache: out std_logic_vector(3 downto 0);
    asb_mem_axi_awprot:  out std_logic_vector(2 downto 0);
    asb_mem_axi_awvalid: out std_logic;
    --     Write data channel
    asb_mem_axi_wid:     out std_logic_vector(5 downto 0);
    asb_mem_axi_wdata:   out std_logic_vector(31 downto 0);
    asb_mem_axi_wstrb:   out std_logic_vector(3 downto 0);
    asb_mem_axi_wlast:   out std_logic;
    asb_mem_axi_wvalid:  out std_logic;
    --     Write response channel
    asb_mem_axi_bready:  out std_logic;
    --   Inputs (slave to master)
    --     Read address channel
    asb_mem_axi_arready: in  std_logic;
    --     Read data channel
    asb_mem_axi_rid:     in  std_logic_vector(5 downto 0);
    asb_mem_axi_rdata:   in  std_logic_vector(31 downto 0);
    asb_mem_axi_rresp:   in  std_logic_vector(1 downto 0);
    asb_mem_axi_rlast:   in  std_logic;
    asb_mem_axi_rvalid:  in  std_logic;
    --     Write address channel
    asb_mem_axi_awready: in  std_logic;
    --     Write data channel
    asb_mem_axi_wready:  in  std_logic;
    --     Write response channel
    asb_mem_axi_bvalid:  in  std_logic;
    asb_mem_axi_bid:     in  std_logic_vector(5 downto 0);
    asb_mem_axi_bresp:   in  std_logic_vector(1 downto 0);

    ------------------------------
    -- UART2MAXILITE (U2M) I/Os --
    ------------------------------
    -- AXI lite master port (control, slave: Xilinx AXI UART)
    --   Outputs (master to slave)
    --     Read address channel
    u2m_uart_axi_araddr:  out std_logic_vector(uart_na - 1 downto 0);
    u2m_uart_axi_arprot:  out std_logic_vector(2 downto 0);
    u2m_uart_axi_arvalid: out std_logic;
    --     Read data channel
    u2m_uart_axi_rready:  out std_logic;
    --     Write address channel
    u2m_uart_axi_awaddr:  out std_logic_vector(uart_na - 1 downto 0);
    u2m_uart_axi_awprot:  out std_logic_vector(2 downto 0);
    u2m_uart_axi_awvalid: out std_logic;
    --     Write data channel
    u2m_uart_axi_wdata:   out std_logic_vector(31 downto 0);
    u2m_uart_axi_wstrb:   out std_logic_vector(3 downto 0);
    u2m_uart_axi_wvalid:  out std_logic;
    --     Write response channel
    u2m_uart_axi_bready:  out std_logic;
    ------------------------------
    -- Inputs (slave to master) --
    ------------------------------
    --     Read address channel
    u2m_uart_axi_arready: in  std_logic;
    --     Read data channel
    u2m_uart_axi_rdata:   in  std_logic_vector(31 downto 0);
    u2m_uart_axi_rresp:   in  std_logic_vector(1 downto 0);
    u2m_uart_axi_rvalid:  in  std_logic;
    --     Write address channel
    u2m_uart_axi_awready: in  std_logic;
    --     Write data channel
    u2m_uart_axi_wready:  in  std_logic;
    --     Write response channel
    u2m_uart_axi_bvalid:  in  std_logic;
    u2m_uart_axi_bresp:   in  std_logic_vector(1 downto 0);

    ---------------------------------
    -- AXI lite master port mem_axi --
    ---------------------------------
    -- Outputs (master to slave) --
    -------------------------------
    --     Read address channel
    u2m_mem_axi_araddr:  out std_logic_vector(31 downto 0);
    u2m_mem_axi_arprot:  out std_logic_vector(2 downto 0);
    u2m_mem_axi_arvalid: out std_logic;
    --     Read data channel
    u2m_mem_axi_rready:  out std_logic;
    --     Write address channel
    u2m_mem_axi_awaddr:  out std_logic_vector(31 downto 0);
    u2m_mem_axi_awprot:  out std_logic_vector(2 downto 0);
    u2m_mem_axi_awvalid: out std_logic;
    --     Write data channel
    u2m_mem_axi_wdata:   out std_logic_vector(31 downto 0);
    u2m_mem_axi_wstrb:   out std_logic_vector(3 downto 0);
    u2m_mem_axi_wvalid:  out std_logic;
    --     Write response channel
    u2m_mem_axi_bready:  out std_logic;
    ------------------------------
    -- Inputs (slave to master) --
    ------------------------------
    --     Read address channel
    u2m_mem_axi_arready: in  std_logic;
    --     Read data channel
    u2m_mem_axi_rdata:   in  std_logic_vector(31 downto 0);
    u2m_mem_axi_rresp:   in  std_logic_vector(1 downto 0);
    u2m_mem_axi_rvalid:  in  std_logic;
    --     Write address channel
    u2m_mem_axi_awready: in  std_logic;
    --     Write data channel
    u2m_mem_axi_wready:  in  std_logic;
    --     Write response channel
    u2m_mem_axi_bvalid:  in  std_logic;
    u2m_mem_axi_bresp:   in  std_logic_vector(1 downto 0);

    ---------------------------
    -- AXI monitor (AM) I/Os --
    ---------------------------
    -- AXI lite slave port ctrl_axi
    --   Inputs (master to slave) --
    --     Read address channel
    am_ctrl_axi_araddr:  in  std_logic_vector(am_ctrl_axi_address_width - 1 downto 0);
    am_ctrl_axi_arprot:  in  std_logic_vector(2 downto 0);
    am_ctrl_axi_arvalid: in  std_logic;
    --     Read data channel
    am_ctrl_axi_rready:  in  std_logic;
    --     Write address channel
    am_ctrl_axi_awaddr:  in  std_logic_vector(am_ctrl_axi_address_width - 1 downto 0);
    am_ctrl_axi_awprot:  in  std_logic_vector(2 downto 0);
    am_ctrl_axi_awvalid: in  std_logic;
    --     Write data channel
    am_ctrl_axi_wdata:   in  std_logic_vector(31 downto 0);
    am_ctrl_axi_wstrb:   in  std_logic_vector(3 downto 0);
    am_ctrl_axi_wvalid:  in  std_logic;
    --     Write response channel
    am_ctrl_axi_bready:  in  std_logic;
    --   Outputs (slave to master) --
    --     Read address channel
    am_ctrl_axi_arready: out std_logic;
    --     Read data channel
    am_ctrl_axi_rdata:   out std_logic_vector(31 downto 0);
    am_ctrl_axi_rresp:   out std_logic_vector(1 downto 0);
    am_ctrl_axi_rvalid:  out std_logic;
    --     Write address channel
    am_ctrl_axi_awready: out std_logic;
    --     Write data channel
    am_ctrl_axi_wready:  out std_logic;
    --     Write response channel
    am_ctrl_axi_bvalid:  out std_logic;
    am_ctrl_axi_bresp:   out std_logic_vector(1 downto 0)
  );
end entity demo;

architecture rtl of demo is

  -- Synchronized version of srst
  signal srst_sync: std_ulogic;

  -- Synchronized version of btn
  signal btn_sync:  std_ulogic;

  -- Local reset (combination of srst and aresetn)
  signal resetn_local: std_ulogic;

  -- GPOs
  signal asb_gpo, u2m_gpo, am_gpo: std_logic_vector(7 downto 0);

  -- Local copies
  signal asb_cpu_axi_arready_local: std_ulogic;
  signal asb_cpu_axi_awready_local: std_ulogic;

begin

  -- srst debouncer
  i_srst_deb: entity global_lib.debouncer(rtl)
  port map(
    clk   => aclk,
    srstn => aresetn,
    d     => srst,
    q     => srst_sync,
    r     => open,
    f     => open,
    a     => open
  );

  -- Local reset
  resetn_local <= (not srst_sync) and aresetn;

  -- btn debouncer
  i_btn_deb: entity global_lib.debouncer(rtl)
  port map(
    clk   => aclk,
    srstn => aresetn,
    d     => btn,
    q     => btn_sync,
    r     => open,
    f     => open,
    a     => open
  );

  -- Debugging module
  i_dbg: entity work.dbgmux(rtl)
  port map(
    aclk    => aclk,
    aresetn => aresetn,
    srst    => srst_sync,
    btn     => btn_sync,
    src0    => gpi,
    src1    => asb_gpo,
    src2    => u2m_gpo,
    src3    => am_gpo,
    dst     => gpo
  );

  i_asb: entity axi_secbus_bridge_lib.axi_secbus_bridge(rtl)
  generic map(
    s1_axi_address_and_mask => asb_cpu_axi_address_and_mask,
    s1_axi_address_or_mask  => asb_cpu_axi_address_or_mask,
    s0_axi_address_width    => asb_ctrl_axi_address_width
  )
  port map(
    aclk           => aclk,
    aresetn        => aresetn,
    srst           => srst_sync,
    s0_axi_araddr  => asb_ctrl_axi_araddr,
    s0_axi_arprot  => asb_ctrl_axi_arprot,
    s0_axi_arvalid => asb_ctrl_axi_arvalid,
    s0_axi_rready  => asb_ctrl_axi_rready,
    s0_axi_awaddr  => asb_ctrl_axi_awaddr,
    s0_axi_awprot  => asb_ctrl_axi_awprot,
    s0_axi_awvalid => asb_ctrl_axi_awvalid,
    s0_axi_wdata   => asb_ctrl_axi_wdata,
    s0_axi_wstrb   => asb_ctrl_axi_wstrb,
    s0_axi_wvalid  => asb_ctrl_axi_wvalid,
    s0_axi_bready  => asb_ctrl_axi_bready,
    s0_axi_arready => asb_ctrl_axi_arready,
    s0_axi_rdata   => asb_ctrl_axi_rdata,
    s0_axi_rresp   => asb_ctrl_axi_rresp,
    s0_axi_rvalid  => asb_ctrl_axi_rvalid,
    s0_axi_awready => asb_ctrl_axi_awready,
    s0_axi_wready  => asb_ctrl_axi_wready,
    s0_axi_bvalid  => asb_ctrl_axi_bvalid,
    s0_axi_bresp   => asb_ctrl_axi_bresp,
    s1_axi_arid    => asb_cpu_axi_arid,
    s1_axi_araddr  => asb_cpu_axi_araddr,
    s1_axi_arlen   => asb_cpu_axi_arlen,
    s1_axi_arsize  => asb_cpu_axi_arsize,
    s1_axi_arburst => asb_cpu_axi_arburst,
    s1_axi_arlock  => asb_cpu_axi_arlock,
    s1_axi_arcache => asb_cpu_axi_arcache,
    s1_axi_arprot  => asb_cpu_axi_arprot,
    s1_axi_arvalid => asb_cpu_axi_arvalid,
    s1_axi_rready  => asb_cpu_axi_rready,
    s1_axi_awid    => asb_cpu_axi_awid,
    s1_axi_awaddr  => asb_cpu_axi_awaddr,
    s1_axi_awlen   => asb_cpu_axi_awlen,
    s1_axi_awsize  => asb_cpu_axi_awsize,
    s1_axi_awburst => asb_cpu_axi_awburst,
    s1_axi_awlock  => asb_cpu_axi_awlock,
    s1_axi_awcache => asb_cpu_axi_awcache,
    s1_axi_awprot  => asb_cpu_axi_awprot,
    s1_axi_awvalid => asb_cpu_axi_awvalid,
    s1_axi_wid     => asb_cpu_axi_wid,
    s1_axi_wdata   => asb_cpu_axi_wdata,
    s1_axi_wstrb   => asb_cpu_axi_wstrb,
    s1_axi_wlast   => asb_cpu_axi_wlast,
    s1_axi_wvalid  => asb_cpu_axi_wvalid,
    s1_axi_bready  => asb_cpu_axi_bready,
    s1_axi_arready => asb_cpu_axi_arready_local,
    s1_axi_rid     => asb_cpu_axi_rid,
    s1_axi_rdata   => asb_cpu_axi_rdata,
    s1_axi_rresp   => asb_cpu_axi_rresp,
    s1_axi_rlast   => asb_cpu_axi_rlast,
    s1_axi_rvalid  => asb_cpu_axi_rvalid,
    s1_axi_awready => asb_cpu_axi_awready_local,
    s1_axi_wready  => asb_cpu_axi_wready,
    s1_axi_bvalid  => asb_cpu_axi_bvalid,
    s1_axi_bid     => asb_cpu_axi_bid,
    s1_axi_bresp   => asb_cpu_axi_bresp,
    m_axi_arid     => asb_mem_axi_arid,
    m_axi_araddr   => asb_mem_axi_araddr,
    m_axi_arlen    => asb_mem_axi_arlen,
    m_axi_arsize   => asb_mem_axi_arsize,
    m_axi_arburst  => asb_mem_axi_arburst,
    m_axi_arlock   => asb_mem_axi_arlock,
    m_axi_arcache  => asb_mem_axi_arcache,
    m_axi_arprot   => asb_mem_axi_arprot,
    m_axi_arvalid  => asb_mem_axi_arvalid,
    m_axi_rready   => asb_mem_axi_rready,
    m_axi_awid     => asb_mem_axi_awid,
    m_axi_awaddr   => asb_mem_axi_awaddr,
    m_axi_awlen    => asb_mem_axi_awlen,
    m_axi_awsize   => asb_mem_axi_awsize,
    m_axi_awburst  => asb_mem_axi_awburst,
    m_axi_awlock   => asb_mem_axi_awlock,
    m_axi_awcache  => asb_mem_axi_awcache,
    m_axi_awprot   => asb_mem_axi_awprot,
    m_axi_awvalid  => asb_mem_axi_awvalid,
    m_axi_wid      => asb_mem_axi_wid,
    m_axi_wdata    => asb_mem_axi_wdata,
    m_axi_wstrb    => asb_mem_axi_wstrb,
    m_axi_wlast    => asb_mem_axi_wlast,
    m_axi_wvalid   => asb_mem_axi_wvalid,
    m_axi_bready   => asb_mem_axi_bready,
    m_axi_arready  => asb_mem_axi_arready,
    m_axi_rid      => asb_mem_axi_rid,
    m_axi_rdata    => asb_mem_axi_rdata,
    m_axi_rresp    => asb_mem_axi_rresp,
    m_axi_rlast    => asb_mem_axi_rlast,
    m_axi_rvalid   => asb_mem_axi_rvalid,
    m_axi_awready  => asb_mem_axi_awready,
    m_axi_wready   => asb_mem_axi_wready,
    m_axi_bvalid   => asb_mem_axi_bvalid,
    m_axi_bid      => asb_mem_axi_bid,
    m_axi_bresp    => asb_mem_axi_bresp,
    irq_c          => irq_c,
    irq_e          => irq_e,
    gpi            => gpi,
    gpo            => asb_gpo
  );

  asb_cpu_axi_arready <= asb_cpu_axi_arready_local;
  asb_cpu_axi_awready <= asb_cpu_axi_awready_local;

  i_u2m: entity uart2maxilite_lib.uart2maxilite(rtl)
  port map(
    aclk             => aclk,
    aresetn          => aresetn,
    srst             => srst_sync,
    uart_axi_araddr  => u2m_uart_axi_araddr,
    uart_axi_arprot  => u2m_uart_axi_arprot,
    uart_axi_arvalid => u2m_uart_axi_arvalid,
    uart_axi_rready  => u2m_uart_axi_rready,
    uart_axi_awaddr  => u2m_uart_axi_awaddr,
    uart_axi_awprot  => u2m_uart_axi_awprot,
    uart_axi_awvalid => u2m_uart_axi_awvalid,
    uart_axi_wdata   => u2m_uart_axi_wdata,
    uart_axi_wstrb   => u2m_uart_axi_wstrb,
    uart_axi_wvalid  => u2m_uart_axi_wvalid,
    uart_axi_bready  => u2m_uart_axi_bready,
    uart_axi_arready => u2m_uart_axi_arready,
    uart_axi_rdata   => u2m_uart_axi_rdata,
    uart_axi_rresp   => u2m_uart_axi_rresp,
    uart_axi_rvalid  => u2m_uart_axi_rvalid,
    uart_axi_awready => u2m_uart_axi_awready,
    uart_axi_wready  => u2m_uart_axi_wready,
    uart_axi_bvalid  => u2m_uart_axi_bvalid,
    uart_axi_bresp   => u2m_uart_axi_bresp,
    mem_axi_araddr   => u2m_mem_axi_araddr,
    mem_axi_arprot   => u2m_mem_axi_arprot,
    mem_axi_arvalid  => u2m_mem_axi_arvalid,
    mem_axi_rready   => u2m_mem_axi_rready,
    mem_axi_awaddr   => u2m_mem_axi_awaddr,
    mem_axi_awprot   => u2m_mem_axi_awprot,
    mem_axi_awvalid  => u2m_mem_axi_awvalid,
    mem_axi_wdata    => u2m_mem_axi_wdata,
    mem_axi_wstrb    => u2m_mem_axi_wstrb,
    mem_axi_wvalid   => u2m_mem_axi_wvalid,
    mem_axi_bready   => u2m_mem_axi_bready,
    mem_axi_arready  => u2m_mem_axi_arready,
    mem_axi_rdata    => u2m_mem_axi_rdata,
    mem_axi_rresp    => u2m_mem_axi_rresp,
    mem_axi_rvalid   => u2m_mem_axi_rvalid,
    mem_axi_awready  => u2m_mem_axi_awready,
    mem_axi_wready   => u2m_mem_axi_wready,
    mem_axi_bvalid   => u2m_mem_axi_bvalid,
    mem_axi_bresp    => u2m_mem_axi_bresp,
    rx               => rx,
    tx               => tx,
    gpi              => gpi,
    gpo              => u2m_gpo
  );

  i_am: entity work.axi_monitor(rtl)
  generic map(
    cpu_axi_address_high   => am_cpu_axi_address_high,
    ctrl_axi_address_width => am_ctrl_axi_address_width
  )
  port map(
    aclk             => aclk,
    aresetn          => aresetn,
    srst             => srst_sync,
    ctrl_axi_araddr  => am_ctrl_axi_araddr,
    ctrl_axi_arprot  => am_ctrl_axi_arprot,
    ctrl_axi_arvalid => am_ctrl_axi_arvalid,
    ctrl_axi_rready  => am_ctrl_axi_rready,
    ctrl_axi_awaddr  => am_ctrl_axi_awaddr,
    ctrl_axi_awprot  => am_ctrl_axi_awprot,
    ctrl_axi_awvalid => am_ctrl_axi_awvalid,
    ctrl_axi_wdata   => am_ctrl_axi_wdata,
    ctrl_axi_wstrb   => am_ctrl_axi_wstrb,
    ctrl_axi_wvalid  => am_ctrl_axi_wvalid,
    ctrl_axi_bready  => am_ctrl_axi_bready,
    ctrl_axi_arready => am_ctrl_axi_arready,
    ctrl_axi_rdata   => am_ctrl_axi_rdata,
    ctrl_axi_rresp   => am_ctrl_axi_rresp,
    ctrl_axi_rvalid  => am_ctrl_axi_rvalid,
    ctrl_axi_awready => am_ctrl_axi_awready,
    ctrl_axi_wready  => am_ctrl_axi_wready,
    ctrl_axi_bvalid  => am_ctrl_axi_bvalid,
    ctrl_axi_bresp   => am_ctrl_axi_bresp,
    cpu_axi_araddr   => asb_cpu_axi_araddr,
    cpu_axi_arvalid  => asb_cpu_axi_arvalid,
    cpu_axi_awaddr   => asb_cpu_axi_awaddr,
    cpu_axi_awvalid  => asb_cpu_axi_awvalid,
    cpu_axi_arready  => asb_cpu_axi_arready_local,
    cpu_axi_awready  => asb_cpu_axi_awready_local,
    gpo              => am_gpo
  );

end architecture rtl;
