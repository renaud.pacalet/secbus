--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- @brief Constants, types and sub-programs for DES and DESX computations.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;

package des_pkg is

  type encryption_mode is (encrypt, decrypt);

  subtype des_word_64 is std_ulogic_vector(1 to 64);
  subtype des_word_56 is std_ulogic_vector(1 to 56);
  subtype des_word_48 is std_ulogic_vector(1 to 48);
  subtype des_word_32 is std_ulogic_vector(1 to 32);

  -- Number of left shifts for the different rounds of the DES key schedule
  type shift_array is array(1 to 16) of integer range 1 to 2;
  constant shiftk: shift_array := (1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1);

  -- DES initial and final permutations
  function ip(val: des_word_64) return des_word_64;
  function fp(val: des_word_64) return des_word_64;

  -- DES Feistel function
  function f(r: des_word_32; rk: des_word_48) return des_word_32;

  -- DES permutated choices 1 and 2
  function pc1(val: des_word_64) return des_word_56;
  function pc1_56(val: des_word_56) return des_word_56;
  function pc2(val: des_word_56) return des_word_48;

  -- 28-bits left and right rotations (on halves of 56-bits words)
  function ls(val: des_word_56) return des_word_56;
  function rs(val: des_word_56) return des_word_56;

  -- Type of DESX key: desx_encrypt(key, m) = key.k2 xor des_encrypt(key.k, m xor key.k1)
  --                   desx_decrypt(key, c) = key.k1 xor des_decrypt(key.k, c xor key.k2)
  type desx_key is record
    k1: des_word_64;
    k:  des_word_56;
    k2: des_word_64;
  end record desx_key;

-- pragma translate_off

  -- Reference DES encryption and decryption functions. Not synthesizable, use only for debugging
  impure function des_encrypt(key: des_word_64; plain_text: des_word_64; debug: boolean := false) return des_word_64;
  impure function des_decrypt(key: des_word_64; cipher_text: des_word_64; debug: boolean := false) return des_word_64;
  impure function des(key: des_word_64; di: des_word_64; mode: encryption_mode; debug: boolean := false) return des_word_64;

  -- Reference DESX encryption and decryption functions. Not synthesizable, use only for debugging
  impure function desx_encrypt(key: desx_key; plain_text: des_word_64; debug: boolean := false) return des_word_64;
  impure function desx_decrypt(key: desx_key; cipher_text: des_word_64; debug: boolean := false) return des_word_64;
  impure function desx(key: desx_key; di: des_word_64; mode: encryption_mode; debug: boolean := false) return des_word_64;

  -- Regression test of the package against reference test patterns of DES standard companion document "DES modes of operation". Run after each package change.
  procedure check_des_pkg;

-- pragma translate_on

end package des_pkg;

-- pragma translate_off
use std.textio.all;
-- pragma translate_on

package body des_pkg is

  type permute_64_64 is array(1 to 64) of integer range 1 to 64;
  type permute_64_56 is array(1 to 56) of integer range 1 to 64;
  type permute_56_48 is array(1 to 48) of integer range 1 to 56;
  type permute_32_48 is array(1 to 48) of integer range 1 to 32;
  type permute_32_32 is array(1 to 32) of integer range 1 to 32;
  type sboxes is array(1 to 8, 0 to 63) of integer range 0 to 31;

  constant ipk: permute_64_64 :=
   (58, 50, 42, 34, 26, 18, 10,  2,
    60, 52, 44, 36, 28, 20, 12,  4,
    62, 54, 46, 38, 30, 22, 14,  6,
    64, 56, 48, 40, 32, 24, 16,  8,
    57, 49, 41, 33, 25, 17,  9,  1,
    59, 51, 43, 35, 27, 19, 11,  3,
    61, 53, 45, 37, 29, 21, 13,  5,
    63, 55, 47, 39, 31, 23, 15,  7);
  constant fpk: permute_64_64 :=
    (40,  8, 48, 16, 56, 24, 64, 32,
     39,  7, 47, 15, 55, 23, 63, 31,
     38,  6, 46, 14, 54, 22, 62, 30,
     37,  5, 45, 13, 53, 21, 61, 29,
     36,  4, 44, 12, 52, 20, 60, 28,
     35,  3, 43, 11, 51, 19, 59, 27,
     34,  2, 42, 10, 50, 18, 58, 26,
     33,  1, 41,  9, 49, 17, 57, 25);
  constant ek: permute_32_48 :=
    (32,  1,  2,  3,  4,  5,
      4,  5,  6,  7,  8,  9,
      8,  9, 10, 11, 12, 13,
     12, 13, 14, 15, 16, 17,
     16, 17, 18, 19, 20, 21,
     20, 21, 22, 23, 24, 25,
     24, 25, 26, 27, 28, 29,
     28, 29, 30, 31, 32,  1);
  constant pk: permute_32_32 :=
    (16,  7, 20, 21,
     29, 12, 28, 17,
      1, 15, 23, 26,
      5, 18, 31, 10,
      2,  8, 24, 14,
     32, 27,  3,  9,
     19, 13, 30,  6,
     22, 11,  4, 25);
  constant sk: sboxes :=
    ((14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
       0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
       4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
      15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13),
     (15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
       3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
       0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
      13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9),
     (10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
      13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
      13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
       1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12),
     ( 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
      13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
      10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
       3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14),
     ( 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9,
      14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6,
       4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14,
      11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3),
     (12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
      10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
       9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
       4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13),
     ( 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
      13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
       1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
       6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12),
     (13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
       1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
       7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
       2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11));
  constant pc1k: permute_64_56 :=
    (57, 49, 41, 33, 25, 17,  9,
      1, 58, 50, 42, 34, 26, 18,
     10,  2, 59, 51, 43, 35, 27,
     19, 11,  3, 60, 52, 44, 36,
     63, 55, 47, 39, 31, 23, 15,
      7, 62, 54, 46, 38, 30, 22,
     14,  6, 61, 53, 45, 37, 29,
     21, 13,  5, 28, 20, 12,  4);
  constant pc2k: permute_56_48 :=
    (14, 17, 11, 24,  1,  5,
      3, 28, 15,  6, 21, 10,
     23, 19, 12,  4, 26,  8,
     16,  7, 27, 20, 13,  2,
     41, 52, 31, 37, 47, 55,
     30, 40, 51, 45, 33, 48,
     44, 49, 39, 56, 34, 53,
     46, 42, 50, 36, 29, 32);

  function ip(val: des_word_64) return des_word_64 is
    variable res: des_word_64;
  begin
    for i in 1 to 64 loop
      res(i) := val(ipk(i));
    end loop;
    return res;
  end ip;

  function fp(val: des_word_64) return des_word_64 is
    variable res: des_word_64;
  begin
    for i in 1 to 64 loop
      res(i) := val(fpk(i));
    end loop;
    return res;
  end fp;

  function e(val: des_word_32) return des_word_48 is
    variable res: des_word_48;
  begin
    for i in 1 to 48 loop
      res(i) := val(ek(i));
    end loop;
    return res;
  end e;

  function p(val: des_word_32) return des_word_32 is
    variable res: des_word_32;
  begin
    for i in 1 to 32 loop
      res(i) := val(pk(i));
    end loop;
    return res;
  end p;

  function s(val: des_word_48) return des_word_32 is
    variable res: des_word_32;
    variable tmp: integer range 0 to 63;
  begin
    for i in 1 to 8 loop
      tmp := to_integer(unsigned(val(6 * i - 5) & val(6 * i) &
        val(6 * i - 4 to 6 * i - 1)));
      res(4 * i - 3 to 4 * i) := std_ulogic_vector(to_unsigned(sk(i, tmp), 4));
    end loop;
    return res;
  end s;

  function f(r: des_word_32; rk: des_word_48) return des_word_32 is
  begin
    return p(s(e(r) xor rk));
  end f;

  function pc1(val: des_word_64) return des_word_56 is
    variable res: des_word_56;
  begin
    for i in 1 to 56 loop
      res(i) := val(pc1k(i));
    end loop;
    return res;
  end pc1;

  function pc1_56(val: des_word_56) return des_word_56 is
    variable tmp: des_word_64 := val(1 to 7) & '0' & val(8 to 14) & '0' & val(15 to 21) & '0' & val(22 to 28) & '0' & val(29 to 35) & '0' & val(36 to 42) & '0' & val(43 to 49) & '0' & val(50 to 56) & '0';
    variable res: des_word_56;
  begin
    for i in 1 to 56 loop
      res(i) := tmp(pc1k(i));
    end loop;
    return res;
  end pc1_56;

  function pc2(val: des_word_56) return des_word_48 is
    variable res: des_word_48;
  begin
    for i in 1 to 48 loop
      res(i) := val(pc2k(i));
    end loop;
    return res;
  end pc2;

  function ls(val: des_word_56) return des_word_56 is
    variable res: des_word_56;
  begin
    res := val(2 to 28) & val(1) & val(30 to 56) & val(29);
    return res;
  end ls;

  function rs(val: des_word_56) return des_word_56 is
    variable res: des_word_56;
  begin
    res := val(28) & val(1 to 27) & val(56) & val(29 to 55);
    return res;
  end rs;

-- pragma translate_off
  impure function des_base(pc1_key: des_word_56; di: des_word_64; mode: encryption_mode; debug: boolean := false) return des_word_64 is
    variable cd: des_word_56;
    variable lr: des_word_64;
    variable l: line;
  begin
    for i in 0 to 16 loop
      if i = 0 then
        cd := pc1_key;
        lr := ip(di);
      elsif mode = encrypt then
        cd := ls(cd);
        if shiftk(i) = 2 then
          cd := ls(cd);
        end if;
        lr := lr(33 to 64) & (lr(1 to 32) xor f(lr(33 to 64), pc2(cd)));
      else
        lr := lr(33 to 64) & (lr(1 to 32) xor f(lr(33 to 64), pc2(cd)));
        cd := rs(cd);
        if shiftk(17 - i) = 2 then
          cd := rs(cd);
        end if;
      end if;
      if debug then
        write(l, string'("C"));
        write(l, i);
        write(l, string'("D"));
        write(l, i);
        write(l, string'("/L"));
        write(l, i);
        write(l, string'("R"));
        write(l, i);
        write(l, string'(": "));
        hwrite(l, cd);
        write(l, string'("/"));
        hwrite(l, lr);
        writeline(output, l);
      end if;
    end loop;
    lr := fp(lr(33 to 64) & lr(1 to 32));
    return lr;
  end function des_base;

  impure function des_encrypt(key: des_word_64; plain_text: des_word_64; debug: boolean := false) return des_word_64 is
    variable lr: des_word_64;
    variable l: line;
  begin
    lr := des_base(pc1(key), plain_text, encrypt, debug);
    if debug then
      write(l, string'("DES(DATA="));
      hwrite(l, plain_text);
      write(l, string'(",KEY="));
      hwrite(l, key);
      write(l, string'(")="));
      hwrite(l, lr);
      writeline(output, l);
    end if;
    return lr;
  end function des_encrypt;

  impure function des_decrypt(key: des_word_64; cipher_text: des_word_64; debug: boolean := false) return des_word_64 is
    variable lr: des_word_64;
    variable l: line;
  begin
    lr := des_base(pc1(key), cipher_text, decrypt, debug);
    if debug then
      write(l, string'("DES-1(DATA="));
      hwrite(l, cipher_text);
      write(l, string'(",KEY="));
      hwrite(l, key);
      write(l, string'(")="));
      hwrite(l, lr);
      writeline(output, l);
    end if;
    return lr;
  end function des_decrypt;

  impure function des(key: des_word_64; di: des_word_64; mode: encryption_mode; debug: boolean := false) return des_word_64 is
  begin
    if mode = encrypt then
      return des_encrypt(key, di, debug);
    else
      return des_decrypt(key, di, debug);
    end if;
  end function des;

  impure function des_encrypt_56(key: des_word_56; plain_text: des_word_64; debug: boolean := false) return des_word_64 is
    variable lr: des_word_64;
    variable l: line;
  begin
    lr := des_base(pc1_56(key), plain_text, encrypt, debug);
    if debug then
      write(l, string'("DES(DATA="));
      hwrite(l, plain_text);
      write(l, string'(",KEY="));
      hwrite(l, key);
      write(l, string'(")="));
      hwrite(l, lr);
      writeline(output, l);
    end if;
    return lr;
  end function des_encrypt_56;

  impure function des_decrypt_56(key: des_word_56; cipher_text: des_word_64; debug: boolean := false) return des_word_64 is
    variable lr: des_word_64;
    variable l: line;
  begin
    lr := des_base(pc1_56(key), cipher_text, decrypt, debug);
    if debug then
      write(l, string'("DES-1(DATA="));
      hwrite(l, cipher_text);
      write(l, string'(",KEY="));
      hwrite(l, key);
      write(l, string'(")="));
      hwrite(l, lr);
      writeline(output, l);
    end if;
    return lr;
  end function des_decrypt_56;

  impure function des_56(key: des_word_56; di: des_word_64; mode: encryption_mode; debug: boolean := false) return des_word_64 is
  begin
    if mode = encrypt then
      return des_encrypt_56(key, di, debug);
    else
      return des_decrypt_56(key, di, debug);
    end if;
  end function des_56;

  impure function desx_encrypt(key: desx_key; plain_text: des_word_64; debug: boolean := false) return des_word_64 is
  begin
    return key.k2 xor des_encrypt_56(key.k, plain_text xor key.k1, debug);
  end function desx_encrypt;

  impure function desx_decrypt(key: desx_key; cipher_text: des_word_64; debug: boolean := false) return des_word_64 is
  begin
    return key.k1 xor des_decrypt_56(key.k, cipher_text xor key.k2, debug);
  end function desx_decrypt;

  impure function desx(key: desx_key; di: des_word_64; mode: encryption_mode; debug: boolean := false) return des_word_64 is
  begin
    if mode = encrypt then
      return desx_encrypt(key, di, debug);
    else
      return desx_decrypt(key, di, debug);
    end if;
  end function desx;

  procedure check_des_pkg is
    constant key: des_word_64 := x"0123456789abcdef";
    type des_word_64_array is array(natural range <>) of des_word_64;
    constant pt: des_word_64_array(1 to 3) := (x"4e6f772069732074",
      x"68652074696d6520", x"666f7220616c6c20");
    constant ct: des_word_64_array(1 to 3) := (x"3fa40e8a984d4815",
      x"6a271787ab8883f9", x"893d51ec4b563b53");
    variable l: line;
  begin
    for i in 1 to 3  loop
      assert des_encrypt(key, pt(i), true) = ct(i)
        report "Encipher non-regression test failed"
        severity failure;
      assert des_decrypt(key, ct(i), true) = pt(i)
        report "Decipher non-regression test failed"
        severity failure;
    end loop;
    write(l, string'("des_pkg non-regression test passed"));
    writeline(output, l);
  end procedure check_des_pkg;
-- pragma translate_on

end package body des_pkg;
