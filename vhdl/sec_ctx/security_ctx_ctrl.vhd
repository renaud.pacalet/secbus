--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- security_ctx_ctrl.vhd
--
-- Registers :
--    R0 (PSPE/SP) : from io_handler (update ctx) 
--                   from security Ctrl (read ctx)
--    R2 (PSPE/SP) : to security controller (update_ctx)
--                       
-- pspe generic cache : full associative, distributed ram, lru*      , write_through  
-- sp   generic cache : set associative , block       ram, pseudo lru, write through                        
-- (*) : not implemented yet, pseudo lru is used instead of.         
-- @todo : Bad command Error handling 

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

library caches_lib;
use caches_lib.all;

entity security_ctx_ctrl is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- protection input parameters
       p_mb_address            : in  addr_t; -- Master Block Base Address
       p_size                  : in  addr_t; -- Size of protected region of memory
       p_b_address             : in  addr_t; -- Base address of protection region of memory

       p_cache_pspe            : in std_ulogic;
       p_cache_sp              : in std_ulogic;

       -- From Security Controller   
       --sc_nbusy                : in  std_ulogic;

       -- Data from io_handler (update_ctx) or Security Ctrl (Read_ctx)
       rd_reg_0                : in  data_register_read;  

       -- Data from MT Ctrl (New/Update PSPE)
       rd_mt_data              : in pspe_t;    

       -- Req from vci_input_ctrl/io_handler/Mt_ctrl 
       rd_ctx_cmd              : in  security_ctx_cmd_out;
       wr_ctx_cmd              : out security_ctx_cmd_in;

       -- Security context to vci_input_ctrl/io_input_ctrl/Mt_ctrl/Ms_Ctrl/Security_Ctrl 
       wr_ctx_register         : out ctx_register_read;

       -- Req To Security Ctrl                     
       wr_sec_cmd              : out security_rq_out;
       rd_sec_cmd              : in  security_rq_in;

       wr_reg_2                : out data_register_write;

       -- Req to IRQ Handler
       wr_irq                  : out irq_out;
       rd_irq_ack              : in std_ulogic
);

end security_ctx_ctrl;

architecture rtl of security_ctx_ctrl is

type fsm_state is (IDLE,
                   CACHE_PSPE_READ   , CACHE_PSPE_WRITE ,                -- in systemc : HIT_PSPE            
                   CACHE_SP_READ     , CACHE_SP_WRITE   ,                --              HIT_SP
                   SC_PSPE_CMD_READ  , SC_PSPE_RSP_READ , CHECK_PSPE ,   --              READ_PSPE, WREAD_PSPE
                   SC_SP_CMD_READ    , SC_SP_RSP_READ   , CHECK_SP   ,   --              READ_SP  , WREAD_SP, func checkSP()
                   FLUSH_REG_2       ,
                   SC_PSPE_CMD_WRITE , SC_PSPE_RSP_WRITE,                --              WRITE_PSPE, WWRITE_PSPE
                   SC_SP_CMD_WRITE   , SC_SP_RSP_WRITE  ,                --              WRITE_SP  , WWRITE_SP
                   IRQ_SEND);
 
signal s_state         , r_state            : fsm_state;
signal s_addr          , r_addr             : addr_t;
signal s_first         , r_first            : std_ulogic;
signal s_is_pspe       , r_is_pspe          : std_ulogic;
signal s_size          , r_size             : natural range 0 to 3;
signal s_slave         , r_slave            : std_ulogic;
signal s_mt            , r_mt               : std_ulogic;
signal s_ctx_rsp       , r_ctx_rsp          : std_ulogic;
signal s_pspe_addr     , r_pspe_addr        : addr_t;
signal s_sp_addr       , r_sp_addr          : unsigned(ADDR_SIZE - 1 downto 0);
signal s_pspe_wdata    , r_pspe_wdata       : pspe_t;
signal s_current_pspe  , r_current_pspe     : pspe_t;
signal s_sp_wdata      , r_sp_wdata         : data_vector(SP_SIZE_WORDS/2 - 1 downto 0);
signal s_irq_type      , r_irq_type         : io_status_errt_e; 
signal s_irq_op        , r_irq_op           : io_status_errc_e;
signal s_ctx_register  , r_ctx_register     : ctx_register_read;

-- pspe cache signals
signal s_pspe_cache_cmd                     : std_ulogic_vector(2 downto 0);
signal s_pspe_cache_set_in                  : std_ulogic_vector(PSPE_CACHE_SETS - 1 downto 0);
signal s_pspe_cache_addr                    : addr_t;
signal s_pspe_cache_data_in                 : pspe_t;
signal s_pspe_cache_hit                     : std_ulogic;
signal s_pspe_cache_data_out                : pspe_t;
signal s_pspe_cache_set_out                 : std_ulogic_vector(PSPE_CACHE_SETS - 1 downto 0);
signal s_pspe_set_in    , r_pspe_set_in     : std_ulogic_vector(PSPE_CACHE_SETS - 1 downto 0);

-- sp cache signals
signal s_sp_cache_cmd                       : std_ulogic_vector(2 downto 0); 
signal s_sp_cache_way_in                    : std_ulogic_vector(SP_CACHE_WAYS - 1 downto 0);
signal s_sp_cache_addr                      : addr_t;
signal s_sp_cache_data_in                   : data_vector(SP_CACHE_BLKS - 1 downto 0);
signal s_sp_cache_way_out                   : std_ulogic_vector(SP_CACHE_WAYS - 1 downto 0);
signal s_sp_cache_data_out                  : data_vector(SP_CACHE_BLKS - 1 downto 0); 
signal s_sp_cache_hit                       : std_ulogic;
signal s_sp_way_in       , r_sp_way_in      : std_ulogic_vector(SP_CACHE_WAYS - 1 downto 0);

-- hit write save
signal s_pspe_hit      , r_pspe_hit         : std_ulogic;
signal s_sp_hit        , r_sp_hit           : std_ulogic;

begin
------
pspe_cache_inst :  entity caches_lib.pspe_cache(rtl) 
generic map(
        SETS            => PSPE_CACHE_SETS,                           
        BLOCKS          => PSPE_CACHE_BLKS -- not used                           
)
port map(
        clk             => clk,                         
        srstn           => srstn,                          
        ce              => ce,                          

        cmd             => s_pspe_cache_cmd,                          
                                                   
        set_in          => s_pspe_cache_set_in,                           
        addr_in         => s_pspe_cache_addr,                          
        data_in         => s_pspe_cache_data_in,                          

        hit             => s_pspe_cache_hit,                          
        data_out        => s_pspe_cache_data_out,                          
        set_out         => s_pspe_cache_set_out                         
);                                                                   

sp_cache_inst : entity caches_lib.sp_cache(rtl) 
generic map(
        WAYS            => SP_CACHE_WAYS, 
        SETS            => SP_CACHE_SETS,
        BLOCKS          => SP_CACHE_BLKS
)
port map(
        clk             => clk, 
        srstn           => srstn, 
        ce              => ce,
        
        cmd             => s_sp_cache_cmd, 
                        
        way_in          => s_sp_cache_way_in,
        addr_in         => s_sp_cache_addr,
        data_in         => s_sp_cache_data_in,
        
        way_out         => s_sp_cache_way_out,
        data_out        => s_sp_cache_data_out,
        hit             => s_sp_cache_hit
);

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state           <= IDLE;
              r_addr            <= (others => '0');
              r_first           <= '0';   
              r_is_pspe         <= '0';   
              r_size            <= 0;   
              r_slave           <= '0';   
              r_mt              <= '0';   
              r_ctx_rsp         <= '0';   
              r_pspe_addr       <= (others => '0');   
              r_sp_addr         <= (others => '0');   
              r_pspe_wdata      <= pspe_none;   
              r_sp_wdata        <= (others => (others => '0'));   
              r_irq_type        <= NONE;    
              r_irq_op          <= LOAD;   
              r_ctx_register    <= ctx_none;   
              r_current_pspe    <= pspe_none;  
              r_pspe_set_in     <= (others => '0'); -- one hot 
              r_sp_way_in       <= (others => '0'); -- one hot 
              r_pspe_hit        <= '0';
              r_sp_hit          <= '0';

        elsif ce = '1' then
              r_state           <= s_state;       
              r_addr            <= s_addr;       
              r_first           <= s_first;
              r_is_pspe         <= s_is_pspe; 
              r_size            <= s_size;
              r_slave           <= s_slave;
              r_mt              <= s_mt;
              r_ctx_rsp         <= s_ctx_rsp;
              r_pspe_addr       <= s_pspe_addr;
              r_sp_addr         <= s_sp_addr;
              r_pspe_wdata      <= s_pspe_wdata;
              r_sp_wdata        <= s_sp_wdata;
              r_irq_type        <= s_irq_type;
              r_irq_op          <= s_irq_op;
              r_ctx_register    <= s_ctx_register;
              r_current_pspe    <= s_current_pspe;
              r_pspe_set_in     <= s_pspe_set_in;
              r_sp_way_in       <= s_sp_way_in;
              r_pspe_hit        <= s_pspe_hit;
              r_sp_hit          <= s_sp_hit;    
        end if;
end if;
end process;

--
pTransition : process(r_state          , r_addr              , r_first              , r_is_pspe            , 
                      r_size           , r_sp_addr           , r_ctx_register       , rd_irq_ack           ,
                      r_slave          , r_mt                , r_ctx_rsp            , r_pspe_addr          ,  
                      r_pspe_wdata     , r_sp_wdata          , r_irq_type           , r_irq_op             ,   
                      rd_ctx_cmd       , p_mb_address        , p_b_address          , p_size               ,  
                      rd_reg_0         , rd_sec_cmd          , r_current_pspe       , rd_mt_data           , 
                      p_cache_pspe     , p_cache_sp          , r_pspe_hit           , r_sp_hit             ,
                      s_pspe_cache_hit , s_pspe_cache_set_out, s_pspe_cache_data_out, r_pspe_set_in        , 
                      s_sp_cache_hit   , s_sp_cache_way_out  , s_sp_cache_data_out  , r_sp_way_in)
                        
                       
variable v_current_pspe : pspe_t;
variable v_pspe_valid   : std_ulogic;
variable v_prot         : std_ulogic;
variable v_pspe_size    : natural range 0 to 3; 
variable v_pspe_addr    : addr_t; 
variable v_sp_addr      : unsigned(ADDR_SIZE - 1 downto 0); 
 
begin

s_state              <= r_state;
s_addr               <= r_addr;
s_first              <= r_first;
s_is_pspe            <= r_is_pspe; 
s_size               <= r_size;
s_slave              <= r_slave;
s_mt                 <= r_mt;
s_ctx_rsp            <= r_ctx_rsp;
s_pspe_addr          <= r_pspe_addr;
s_sp_addr            <= r_sp_addr;
s_pspe_wdata         <= r_pspe_wdata;
s_sp_wdata           <= r_sp_wdata;
s_irq_type           <= r_irq_type;
s_irq_op             <= r_irq_op;
s_ctx_register       <= r_ctx_register;
s_current_pspe       <= r_current_pspe;

s_pspe_set_in        <= r_pspe_set_in;

s_pspe_cache_cmd     <= (others => '0');
s_pspe_cache_addr    <= (others => '0');
s_pspe_cache_set_in  <= (others => '0');
s_pspe_cache_data_in <= pspe_none;

s_sp_way_in          <= r_sp_way_in;

s_sp_cache_cmd       <= (others => '0');
s_sp_cache_addr      <= (others => '0');
s_sp_cache_data_in   <= (others => (others => '0'));
s_sp_cache_way_in    <= (others => '0');

s_sp_hit             <= r_sp_hit;
s_pspe_hit           <= r_pspe_hit;

case r_state is
       when IDLE =>
             if rd_ctx_cmd.enable = '1' then
                v_pspe_addr     := (others => '0');
                v_sp_addr       := (others => '0');
                s_first         <= '0';
                s_is_pspe       <= '0';
                s_addr          <= rd_ctx_cmd.address;
                s_size          <= 0;
                s_slave         <= '0';    
                s_ctx_rsp       <= '0';    
                s_mt            <= rd_ctx_cmd.mt;
             -- s_ctx_register  <= ctx_none;
              
                if rd_ctx_cmd.cmd = READ then
                   s_irq_op    <= LOAD;
                   v_pspe_addr := getPSPEAddressFromPageSize(rd_ctx_cmd.address, 0, p_mb_address, p_b_address);  
                   s_pspe_addr <= v_pspe_addr; 
                   if p_cache_pspe = '1' then
                   -- @todo :  verifier si pas d'update en cours 
                      s_pspe_cache_cmd(2) <= '1';
                      s_pspe_cache_addr   <= v_pspe_addr;
                      s_state             <= CACHE_PSPE_READ; 
                   else
                      s_state             <= SC_PSPE_CMD_READ;
                   end if;

                elsif rd_ctx_cmd.cmd = UPDATE then
                    -- address between PSPEBaseAddress and SPBaseAddress
                    -- PSPE update
                    if inclusion(rd_ctx_cmd.address,
		                         p_mb_address,                                          
                                 getSPBaseAddress(p_mb_address, p_size)) = '1' then

                         s_is_pspe   <= '1';
                         if rd_ctx_cmd.mt = '1' then -- request from Mt Ctrl
                            if rd_ctx_cmd.address = r_ctx_register.s_pspe_addr then
                               s_ctx_register.s_pspe <= rd_mt_data;
                            end if;

                            s_pspe_wdata <= rd_mt_data;
                         else -- request from io_handler (input ctrl)
                            for i in s_pspe_wdata'range loop
                                s_pspe_wdata(i) <= rd_reg_0.data(word_index(rd_ctx_cmd.address)+i);
                            end loop;
                         end if;

                         if p_cache_pspe = '1' then
                            s_pspe_cache_cmd(2) <= '1';
                            s_pspe_cache_addr   <= rd_ctx_cmd.address;
                            s_state             <= CACHE_PSPE_WRITE; 
                         else
                            s_state             <= FLUSH_REG_2;
                         end if; 

                    -- SP update
                    elsif inclusion(rd_ctx_cmd.address,
	                                getSPBaseAddress(p_mb_address, p_size),
		                            getMMacTBaseAddress(p_mb_address, p_size)) = '1' then

                         for i in s_sp_wdata'range loop   
                             s_sp_wdata(i)   <= rd_reg_0.data(i);
                         end loop;
                         --s_is_pspe <= '0';
                         if p_cache_sp = '1' then
                            s_sp_cache_cmd(2) <= '1';
                            s_sp_cache_addr   <= rd_ctx_cmd.address;
                            s_state           <= CACHE_SP_WRITE; 
                         else
                            s_state           <= FLUSH_REG_2;
                         end if; 
                         
                    else -- non valid PSPE/SP address to update
                        s_irq_op   <= STORE;
                        s_irq_type <= PSPE;
                        s_state    <= IRQ_SEND;                          
                    end if;
 
                else
                   s_state <= IDLE;   -- @to do :  error => consume the entire burst and send error
                end if;
             end if; -- cmdval

       when CACHE_PSPE_READ =>
            if s_pspe_cache_hit = '1' then
               if r_slave = '1' then -- from checkSP 
                  s_ctx_register.s_pspe_addr    <= r_pspe_addr;
                  s_ctx_register.s_pspe         <= s_pspe_cache_data_out;
                  s_ctx_rsp                     <= '1'; 
                  s_state                       <= IDLE;
               else -- !r_slave
                  s_current_pspe <= s_pspe_cache_data_out; 
                  s_state        <= CHECK_PSPE;
               end if;
            else
               s_pspe_set_in <= s_pspe_cache_set_out;
               s_state       <= SC_PSPE_CMD_READ;
            end if; -- hit
 
       when CACHE_PSPE_WRITE =>
            s_pspe_hit    <= s_pspe_cache_hit;
            s_pspe_set_in <= s_pspe_cache_set_out;
            s_state       <= FLUSH_REG_2;

        when CACHE_SP_READ =>
            if s_sp_cache_hit = '1' then
               if r_first = '1' then 
                  s_first <= '0';
                  -- filling half sp block : SIZE_WORDS/2 

                  for i in s_sp_cache_data_out'range loop 
                        s_ctx_register.sp(i) <= s_sp_cache_data_out(i);
                  end loop;

                  v_sp_addr         := r_sp_addr + SIZE_LINE_BYTES;   
                  s_sp_addr         <= v_sp_addr;
                  s_sp_cache_cmd(2) <= '1';
                  s_sp_cache_addr   <= std_ulogic_vector(v_sp_addr);
                  s_state           <= CACHE_SP_READ;         

               else -- !first   
                  for i in s_sp_cache_data_out'range loop 
                        s_ctx_register.sp(i+SP_CACHE_BLKS) <= s_sp_cache_data_out(i);
                  end loop;
                  s_state        <= CHECK_SP;
               end if;
            else
               s_sp_way_in  <= s_sp_cache_way_out;
               s_state      <= SC_SP_CMD_READ;
            end if; -- hit
           
       when CACHE_SP_WRITE =>
            s_sp_hit    <= s_sp_cache_hit;
            s_sp_way_in <= s_sp_cache_way_out;
            s_state     <= FLUSH_REG_2;

       when SC_PSPE_CMD_READ =>    
            if rd_sec_cmd.cmdack = '1' then            
               s_state <= SC_PSPE_RSP_READ;
            end if;

       when SC_PSPE_RSP_READ =>       
            v_current_pspe := pspe_none;
  
            if rd_sec_cmd.rspval = '1' then
              
              for i in v_current_pspe'range loop
                 v_current_pspe(i) := rd_reg_0.data(word_index(r_pspe_addr)+i);
              end loop;                
                            
              -- updating pspe cache
              if p_cache_pspe = '1' then
                 s_pspe_cache_cmd(1)    <= '1';                  
                 s_pspe_cache_cmd(0)    <= '1';                  
                 s_pspe_cache_set_in    <= r_pspe_set_in;
                 s_pspe_cache_addr      <= r_pspe_addr;
                 s_pspe_cache_data_in   <= v_current_pspe;
              end if;
                              
              if r_slave = '1' then -- from checkSP 
                 s_ctx_register.s_pspe_addr    <= r_pspe_addr;
                 s_ctx_register.s_pspe         <= v_current_pspe;
                 s_ctx_rsp                     <= '1'; 
                 s_state                       <= IDLE;
              else -- !r_slave
                 s_current_pspe <= v_current_pspe; 
                 s_state        <= CHECK_PSPE;
              end if;
            end if;
   
         when CHECK_PSPE =>
              v_pspe_valid := pspe_getValid(r_current_pspe);
              v_pspe_size  := pspe_getSize(r_current_pspe);

              if (v_pspe_valid = '1') and (r_size = v_pspe_size) then
                    
                    if (r_mt = '1')  then   -- req from Mt Ctrl
                       s_ctx_register.s_pspe_addr <= r_pspe_addr;
                       s_ctx_register.s_pspe      <= r_current_pspe;
                       s_ctx_rsp                  <= '1';
                       --
                       if r_pspe_addr /= getPSPEAddressFromPageSize(pspe_getMSAdd(r_ctx_register.m_pspe), 
                                                                            pspe_getSize(r_ctx_register.m_pspe),
                                                                            p_mb_address,
			  		                                                        p_b_address) then
                             s_ctx_register.page_addr <= (others => '1');
                       end if;      
                       --
                       s_state                    <= IDLE; 
                    else
                      s_ctx_register.page_addr <= r_pspe_addr;
                      s_ctx_register.m_pspe    <= r_current_pspe;

                      v_prot := pspe_getProt(r_current_pspe);
                      if v_prot = '1' then -- no prot
                            for i in 0 to SP_SIZE_WORDS - 1 loop 
                                  s_ctx_register.sp(i) <= (others => '0');
                            end loop;
                          s_ctx_rsp            <= '1';
                          s_state              <= IDLE;
                      else
                           v_sp_addr := unsigned(getSPAddressFromIdx(pspe_getSPIdx(r_current_pspe), p_mb_address, p_size));  
                           s_sp_addr <= v_sp_addr;
                           s_first   <= '1';

                           if p_cache_sp = '1' then
                                s_sp_cache_cmd(2) <= '1';
                                s_sp_cache_addr   <= std_ulogic_vector(v_sp_addr);
                                s_state           <= CACHE_SP_READ;         
                           else
                                s_state           <= SC_SP_CMD_READ;
                           end if; 
                       end if; -- prot
                    end if;
              elsif r_size = pageLevel - 1 then -- !valid || no match exising page size matches 
                    s_irq_type <= PSPE;
                    s_state    <= IRQ_SEND;
              else -- !valid  incr size
                   s_size      <= r_size + 1;

                   v_pspe_addr := getPSPEAddressFromPageSize(r_addr, r_size + 1, p_mb_address, p_b_address);   
                   s_pspe_addr <= v_pspe_addr;
                   if p_cache_pspe = '1' then
                       s_pspe_cache_cmd(2) <= '1';
                       s_pspe_cache_addr   <= v_pspe_addr;
                       s_state             <= CACHE_PSPE_READ; 
                  else
                       s_state             <= SC_PSPE_CMD_READ;
                  end if; 
              end if;

       when SC_SP_CMD_READ =>    
            if rd_sec_cmd.cmdack = '1' then            
               s_state <= SC_SP_RSP_READ;
            end if;

       when SC_SP_RSP_READ =>   
            -- check sp
            if rd_sec_cmd.rspval = '1' then
              if p_cache_sp = '1' then
                 s_sp_cache_cmd(1)    <= '1';                  
                 s_sp_cache_cmd(0)    <= '1';                  
                 s_sp_cache_way_in    <= r_sp_way_in;
                 s_sp_cache_addr      <= std_ulogic_vector(r_sp_addr);
                 s_sp_cache_data_in   <= rd_reg_0.data;
              end if;

              if r_first = '1' then
                    s_first <= '0';
                    -- filling half sp block : SIZE_WORDS/2 
                    for i in rd_reg_0.data'range loop 
                          s_ctx_register.sp(i) <= rd_reg_0.data(i);
                    end loop;
                    s_sp_addr <= r_sp_addr + SIZE_LINE_BYTES;   
                    s_state   <= SC_SP_CMD_READ;
              else
                    for i in rd_reg_0.data'range loop 
                          s_ctx_register.sp(i+SP_CACHE_BLKS) <= rd_reg_0.data(i); 
                    end loop;
                    s_state <= CHECK_SP;
              end if;
            end if; --rspval

        when CHECK_SP =>   
             if sp_getValid(r_ctx_register.sp) = '1' then
                if sp_getModeI(r_ctx_register.sp) =  MODE_I_MACTREE then 
                   -- sp requires integrity protection based on MACTREE
                   -- fetching slave PSPE
                   s_slave     <= '1';
                   v_pspe_addr := getPSPEAddressFromPageSize(pspe_getMSAdd(r_ctx_register.m_pspe), 
			                                                 pspe_getSize(r_ctx_register.m_pspe),
			  		                                         p_mb_address,
			  		                                         p_b_address);
                   s_pspe_addr <= v_pspe_addr;

                   if p_cache_pspe = '1' then
                     s_pspe_cache_cmd(2) <= '1';
                     s_pspe_cache_addr   <= v_pspe_addr;
                     s_state             <= CACHE_PSPE_READ; 
                   else
                     s_state <= SC_PSPE_CMD_READ;
                   end if;
                else
                   s_ctx_rsp <= '1';
                   s_state   <= IDLE;
                end if; 
             else
                s_irq_type <= SP;
                s_state    <= IRQ_SEND;
             end if;
 
        when FLUSH_REG_2 =>  
             if r_is_pspe = '1' then 
                s_state <= SC_PSPE_CMD_WRITE;
             else  -- sp write
                s_state <= SC_SP_CMD_WRITE;
             end if;

       when SC_PSPE_CMD_WRITE =>   
            if rd_sec_cmd.cmdack = '1' then            
               s_state <= SC_PSPE_RSP_WRITE;
            end if;

       when SC_PSPE_RSP_WRITE =>  
            if rd_sec_cmd.rspval = '1' or r_mt = '1' then

               if p_cache_pspe = '1' then
                    if r_pspe_hit = '0' then
                       s_pspe_cache_cmd(1) <= '1';   -- write dir
                    end if;
                    
                    s_pspe_cache_addr    <= r_addr;
                    s_pspe_cache_cmd(0)  <= '1';     -- write data
                    s_pspe_cache_data_in <= r_pspe_wdata; 
                    s_pspe_cache_set_in  <= r_pspe_set_in;   
               end if;

               s_ctx_rsp <= '1'; --#
               s_state   <= IDLE;
            end if;

       when SC_SP_CMD_WRITE =>   
            if rd_sec_cmd.cmdack = '1' then            
               s_state <= SC_SP_RSP_WRITE;
            end if;

       when SC_SP_RSP_WRITE =>  
            if rd_sec_cmd.rspval = '1' then

              if p_cache_sp = '1' then
                    if r_sp_hit = '0' then
                       s_sp_cache_cmd(1) <= '1';      -- write dir
                    end if;

                    s_sp_cache_addr    <= r_addr;
                    s_sp_cache_cmd(0)  <= '1';        -- write data
                    s_sp_cache_data_in <= r_sp_wdata; 
                    s_sp_cache_way_in  <= r_sp_way_in;     
              end if;

              s_ctx_rsp <= '1'; --#
              s_state   <= IDLE;
            end if;

       when IRQ_SEND =>  
           --if rd_irq_ack = '1' then 
           --   s_state <= IDLE;
           --end if; 

end case;

end process;

wr_ctx_cmd.rspval <= r_ctx_rsp;
wr_ctx_register   <= r_ctx_register; -- when r_ctx_rsp = '1' else (others => '0')
  
pGeneration : process(r_state, r_pspe_addr, r_sp_addr, r_addr, r_pspe_wdata, r_sp_wdata, r_irq_type, r_irq_op) -- sc_nbusy)
begin

wr_reg_2.enable    <= '0';
wr_reg_2.addr      <= (others => '0');

wr_reg_2.flush     <= '0';
wr_reg_2.data      <= (others => (others => '0'));
wr_reg_2.be        <= (others => (others => '0'));


wr_sec_cmd         <= security_rq_out_none;

wr_ctx_cmd.cmdack  <= '0';

wr_irq.irq         <= '0';
wr_irq.addr        <= (others => '0');
wr_irq.typ         <= NONE;
wr_irq.op          <= LOAD;

case r_state is
       when IDLE =>  
            wr_ctx_cmd.cmdack <= '1';

       when CACHE_PSPE_READ  =>
            null;

       when CACHE_PSPE_WRITE =>
            null;

       when CACHE_SP_READ  =>
            null;

       when CACHE_SP_WRITE =>
            null;

       when SC_PSPE_CMD_READ =>
            wr_sec_cmd.address <= line_aligned(r_pspe_addr);
            wr_sec_cmd.cmdval  <= '1';
            wr_sec_cmd.cmd     <= READ;
            wr_sec_cmd.mb      <= '1';
 
       when SC_PSPE_RSP_READ =>
            null;                      

       when CHECK_PSPE =>
            null;                      

       when SC_SP_CMD_READ =>
            wr_sec_cmd.cmdval  <= '1';
            wr_sec_cmd.cmd     <= READ;

            wr_sec_cmd.mb      <= '1';
            wr_sec_cmd.address <= std_ulogic_vector(r_sp_addr);

       when SC_SP_RSP_READ =>
            null;                      

       when CHECK_SP =>
            null;                      

       when FLUSH_REG_2 =>
            wr_reg_2.enable <= '1'; --sc_nbusy;
            wr_reg_2.flush  <= '1';

       when SC_PSPE_CMD_WRITE =>
            wr_reg_2.enable <= '1';
            wr_reg_2.addr   <= r_addr;

            for i in r_pspe_wdata'range loop   
                wr_reg_2.data(word_index(r_addr)+i) <= r_pspe_wdata(i);
                wr_reg_2.be(word_index(r_addr)+i)   <= (others => '1'); 
            end loop;

            wr_sec_cmd.cmdval  <= '1';
            wr_sec_cmd.address <= r_addr;  
            wr_sec_cmd.mb      <= '1';
            wr_sec_cmd.cmd     <= WRITE;

       when SC_PSPE_RSP_WRITE  =>
            null;

       when SC_SP_CMD_WRITE =>
            wr_reg_2.enable <= '1';
            wr_reg_2.addr   <= r_addr;

            for i in wr_reg_2.data'range loop   --  Write half SP 
                wr_reg_2.be(i)   <= (others => '1'); 
                wr_reg_2.data(i) <= r_sp_wdata(i);
            end loop;
 
            wr_sec_cmd.cmdval  <= '1';
            wr_sec_cmd.address <= r_addr;  
            wr_sec_cmd.mb      <= '1';
            wr_sec_cmd.cmd     <= WRITE;

       when SC_SP_RSP_WRITE  =>
            null;

       when IRQ_SEND =>
            wr_irq.irq     <= '1';
            wr_irq.addr    <= r_addr;
            wr_irq.typ     <= r_irq_type;
            wr_irq.op      <= r_irq_op;

end case;

end process;

end rtl;


