--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;

package uart2maxilite_pkg is

  -- Bit-width of addresses of AXI interface with UART
  constant uart_na: positive := 12;

  -- UART registers and fields
  constant num_uart_regs:   natural := 4;
  constant uart_rx_idx:     natural range 0 to num_uart_regs - 1 := 0;
  constant uart_rx_addr:    std_ulogic_vector(31 downto 0) := std_ulogic_vector(to_unsigned(4 * uart_rx_idx, 32));
  constant uart_tx_idx:     natural range 0 to num_uart_regs - 1 := uart_rx_idx + 1;
  constant uart_tx_addr:    std_ulogic_vector(31 downto 0) := std_ulogic_vector(to_unsigned(4 * uart_tx_idx, 32));
  constant uart_status_idx: natural range 0 to num_uart_regs - 1 := uart_tx_idx + 1;
  constant uart_status_addr:    std_ulogic_vector(31 downto 0) := std_ulogic_vector(to_unsigned(4 * uart_status_idx, 32));
  constant uart_ctrl_idx:   natural range 0 to num_uart_regs - 1 := uart_status_idx + 1;
  constant uart_ctrl_addr:    std_ulogic_vector(31 downto 0) := std_ulogic_vector(to_unsigned(4 * uart_ctrl_idx, 32));

  constant uart_status_rx_fifo_valid_data: natural range 0 to 31 := 0;
  constant uart_status_rx_fifo_full:       natural range 0 to 31 := 1;
  constant uart_status_tx_fifo_empty:      natural range 0 to 31 := 2;
  constant uart_status_tx_fifo_full:       natural range 0 to 31 := 3;
  constant uart_status_intr_enabled:       natural range 0 to 31 := 4;
  constant uart_status_overrun_error:      natural range 0 to 31 := 5;
  constant uart_status_frame_error:        natural range 0 to 31 := 6;
  constant uart_status_parity_error:       natural range 0 to 31 := 7;
  constant uart_ctrl_rst_tx_fifo: natural range 0 to 31 := 0;
  constant uart_ctrl_rst_rx_fifo: natural range 0 to 31 := 1;
  constant uart_ctrl_enable_intr: natural range 0 to 31 := 4;

  -- Commands
  constant cmd_mem_read:  std_ulogic_vector(1 downto 0) := "00";
  constant cmd_mem_write: std_ulogic_vector(1 downto 0) := "01";
  constant cmd_reg_read:  std_ulogic_vector(1 downto 0) := "10";
  constant cmd_reg_write: std_ulogic_vector(1 downto 0) := "11";

  constant okay_char:   std_ulogic_vector(7 downto 0) := X"00";
  constant slverr_char: std_ulogic_vector(7 downto 0) := X"02";
  constant decerr_char: std_ulogic_vector(7 downto 0) := X"03";

end package uart2maxilite_pkg;
