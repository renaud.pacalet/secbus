--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- security_ctrl.vhd
--
-- Registers R0 cleartext       from Memory Ctrl(cipher=0) or CryptoConf (cipher=1)  
--                              to vci_input/io_handler_input/security ctx ctrl 
--           R1 Ciphertex       from Memory Ctrl
--           R2 PSPE/SP         from security ctx controller 
--           R3 General Purpose from vci_input_ctrl/io_handler_ctrl

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

library des_lib;
use des_lib.des_pkg.all;
entity security_ctrl is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- protection input parameters
       p_mb_address            : in  addr_t; -- Master Block Base Address
       p_size                  : in  addr_t; -- Size of protected region of memory
       p_b_address             : in  addr_t; -- Base address of protection region of memory

       -- Acknowledges from Mt/Ms) Ctrl
       mt_ack                  : in  integrity_ctrl_ack;
       ms_ack                  : in  integrity_ctrl_ack;

       -- Data from reg bank
       rd_reg_0                : in  data_register_read;  
       rd_reg_1                : in  data_register_read;  
       rd_reg_2                : in  data_register_read;  
       rd_reg_3                : in  data_register_read;  -- @ in systemC : register_ad 

       -- Security context from security context controller
       rd_ctx_register         : in  ctx_register_read;

       -- Master Security Context from Configuration Registers
       rd_master_ctx_init      : in  master_ctx_reg_read;

        
       -- Master Security Context from MT Ctrl                       
       rd_master_update        : in  master_ctx_reg_read; 

       p_secbus_enable         : in  std_ulogic;
 
       -- To io_handler Ctrl/Vci_input_ctrl/security_ctx_ctrl
       sc_nbusy                : out std_ulogic;  -- @ in systemC : sc_busy (but means ack)

       -- Req to Mac Tree Ctrl/MS Ctrl
       mt_cmd                  : out integrity_ctrl_cmd;
       ms_cmd                  : out integrity_ctrl_cmd;
 
       -- Data to reg Bank
       wr_reg_0                : out data_register_write;

       -- Master Security Context to MT_Ctrl
       wr_master_ctx           : out master_ctx_reg_read;
 
       -- From security Ctx / Vci input / io_handler
       rd_sec_cmd              : in  security_rq_out;
       wr_sec_cmd              : out security_rq_in;

       -- Request To Memory Ctrl    
       wr_mem_rq               : out memory_ctrl_cmd_out; -- @ mem_rq : single record in spec 
       rd_mem_rq               : in  memory_ctrl_cmd_in;  -- cmdack, rspval

        -- request to Crypto Conf       
       wr_crypto_cmd           : out crypto_cmd_out;  
       rd_crypto_cmd           : in  crypto_cmd_in        -- cmdack, rspval

);

end security_ctrl;

architecture rtl of security_ctrl is

type fsm_state is (IDLE,
                   SC_IN_READ      ,                                   -- securitiy Ctrl in req  systemC : R_IDLE, 
                   RD_MEM_CMD_READ , RD_MEM_RSP_READ,                  -- Memory  req                      R_START_READ, R_WAIT_READ
                   RD_MT_WAIT_UP   ,
                   RD_MT_CMD_CHECK , RD_MT_WAIT_CHECK,                 -- Mt Ctrl req                      R_START_CINT
                   RD_MS_CMD_CHECK , RD_MS_WAIT_CHECK,                 -- Ms Ctrl req                      @ not in systemC
                   RD_CONF_CMD_DEC , RD_CONF_RSP_DEC,                  -- Crypto_Conf req                  R_START_BLOCK_DEC, R_WAIT_BLOCK_DEC
                   SC_IN_WRITE     ,                                   -- securitiy Ctrl in req  systemC : W_IDLE, 
                   WR_MEM_CMD_READ , WR_MEM_RSP_READ,                  -- Memory  req                      W_START_READ, W_WAIT_READ
                   WR_MEM_CMD_WRITE, WR_MEM_RSP_WRITE,                 -- Memory  req                      W_START_WRITE, W_WAIT_WRITE
                   WR_MT_WAIT_UP   ,
                   WR_MS_CMD_CHECK , WR_MS_WAIT_CHECK,                 
                   WR_MT_CMD_CHECK , WR_MT_WAIT_CHECK,                 -- Mt Ctrl req                      W_START_CINTEGRITY 
                   WR_MT_CMD_UPDT  , WR_MT_WAIT_UPDT,                  -- Mt Ctrl req                      W_START_UINT, W_WAIT_UINT
                   WR_MS_CMD_UPDT  , WR_MS_WAIT_UPDT,                  -- Ms Ctrl req                      @ not in systemC
                   WR_CONF_CMD_DEC , WR_CONF_RSP_DEC,                  -- Crypto_Conf req                  W_START_BLOCK_DEC, W_WAIT_BLOCK_DEC
                   WR_CONF_CMD_ENC , WR_CONF_RSP_ENC,                  -- Crypto_Conf req                  W_START_BLOCK_ENC, W_WAIT_BLOCK_ENC
                   WR_UPDATE_REG_0);                                   --                                  W_UPDATE_REG_0  

Signal s_state       , r_state            : fsm_state;
signal s_addr        , r_addr             : addr_t;
signal s_cipher      , r_cipher           : std_ulogic;
signal s_mb          , r_mb               : std_ulogic;
signal s_mac         , r_mac              : std_ulogic;
signal s_int         , r_int              : std_ulogic;
signal s_cry_mode    , r_cry_mode         : crypto_mode_e;
signal s_reg_src     , r_reg_src          : natural range 0 to REG_MAX - 1;
signal s_int_src     , r_int_src          : natural range 0 to REG_MAX - 1;
signal s_reg_dst     , r_reg_dst          : natural range 0 to REG_MAX - 1;
signal s_keyC        , r_keyC             : desx_key; 
signal s_master_sp   , r_master_sp        : sp_t;
signal s_master_pspe , r_master_pspe      : pspe_t;
signal s_match_reg_0                      : std_ulogic;
signal s_plen        , r_plen             : unsigned(PLEN_SIZE - 1 downto 0);

begin
-----
pMPspe : process(r_master_pspe, p_secbus_enable, rd_master_update, rd_master_ctx_init)
begin
s_master_pspe <= r_master_pspe;

if p_secbus_enable = '0' then
   s_master_pspe <= rd_master_ctx_init.pspe; 
elsif rd_master_update.enable = '1' then
   s_master_pspe <= rd_master_update.pspe;
end if;

end process;

--s_master_pspe <= rd_master_ctx_init.pspe when p_secbus_enable         = '0' else
--                 rd_master_update.pspe   when rd_master_update.enable = '1' else                  
--                 r_master_pspe;

s_master_sp   <= rd_master_ctx_init.sp   when p_secbus_enable = '0'         else
                 r_master_sp;


wr_master_ctx.pspe   <= r_master_pspe;
wr_master_ctx.sp     <= r_master_sp;
wr_master_ctx.enable <= '1';    -- ?
 
----
pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state         <= IDLE;
              r_addr          <= (others => '0');
              r_cipher        <= '0';
              r_mb            <= '0';
              r_mac           <= '0';
              r_int           <= '0';
              r_cry_mode      <= CRY_CTR;
              r_keyC          <= key_null;
              r_int_src       <= 0;
              r_reg_src       <= 0;
              r_reg_dst       <= 0;
              r_master_sp     <= (others => (others => '0'));   
              r_master_pspe   <= (others => (others => '0'));
              r_plen          <= (others => '0');

        elsif ce = '1' then
              r_state         <= s_state;       
              r_addr          <= s_addr;        
              r_cipher        <= s_cipher;      
              r_mb            <= s_mb;          
              r_mac           <= s_mac;         
              r_int           <= s_int;         
              r_cry_mode      <= s_cry_mode; 
              r_keyC          <= s_keyC;        
              r_int_src       <= s_int_src;     
              r_reg_src       <= s_reg_src;     
              r_reg_dst       <= s_reg_dst;     
              r_master_sp     <= s_master_sp;   
              r_master_pspe   <= s_master_pspe; 
              r_plen          <= s_plen; 
        end if;
end if;
end process;

-- @ in systemC , additional condition :  !sc_cmd.mb
s_match_reg_0 <= '1' when rd_reg_0.addr(rd_reg_0.addr'left downto LINE_BITS) = r_addr(r_addr'left downto LINE_BITS) else
                 '0';

--

pTransition : process(r_state      , p_mb_address   , p_size       , 
                      r_addr       , r_cipher       , r_mb         , 
                      r_mac        , r_int          , r_cry_mode   ,  
                      r_reg_src    , r_reg_dst      , r_keyC       ,
                      r_int_src    , r_plen         ,                      
                      rd_sec_cmd   , rd_ctx_register, rd_mem_rq    ,  
                      rd_reg_2     , rd_reg_3       ,                         
                      mt_ack       , ms_ack         , rd_crypto_cmd, 
                      r_master_sp  , 
                      s_match_reg_0)   
 
variable v_modeC   : std_ulogic_vector(1 downto 0);
variable v_modeI   : std_ulogic_vector(1 downto 0);
variable v_be_vect : unsigned(SIZE_LINE_BYTES - 1 downto 0); 

begin

s_state         <= r_state;
s_addr          <= r_addr;
s_cipher        <= r_cipher;
s_mb            <= r_mb;
s_mac           <= r_mac;
s_int           <= r_int;
s_cry_mode      <= r_cry_mode;
s_keyC          <= r_keyC;
s_reg_src       <= r_reg_src;  
s_reg_dst       <= r_reg_dst; 
s_int_src       <= r_int_src; 
s_plen          <= r_plen;

v_modeC := MODE_C_NONE; 
v_modeI := MODE_I_NONE; 

case r_state is
       when IDLE =>
             if mt_ack.cack = '1' and rd_sec_cmd.cmdval = '1' then
                s_cipher <= '0';
                s_mac    <= '0';
                s_int    <= '0';
                s_addr   <= rd_sec_cmd.address;

                if rd_sec_cmd.mb  = '1' then
                   v_be_vect := be_arr_2_vect(rd_reg_2.be);
                   s_mb   <= '1';
                   s_keyC <= sp_getKeyC(r_master_sp);

                   -- addr is PSPE 
                   if inclusion(rd_sec_cmd.address, p_mb_address, getSPBaseAddress(p_mb_address, p_size)) = '1' then 
                      s_int      <= '1';     -- MAC_TREE
                    --s_cipher   <= '0';    
                   else
                      s_int      <= '1';     -- MAC_TREE
                      s_cipher   <= '1';
                      s_cry_mode <= CRY_CBC;
                   end if;  
                else
                   v_be_vect := be_arr_2_vect(rd_reg_3.be);
                   s_mb <= '0';
                   v_modeC := sp_getModeC(rd_ctx_register.sp);
                   case v_modeC is
                        when MODE_C_CTR  =>
                             s_cipher     <= '1';
                             s_cry_mode   <= CRY_CTR;
                        when MODE_C_CBC  =>
                             s_cipher     <= '1'; 
                             s_cry_mode   <= CRY_CBC;
                        when others => -- MODE_C_NONE
                           --s_cipher     <= '0';
                   end case;
      
                   v_modeI := sp_getModeI(rd_ctx_register.sp);
                   case v_modeI is
                        when MODE_I_MAC  =>
                           --s_int  <= '0';
                             s_mac  <= '1';
                        when MODE_I_MACTREE =>
                             s_int  <= '1';
                           --s_mac  <= '0';
                             null;
                        when others => -- MODE_I_NONE
                           --s_int  <= '0';
                           --s_mac  <= '0';

                   end case;                       

                   s_keyC <= sp_getKeyC(rd_ctx_register.sp);
   
                end if; -- is mb
               
                if rd_sec_cmd.cmd = READ then
                   s_state <= SC_IN_READ;
                elsif rd_sec_cmd.cmd = WRITE then
                   s_plen  <= to_unsigned(SIZE_LINE_BYTES - ctz(v_be_vect) - clz(v_be_vect), PLEN_SIZE);
                   s_state <= SC_IN_WRITE;
                else
                   s_state <= IDLE;   -- @to do :  error => consume the whole burst (till the last flit) and send error
                end if;
             end if; -- cmdval

       when SC_IN_READ =>                   
         -- if s_match_reg_0 = '1' and r_mb = '0' then 
            if s_match_reg_0 = '1' then 
               -- clear data to vci_input/io_handler_input/sec_ctx_ctrl
               s_state <= IDLE;
            else
               if r_cipher = '1' then 
                  s_reg_src <= 1; 
                  s_reg_dst <= 0; 
               else
                  s_reg_src <= 0; 
                  s_reg_dst <= 0; 
               end if;   
               s_int_src    <= 2; 
               s_state      <= RD_MEM_CMD_READ;     
            end if; -- hit 0

       when RD_MEM_CMD_READ =>    
            if rd_mem_rq.cmdack = '1' then            
               s_state <= RD_MEM_RSP_READ;
            end if;

       when RD_MEM_RSP_READ =>  -- if cry_mode = CTR, cry_data ready  
            if rd_mem_rq.rspval = '1' then
               if r_cipher = '1' then
                  s_state <= RD_CONF_CMD_DEC; 
               elsif r_mac = '1' then   
                     s_state <= RD_MS_CMD_CHECK;
               elsif r_int = '1' then
                  if (mt_ack.uack = '1') or (mt_ack.alap_ack = '1') then
                     s_state <= RD_MT_CMD_CHECK;  
                  else
                     s_state <= RD_MT_WAIT_UP;
                  end if;
               else
                  s_state <= IDLE;    
               end if; -- cipher
            end if; --rspval
       
        when RD_MT_WAIT_UP =>
           if (mt_ack.uack = '1') or (mt_ack.alap_ack = '1') then
              s_state <= RD_MT_CMD_CHECK;
           end if;

        when RD_MT_CMD_CHECK =>  
            s_state <= RD_MT_WAIT_CHECK;

        when RD_MT_WAIT_CHECK =>  
           if mt_ack.cack = '1' then
              s_state <= IDLE;
           end if; 

        when RD_MS_CMD_CHECK =>  
            s_state <= RD_MS_WAIT_CHECK;

        when RD_MS_WAIT_CHECK => 
          if ms_ack.cack = '1' then 
              s_state <= IDLE; 
          end if;

        when RD_CONF_CMD_DEC => 
          if rd_crypto_cmd.cmdack = '1' then
             s_state <= RD_CONF_RSP_DEC;
          end if;

        when RD_CONF_RSP_DEC => 
          if rd_crypto_cmd.rspval = '1' then
             if r_mac = '1' then
                s_state <= RD_MS_CMD_CHECK;
             elsif r_int = '1' then
                -- @ check mt ready ?
                s_state <= RD_MT_CMD_CHECK;                   
             else
                s_state <= IDLE;
             end if;
          end if;  
----*-- 
       when SC_IN_WRITE =>
            if r_cipher = '0' then
               if (r_int = '0') and (r_mac = '0') then -- or r_plen = SIZE_LINE_BYTES
                  if r_mb = '1' then --
                     s_reg_src <= 2;  -- from security ctx ctrl
                  else 
                     s_reg_src <= 3;  -- from vci_input/io_handler_input
                  end if;
                  s_state   <= WR_MEM_CMD_WRITE; 
               else -- int or mac
                  s_reg_src <= 0;
                  s_int_src <= 0;
                  s_plen    <= to_unsigned(SIZE_LINE_BYTES, PLEN_SIZE);
               -- if (r_plen = SIZE_LINE_BYTES) or (s_match_reg_0 = '1' and r_mb = '0') then -- and all be = X"F" 
                  if (r_plen = SIZE_LINE_BYTES) or (s_match_reg_0 = '1') then -- and all be = X"F" 
                     s_state <= WR_UPDATE_REG_0;
                  else
                     s_state <= WR_MEM_CMD_READ;
                  end if; -- cipher
               end if;   
            else
               s_reg_src <= 1;
               s_reg_dst <= 0;
               s_int_src <= 1;
               s_plen    <= to_unsigned(SIZE_LINE_BYTES, PLEN_SIZE);
            -- if (r_plen = SIZE_LINE_BYTES) or (s_match_reg_0 = '1' and r_mb = '0') then -- and all be = X"F" 
               if (r_plen = SIZE_LINE_BYTES) or (s_match_reg_0 = '1') then -- and all be = X"F" 
                  s_state <= WR_UPDATE_REG_0;
               else
                  s_state <= WR_MEM_CMD_READ;
               end if; 
            end if; -- cipher   

       when WR_MEM_CMD_READ =>    
            if rd_mem_rq.cmdack = '1' then            
               s_state <= WR_MEM_RSP_READ;
            end if;

       when WR_MEM_RSP_READ =>   
            if rd_mem_rq.rspval = '1' then
               if r_mac = '1' then
                   s_state <= WR_MS_CMD_CHECK;
               elsif r_int = '1' then
                   if mt_ack.uack = '1' or mt_ack.alap_ack = '1' then
                         s_state <= WR_MT_CMD_CHECK;
                   else
                         s_state <= WR_MT_WAIT_UP; 
                   end if;
               elsif r_cipher = '1' then
                   s_state <= WR_CONF_CMD_DEC;  
               else -- !cipher and !int 
                   s_state <= WR_UPDATE_REG_0;
               end if; -- cipher
            end if; --rspval

        when WR_MT_WAIT_UP => 
             if mt_ack.uack = '1' or mt_ack.alap_ack = '1' then
                 s_state <= WR_MT_CMD_CHECK;
             end if;

        when WR_MS_CMD_CHECK =>  
             s_state <= WR_MS_WAIT_CHECK;

        when WR_MS_WAIT_CHECK =>  
             -- send request to MT_CTRL
             if ms_ack.cack = '1' then 
                if r_cipher = '1' then
                    s_state <= WR_CONF_CMD_DEC;
                else
                    s_state <= WR_UPDATE_REG_0;
                end if;
             end if;

        when WR_MT_CMD_CHECK =>  
             s_state <= WR_MT_WAIT_CHECK;

        when WR_MT_WAIT_CHECK =>  
             -- send request to MT_CTRL
             if mt_ack.cack = '1' then 
                if r_cipher = '1' then
                    s_state <= WR_CONF_CMD_DEC;
                else
                    s_state <= WR_UPDATE_REG_0;
                end if;
             end if;

        when WR_CONF_CMD_DEC =>
             if rd_crypto_cmd.cmdack = '1' then
                s_state <= WR_CONF_RSP_DEC;
             end if;

        when WR_CONF_RSP_DEC => 
             if rd_crypto_cmd.rspval = '1' then
                s_state <= WR_UPDATE_REG_0;
             end if;   
 
        when WR_UPDATE_REG_0 =>
             s_addr <= line_aligned(r_addr);  
             if r_cipher = '1' then 
                s_state <= WR_CONF_CMD_ENC;
             elsif mt_ack.cack = '1' then -- ms_ack ??
                s_state <= WR_MEM_CMD_WRITE;
             end if;

        when WR_CONF_CMD_ENC =>
             if rd_crypto_cmd.cmdack = '1' then
                s_state <= WR_CONF_RSP_ENC;
             end if;

        when WR_CONF_RSP_ENC => 
             if rd_crypto_cmd.rspval = '1' then
                s_state <= WR_MEM_CMD_WRITE;
             end if;   

       when WR_MEM_CMD_WRITE => 
             if rd_mem_rq.cmdack = '1' then            
                 s_state <= WR_MEM_RSP_WRITE;
             end if;

       when WR_MEM_RSP_WRITE =>  
             if rd_mem_rq.rspval = '1' then
                if r_mac = '1' then
                   s_state <= WR_MS_CMD_UPDT;
                elsif r_int = '1' then
                   s_state <= WR_MT_CMD_UPDT;  
                else
                   s_state <= IDLE;
                end if;
             end if; --rspval

        when WR_MT_CMD_UPDT =>  
              s_state <= WR_MT_WAIT_UPDT;

        when WR_MT_WAIT_UPDT =>  
              if ms_ack.uack = '1' then 
                 s_state <= IDLE;
              end if;
 
        when WR_MS_CMD_UPDT =>  
              s_state <= WR_MS_WAIT_UPDT;

        when WR_MS_WAIT_UPDT =>  
              if ms_ack.uack = '1' then 
                 s_state <= IDLE;
              end if;
  

end case;

end process;

pGeneration : process(r_state, r_addr, r_mb, r_reg_src, r_reg_dst, r_int_src, r_cry_mode, r_keyC, 
                      rd_reg_2, rd_reg_3, rd_reg_0, r_cipher, r_int, r_plen, 
                      rd_mem_rq, rd_crypto_cmd, mt_ack, r_mac, 
                      s_match_reg_0)
 
variable v_mask_data : data_t;

begin
wr_mem_rq.cmdval          <= '0';
wr_mem_rq.cmd             <= READ;
wr_mem_rq.start_address   <= (others => '0');
wr_mem_rq.plen            <= (others => '0');
wr_mem_rq.id              <= (others => '0'); 
wr_mem_rq.direct_write    <= '0'; 

mt_cmd.enable             <= '0';
mt_cmd.addr               <= (others => '0');
mt_cmd.cmd                <= CHECK;
mt_cmd.src                <= (others => '0');  

ms_cmd.enable             <= '0';
ms_cmd.addr               <= (others => '0');
ms_cmd.cmd                <= CHECK;
ms_cmd.src                <= (others => '0');  

wr_crypto_cmd.cmdval      <= '0';  
wr_crypto_cmd.mode        <= CRY_CTR;
wr_crypto_cmd.key         <= key_null;
wr_crypto_cmd.iv          <= (others => '0');
wr_crypto_cmd.size        <= (others => '0');      
wr_crypto_cmd.cipher      <= '0';
wr_crypto_cmd.id_src      <= (others => '0');
wr_crypto_cmd.id_dest     <= (others => '0');
wr_crypto_cmd.address     <= (others => '0');
wr_crypto_cmd.direct_data <= '0'; 
wr_crypto_cmd.no_init     <= '0';            
wr_crypto_cmd.data_ready  <= '0';            

wr_reg_0.enable           <= '0';
wr_reg_0.flush            <= '0';
wr_reg_0.addr             <= (others => '0');
wr_reg_0.data             <= (others => (others => '0'));
wr_reg_0.be               <= (others => (others => '0'));

wr_sec_cmd.cmdack         <= '0';
wr_sec_cmd.rspval         <= '0';

sc_nbusy                  <= '0';

case r_state is
       when IDLE =>  
            wr_sec_cmd.cmdack <= mt_ack.cack;
            sc_nbusy          <= mt_ack.cack;

       when SC_IN_READ =>
         -- wr_sec_cmd.rspval  <= s_match_reg_0 and not r_mb;  
            wr_sec_cmd.rspval  <= s_match_reg_0;  
 
          --if (r_cipher = '1') and (r_cry_mode = CRY_CTR) and (s_match_reg_0 = '0' or r_mb = '1') then 
            if (r_cipher = '1') and (r_cry_mode = CRY_CTR) and (s_match_reg_0 = '0') then 
               wr_crypto_cmd.cmdval     <= '1';  
               wr_crypto_cmd.mode       <= r_cry_mode;
               wr_crypto_cmd.key        <= r_keyC;
               wr_crypto_cmd.size       <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));     
               wr_crypto_cmd.cipher     <= '0';
               wr_crypto_cmd.id_src     <= (others => '0');
               wr_crypto_cmd.id_dest    <= (others => '0');
               wr_crypto_cmd.address    <= r_addr;
               wr_crypto_cmd.data_ready <= '0';
            end if;

       when RD_MEM_CMD_READ =>
            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= READ;
            wr_mem_rq.start_address <= r_addr;  
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(SIZE_LINE_BYTES, PLEN_SIZE));
            wr_mem_rq.id            <= (others => '0'); 
            wr_mem_rq.id(r_reg_src) <= '1'; 
            wr_mem_rq.id(r_int_src) <= '1'; 

       when RD_MEM_RSP_READ =>
            if rd_mem_rq.rspval = '1' and r_cipher = '0' then
                  wr_sec_cmd.rspval <= '1';
            end if;

       when RD_MS_CMD_CHECK =>
            ms_cmd.enable  <= '1';
            ms_cmd.cmd     <= CHECK;
            ms_cmd.addr    <= r_addr;         
            ms_cmd.src     <= std_ulogic_vector(to_unsigned(r_int_src, REG_SIZE));

       when RD_MS_WAIT_CHECK =>
            null;

       when RD_MT_WAIT_UP =>
            null;

       when RD_MT_CMD_CHECK =>
            mt_cmd.enable  <= '1';
            mt_cmd.cmd     <= CHECK;
            mt_cmd.addr    <= r_addr;         
            mt_cmd.src     <= std_ulogic_vector(to_unsigned(r_int_src, REG_SIZE));

       when RD_MT_WAIT_CHECK =>
            null;

       when RD_CONF_CMD_DEC =>
            wr_crypto_cmd.cmdval     <= '1';  
            wr_crypto_cmd.mode       <= r_cry_mode;
            wr_crypto_cmd.key        <= r_keyC;
            wr_crypto_cmd.size       <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));     
            wr_crypto_cmd.cipher     <= '0';
            wr_crypto_cmd.id_src     <= std_ulogic_vector(to_unsigned(r_reg_src, REG_SIZE));
            wr_crypto_cmd.id_dest    <= std_ulogic_vector(to_unsigned(r_reg_dst, REG_SIZE));
            wr_crypto_cmd.address    <= r_addr;
            wr_crypto_cmd.data_ready <= '1';

       when RD_CONF_RSP_DEC =>
            wr_sec_cmd.rspval       <= rd_crypto_cmd.rspval; -- clear text valid in R0

       when SC_IN_WRITE =>
             if r_cipher = '0' and r_int = '0' and r_mac = '0' then
                wr_reg_0.enable <= '1';
                wr_reg_0.addr   <= (others => '1');
             end if;

             if (r_cipher = '1') and (r_cry_mode = CRY_CTR) then 
                 wr_crypto_cmd.cmdval     <= '1';  
                 wr_crypto_cmd.mode       <= r_cry_mode;
                 wr_crypto_cmd.key        <= r_keyC;
                 wr_crypto_cmd.size       <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));      
                 wr_crypto_cmd.cipher     <= '0';
                 wr_crypto_cmd.id_src     <= (others => '0');
                 wr_crypto_cmd.id_dest    <= (others => '0');
                 wr_crypto_cmd.address    <= line_aligned(r_addr);
                 wr_crypto_cmd.data_ready <= '0';          
            end if;  

       when WR_MEM_CMD_READ =>
            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= READ;
            wr_mem_rq.start_address <= line_aligned(r_addr);  
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(SIZE_LINE_BYTES, PLEN_SIZE));
            wr_mem_rq.id            <= (others => '0'); 
            wr_mem_rq.id(r_reg_src) <= '1'; 
            wr_mem_rq.id(r_int_src) <= '1'; 

       when WR_MEM_RSP_READ =>
            null;

       when WR_MT_WAIT_UP =>
            null;

       when WR_MS_CMD_CHECK =>
            ms_cmd.enable  <= '1';
            ms_cmd.cmd     <= CHECK;
            ms_cmd.addr    <= line_aligned(r_addr);
            ms_cmd.src     <= std_ulogic_vector(to_unsigned(r_int_src, REG_SIZE));

       when WR_MS_WAIT_CHECK =>
            null;

       when WR_MT_CMD_CHECK =>
            mt_cmd.enable  <= '1';
            mt_cmd.cmd     <= CHECK;
            mt_cmd.addr    <= line_aligned(r_addr);
            mt_cmd.src     <= std_ulogic_vector(to_unsigned(r_int_src, REG_SIZE));

       when WR_MT_WAIT_CHECK =>
            null;

       when WR_CONF_CMD_DEC =>
            wr_crypto_cmd.cmdval     <= '1';  
            wr_crypto_cmd.mode       <= r_cry_mode;
            wr_crypto_cmd.key        <= r_keyC;
            wr_crypto_cmd.size       <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));      
            wr_crypto_cmd.cipher     <= '0';
            wr_crypto_cmd.id_src     <= std_ulogic_vector(to_unsigned(r_reg_src, REG_SIZE));
            wr_crypto_cmd.id_dest    <= std_ulogic_vector(to_unsigned(r_reg_dst, REG_SIZE));
            wr_crypto_cmd.address    <= line_aligned(r_addr);
            wr_crypto_cmd.data_ready <= '1';

       when WR_CONF_RSP_DEC =>
            null;

       when WR_UPDATE_REG_0 =>
        
            wr_reg_0.enable <= '1';
            wr_reg_0.addr   <= r_addr;
            
            if r_mb = '1' then 
               wr_reg_0.be <= rd_reg_2.be;
               for i in rd_reg_2.data'range loop  
                   for j in CELL_SIZE - 1 downto 0 loop
                       if rd_reg_2.be(i)(j) = '1' then
                          v_mask_data((j+1)*8 - 1 downto (j*8)) := X"FF";                   
                       else
                          v_mask_data((j+1)*8 - 1 downto (j*8)) := (others => '0');                   
                       end if; 
                   end loop;

                   wr_reg_0.data(i) <= (rd_reg_0.data(i) and not v_mask_data) or (rd_reg_2.data(i) and v_mask_data);
               end loop;

            else 
               wr_reg_0.be <= rd_reg_3.be;   
               for i in rd_reg_3.data'range loop  
                   for j in CELL_SIZE - 1 downto 0 loop
                       if rd_reg_3.be(i)(j) = '1' then
                          v_mask_data((j+1)*8 - 1 downto (j*8)) := X"FF";                   
                       else
                          v_mask_data((j+1)*8 - 1 downto (j*8)) := (others => '0');                   
                       end if; 
                   end loop;

                   wr_reg_0.data(i) <= (rd_reg_0.data(i) and not v_mask_data) or (rd_reg_3.data(i) and v_mask_data);
               end loop;
            end if; -- is mb          
 
       when WR_CONF_CMD_ENC =>
            wr_crypto_cmd.cmdval     <= '1';  
            wr_crypto_cmd.mode       <= r_cry_mode;
            wr_crypto_cmd.key        <= r_keyC;
            wr_crypto_cmd.size       <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));  
            wr_crypto_cmd.cipher     <= '1';
            wr_crypto_cmd.id_src     <= std_ulogic_vector(to_unsigned(r_reg_dst, REG_SIZE)); 
            wr_crypto_cmd.id_dest    <= std_ulogic_vector(to_unsigned(r_reg_src, REG_SIZE));
            wr_crypto_cmd.address    <= line_aligned(r_addr);
            wr_crypto_cmd.data_ready <= '1';

       when WR_CONF_RSP_ENC =>
            null;

       when WR_MEM_CMD_WRITE =>
            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= WRITE;
            wr_mem_rq.start_address <= r_addr; 
            wr_mem_rq.plen          <= std_ulogic_vector(r_plen);
            wr_mem_rq.id            <= (others => '0'); 
            wr_mem_rq.id(r_reg_src) <= '1'; 

       when WR_MEM_RSP_WRITE => 
            wr_sec_cmd.rspval       <= rd_mem_rq.rspval; 

       when WR_MS_CMD_UPDT  =>
            ms_cmd.enable  <= '1';
            ms_cmd.cmd     <= UPDATE;
            ms_cmd.addr    <= line_aligned(r_addr);
            ms_cmd.src     <= std_ulogic_vector(to_unsigned(r_int_src, REG_SIZE));

       when WR_MS_WAIT_UPDT =>
            null;

       when WR_MT_CMD_UPDT  =>
            mt_cmd.enable  <= '1';
            mt_cmd.cmd     <= UPDATE;
            mt_cmd.addr    <= line_aligned(r_addr);
            mt_cmd.src     <= std_ulogic_vector(to_unsigned(r_int_src, REG_SIZE));

       when WR_MT_WAIT_UPDT =>
            null;
end case;

end process;

end rtl;


