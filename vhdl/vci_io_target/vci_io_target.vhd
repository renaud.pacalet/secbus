--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- vci_io_target.vhd

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

entity vci_io_target is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- Read from io_input_ctrl
       io_handler_data_in      : in  data_out ;  -- 8 words (256 bits)
       io_ack                  : in  std_ulogic; 
      
       busy                    : in  std_ulogic; -- flag to status reg;

       -- Req to IRQ Handler
       rd_irq                  : in  irq_out;
       wr_irq_ack              : out std_ulogic;     

       -- Vci Req from CPU      
       p_vci_io_tgt_in         : in  vci_i2t;
       p_vci_io_tgt_out        : out vci_t2i;

       -- Req to io_input_ctrl         
       wr_io_handler           : out io_block;

       -- Master Security Context to security Ctrl
       wr_master_ctx_init      : out master_ctx_reg_read;

       -- JustForDebug
       -- From Security controller
       rd_master_ctx           : in  master_ctx_reg_read;

       p_enable                : out std_ulogic;
       p_b_address             : out addr_t; 
       p_mb_address            : out addr_t; 
       p_size                  : out addr_t; 
       p_cache_sp              : out std_ulogic;
       p_cache_pspe            : out std_ulogic;
       p_cache_ms              : out std_ulogic;
       p_cache_mt              : out std_ulogic;

       p_irq_c                 : out std_ulogic;
       p_irq_e                 : out std_ulogic;
       gpi                     : in  std_ulogic_vector(7 downto 0);
       gpo                     : out std_ulogic_vector(7 downto 0)
);

end vci_io_target;

architecture rtl of vci_io_target is

type fsm_state is (IDLE , IO_SEND_REQ , IO_WAIT , TGT_RSP);                                                     

signal s_state        , r_state         : fsm_state;
signal s_rdata        , r_rdata         : data_t;
signal s_config       , r_config        : config_t;
signal s_status       , r_status        : status_t;
signal s_cmd          , r_cmd           : io_cmd_cmd_e;
signal s_pspe         , r_pspe          : pspe_t;
signal s_sp           , r_sp            : sp_t;

signal s_data_block   , r_data_block    : data_vector(SIZE_LINE_WORDS - 1 downto 0);  
signal s_addr_block   , r_addr_block    : addr_t;
signal s_error        , r_error         : rerror_t;
signal s_read_status  , r_read_status   : std_ulogic;

signal s_srcid        , r_srcid         : srcid_t;
signal s_trdid        , r_trdid         : trdid_t;
signal s_pktid        , r_pktid         : pktid_t;

signal s_mb_address                     : addr_t;

signal r_gpi                            : std_ulogic_vector(31 downto 0);
signal s_gpi_ce       , r_gpi_ce        : std_ulogic;
signal s_gpo          , r_gpo           : std_ulogic_vector(7 downto 0);
--------------------------------------------------------
begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state         <= IDLE;
              r_rdata         <= (others => '0'); 
              r_config        <= config_none; 
              r_status        <= status_none;
              r_cmd           <= NONE;
              r_pspe          <= pspe_none;
              r_sp            <= sp_none; 
              r_data_block    <= (others => (others => '0'));
              r_addr_block    <= (others => '0');
              r_srcid         <= (others => '0');
              r_trdid         <= (others => '0');
              r_pktid         <= (others => '0');
              r_error         <= (others => '0');
              r_read_status   <= '0';
              r_gpi           <= (others => '0');
              r_gpi_ce        <= '1';
              r_gpo           <= (others => '0');
        elsif ce = '1' then
              r_state         <= s_state;
              r_rdata         <= s_rdata;    
              r_config        <= s_config;    
              r_status        <= s_status;    
              r_cmd           <= s_cmd;       
              r_pspe          <= s_pspe;      
              r_sp            <= s_sp;        
              r_data_block    <= s_data_block;
              r_addr_block    <= s_addr_block;
              r_srcid         <= s_srcid;
              r_trdid         <= s_trdid;
              r_pktid         <= s_pktid;
              r_error         <= s_error;
              r_read_status   <= s_read_status;
              r_gpi_ce        <= s_gpi_ce;
              if r_gpi_ce = '1' then
                r_gpi           <= gpi & r_gpi(31 downto 8);
              end if;
              r_gpo           <= s_gpo;
        end if;
end if;
end process;

gpo <= r_gpo;

pTransition : process(r_state        , busy        ,  
                      p_vci_io_tgt_in, r_cmd       , io_handler_data_in,
                      r_rdata        , r_error     , io_ack       ,                     
                      r_config       , r_status    , r_read_status, 
                      r_pspe         , r_sp        , 
                      r_data_block   , r_addr_block,
                      r_srcid        , r_trdid     , r_pktid, rd_master_ctx, -- JustForDebug
                      r_gpi_ce       , r_gpi       , r_gpo)
                   -- r_srcid        , r_trdid     , r_pktid)
   
variable v_io_cell : io_register_e;
variable v_index   : natural range 0 to 15; -- max sp_words = 16

begin

s_state         <= r_state;
s_rdata         <= r_rdata;
s_error         <= r_error;
s_read_status   <= r_read_status;
s_cmd           <= r_cmd;
s_config        <= r_config;
s_pspe          <= r_pspe;
s_sp            <= r_sp;
s_data_block    <= r_data_block;
s_addr_block    <= r_addr_block;
s_srcid         <= r_srcid;      
s_trdid         <= r_trdid;      
s_pktid         <= r_pktid;     
s_gpi_ce        <= r_gpi_ce;
s_gpo           <= r_gpo;

 
case r_state is
       when IDLE =>
            s_cmd <= NONE;

            if p_vci_io_tgt_in.cmdval = '1' then
--
              assert (p_vci_io_tgt_in.eop = '1')
              report "vci io request burst size must be one flit "
              severity FAILURE; 
--
              s_read_status <= '0';
              s_srcid       <= p_vci_io_tgt_in.srcid;
              s_trdid       <= p_vci_io_tgt_in.trdid;
              s_pktid       <= p_vci_io_tgt_in.pktid;

              s_rdata       <= (others => '0');
              s_error       <= (others => '0');

              v_io_cell     := io_register_e'val(to_integer(unsigned(p_vci_io_tgt_in.addr(6 downto 2))));
              v_index       := 0;

              if p_vci_io_tgt_in.cmd = VCI_READ then
                 case v_io_cell is
                      when io_config =>
                           s_rdata <= config_2_vector(r_config);
 
                      when io_status =>
                           s_read_status <= '1';
                           s_rdata       <= status_2_vector(r_status);

                      -- JustForDebug
                      when io_pspe_0 | 
                           io_pspe_1 => 
                           v_index := to_integer(unsigned(p_vci_io_tgt_in.addr(6 downto 2))) - io_register_e'pos(io_pspe_0);
                           s_rdata <= rd_master_ctx.pspe(v_index);

                           assert (v_index < 2)
                           report "wrong pspe index from vci_address" severity failure;


                      when io_block_data_0 | 
                           io_block_data_1 | 
                           io_block_data_2 | 
                           io_block_data_3 | 
                           io_block_data_4 | 
                           io_block_data_5 | 
                           io_block_data_6 | 
                           io_block_data_7  =>
                           v_index := to_integer(unsigned(p_vci_io_tgt_in.addr(6 downto 2))) - io_register_e'pos(io_block_data_0);
 
                           assert (v_index < SIZE_LINE_WORDS)
                           report "wrong data block address" severity failure;

                           s_rdata <= r_data_block(v_index);

                      when io_gpi => s_rdata <= X"000000" & r_gpi(7 downto 0);
                                     s_gpi_ce <= '0';

                      when others => -- wrong io register address
                           s_error <= (0 => '1', others => '0');
                           s_rdata <= (others => '0');
                           s_state <= TGT_RSP;
                  end case;

                --if p_vci_io_tgt_in.eop = '1'  then
                  s_state <= TGT_RSP;
                --else
                --s_state <= IDLE;
                --end if;
                  

              elsif p_vci_io_tgt_in.cmd = VCI_WRITE then
                 case v_io_cell is
                      when io_config =>
                           s_config  <= vector_2_config(p_vci_io_tgt_in.wdata);

                      when io_pspe_0 | 
                           io_pspe_1 => 
                           v_index := to_integer(unsigned(p_vci_io_tgt_in.addr(6 downto 2))) - io_register_e'pos(io_pspe_0);
                           s_pspe(v_index) <= p_vci_io_tgt_in.wdata;

                           assert (v_index < 2)
                           report "wrong pspe index from vci_address" severity failure;

                      when io_sp_0 | io_sp_8 | 
                           io_sp_1 | io_sp_9 | 
                           io_sp_2 | io_sp_10| 
                           io_sp_3 | io_sp_11| 
                           io_sp_4 | io_sp_12| 
                           io_sp_5 | io_sp_13| 
                           io_sp_6 | io_sp_14| 
                           io_sp_7 | io_sp_15 => 
                           
                           v_index := to_integer(unsigned(p_vci_io_tgt_in.addr(6 downto 2))) - io_register_e'pos(io_sp_0);
                           s_sp(v_index) <= p_vci_io_tgt_in.wdata;

                      when io_block_data_0 | 
                           io_block_data_1 | 
                           io_block_data_2 | 
                           io_block_data_3 | 
                           io_block_data_4 | 
                           io_block_data_5 | 
                           io_block_data_6 | 
                           io_block_data_7  => 
                           v_index := to_integer(unsigned(p_vci_io_tgt_in.addr(6 downto 2))) - io_register_e'pos(io_block_data_0);
                           s_data_block(v_index) <= p_vci_io_tgt_in.wdata;

                           assert (v_index < SIZE_LINE_WORDS)
                           report "Wrong data block index from vci address" severity failure;

                     when io_block_addr =>
                           -- @todo : resize(vci_wdata, addr_size) 
                           s_addr_block <= p_vci_io_tgt_in.wdata;

                     when io_cmd  =>
                           assert (to_integer(unsigned(p_vci_io_tgt_in.wdata(2 downto 0))) < 5)
                           report "Bad command from vci wdata" severity failure;
                           s_cmd   <= io_cmd_cmd_e'val(to_integer(unsigned(p_vci_io_tgt_in.wdata(2 downto 0))));
                           --s_state <= IO_SEND_REQ;

                      when io_gpi => s_gpo <= p_vci_io_tgt_in.wdata(7 downto 0);

                     when others => -- wrong io register address
                           s_error <= (0 => '1', others => '0');
                           s_rdata <= (others => '0');
                           s_state <= TGT_RSP;
                 end case; 

               -- @replace with assert eop = '1'           
               --if p_vci_io_tgt_in.eop = '1' then 
                     if v_io_cell = io_cmd  then
                           s_state <= IO_SEND_REQ;
                     else
                           s_state <= TGT_RSP;
                     end if; 
               --else
               --     s_state <= IDLE;
               --end if;

              else    -- @todo : wrong vci command, to be handled
              -- if p_vci_io_tgt_in.eop = '1' then
                      s_state <= TGT_RSP;
              -- else
              --      s_state <= IDLE;
              -- end if;
              end if;
           end if; -- cmdval
 
       when IO_SEND_REQ =>
            if busy = '0' then
               s_state <= IO_WAIT;
            end if;

       when IO_WAIT =>
            if io_ack = '1' then
               if r_cmd = LOAD then
                    s_data_block <= io_handler_data_in.data;
               end if;
               s_state <= TGT_RSP;
            end if;
       when TGT_RSP =>   
            -- data valid in reg0 => send vci read response  
            if p_vci_io_tgt_in.rspack = '1' then
               s_state <= IDLE;
            end if;     
end case;

end process;

pGeneration : process(r_state, 
                      r_rdata, r_error     , 
                      r_srcid, r_trdid     , r_pktid     ,
                      r_cmd  , r_data_block, r_addr_block)
begin

p_vci_io_tgt_out      <= vci_t2i_none;

wr_io_handler.data    <= (others => (others => '0'));
wr_io_handler.address <= (others => '0');
wr_io_handler.cmd     <= NONE;

case r_state is
       when IDLE =>  
            p_vci_io_tgt_out.cmdack <= '1'; 

       when IO_SEND_REQ =>
            wr_io_handler.data    <= r_data_block;
            wr_io_handler.address <= r_addr_block;
            wr_io_handler.cmd     <= r_cmd;

       when IO_WAIT =>
            null;

       when TGT_RSP =>
            -- send vci rdata from data register 0
            p_vci_io_tgt_out.rspval <= '1';
            p_vci_io_tgt_out.rerror <= r_error;
            p_vci_io_tgt_out.reop   <= '1';
            p_vci_io_tgt_out.rdata  <= r_rdata;
            p_vci_io_tgt_out.rsrcid <= r_srcid;
            p_vci_io_tgt_out.rtrdid <= r_trdid;
            p_vci_io_tgt_out.rpktid <= r_pktid;

end case;

end process;

--
s_mb_address          <= std_ulogic_vector(shift_left(resize(unsigned(r_config.Mbba), addr_size), addr_size - 8));          

p_enable              <= r_config.En(r_config.En'right);
p_b_address           <= std_ulogic_vector(shift_left(resize(unsigned(r_config.Padd), addr_size) , addr_size - 8));          
p_mb_address          <= s_mb_address;           
p_cache_sp            <= r_config.Spce(r_config.Spce'right);
p_cache_pspe          <= r_config.Pce(r_config.Pce'right);
p_cache_ms            <= r_config.Msce(r_config.Msce'right);
p_cache_mt            <= r_config.Mtce(r_config.Mtce'right);

-- 
wr_irq_ack            <= not r_config.Ie(r_config.Ie'right) or r_read_status;

s_status.Busy(0)      <= busy;   
s_status.Errt         <= std_ulogic_vector(to_unsigned(io_status_errt_e'pos(rd_irq.typ), s_status.Errt'length));
s_status.Errc         <= std_ulogic_vector(to_unsigned(io_status_errc_e'pos(rd_irq.op) , s_status.Errc'length));
s_status.ErrAddr      <= rd_irq.addr(s_status.ErrAddr'range);

-- pragma translate off
genSyn : if false generate
-- pragma translate on
-- JustForDebug
--  p_size   <= X"00100000"; -- psiz = 1 => 64 MB ;   
    p_irq_c  <= '1' when rd_irq.irq = '1' and rd_irq.typ  = NONE else '0';
    p_irq_e  <= '1' when rd_irq.irq = '1' and rd_irq.typ /= NONE else '0'; 

    p_size   <= size_t(to_integer(unsigned(r_config.Psiz)));        
--  p_irq_c  <= '1' when r_config.Ie(r_config.Ie'right) = '1' and rd_irq.irq = '1' and rd_irq.typ  = NONE else '0';
--  p_irq_e  <= '1' when r_config.Ie(r_config.Ie'right) = '1' and rd_irq.irq = '1' and rd_irq.typ /= NONE else '0'; 
-- pragma translate off
end generate genSyn;
-- pragma translate on

-- for simulation debug : IRQs disabled 
-- pragma translate off
   p_size   <= X"04000000"; -- psiz = 1 => 64 MB ;   
   p_irq_c  <= '1' when rd_irq.irq = '1' and rd_irq.typ  = NONE else '0';
   p_irq_e  <= '1' when rd_irq.irq = '1' and rd_irq.typ /= NONE else '0'; 
-- pragma translate on

-- @todo : update master_ctx only when secbus not enabled
wr_master_ctx_init.enable  <= '1';
wr_master_ctx_init.pspe    <= r_pspe;
wr_master_ctx_init.sp      <= r_sp;

end rtl;


