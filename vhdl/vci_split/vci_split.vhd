--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- vci_split.vhd

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

entity vci_split is
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       p_enable              : in  std_ulogic;
       p_addr                : in  addr_t;     
       p_size                : in  addr_t;

       -- Vci Req from  CPU - target
       p_vci_ini_in          : in  vci_i2t;
       p_vci_ini_out         : out vci_t2i; 

       -- Vci Req to  Vci Merge (unsecure) - init
       p_vci_tgt_sec_out     : out vci_i2t;
       p_vci_tgt_sec_in      : in  vci_t2i;

       -- Vci Req to  Vci Input_Ctrl (secure) init
       p_vci_tgt_uns_out     : out vci_i2t;
       p_vci_tgt_uns_in      : in  vci_t2i

);
end vci_split;

architecture rtl of vci_split is
type fsm_state is (IDLE, CMD_SEC, CMD_UNS, RSP_SEC, RSP_UNS);

signal s_state, r_state : fsm_state;

begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state <= IDLE;
        elsif ce = '1' then
              r_state <= s_state;
        end if;
end if;
end process;

pTransition : process(r_state, p_vci_ini_in, p_addr, p_enable, p_size, p_vci_tgt_uns_in, p_vci_tgt_sec_in)
variable v_up_addr : addr_t;
variable v_addr_in : std_ulogic;
begin

s_state <= r_state;

case r_state is
     when IDLE =>
          if p_vci_ini_in.cmdval = '1' then
                 v_up_addr := std_ulogic_vector(unsigned(p_addr) + unsigned(p_size));
                 v_addr_in := inclusion(p_vci_ini_in.addr, p_addr, v_up_addr);
                 if (p_enable = '1') and (v_addr_in = '1') then
                         s_state <= CMD_SEC;
                 else
                         s_state <= CMD_UNS;
                 end if;
          else
                 s_state <= IDLE;
          end if;

     when CMD_UNS =>

          if (p_vci_ini_in.cmdval = '1' and p_vci_ini_in.eop = '1' and p_vci_tgt_uns_in.cmdack = '1') then 
                 s_state <= RSP_UNS;
          else
                 s_state <= CMD_UNS;
          end if;

     when CMD_SEC =>
          if (p_vci_ini_in.cmdval = '1' and p_vci_ini_in.eop = '1' and p_vci_tgt_sec_in.cmdack = '1') then 
                 s_state <= RSP_SEC;
          else
                 s_state <= CMD_SEC;
          end if;
      
     when RSP_UNS =>
          if (p_vci_tgt_uns_in.rspval = '1' and p_vci_tgt_uns_in.reop = '1' and p_vci_ini_in.rspack = '1') then
                  s_state <= IDLE;
          else
                  s_state <= RSP_UNS;
          end if;  

     when RSP_SEC =>
          if (p_vci_tgt_sec_in.rspval = '1' and p_vci_tgt_sec_in.reop = '1' and p_vci_ini_in.rspack = '1') then
                  s_state <= IDLE;
          else
                  s_state <= RSP_SEC;
          end if;
end case;

end process;


pGeneration : process(r_state, p_vci_tgt_sec_in, p_vci_tgt_uns_in, p_vci_ini_in)

begin

p_vci_tgt_sec_out <= vci_i2t_none;
p_vci_tgt_uns_out <= vci_i2t_none;
p_vci_ini_out     <= vci_t2i_none;

case r_state is 
         when IDLE =>
               null;

         when CMD_SEC => 
              p_vci_ini_out.cmdack     <= p_vci_tgt_sec_in.cmdack;
              p_vci_tgt_sec_out        <= p_vci_ini_in;

         when CMD_UNS =>
              p_vci_ini_out.cmdack     <= p_vci_tgt_uns_in.cmdack;
              p_vci_tgt_uns_out        <= p_vci_ini_in;

         when RSP_SEC =>         
              p_vci_tgt_sec_out.rspack <= p_vci_ini_in.rspack;
              p_vci_ini_out            <= p_vci_tgt_sec_in;
  
         when RSP_UNS =>         
              p_vci_tgt_uns_out.rspack <= p_vci_ini_in.rspack;
              p_vci_ini_out            <= p_vci_tgt_uns_in;

end case;

end process;

end rtl;

