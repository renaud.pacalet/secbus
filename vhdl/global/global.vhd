--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- global.vhd

library ieee;
use ieee.std_logic_1164.all;

library des_lib;
use des_lib.des_pkg.all;

use work.numeric_std.all;

package global is 

  -- #########################################
  -- ### PSPE Structures                   ###
  -- #########################################
  --type master_pspe_64 is
  --  record
  --    valid     : std_ulogic_vector(0  downto  0);  --          0
  --    size      : std_ulogic_vector(2  downto  1);  --          2  size indicator ( 3 = 4KB, 2 = 64KB, 1 = 1 MB, 0 = 16 MB)
  --    prot      : std_ulogic_vector(3  downto  3):  --          1  unprotected page
  --    spidx     : std_ulogic_vector(19 downto  4);  --          16 security policy index  (0 to 65535) 
  --    ividx     : std_ulogic_vector(21 downto 20);  --          2  IV set index in page   (0 to 3)
  --    ivadd     : std_ulogic_vector(41 downto 22);  --          20 IV page address
  --    msmtidx   : std_ulogic_vector(43 downto 42);  -- 11- 10   2  Mac Set Tree Index     (0, 1, 2)
  --    msmtadd   : std_ulogic_vector(63 downto 44);  -- 31- 12   20 Mac Set Tree page address
  --   end record; 
  --    
  --type slave_pspe_64 is
  --  record
  --    mac       : std_ulogic_vector(61 downto 0);
  --    size      : std_ulogic_vector(1  downto 0);  --  size indicator ( 3 = 4KB, 2 = 64KB, 1 = 1 MB, 0 = 16 MB)
  --  end record;

  -- #########################################
  -- ### SP Structure                      ###
  -- #########################################
  --type sp_512 is
  --  record
  --    valid     : std_ulogic;
  --    intmode   : std_ulogic_vector(1  downto 0);      -- Integrity mode       (0 = NONE, 1 = MAC, 2 = MACTREE)
  --    cnfmode   : std_ulogic_vector(1  downto 0);      -- Confidentiality mode (0 = NONE, 1 = CTR, 2 = CBC)
  --    dummy     : std_ulogic_vector(58 downto 0);      -- keys are 64 bits word aligned
  --    cnfk1     : std_ulogic_vector(63 downto 0);      -- Confidentiality DES-X k       Words 2 , 3 -- 56 bits real size 
  --    cnfk2     : std_ulogic_vector(63 downto 0);      -- Confidentiality DES-X k1            4 , 5 
  --    cnfk3     : std_ulogic_vector(63 downto 0);      -- Confidentiality DES-X k2      --    6 , 7 
  --    intk1     : std_ulogic_vector(63 downto 0);      -- Integrity       DES-X k             8 , 9 -- 56 bits real size 
  --    intk2     : std_ulogic_vector(63 downto 0);      -- Integrity       DES-X k1            10,11 
  --    intk3     : std_ulogic_vector(63 downto 0);      -- Integrity       DES-X k2      --    12,13 
  --    dummy     : std_ulogic_vector(63 downto 0);
  --  end record;      

  -- #########################################
  -- ### Configuration Register Addr : 0x0 ###
  -- #########################################
  type config_t is
    record
      Mbba              : std_ulogic_vector( 7 downto  0);     -- Master Block Base Address,  Aligned multiple of 16 MB
      En                : std_ulogic_vector( 8 downto  8);     -- Hsm Enable
      Ie                : std_ulogic_vector( 9 downto  9);     -- Interrupt_Enable 
      Pce               : std_ulogic_vector(10 downto 10);     -- Pspe_Cache_Enable
      Spce              : std_ulogic_vector(11 downto 11);     -- Sp_Cache_Enable  
      Msce              : std_ulogic_vector(12 downto 12);     -- MS_Cache_Enable  
      Mtce              : std_ulogic_vector(13 downto 13);     -- MT_Cache_Enable  
      Ive               : std_ulogic_vector(14 downto 14);     -- IV_Enable        Not used 
      Ivce              : std_ulogic_vector(15 downto 15);     -- IV_Cache_Enable  Not used
      padding           : std_ulogic_vector(20 downto 16);     -- 
      Psiz              : std_ulogic_vector(23 downto 21);     -- Size of protected memory area, 1 = 64 MB, 2 = 256 MB, 3 = 1 GB, 4 = 4 GB
      Padd              : std_ulogic_vector(31 downto 24);     -- Start address of protected memory, Aligned multiple of 16 MB
    end record;

    constant config_none : config_t := ((others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'),
                                        (others => '0'));

  -- #########################################
  -- ### Status Register Addr : 0x4        ###
  -- #########################################
  type status_t is   
    record
      Busy              : std_ulogic_vector(0  downto 0);       -- 0 : Idle, 1 : busy
      Errt              : std_ulogic_vector(3  downto 1);       -- io_status_errt_e;         
      Errc              : std_ulogic_vector(4  downto 4);       -- io_status_errc_e;                    
      ErrAddr           : std_ulogic_vector(31 downto 5);      --        
     end record;

  constant status_none : status_t := ((others => '0'),
                                      (others => '0'),
                                      (others => '0'),
                                      (others => '0'));

  type io_register_e is (
	io_config,        -- 0x00 
	io_status,        -- 0x04 
	 	         
	io_sp_0,          -- 0x08 
	io_sp_1,          -- 0x0c 
	io_sp_2,          -- 0x10 
	io_sp_3,          -- 0x14 
	io_sp_4,          -- 0x18 
	io_sp_5,          -- 0x1c 
	io_sp_6,          -- 0x20 
	io_sp_7,          -- 0x24 
	io_sp_8,          -- 0x28 
  	io_sp_9,          -- 0x2c 
	io_sp_10,         -- 0x30 
	io_sp_11,         -- 0x34 
	io_sp_12,         -- 0x38 
	io_sp_13,         -- 0x3c 
	io_sp_14,         -- 0x40 
	io_sp_15,         -- 0x44 
	 	         -- 
	io_pspe_0,        -- 0x48 
	io_pspe_1,        -- 0x4c 

	io_block_addr,    -- 0x50 
  
	io_block_data_0,  -- 0x54 
	io_block_data_1,  -- 0x58 
	io_block_data_2,  -- 0x5c 
	io_block_data_3,  -- 0x60 
	io_block_data_4,  -- 0x64 
	io_block_data_5,  -- 0x68 
	io_block_data_6,  -- 0x6c 
 	io_block_data_7,  -- 0x70 

	io_cmd,           -- 0x74 
	io_gpi            -- 0x78 
      );



  -- compute word index in 32 bytes line (256 bits) from an address
  -- in systemC : function index(addr, size_line_bytes) in vci_seclib.cpp
  --   addr ---------------------------------------------------
  --        |                             | word      | byte  |
  --        |                             | index     | index |
  --        ---------------------------------------------------
  --                                        4        2  1    0   
  --                                      <--- LINE_BITS ---->                    

  constant BC_RND_BITS  : natural := 0; -- log2 of rounds per clock cycle
  constant LINE_BITS    : natural := 5; -- log2(SIZE_LINE_BYTES) 
  constant CELL_BITS    : natural := 2; -- log2(CELL_SIZE)
  constant WORD_BITS    : natural := LINE_BITS - CELL_BITS;

  -- vci params
  constant CELL_SIZE    : natural := 4;
  constant PLEN_SIZE    : natural := 6; --  log2(SIZE_LINE_BYTES), 
  constant ADDR_SIZE    : natural := 32;
  constant DATA_SIZE    : natural := CELL_SIZE*8;
  constant RERROR_SIZE  : natural := 2;
  constant CLEN_SIZE    : natural := 1;
  constant SRCID_SIZE   : natural := 6;   
  constant TRDID_SIZE   : natural := 6;
  constant PKTID_SIZE   : natural := 4;
  constant WRPLEN_SIZE  : natural := 1;
 

  type vci_cmd_e is (VCI_NOP, VCI_READ, VCI_WRITE, VCI_LOCKED_READ);

  subtype addr_t   is std_ulogic_vector(ADDR_SIZE   - 1 downto 0);
  subtype data_t   is std_ulogic_vector(DATA_SIZE   - 1 downto 0);
  subtype be_t     is std_ulogic_vector(CELL_SIZE   - 1 downto 0);
  subtype plen_t   is std_ulogic_vector(PLEN_SIZE   - 1 downto 0);
  subtype rerror_t is std_ulogic_vector(RERROR_SIZE - 1 downto 0);
  subtype srcid_t  is std_ulogic_vector(SRCID_SIZE  - 1 downto 0);
  subtype trdid_t  is std_ulogic_vector(TRDID_SIZE  - 1 downto 0);
  subtype pktid_t  is std_ulogic_vector(PKTID_SIZE  - 1 downto 0);


  type data_vector is array(natural range <>) of data_t;
  type be_vector   is array(natural range <>) of be_t;


  -- VCI INITIATOR TO TARGET
  type vci_i2t is
    record
      rspack    : std_ulogic;
      cmdval    : std_ulogic;
      addr      : addr_t;
      cmd       : vci_cmd_e;
      wdata     : data_t;    
      be        : be_t;
      plen      : plen_t;
      eop       : std_ulogic;
      cons      : std_ulogic;
      contig    : std_ulogic;
      wrap      : std_ulogic;
      cfixed    : std_ulogic;
      clen      : std_ulogic;
      srcid     : srcid_t;
      trdid     : trdid_t;
      pktid     : pktid_t;
    end record;

  --------------------------------
  -- VCI TARGET TO INITIATOR
  type vci_t2i is
    record
      cmdack    : std_ulogic;
      rspval    : std_ulogic;
      rdata     : data_t;
      reop      : std_ulogic;
      rerror    : rerror_t;
      rsrcid    : srcid_t;
      rtrdid    : trdid_t;
      rpktid    : pktid_t;
    end record;

  type vci_i2t_vector is array(natural range <>) of vci_i2t;
  type vci_t2i_vector is array(natural range <>) of vci_t2i;

  constant vci_i2t_none: vci_i2t := (
                          '0',                 --rspack    
                          '0',                 --cmdval       
                          (others => '0'),     --addr          
                          VCI_NOP,             --cmd              
                          (others => '0'),     --wdata         
                          (others => '0'),     --be               
                          (others => '0'),     --plen               
                          '0',                 --eop          
                          '0',                 --cons         
                          '0',                 --contig       
                          '0',                 --wrap        
                          '0',                 --cfixed     
                          '0',                 --clen      
                          (others => '0'),     --srcid             
                          (others => '0'),     --trdid              
                          (others => '0')      --pktid     
                        );

  constant vci_t2i_none: vci_t2i := (
                          '0',                 --cmdack  
                          '0',                 --rspval  
                          (others => '0'),     --rdata    
                          '0',                 --reop    
                          (others => '0'),     --rerror    
                          (others => '0'),     --rsrcid  
                          (others => '0'),     --rtrdid  
                          (others => '0')      --rpktid  
                        );

  constant SIZE_LINE_BITS  : natural := 256;
  constant SIZE_LINE_BYTES : natural := SIZE_LINE_BITS/8;         -- 32
  constant SIZE_LINE_WORDS : natural := SIZE_LINE_BITS/DATA_SIZE; -- 8

  constant SP_SIZE_WORDS   : natural := 16;
  constant SP_SIZE_BYTES   : natural := 64;
  constant SP_BITS         : natural := 6; --log2(SP_SIZE_BYTES)

  constant PSPE_SIZE_WORDS : natural := 2;
  constant PSPE_SIZE_BYTES : natural := 8;
  constant PSPE_BITS       : natural := 3; -- log2(PSPE_SIZE_BYTES)

  -- MT cache params
  constant MT_CACHE_WAYS   : natural := 8;
  constant MT_CACHE_SETS   : natural := 64;
  constant MT_CACHE_BLKS   : natural := 2;
  constant MT_CACHE_TSH    : natural := MT_CACHE_WAYS/2;

  -- MS cache params
  constant MS_CACHE_WAYS   : natural := 4;
  constant MS_CACHE_SETS   : natural := 64;   
  constant MS_CACHE_BLKS   : natural := 2;

  -- PSPE cache params
  constant PSPE_CACHE_SETS : natural := 16;
  constant PSPE_CACHE_BLKS : natural := 2;

  -- SP cache params
  constant SP_CACHE_WAYS   : natural := 2;
  constant SP_CACHE_SETS   : natural := 64;
  constant SP_CACHE_BLKS   : natural := SP_SIZE_WORDS/2;

  -- registers param
  constant REG_MAX         : natural := 5;
  constant REG_SIZE        : natural := 3; -- up to 8 registers (only 5 used)

  type sp_t   is array(SP_SIZE_WORDS   - 1 downto 0) of data_t;
  type pspe_t is array(PSPE_SIZE_WORDS - 1 downto 0) of data_t;

  subtype reg_t is std_ulogic_vector(REG_SIZE - 1 downto 0);

   ----
  type size_array is array (0 to 4) of addr_t;
  --                                  0           64 MB         256MB         1 GB        2GB
  constant size_t : size_array := (X"00000000", X"04000000", X"10000000", X"40000000", X"80000000");

  constant pageLevel  : natural := 4;         
  -- pageSize[4] = { 4 KB, 64 KB, 1 MB, 16 MB} 
  -- since here we need to divide we only consider the number of shifts to be performed
  type page_array is array(3 downto 0) of integer range 0 to 24;
  type mask_array is array(3 downto 0) of addr_t;                  
  constant shift_page     : page_array := (12, 16, 20, 24);
  constant page_mask      : mask_array := (X"00000FFF", X"0000FFFF", X"000FFFFF", X"00FFFFFF");
  -- page[size]/page[level - 1] => shift nb_pages
  constant shift_nb_pages : page_array := (0, 4, 8, 12);

  -- DATA_OUT
  type data_out is
    record
      enable      : std_ulogic;
      data        : data_vector(SIZE_LINE_WORDS - 1 downto 0);
    end record;

  -- DATA_OUT
  type direct_data is
    record
      enable      : std_ulogic;
      data        : data_vector(1 downto 0);
    end record;

  type direct_data_array is array(natural range <>) of direct_data;

  constant pspe_none : pspe_t := (others => (others => '0'));
  constant sp_none   : sp_t   := (others => (others => '0'));

   -- MASTER_CTX_REG_READ
  type master_ctx_reg_read is
    record
      enable         : std_ulogic;   
      pspe           : pspe_t;       --data_vector(PSPE_SIZE_WORDS - 1 downto 0);
      sp             : sp_t;         --data_vector(SP_SIZE_WORDS   - 1 downto 0);
    end record;

  constant master_ctx_reg_read_none: master_ctx_reg_read := (enable => '0', pspe => pspe_none, sp => sp_none);

  -- IO_BLOCK:
  type io_cmd_cmd_e is (NONE, LOAD, STORE, INIT, CONTINUE);

  type io_block is
    record
      data          : data_vector(SIZE_LINE_WORDS - 1 downto 0);
      cmd           : io_cmd_cmd_e;
      address       : addr_t;
    end record;


  -- in systemc model each state has additional prefix io_status_e_
  type io_status_errt_e is (NONE, PSPE, SP, MAC, HT);
  type io_status_errc_e is (LOAD, STORE);

  type irq_out is
    record
      addr       : addr_t;
      irq        : std_ulogic;
      typ        : io_status_errt_e; -- called 'type' in systemc
      op         : io_status_errc_e;
    end record;

  type irq_out_array is array(natural range <>) of irq_out;

  -- DATA_REGISTER_READ
  type data_register_read is
    record     
      addr         : addr_t;
      data         : data_vector(SIZE_LINE_WORDS - 1 downto 0);
      be           : be_vector(SIZE_LINE_WORDS - 1 downto 0);
    end record;
  
  type data_register_read_array is array(natural range <>) of data_register_read;

  -- DATA_REGISTER_WRITE
  type data_register_write is
    record
      enable       : std_ulogic;
      addr         : addr_t;
      flush        : std_ulogic;
      data         : data_vector(SIZE_LINE_WORDS - 1 downto 0);
      be           : be_vector(SIZE_LINE_WORDS - 1 downto 0);
    end record;

  type data_register_write_array is array(natural range <>) of data_register_write;

  -- MEMORY_CTRL_CMD
  type cmd_mem_e is (READ, WRITE);

  type memory_ctrl_cmd_out is -- output signals to memory_ctrl_cmd_master
    record
      cmdval        : std_ulogic;
      start_address : addr_t;
      plen          : plen_t;
      cmd           : cmd_mem_e;
      id            : std_ulogic_vector(REG_MAX - 1 downto 0);  -- one hot encoding
      direct_write  : std_ulogic;
    end record;

  type memory_ctrl_cmd_in is -- input signals from memory_ctrl_cmd_master
    record
      cmdack       : std_ulogic;
      rspval       : std_ulogic;
    end record;

  type memory_ctrl_cmd_out_array is array(natural range <>) of memory_ctrl_cmd_out; 
  type memory_ctrl_cmd_in_array  is array(natural range <>) of memory_ctrl_cmd_in;

  -- CTX_REGISTER_READ:
  type ctx_register_read is
    record
      page_addr      : addr_t;       -- master pspe address
      s_pspe_addr    : addr_t;       -- slave  pspe address
      m_pspe         : pspe_t;       -- data_vector(PSPE_SIZE_WORDS - 1 downto 0);
      s_pspe         : pspe_t;       -- data_vector(PSPE_SIZE_WORDS - 1 downto 0);
      sp             : sp_t;         -- data_vector(SP_SIZE_WORDS   - 1 downto 0);
    --s_page_addr    : addr_t;       -- not used
    end record;

  constant ctx_none  : ctx_register_read := (
                                          (others => '0'),    -- page_address 
                                          (others => '0'),    -- s_pspe_address
                                          pspe_none,         -- m_pspe
                                          pspe_none,         -- s_pspe
                                          sp_none            -- sp
                                        );


  -- SECURITY CTX CMD
  type ctx_cmd_e is (READ, UPDATE);

  type security_ctx_cmd_out is
    record
      enable       : std_ulogic;
      address      : addr_t;
      cmd          : ctx_cmd_e;
      mt           : std_ulogic;
    end record;
 
  type security_ctx_cmd_in is
    record
      cmdack       : std_ulogic;  -- @ in systemc ack
      rspval       : std_ulogic;  -- @ not in systemc 
    end record;

  type security_ctx_cmd_out_array is array(natural range <>) of security_ctx_cmd_out;
  type security_ctx_cmd_in_array  is array(natural range <>) of security_ctx_cmd_in;

  -- SECURITY_REQ_CMD
  type security_rq_in is
    record
      cmdack        : std_ulogic;
      rspval        : std_ulogic;
    end record;

  type security_rq_out is
    record
      cmdval     : std_ulogic;
      address    : addr_t;
      cmd        : cmd_mem_e;
      mb         : std_ulogic;
    end record;

  type security_rq_out_array is array(natural range <>) of security_rq_out;
  type security_rq_in_array  is array(natural range <>) of security_rq_in;


  constant security_rq_out_none  : security_rq_out := ('0',             -- cmdval
                                                       (others => '0'), -- address 
                                                        READ,           -- cmd
                                                        '0'             -- mb
                                                        );

  -- INTEGRITY_CTRL_CMD:
  type cmd_integrity_e is (CHECK, UPDATE, INITIALIZE, INITIALIZE_PAGE);

  type integrity_ctrl_cmd is
    record
      enable         : std_ulogic;
      addr           : addr_t;
      cmd            : cmd_integrity_e;
      src            : reg_t;
    end record;

  -- INTEGRITY_CTRL_ACK:
  type integrity_ctrl_ack is
    record
      cack           : std_ulogic;
      uack           : std_ulogic;
      alap_ack       : std_ulogic;
      iack           : std_ulogic;
    end record;

  type integrity_ctrl_cmd_array is array(natural range <>) of integrity_ctrl_cmd;
  type integrity_ctrl_ack_array is array(natural range <>) of integrity_ctrl_ack;

  -- CRYPTO_CMD

-- type sp_mode_c     is (MODE_C_NONE, MODE_C_CTR, MODE_C_CBC);
-- type sp_mode_i     is (MODE_I_NONE, MODE_I_MAC, MODE_I_MACTREE);

  constant MODE_I_NONE    : std_ulogic_vector(1 downto 0) := "00";
  constant MODE_I_MAC     : std_ulogic_vector(1 downto 0) := "01";
  constant MODE_I_MACTREE : std_ulogic_vector(1 downto 0) := "10";

  constant MODE_C_NONE    : std_ulogic_vector(1 downto 0) := "00";
  constant MODE_C_CTR     : std_ulogic_vector(1 downto 0) := "01";
  constant MODE_C_CBC     : std_ulogic_vector(1 downto 0) := "10";

  type crypto_mode_e is (CRY_CTR, CRY_CBC, CRY_MAC);

  constant key_null : desx_key := ((others => '0'), (others => '0'), (others => '0'));

  type crypto_cmd_out is       -- output signals to crypto_engines (conf & integrity)
    record
      cmdval       : std_ulogic;
      address      : addr_t;
      cipher       : std_ulogic;  
      direct_data  : std_ulogic;
      no_init      : std_ulogic;
      key          : desx_key;
      iv           : des_word_64;
      size         : std_ulogic_vector(3 downto 0); --  up to 16 word_32 = 6 Mac Set Blocks     
      id_src       : reg_t; 
      id_dest      : reg_t;
      mode         : crypto_mode_e;
      data_ready   : std_ulogic;  
--    enable       : std_ulogic;
    end record;
  
  type crypto_cmd_in is       -- input signals from crypto_engines (conf & integrity)
    record
      cmdack       : std_ulogic;
      rspval       : std_ulogic;
    end record;

  type crypto_cmd_out_array is array(natural range <>) of crypto_cmd_out; 
  type crypto_cmd_in_array  is array(natural range <>) of crypto_cmd_in;

  constant HASH_ARITY      : natural := 4;
  constant LAST_HASH_ARITY : natural := 6;

  -- MT_CACHE  
  type cmd_mt_cache_e is (READ, WRITE, SYNCHRONIZE, RESTORE, INVALIDATE, IMMEDIATE);

  type cache_ctrl_cmd is
  record
     enable        : std_ulogic;
     address       : addr_t;
     cmd           : cmd_mt_cache_e; 
  end record;

  type cache_ctrl_ack is
  record       
     ack           : std_ulogic;
     full          : std_ulogic;
     hit           : std_ulogic;
     dirty         : std_ulogic;
     victim_addr   : addr_t;
  end record;

  constant cache_ctrl_ack_none : cache_ctrl_ack := ('0', '0', '0', '0', (others => '0'));

  -- functions  
  function inclusion(addr, low_addr, up_addr : addr_t) return std_ulogic;
  function be_arr_2_vect(be_array : be_vector) return unsigned; 
  function ctz(vect : unsigned) return natural;
  function clz(vect : unsigned) return natural;
  function line_aligned(addr  : std_ulogic_vector) return std_ulogic_vector;
  function word_index(addr : std_ulogic_vector)    return natural;
  function set_lru(idx_vect, lru_in : unsigned) return unsigned;
  function onehot_encoder(p_in : unsigned)      return unsigned;
  function isOneHot(v : unsigned)         return boolean;
  function log2_up(x : natural)             return natural;
  function log10_down(x : positive) return natural;
  function log10_up(x : positive)             return natural;
  function status_2_vector(S : status_t)  return data_t;
  function vector_2_status(L : data_t)    return status_t;
  function config_2_vector(C : config_t)  return data_t;
  function vector_2_config(L : data_t)    return config_t;
  function getPSPEAddressFromPageSize(addr : addr_t; size : natural; mb_addr, b_addr : addr_t) return addr_t;
  function getSPBaseAddress(mb_address , size : addr_t) return addr_t;   
  function getSPAddressFromIdx(sp_idx : std_ulogic_vector; mb_address , size : addr_t) return addr_t;
  function getMMacTBaseAddress(mb_address , size : addr_t) return addr_t;
  function sp_getModeC(s : sp_t) return std_ulogic_vector;
  function sp_getModeI(s : sp_t) return std_ulogic_vector;
  function sp_getKeyC(s  : sp_t) return desx_key;
  function sp_getKeyI(s  : sp_t) return desx_key;
  function pspe_getSize(p : pspe_t) return natural;
  function pspe_getMSAdd(p : pspe_t) return addr_t;
  function pspe_getMSIdx(p : pspe_t) return natural;
  function pspe_getProt(p : pspe_t) return std_ulogic;
  function pspe_getValid(p : pspe_t) return std_ulogic;
  function sp_getValid(s : sp_t) return std_ulogic;
  function pspe_getSPIdx(p : pspe_t) return std_ulogic_vector;
  function getPageSize(index : natural) return addr_t;
  function ms_getMacAddress(address : addr_t; p : pspe_t) return addr_t;
  function vec2str(vec : std_ulogic_vector) return string;

  -- Mt functions
  function getBlockIndex(address, mb_addr, size : addr_t; level : natural) return unsigned;
  function getSetAddress(address, mb_addr, size : addr_t; level : natural) return addr_t;    
  function getBrotherAddress(address, mb_addr, size : addr_t; level: natural) return addr_t;
  function isMasterTree(address, mb_address, size : addr_t) return std_ulogic;
  function isLastLevel(address, mb_addr, size : addr_t; level : natural) return boolean;
  function getMMTSize(size : addr_t) return unsigned; 
  function getLevelFromSize(size : addr_t) return natural;
  function getParentLeavesAddress(address : addr_t; p : pspe_t) return addr_t;
  function getParentAddress(address, mb_addr, size : addr_t; level : natural) return addr_t;
  function getMTParentLeavesAddress(address, mb_address, size : addr_t) return addr_t;

end package global;

package body global is

  function inclusion(addr, low_addr, up_addr: addr_t) return std_ulogic is
  variable v_addr_in : std_ulogic;
  begin
    if (unsigned(addr) >= unsigned(low_addr) and unsigned(addr) < unsigned(up_addr)) then  
       v_addr_in := '1';
    else
       v_addr_in := '0';
    end if;
    return v_addr_in;
  end function;

  function be_arr_2_vect(be_array : be_vector) return unsigned is 
  variable be_vect : unsigned(SIZE_LINE_BYTES - 1 downto 0);
  begin
      for i in be_array'range loop
          be_vect((i+1)*CELL_SIZE - 1 downto i*CELL_SIZE) := unsigned(be_array(i));     
      end loop;
      return be_vect;
     
  end function;

  function ctz(vect : unsigned) return natural is
  begin
      for i in vect'reverse_range loop
          if vect(i) = '1' then
             return i;
          end if;
      end loop;
      return 0;
  end function;

  function clz(vect : unsigned) return natural is
  begin
      for i in vect'reverse_range loop
          if vect(vect'left - i) = '1' then
             return i;
          end if;
      end loop;
      return 0;
  end function;

  function line_aligned(addr : std_ulogic_vector)  return std_ulogic_vector is
   variable addr_ret : addr_t; --std_ulogic_vector(LINE_BITS - 1 downto 0);
   begin
    addr_ret := (others => '0');
    addr_ret(addr'left downto LINE_BITS) := addr(addr'left downto LINE_BITS);
    return addr_ret;
  end function;

  function word_index(addr : std_ulogic_vector) return natural is
  begin
    return to_integer(unsigned(addr(LINE_BITS - 1 downto CELL_BITS)));
  end function;

 -- pseudo lru function
 function set_lru(idx_vect, lru_in : unsigned) return unsigned is
 variable v_lru :  unsigned(idx_vect'left downto 0);
 begin
     v_lru := lru_in;
     for i in idx_vect'reverse_range loop
         if idx_vect(i) = '1' then
            v_lru(i) := '1';
         end if; 
     end loop;       

     for i in idx_vect'reverse_range loop
         if idx_vect(i) = '0' and lru_in(i) ='0' then
            return v_lru;
         end if; 
     end loop;       

     -- all lines are new => they all become old
     for i in idx_vect'reverse_range loop
            v_lru(i) := '0';
     end loop;       
     return v_lru;  
 end function;

 function onehot_encoder(p_in : unsigned) return unsigned is 
 variable v_out : unsigned(p_in'range);
 begin
      v_out := (others => '0');
      for i in p_in'reverse_range loop
          if p_in(i) = '1' then
             v_out(i) := '1';
          end if;
          exit when p_in(i) = '1';
      end loop;
      return v_out;
 end function;

 function log2_up(x : natural) return natural is
 begin
      if x <= 1 then 
         return 0;
      else
         return log2_up((x+1)/2) + 1;
      end if;
 end function;

 function log10_down(x : positive) return natural is
 begin
   if x <= 9 then 
     return 0;
   else
     return log10_down(x / 10) + 1;
   end if;
 end function;

 function log10_up(x : positive) return natural is
 begin
   return log10_down(x) + 1;
 end function;

 function isOneHot(v : unsigned) return boolean is
   variable tmp: boolean := false;
 begin
   for i in v'range loop
     if v(i) = '1' then
       if tmp then
         return false;
       else
         tmp := true;
       end if;
     end if;
   end loop;
   return tmp;
 end function isOneHot;

 function config_2_vector(C : config_t) return data_t is
     variable L : data_t;
     begin
         L := (others => '0');
         L(C.Mbba'range)    := C.Mbba;
         L(C.En'range)      := C.En;
         L(C.Ie'range)      := C.Ie;
         L(C.Pce'range)     := C.Pce;
         L(C.Spce'range)    := C.Spce;
         L(C.Msce'range)    := C.Msce;
         L(C.Mtce'range)    := C.Mtce;
         L(C.Ive'range)     := C.Ive;
         L(C.Ivce'range)    := C.Ivce;
         L(C.Psiz'range)    := C.Psiz;
         L(C.Padd'range)    := C.Padd;
     return L;
 end function;

 function vector_2_config(L : data_t) return config_t is
     variable C : config_t;
     begin
        C.Mbba    := L(C.Mbba'range);
        C.En      := L(C.En'range)  ;
        C.Ie      := L(C.Ie'range)  ;
        C.Pce     := L(C.Pce'range) ;
        C.Spce    := L(C.Spce'range);
        C.Msce    := L(C.Msce'range);
        C.Mtce    := L(C.Mtce'range);
        C.Ive     := L(C.Ive'range) ;
        C.Ivce    := L(C.Ivce'range);
        C.Psiz    := L(C.Psiz'range);
        C.Padd    := L(C.Padd'range);
        C.Padding := (others => '0');
     return C;
 end function;

 function status_2_vector(S : status_t) return data_t is
     variable L : data_t;
     begin
         L := (others => '0');
         L(S.Busy'range)    := S.Busy;
         L(S.Errt'range)    := S.Errt;
         L(S.Errc'range)    := S.Errc;
         L(S.ErrAddr'range) := S.ErrAddr;
     return L;
 end function;

 function vector_2_status(L : data_t) return status_t is
     variable S : status_t;
     begin
         S.Busy     := L(S.Busy'range);
         S.Errt     := L(S.Errt'range);
         S.Errc     := L(S.Errc'range);
         S.ErrAddr  := L(S.ErrAddr'range);
     return S;
 end function;

 function getPSPEAddressFromPageSize(addr : addr_t; size : natural; mb_addr, b_addr: addr_t) return addr_t is
    variable v_offset  : unsigned(ADDR_SIZE - 1 downto 0);         
 begin
    -- C version is: return mb_address + ((uint32_t)((address-b_address)/pageSize[size]))*PSPE_SIZE_IN_BYTES*(pageSize[size]/pageSize[pageLevel-1]);
    v_offset :=  shift_left(shift_right(unsigned(addr) - unsigned(b_addr), shift_page(size)), PSPE_BITS + shift_nb_pages(size)); 
    
    return std_ulogic_vector(unsigned(mb_addr) + v_offset);
 end function;

  -- p_mb (pspe base addr)        sp base addr              MMac Tree base addr
  -- ----------------------------------------------------------------------------------------
  -- |      PSPEs                 |    SPs  (PSPEs size/2)  |    MacTrees (PSPEs size /2)   |
  -- ----------------------------------------------------------------------------------------      
  -- <---------------------------------- Master Mac Tree size (MMT Size) ------------------->

  function getSPBaseAddress(mb_address , size : addr_t) return addr_t is
  variable v_addr , v_pspe_size : unsigned(ADDR_SIZE - 1 downto 0);
    --C version : uint32_t nb_pspe = size / (pageSize[pageLevel - 1]) + 1;
    --            return unsigned(mb_address) + nb_pspe * PSPE_SIZE_IN_BYTES;  
  begin
  --v_pspe_size := resize((unsigned(size(size'left downto shift_page(pageLevel -  1))) & (PSPE_BITS - 1 downto 0 => '0')), ADDR_SIZE); 
    v_pspe_size := resize((unsigned(size(size'left downto shift_page(pageLevel -  1))) & "000"), ADDR_SIZE); 
    return std_ulogic_vector(unsigned(mb_address) + v_pspe_size);
  end function;

  function getSPAddressFromIdx(sp_idx : std_ulogic_vector; mb_address , size : addr_t) return addr_t is
  begin
    --return getSPBaseAddress(mb_address, size) + sp_idx * SP_SIZE_IN_BYTES;
    return std_ulogic_vector(unsigned(getSPBaseAddress(mb_address, size)) + shift_left(unsigned(sp_idx), SP_BITS));
  end function;

  -- @to do : in systemC : size bits  : 2 downto 1 
  --          in spec                 : 3 downto 2
  --          see master_pspe record structure above
  function pspe_getSize(p : pspe_t) return natural is
  begin
    return to_integer(unsigned(p(0)(2 downto 1)));  
  end function;

 function getMMacTBaseAddress(mb_address , size : addr_t) return addr_t is
  variable v_addr , v_pspe_size, v_sp_size : unsigned(addr_size - 1 downto 0);
  begin
  --v_addr := std_ulogic_vector(unsigned(mb_address) + (unsigned(getSPBaseAddress(mb_address, size)) - unsigned(mb_address))*3/2);
  --v_pspe_size := resize((unsigned(size(size'left downto shift_page(pageLevel -  1))) & (PSPE_BITS - 1 downto 0 => '0')), ADDR_SIZE); 
    v_pspe_size := resize((unsigned(size(size'left downto shift_page(pageLevel -  1))) & "000"), ADDR_SIZE); 
  --v_sp_size   := shift_right(v_pspe_size, 1);
    v_sp_size   := '0' & v_pspe_size(v_pspe_size'left downto 1); -- sp_size = pspe_size / 2
 
    v_addr := unsigned(mb_address) + v_pspe_size + v_sp_size;
    return std_ulogic_vector(v_addr);
  end function;

  function pspe_getMSAdd(p : pspe_t) return addr_t is
  variable v_addr : addr_t;
  begin
      v_addr := (others => '0');
      v_addr(31 downto 12) := p(1)(31 downto 12);
      return v_addr; 
  end function;

  function pspe_getMSIdx(p : pspe_t) return natural is
  begin
      return to_integer(unsigned(p(1)(11 downto 10)));
  end function;

  function pspe_getProt(p : pspe_t) return std_ulogic is
  begin
      return p(0)(3);
  end function;

  function pspe_getValid(p : pspe_t) return std_ulogic is
  begin
      return p(0)(0);
  end function;

  function sp_getValid(s : sp_t) return std_ulogic is
  begin
      return s(0)(0);
  end function;

  function pspe_getSPIdx(p : pspe_t) return std_ulogic_vector is
  begin
      return p(0)(19 downto 4);
  end function;
  
  function getPageSize(index : natural) return addr_t is
  variable v_addr : addr_t;
  begin
     v_addr := (others => '0');  
     v_addr(shift_page(index)) := '1';  
     return v_addr;
  end function;

 function ms_getMacAddress(address : addr_t; p : pspe_t) return addr_t is
  variable v_level        : natural;
  variable v_page_address : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_addr         : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_page_offset  : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_page_index   : unsigned(2*ADDR_SIZE - 1 downto 0); 
  begin
    v_level        := pspe_getSize(p);
    v_page_index   := shift_right(unsigned(getPageSize(v_level)),2)*pspe_getMSIdx(p);
    v_page_address := unsigned(pspe_getMSAdd(p)) + v_page_index(ADDR_SIZE - 1 downto 0);
    v_page_offset  := unsigned(address) and unsigned(page_mask(v_level));
    v_addr         := v_page_address + (v_page_offset(v_page_address'left downto LINE_BITS) & "000");

    return std_ulogic_vector(v_addr);
  end function;

--
-- function ms_getMacAddress(address : addr_t;  pspe : pspe_t) return addr_t is
-- begin
--    page_address = pspe_getMSAdd(&mpspe) +
--                  (getPageSize(pspe_getSize(&mpspe))/4)*pspe_getMSIdx(&mpspe);
--
--    level        = pspe_getSize(&mpspe);
--    uint32_t mask = 0xFFFFFFFF >> (32 - _ctz(getPageSize(level), 32));
--    uint32_t page_offset = address&mask;
--
--    return page_address + page_offset/MS_SIZE_IN_BYTES*SIZE_64_IN_BYTES;
-- end function;
--
 function sp_getModeI(s : sp_t) return std_ulogic_vector is
 begin       
       return s(0)(2 downto 1);
 end function;

 function sp_getModeC(s : sp_t) return std_ulogic_vector is
 begin       
       return s(0)(4 downto 3);
 end function;

 function sp_getKeyI(s : sp_t) return desx_key is
 variable vkey : desx_key;
 begin
          vkey.k2 := s(13) & s(12);             -- DESX 64
          vkey.k  := s(9)(23 downto 0) & s(8);  -- DESX 56
          vkey.k1 := s(11) & s(10);             -- DESX 64
          return vkey;  
 end function;

 function sp_getKeyC(s : sp_t) return desx_key is
 variable vkey : desx_key;
 begin
          vkey.k1 := s(5) & s(4);               -- DESX 64
          vkey.k  := s(3)(23 downto 0) & s(2);  -- DESX 56
          vkey.k2 := s(7) & s(6);               -- DESX 64
          return vkey;  
 end function;

-- function convert vector to string 
-- for use in assert statements
  function vec2str(vec : std_ulogic_vector) return string is
  variable stmp : string(vec'left+1 downto 1);
  
  begin
  
      for i in vec'reverse_range loop
          if vec(i) = '1' then
               stmp(i+1) := '1';
          elsif vec(i) = '0' then
               stmp(i+1) := '0';
          else
               stmp(i+1) := 'x';
          end if;
      end loop;
      
      return stmp;
  
  end vec2str;

  -- Mt functions
  function getSetAddress(address, mb_addr, size : addr_t; level : natural) return addr_t  is
  variable v_isLast : Boolean;
  variable v_addr   : unsigned(ADDR_SIZE - 1 downto 0);                              
  begin
        v_isLast := isLastLevel(address, mb_addr, size, level);
        v_addr   := unsigned(address);
        v_addr(4 downto 0) := (others => '0');

        if v_isLast then        
            v_addr(5):= '0'; 
        end if;    
        return std_ulogic_vector(v_addr); 
  end function;

  function isMasterTree(address, mb_address, size : addr_t) return std_ulogic is
  variable v_isMaster : std_ulogic;
  variable v_mmt_addr : unsigned(ADDR_SIZE - 1 downto 0);     
  begin
         v_mmt_addr := unsigned(mb_address) + getMMTSize(size); 
         v_isMaster := inclusion(address, mb_address, std_ulogic_vector(v_mmt_addr));
  return v_isMAster;                                                       
  end function;

  function isLastLevel(address, mb_addr, size : addr_t; level : natural) return boolean is
  variable v_offset : unsigned(ADDR_SIZE - 1 downto 0);
  begin
         if isMasterTree(address, mb_addr, size) = '1' then
            v_offset :=  unsigned(address) - unsigned(getMMacTBaseAddress(mb_addr, size));
         else
            v_offset  := unsigned(address) and unsigned(page_mask(level));
         end if;

         return v_offset < 2*HASH_ARITY*PSPE_SIZE_BYTES;
  end function;

  function getBlockIndex(address, mb_addr, size : addr_t; level : natural) return unsigned is
  variable v_isLast : Boolean;
  variable v_index  : unsigned(2 downto 0);
  begin
        v_isLast := isLastLevel(address, mb_addr, size, level);
        v_index  := (others => '0');

        if v_isLast then
           v_index := unsigned(address(5 downto 3)); -- address & 0x38 >> 3
        else
           v_index(1 downto 0) := unsigned(address(4 downto 3)); -- address & 0x18 >> 3
        end if;
        return v_index;
  end function;

  function getBrotherAddress(address, mb_addr, size : addr_t; level: natural) return addr_t is
  variable v_index  : unsigned(2 downto 0);
  variable v_isLast : Boolean;
  variable v_addr   : unsigned(ADDR_SIZE - 1 downto 0);
  variable v_arity  : natural;
  begin

        v_isLast := isLastLevel(address, mb_addr, size, level);
        if v_isLast then
            v_arity := LAST_HASH_ARITY;
        else
            v_arity := HASH_ARITY;
        end if;

        v_index := getBlockIndex(address, mb_addr, size, level) + 1; -- index % arity
        if v_index = v_arity then
             v_index := (others => '0'); 
        end if; 

      --v_addr := address(address'left downto PSPE_BITS) & (PSPE_BITS - 1 downto 0 => '0');
        v_addr := unsigned(address(address'left downto PSPE_BITS)) & "000";

        if v_isLast then        
            v_addr(5 downto 3) := v_index; 
        else
            v_addr(4 downto 3) := v_index(1 downto 0); 
        end if;    
        return std_ulogic_vector(v_addr); 
  end function;

  function getMMTSize(size : addr_t) return unsigned is 
  variable v_mmt_size : unsigned(ADDR_SIZE   - 1 downto 0);
  begin
    -- mmt size = pspe_size * 2; (pspe + (sp + macTree))
    --v_mmt_size := resize((unsigned(size(size'left downto shift_page(pageLevel -  1))) & (PSPE_BITS - 1 downto 0 => '0')) & "0", ADDR_SIZE);
    v_mmt_size := resize(unsigned(size(size'left downto shift_page(pageLevel -  1))) & "0000", ADDR_SIZE); -- *8*2
    return v_mmt_size;
  end function;

  function getLevelFromSize(size : addr_t) return natural is
  begin 
    for i in 0 to pageLevel - 1 loop
        if shift_page(i) = ctz(unsigned(size)) then 
           return i;
        end if;
    end loop;
    return 0;
  end function;

 function getParentLeavesAddress(address : addr_t; p : pspe_t) return addr_t is
  variable v_level        : natural;
  variable v_page_address : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_addr         : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_page_offset  : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_page_index   : unsigned(2*ADDR_SIZE - 1 downto 0); 
  begin
    v_level        := pspe_getSize(p); 
    v_page_index   := shift_right(unsigned(getPageSize(v_level)),2)*pspe_getMSIdx(p);
  --v_page_address := unsigned(pspe_getMSAdd(p)(v_page_address'left downto 12)) & v_page_index(11 downto 0);
    v_page_address := unsigned(pspe_getMSAdd(p)) + v_page_index(ADDR_SIZE - 1 downto 0);
    v_page_offset  := unsigned(address) and unsigned(page_mask(v_level));

    v_addr         := v_page_address + v_page_offset(v_page_offset'left downto 2);

    return std_ulogic_vector(v_addr);
  end function;

 function getMTParentLeavesAddress(address, mb_address, size : addr_t) return addr_t is
  variable v_addr          : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_mmt_size      : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_page_offset   : unsigned(ADDR_SIZE   - 1 downto 0); 
  variable v_mac_base_addr : unsigned(ADDR_SIZE   - 1 downto 0); 
  begin
    v_mac_base_addr := unsigned(getMMacTBaseAddress(mb_address, size));
    v_page_offset   := unsigned(address) - unsigned(mb_address);          -- @ replace with mask(level)
    v_page_offset   := "00" & v_page_offset(v_page_offset'left downto 2); -- offset/HASH_ARITY (hash_arity = 4) 
    v_mmt_size      := getMMTSize(size);
    v_mmt_size      := "0000" & v_mmt_size(v_mmt_size'left downto 4);     -- mmt_size/16
    v_addr          := v_mac_base_addr + v_mmt_size + v_page_offset; 

    return std_ulogic_vector(v_addr);
  end function;

  function getParentAddress(address, mb_addr, size : addr_t; level : natural) return addr_t is
  variable v_addr   , v_offset   : unsigned(ADDR_SIZE - 1 downto 0); 
  variable v_index64, v_index256 : unsigned(ADDR_SIZE - 1 downto 0); 
  begin
    if isMasterTree(address, mb_addr, size) = '1' then
      v_addr   :=  unsigned(getMMacTBaseAddress(mb_addr, size));
      v_offset :=  unsigned(address) - v_addr;
    else
      v_addr    := unsigned(address) and unsigned(not page_mask(level));
      v_offset  := unsigned(address) and unsigned(page_mask(level));
    end if;
    -- v_index = number of 256 bits blocks
    v_index64  := shift_right(v_offset, PSPE_BITS); -- divide by 8 (size_64_in_bytes), node address
    v_index256 := shift_right(v_index64, 2);        -- divide by HASH_ARITY (4)      , set  address

    if v_index64 >= X"8" and v_index64 < X"20" then -- next to last set
        v_index256 := v_index256 - 2;
    end if;

    return std_ulogic_vector(v_addr + shift_left(v_index256, PSPE_BITS));
  end function;

end package body global;
