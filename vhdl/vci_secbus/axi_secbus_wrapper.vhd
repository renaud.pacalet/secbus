--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- axi_secbus_wrapper.vhd
--
--
library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

library axi_lib;
use axi_lib.axi_pkg.all;

library axi_vci_lib;
use axi_vci_lib.all;

entity axi_secbus_wrapper is
port (
       clk           : in  std_ulogic;
       srstn         : in  std_ulogic;
       ce            : in  std_ulogic;

       p_axi_ini_in  : in  axi_gp_m2s;
       p_axi_ini_out : out axi_gp_s2m;

       p_axi_io_in   : in  axilite_gp_m2s;
       p_axi_io_out  : out axilite_gp_s2m;

       p_axi_tgt_out : out axi_gp_m2s; 
       p_axi_tgt_in  : in  axi_gp_s2m;

       p_irq_c       : out std_ulogic;
       p_irq_e       : out std_ulogic;

       gpi           : in  std_ulogic_vector(7 downto 0);
       gpo           : out std_ulogic_vector(7 downto 0)

);
end axi_secbus_wrapper;

architecture netlist of axi_secbus_wrapper is

  signal s_vci_ini_in  : vci_i2t;
  signal s_vci_ini_out : vci_t2i;

  signal s_vci_io_in   : vci_i2t;
  signal s_vci_io_out  : vci_t2i;

  signal s_vci_tgt_in  : vci_t2i;
  signal s_vci_tgt_out : vci_i2t;

begin 

ivci_2_axi : entity axi_vci_lib.vci_2_axi4(rtl) 
port map(
       clk         => clk           , 
       srstn       => srstn         , 
       ce          => ce            , 

       -- Axi4 out 
       p_axi_in    => p_axi_tgt_in  , -- in  axi_gp_s2m; 
       p_axi_out   => p_axi_tgt_out , -- out axi_gp_m2s; 

       -- Vci in  
       p_vci_out   => s_vci_tgt_in  , -- out vci_t2i;
       p_vci_in    => s_vci_tgt_out   -- in  vci_i2t

);

iaxi_2_vci : entity axi_vci_lib.axi4_2_vci(rtl)
port map(
       clk         => clk           , 
       srstn       => srstn         , 
       ce          => ce            ,

       -- Axi4 in
       p_axi_in    => p_axi_ini_in  , -- in  axi_gp_m2s; 
       p_axi_out   => p_axi_ini_out , -- out axi_gp_s2m; 

       -- Vci out 
       p_vci_out   => s_vci_ini_in  , -- out vci_i2t;
       p_vci_in    => s_vci_ini_out   -- in  vci_t2i
);

iaxi4lite_2_vci : entity axi_vci_lib.axi4lite_2_vci(rtl)
port map(
       clk          => clk           , 
       srstn        => srstn         , 
       ce           => ce            ,

       -- Axi4 in
       p_axi_in     => p_axi_io_in   , -- in  axi_gp_m2s; 
       p_axi_out    => p_axi_io_out  , -- out axi_gp_s2m; 

       -- Vci out 
       p_vci_out    => s_vci_io_in   , -- out vci_i2t;
       p_vci_in     => s_vci_io_out    -- in  vci_t2i
);

ivci_secbus: entity work.vci_secbus(netlist)
port map(
         clk                  => clk,
         srstn                => srstn,
         ce                   => ce,

         p_vci_ini_in         => s_vci_ini_in  , --from cpu
         p_vci_ini_out        => s_vci_ini_out , --

         p_vci_io_in          => s_vci_io_in   , --from cpu
         p_vci_io_out         => s_vci_io_out  , --

         p_vci_tgt_out        => s_vci_tgt_out , --to xram
         p_vci_tgt_in         => s_vci_tgt_in  , --
                                             
         p_irq_c              => p_irq_c       , --
         p_irq_e              => p_irq_e       ,

         gpi                  => gpi           ,
         gpo                  => gpo
);

end netlist;
