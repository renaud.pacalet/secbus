--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- vci_secbus.vhd
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

library vci_split_lib;
use vci_split_lib.all;

library vci_merge_lib;
use vci_merge_lib.all;

library vci_input_lib;
use vci_input_lib.all;

library vci_io_target_lib;
use vci_io_target_lib.all;

library sec_ctrl_lib;
use sec_ctrl_lib.all;

library sec_ctx_lib;
use sec_ctx_lib.all;

library mem_ctrl_lib;
use mem_ctrl_lib.all;

library io_input_lib;
use io_input_lib.all;

library crypto_lib;
use crypto_lib.all;

library crypto_lib;
use crypto_lib.all;

library ms_ctrl_lib;
use ms_ctrl_lib.all;

library mt_ctrl_lib;
use mt_ctrl_lib.all;

library register_lib;
use register_lib.all;

library arbiters_lib;
use arbiters_lib.all;


entity vci_secbus is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- Vci Req from CPU      
       p_vci_ini_in            : in  vci_i2t;
       p_vci_ini_out           : out vci_t2i;

       -- Vci Req from CPU      
       p_vci_io_in             : in  vci_i2t;
       p_vci_io_out            : out vci_t2i;

       -- Vci Req To External Memory
       p_vci_tgt_out           : out vci_i2t;
       p_vci_tgt_in            : in  vci_t2i;

       p_irq_c                 : out std_ulogic;
       p_irq_e                 : out std_ulogic;

       gpi                     : in  std_ulogic_vector(7 downto 0);
       gpo                     : out std_ulogic_vector(7 downto 0)
);

end vci_secbus;

architecture netlist of vci_secbus is
signal s_enable                : std_ulogic; -- secbus enable 
signal s_mb_address            : addr_t;     -- Master Block Base Address
signal s_size                  : addr_t;     -- Size of protected region of memory
signal s_b_address             : addr_t;     -- Base address of protection region of memory

signal s_vci_split_2_merge     : vci_i2t;
signal s_vci_merge_2_split     : vci_t2i;

signal s_vci_split_2_input     : vci_i2t;
signal s_vci_input_2_split     : vci_t2i;

signal s_vci_mem_2_merge       : vci_i2t;
signal s_vci_merge_2_mem       : vci_t2i;

signal s_mem_rq                : memory_ctrl_cmd_out;  
signal s_mem_ack               : memory_ctrl_cmd_in;   -- cmdack, rspval

signal s_mem_rq_vector         : memory_ctrl_cmd_out_array(2 downto 0);    
signal s_mem_ack_vector        : memory_ctrl_cmd_in_array(2 downto 0);     

signal s_reg_0_write_vector    : data_register_write_array(3 downto 0);
signal s_reg_1_write_vector    : data_register_write_array(1 downto 0);
signal s_reg_2_write_vector    : data_register_write_array(1 downto 0);
signal s_reg_3_write_vector    : data_register_write_array(1 downto 0);

signal s_reg_0_write           : data_register_write;
signal s_reg_1_write           : data_register_write;
signal s_reg_2_write           : data_register_write;
signal s_reg_3_write           : data_register_write;
signal s_reg_4_write           : data_register_write;

signal s_reg_0_read            : data_register_read;
signal s_reg_1_read            : data_register_read;
signal s_reg_2_read            : data_register_read;
signal s_reg_3_read            : data_register_read;
signal s_reg_4_read            : data_register_read;

signal s_cache_pspe            : std_ulogic;
signal s_cache_sp              : std_ulogic;
signal s_cache_ms              : std_ulogic;
signal s_cache_mt              : std_ulogic;

signal s_sc_nbusy              : std_ulogic;
signal s_busy                  : std_ulogic;

signal s_sec_cmd               : security_rq_out;
signal s_sec_ack               : security_rq_in;

signal s_sec_cmd_vector        : security_rq_out_array(2 downto 0);
signal s_sec_ack_vector        : security_rq_in_array(2 downto 0);

signal s_ctx_cmd               : security_ctx_cmd_out;
signal s_ctx_ack               : security_ctx_cmd_in;

signal s_ctx_cmd_vector        : security_ctx_cmd_out_array(2 downto 0);
signal s_ctx_ack_vector        : security_ctx_cmd_in_array(2 downto 0);

signal s_cry_conf_cmd          : crypto_cmd_out;  
signal s_cry_conf_ack          : crypto_cmd_in;       

signal s_ms_cmd                : integrity_ctrl_cmd;
signal s_ms_ack                : integrity_ctrl_ack;

signal s_cry_int_cmd           : crypto_cmd_out;  
signal s_cry_int_ack           : crypto_cmd_in;        

signal s_cry_int_cmd_vector    : crypto_cmd_out_array(1 downto 0);
signal s_cry_int_ack_vector    : crypto_cmd_in_array(1 downto 0);  

signal s_cry_int_data_ms       : data_vector(11 downto 0);
signal s_cry_int_data_mt       : data_vector(11 downto 0);

signal s_cry_int_data_out      : data_vector(11 downto 0); 
signal s_cry_int_mac           : data_vector(1  downto 0); 

signal s_mt_up_data            : pspe_t;                     

signal s_mt_cmd_vector         : integrity_ctrl_cmd_array(1 downto 0);
--signal s_mt_ack_vector         : integrity_ctrl_ack_array(1 downto 0);
 
signal s_mt_cmd                : integrity_ctrl_cmd;  
signal s_mt_ack                : integrity_ctrl_ack;   

signal s_direct_mem            : std_ulogic;
signal s_wr_cache_cmd          : cache_ctrl_cmd; 
signal s_wr_cache_data         : data_vector(1 downto 0);
signal s_rd_cache_ack          : cache_ctrl_ack;
signal s_rd_cache_data         : data_vector(1 downto 0);

signal s_io_handler_data       : data_out ;  -- 8 words (256 bits)
signal s_io_ack                : std_ulogic; 

signal s_io_block              : io_block;

signal s_ctx_register          : ctx_register_read;
signal s_master_ctx_init       : master_ctx_reg_read;
signal s_master_ctx_update     : master_ctx_reg_read;
signal s_master_ctx            : master_ctx_reg_read;

signal s_irq_vector            : irq_out_array(3 downto 0);
signal s_irq_ack_vector        : std_ulogic_vector(3 downto 0);

signal s_irq                   : irq_out;                            
signal s_irq_ack               : std_ulogic;

signal s_direct_data_vector    : direct_data_array(1 downto 0);
signal s_direct_data           : data_vector(1 downto 0);

signal s_mem_rd_reg_vector     : data_register_read_array(REG_MAX - 1 downto 0);   
signal s_mem_wr_reg_vector     : data_register_write_array(REG_MAX - 1 downto 0); 

--##---
begin

vci_split_inst :  entity vci_split_lib.vci_split 
port map(
       clk                   =>  clk                , 
       srstn                 =>  srstn              , 
       ce                    =>  ce                 , 

       p_enable              =>  s_enable           , 
       p_addr                =>  s_b_address        , 
       p_size                =>  s_size             , 
                                              
       p_vci_ini_in          =>  p_vci_ini_in       , -- vci_i2t;
       p_vci_ini_out         =>  p_vci_ini_out      , -- vci_t2i; 

       -- to vci_input_ctrl 
       p_vci_tgt_sec_out     => s_vci_split_2_input , -- vci_i2t;
       p_vci_tgt_sec_in      => s_vci_input_2_split , -- vci_t2i

       -- to vci_merge
       p_vci_tgt_uns_out     => s_vci_split_2_merge , -- vci_i2t;
       p_vci_tgt_uns_in      => s_vci_merge_2_split   -- vci_t2i
);

-----
vci_merge_inst : entity vci_merge_lib.vci_merge
port map(
       clk                   => clk  ,
       srstn                 => srstn,
       ce                    => ce   ,

       -- Req from  Vci_Split - Target
       p_vci_ini_0_in        => s_vci_split_2_merge , -- vci_i2t;
       p_vci_ini_0_out       => s_vci_merge_2_split , -- vci_t2i  

       -- Req from  Vci_Mem_Ctrl - Target
       p_vci_ini_1_in        => s_vci_mem_2_merge   , -- vci_i2t;
       p_vci_ini_1_out       => s_vci_merge_2_mem   , -- vci_t2i  

       -- Req to  Vci External Memory Ctrl - Init
       p_vci_tgt_out         => p_vci_tgt_out,
       p_vci_tgt_in          => p_vci_tgt_in      

);

vci_input_inst : entity vci_input_lib.vci_input_ctrl 
port map(
       clk                     => clk                    , 
       srstn                   => srstn                  , 
       ce                      => ce                     , 

       -- Security context from security context controller
       rd_ctx_register         => s_ctx_register         ,

       -- Data from Security Ctrl
       rd_reg_0                => s_reg_0_read           ,  
     
     --mt_ack                  => s_mt_ack_vector(0)     , 
       mt_ack                  => s_mt_ack               , 
       ms_ack                  => s_ms_ack               , 

       -- flag from Security Ctrl
       sc_nbusy                => s_sc_nbusy             ,

       -- protection input parameters
       p_mb_address            => s_mb_address           ,
       p_size                  => s_size                 ,
       p_b_address             => s_b_address            ,
        
       -- Data to Security Ctrl
       wr_reg_3                => s_reg_3_write_vector(1),

       -- Vci Req from Split    
       p_vci_ini_in            => s_vci_split_2_input    , -- vci_i2t;
       p_vci_ini_out           => s_vci_input_2_split    , -- vci_t2i

       -- Req to Security Ctx Ctrl
       wr_ctx_cmd              => s_ctx_cmd_vector(2)    ,
       rd_ctx_cmd              => s_ctx_ack_vector(2)    ,

       -- Req to Security Ctrl
       wr_sec_cmd              => s_sec_cmd_vector(2)    ,
       rd_sec_cmd              => s_sec_ack_vector(2)    

       -- flag to  vci_io_target
       --busy                    : out std_ulogic
);

vci_io_tgt_inst : entity vci_io_target_lib.vci_io_target 
port map(
       clk                     =>  clk                , 
       srstn                   =>  srstn              , 
       ce                      =>  ce                 , 

       -- Read from io_input_ctrl
       io_handler_data_in      => s_io_handler_data   ,
       io_ack                  => s_io_ack            ,
      
       busy                    => s_busy              ,

       -- Req to IRQ Handler
       rd_irq                  => s_irq                ,
       wr_irq_ack              => s_irq_ack            ,

       -- Vci Req from CPU     
       p_vci_io_tgt_in         => p_vci_io_in           ,
       p_vci_io_tgt_out        => p_vci_io_out          ,

       -- Req to io_input_ctrl         
       wr_io_handler           => s_io_block            , 

       -- Master Security Context to security Ctrl
       wr_master_ctx_init      => s_master_ctx_init     , 

       -- JustForDebug
       -- From Security controller
       rd_master_ctx          => s_master_ctx           ,

       p_enable                => s_enable              ,
       p_b_address             => s_b_address           ,
       p_mb_address            => s_mb_address          ,
       p_size                  => s_size                ,

       p_cache_pspe            => s_cache_pspe          ,      
       p_cache_sp              => s_cache_sp            ,

       p_cache_ms              => s_cache_ms            ,
       p_cache_mt              => s_cache_mt            ,

       p_irq_c                 => p_irq_c               ,
       p_irq_e                 => p_irq_e               ,
       gpi                     => gpi                   ,
       gpo                     => gpo
);

-----
s_reg_0_write_vector(1) <=  s_mem_wr_reg_vector(0);
s_reg_1_write_vector(1) <=  s_mem_wr_reg_vector(1);
s_reg_2_write_vector(1) <=  s_mem_wr_reg_vector(2);
s_reg_4_write           <=  s_mem_wr_reg_vector(4);

s_mem_rd_reg_vector(0) <= s_reg_0_read;
s_mem_rd_reg_vector(1) <= s_reg_1_read;
s_mem_rd_reg_vector(2) <= s_reg_2_read; -- not used
s_mem_rd_reg_vector(3) <= s_reg_3_read; -- not used
s_mem_rd_reg_vector(4) <= s_reg_4_read; -- not used
------
vci_mem_inst : entity mem_ctrl_lib.vci_mem_ctrl 
port map(
       clk                     =>  clk                  , 
       srstn                   =>  srstn                , 
       ce                      =>  ce                   , 

       p_srcid                 =>  (others => '0')      , -- ?

       -- Data from Mt or Ms (ctrl/cache)
       rd_mac_data             => s_direct_data         ,

       -- Data from reg bank
       rd_reg                  => s_mem_rd_reg_vector   ,
      
       -- Data to reg Bank
       wr_reg                  => s_mem_wr_reg_vector   ,

       -- Request from Mem_Arbiter
       rd_mem_rq               => s_mem_rq              , 
       wr_mem_rq               => s_mem_ack             ,
 
       -- Req to  Vci_Merge
       p_vci_ini_out           => s_vci_mem_2_merge     , -- out vci_i2t;
       p_vci_ini_in            => s_vci_merge_2_mem       -- in  vci_t2i
);
----
mem_arb_inst : entity arbiters_lib.mem_arbiter 
generic map(
       NRQ                   => 3  -- security_ctrl(0), mt_cache_ctrl(1), ms_ctrl(2)
)
port map(
       clk                   =>  clk                , 
       srstn                 =>  srstn              , 
       ce                    =>  ce                 , 

       -- 
       rd_mem_rq_a           =>  s_mem_rq_vector    , 
       wr_mem_rq_a           =>  s_mem_ack_vector   , 

       -- Request to vci_mem_ctrl 
       wr_mem_rq             => s_mem_rq            , 
       rd_mem_rq             => s_mem_ack            
);
------
sc_inst : entity sec_ctrl_lib.security_ctrl 
port map(
       clk                     => clk                  , 
       srstn                   => srstn                , 
       ce                      => ce                   , 

       -- protection input parameters
       p_mb_address            => s_mb_address         ,
       p_size                  => s_size               ,
       p_b_address             => s_b_address          ,
       p_secbus_enable         => s_enable             ,
 
       -- Acknowledges from Mt/Ms) Ctrl
     --mt_ack                  => s_mt_ack_vector(1)   , 
       mt_ack                  => s_mt_ack             , 
       ms_ack                  => s_ms_ack             , 

       -- Data from reg bank
       rd_reg_0                => s_reg_0_read         ,
       rd_reg_1                => s_reg_1_read         ,
       rd_reg_2                => s_reg_2_read         ,
       rd_reg_3                => s_reg_3_read         , 

       -- Security context from security context controller
       rd_ctx_register         => s_ctx_register       , 

       -- Master Security Context from Configuration Registers
       rd_master_ctx_init      => s_master_ctx_init    , 
        
       -- Master Security Context from MT Ctrl                       
       rd_master_update        => s_master_ctx_update  , 
 
       -- Req to Mac Tree Ctrl/MS Ctrl
       mt_cmd                  => s_mt_cmd_vector(1)   ,
       ms_cmd                  => s_ms_cmd             ,
 
       -- Data to reg Bank
       wr_reg_0                => s_reg_0_write_vector(3), 

       -- Master Security Context to MT_Ctrl
       wr_master_ctx           => s_master_ctx           , 
 
       -- flag to io_handler Ctrl
       sc_nbusy                => s_sc_nbusy             ,        

       -- From security Ctx / Vci input / io_handler
       wr_sec_cmd              => s_sec_ack              ,
       rd_sec_cmd              => s_sec_cmd              ,

       -- Request To Memory Ctrl    
       wr_mem_rq               => s_mem_rq_vector(0)     , 
       rd_mem_rq               => s_mem_ack_vector(0)    ,

        -- request to Crypto Conf       
       wr_crypto_cmd           => s_cry_conf_cmd         , 
       rd_crypto_cmd           => s_cry_conf_ack         
);

sc_ctx_inst : entity sec_ctx_lib.security_ctx_ctrl 
port map(
       clk                     => clk                   , 
       srstn                   => srstn                 , 
       ce                      => ce                    , 

       -- protection input parameters
       p_mb_address            => s_mb_address          ,
       p_size                  => s_size                ,
       p_b_address             => s_b_address           ,

       p_cache_pspe            => s_cache_pspe          ,      
       p_cache_sp              => s_cache_sp            ,

       -- Data from io_handler (update_ctx) or Security Ctrl (Read_ctx)
       rd_reg_0                => s_reg_0_read          ,  

       -- Data from MT Ctrl (New/Update PSPE)
       rd_mt_data              => s_mt_up_data          , -- in pspe_t;  

       -- Req from vci_input_ctrl/io_handler/Mt_ctrl 
       wr_ctx_cmd              => s_ctx_ack             ,   
       rd_ctx_cmd              => s_ctx_cmd             ,   

       -- Security context to vci_input_ctrl/io_input_ctrl/Mt_ctrl/Ms_Ctrl/Security_Ctrl 
       wr_ctx_register         => s_ctx_register        , 

       -- flag from Security Ctrl
       --sc_nbusy                => s_sc_nbusy             ,                                      

       -- Req To Security Ctrl                     
       wr_sec_cmd              => s_sec_cmd_vector(0)    ,
       rd_sec_cmd              => s_sec_ack_vector(0)    , 

       wr_reg_2                => s_reg_2_write_vector(0),

       -- Req to IRQ Handler
       wr_irq                  => s_irq_vector(0)        , 
       rd_irq_ack              => s_irq_ack_vector(0)    
);

----
reg_arb_0_inst :  entity arbiters_lib.reg_arbiter 
generic map(
       NRQ                   => 4 --Crypto_conf(0), vci_mem_ctrl(1), Io_input_Ctrl(2), Security_Ctrl(3) 
)
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write_a   => s_reg_0_write_vector ,
       wr_register_write     => s_reg_0_write        
);

reg_0_inst : entity register_lib.reg_data 
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write     => s_reg_0_write        ,   

       wr_register_read      => s_reg_0_read     
);

reg_arb_1_inst :  entity arbiters_lib.reg_arbiter 
generic map(
       NRQ                   => 2  -- CryptoConf(0), vci_mem_ctrl(1)            
)
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write_a   => s_reg_1_write_vector ,

       wr_register_write     => s_reg_1_write        
);

reg_1_inst : entity register_lib.reg_data 
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write     => s_reg_1_write        ,   

       wr_register_read      => s_reg_1_read     
);

reg_arb_2_inst :  entity arbiters_lib.reg_arbiter 
generic map(
       NRQ                   => 2  -- Security_ctx_ctrl(0), vci_mem_ctrl(1)     
)
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write_a   => s_reg_2_write_vector ,

       wr_register_write     => s_reg_2_write        
);

reg_2_inst : entity register_lib.reg_data 
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write     => s_reg_2_write        ,   

       wr_register_read      => s_reg_2_read     
);


reg_arb_3_inst :  entity arbiters_lib.reg_arbiter 
generic map(
       NRQ                   => 2  -- io_input_ctrl(0), vci_input_ctrl(1)
)
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write_a   => s_reg_3_write_vector ,

       wr_register_write     => s_reg_3_write        
);

reg_3_inst : entity register_lib.reg_data 
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write     => s_reg_3_write        ,   
       wr_register_read      => s_reg_3_read     
);

reg_4_inst : entity register_lib.reg_data        -- updated by vci_mem_ctrl
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_register_write     => s_reg_4_write        ,   

       wr_register_read      => s_reg_4_read     
);

sc_arb_inst : entity arbiters_lib.sc_arbiter 
generic map(
       NRQ                   => 3 -- io_input(1), vci_input(2), sec_ctx(0)
)
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_sec_cmd_a          => s_sec_cmd_vector     , 
       wr_sec_cmd_a          => s_sec_ack_vector     , 

       -- Request to Security ctrl 
       wr_sec_cmd            => s_sec_cmd            , 
       rd_sec_cmd            => s_sec_ack             
);

io_input_inst : entity io_input_lib.io_input_ctrl 
port map(
       clk                     => clk                   , 
       srstn                   => srstn                 , 
       ce                      => ce                    , 

       -- protection input parameters
       p_mb_address            => s_mb_address          ,
       p_size                  => s_size                ,
       p_b_address             => s_b_address           ,

       -- Security context from security context controller
       rd_ctx_register         => s_ctx_register        ,                    

       -- Data from Security Ctrl
       rd_reg_0                => s_reg_0_read          ,  
     
       -- Req from IO registers
       rd_io_handler           => s_io_block            ,

       -- flag from Security Ctrl
       sc_nbusy                => s_sc_nbusy            ,                                     

       -- Acknowledge from Ms  Ctrl
       --ms_ack                  : in  integrity_ctrl_ack;

       -- Req to Mac Tree Ctrl
       mt_cmd                  => s_mt_cmd_vector(0)    ,
       mt_ack                  => s_mt_ack              , 
       ms_ack                  => s_ms_ack              , 

       -- Data to Security Context Ctrl 
       wr_reg_0                => s_reg_0_write_vector(2), 

       -- Data to Security Ctrl 
       wr_reg_3                => s_reg_3_write_vector(0),

       -- Write req to IO reg
       io_handler_data_out     => s_io_handler_data      ,               
       io_ack                  => s_io_ack               ,   
      
       busy                    => s_busy                 ,                    

       -- Req to Security Ctx Ctrl
       wr_ctx_cmd              => s_ctx_cmd_vector(1)    ,
       rd_ctx_cmd              => s_ctx_ack_vector(1)    ,

       -- Req to Security Ctrl
       wr_sec_cmd              => s_sec_cmd_vector(1)    ,
       rd_sec_cmd              => s_sec_ack_vector(1)    ,

       -- Req to IRQ Handler
       wr_irq                  => s_irq_vector(1)        , 
       rd_irq_ack              => s_irq_ack_vector(1)    
);

ctx_arb_inst : entity arbiters_lib.ctx_arbiter 
generic map(
       NRQ                   => 3   -- io_input_ctrl(1), vci_input_ctrl(2), mt_ctrl(0)
)
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_ctx_cmd_a          => s_ctx_cmd_vector     ,
       wr_ctx_cmd_a          => s_ctx_ack_vector     ,

       -- Request to Security context ctrl 
       wr_ctx_cmd            => s_ctx_cmd            ,
       rd_ctx_cmd            => s_ctx_ack            
);
---------------
cry_conf_inst : entity crypto_lib.cryptoConf 
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- Data from reg bank
       rd_reg_0              => s_reg_0_read         ,
       rd_reg_1              => s_reg_1_read         ,
      
       -- Data to reg Bank
       wr_reg_0              => s_reg_0_write_vector(0),
       wr_reg_1              => s_reg_1_write_vector(0),

       -- request from Security_Ctrl
       rd_crypto_cmd         => s_cry_conf_cmd         ,
       wr_crypto_cmd         => s_cry_conf_ack         
);
-----------
ms_ctrl_inst : entity ms_ctrl_lib.ms_ctrl 
port map(
       clk                   => clk                    , 
       srstn                 => srstn                  , 
       ce                    => ce                     , 

       p_cache               => s_cache_ms             ,

       -- Request from Security Ctrl
       rd_ms_cmd             => s_ms_cmd               ,
       wr_ms_ack             => s_ms_ack               ,

       -- crypto_engine_int
       wr_crypto_cmd         => s_cry_int_cmd_vector(1),
       wr_crypto_data        => s_cry_int_data_ms      ,
 
       rd_crypto_cmd         => s_cry_int_ack_vector(1),
       rd_crypto_data        => s_cry_int_mac          ,    

       -- Data from Memory    
       rd_reg_4               => s_reg_4_read         ,   

       -- from Security Ctrl
       rd_reg_0               => s_reg_0_read         ,
       rd_reg_1               => s_reg_1_read         ,
       rd_reg_2               => s_reg_2_read         ,

       -- Security context from Security context controller
       rd_ctx_register        => s_ctx_register       ,
     
       -- Request to Mem Ctrl
       wr_mem_rq              => s_mem_rq_vector(2)   ,       
       rd_mem_rq              => s_mem_ack_vector(2)  ,                        

       wr_mem_data            => s_direct_data_vector(1),                         
 
       -- Req to IRQ Handler
       wr_irq                 => s_irq_vector(3)     , 
       rd_irq_ack             => s_irq_ack_vector(3)    
);
---------
cry_int_arb : entity arbiters_lib.crypto_int_arbiter(rtl) 
generic map(
       NRQ                  => 2 -- mt_ctrl(0), ms_ctrl(1)  
)
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- 
       rd_crypto_cmd_a       => s_cry_int_cmd_vector ,
       wr_crypto_cmd_a       => s_cry_int_ack_vector ,
       
       rd_crypto_data_ms     => s_cry_int_data_ms    ,
       rd_crypto_data_mt     => s_cry_int_data_mt    ,
    
       -- Request to Crypto_int       
       wr_crypto_cmd         => s_cry_int_cmd        ,
       rd_crypto_cmd         => s_cry_int_ack        ,

       wr_crypto_data        => s_cry_int_data_out           
);
-----
cry_int_inst : entity crypto_lib.cryptoInt 
port map(
       clk                   => clk                  , 
       srstn                 => srstn                , 
       ce                    => ce                   , 

       -- Req From Ms/MT Ctrl  
       rd_crypto_cmd         => s_cry_int_cmd        ,
       rd_crypto_data        => s_cry_int_data_out   , 
 
       wr_crypto_cmd         => s_cry_int_ack        ,
       wr_crypto_data        => s_cry_int_mac            
);
---
mt_cache_ctrl_inst : entity mt_ctrl_lib.mt_cache_ctrl 
generic map(
        WAYS            => MT_CACHE_WAYS,
        SETS            => MT_CACHE_SETS,
        BLOCKS          => MT_CACHE_BLKS,
        TSH             => MT_CACHE_TSH    
)
port map(
       clk              => clk                    , 
       srstn            => srstn                  , 
       ce               => ce                     , 

       p_cache          => s_cache_mt             ,
       p_direct_mem     => s_direct_mem           ,

       rd_cache_cmd     => s_wr_cache_cmd         , 
       rd_cache_data    => s_wr_cache_data        ,
                           
       wr_cache_ack     => s_rd_cache_ack         ,
       wr_cache_data    => s_rd_cache_data        ,

       -- Request to Mem Ctrl
       wr_mem_rq       => s_mem_rq_vector(1)      ,
       rd_mem_rq       => s_mem_ack_vector(1)     ,

       wr_mem_data     => s_direct_data_vector(0) ,     

       -- Data from Memory    
       rd_reg_4        => s_reg_4_read               

);

----
mt_ctrl_inst : entity mt_ctrl_lib.mt_ctrl 
port map(
       clk                    => clk                    , 
       srstn                  => srstn                  , 
       ce                     => ce                     , 

       -- protection input parameters
       p_mb_address           => s_mb_address           ,
       p_size                 => s_size                 ,
       p_b_address            => s_b_address            ,

       p_cache                => s_cache_mt             ,

       -- Request from Security Ctrl/Io _input Ctrl
       rd_mt_cmd              => s_mt_cmd               ,
       wr_mt_ack              => s_mt_ack               ,

       -- Security context from Security context controller
       rd_ctx_register        => s_ctx_register         ,

       -- From Security controller
       rd_master_ctx          => s_master_ctx           ,

       -- MT Cache Ctrl
       p_direct_mem           => s_direct_mem           , 

       wr_cache_cmd           => s_wr_cache_cmd         ,
       wr_cache_data          => s_wr_cache_data        ,
                                              
       rd_cache_ack           => s_rd_cache_ack         ,
       rd_cache_data          => s_rd_cache_data        ,

       -- crypto_engine_int
       wr_crypto_cmd          => s_cry_int_cmd_vector(0),
       wr_crypto_data         => s_cry_int_data_mt      ,
 
       rd_crypto_cmd          => s_cry_int_ack_vector(0),
       rd_crypto_data         => s_cry_int_mac          , 

       -- Data from Memory     
       rd_reg_4               => s_reg_4_read           ,   

       -- from Security Ctrl
       rd_reg_0               => s_reg_0_read           ,
       rd_reg_1               => s_reg_1_read           ,
       rd_reg_2               => s_reg_2_read           ,

       -- Req To Security Ctx Ctrl 
       wr_ctx_cmd             => s_ctx_cmd_vector(0)    ,
       rd_ctx_cmd             => s_ctx_ack_vector(0)    ,
 
       -- Master Security Context To Security Ctrl                       
       wr_master_update       => s_master_ctx_update    , 

       -- to Security Ctx Ctrl
       wr_mt_data             => s_mt_up_data           ,
  
       -- Req to IRQ Handler
       wr_irq                 => s_irq_vector(2)        , 
       rd_irq_ack             => s_irq_ack_vector(2)    
);
-----
mt_arb_inst : entity arbiters_lib.mt_arbiter 
generic map(
       NRQ                  => 2 -- io_input_ctrl(0), security_ctrl(1)
)
port map(
       clk                  => clk                    , 
       srstn                => srstn                  , 
       ce                   => ce                     , 

       -- 
       rd_mt_cmd_a          => s_mt_cmd_vector        ,
--     wr_mt_ack_a          => s_mt_ack_vector        ,

       -- Request to MT ctrl 
       wr_mt_cmd            => s_mt_cmd               
--     rd_mt_ack            => s_mt_ack               
);
----
irq_arb_inst : entity arbiters_lib.irq_arbiter 
generic map(
       NRQ                  => 4   -- security_ctx_ctrl(0), io_input_ctrl(1), mt_ctrl(2), ms_ctrl(3)
)
port map(
       clk                  => clk                    , 
       srstn                => srstn                  , 
       ce                   => ce                     , 

       -- 
       rd_irq_a             => s_irq_vector           ,
       wr_irq_ack_a         => s_irq_ack_vector       ,

       -- to vci_io_target
       wr_irq               => s_irq                  ,
       rd_irq_ack           => s_irq_ack              
);

dir_data_arb_inst : entity arbiters_lib.direct_data_arbiter 
generic map(
       NRQ                  => 2  -- mt_ctrl(0), ms_ctrl(1) 
)
port map(
       clk                  => clk                  , 
       srstn                => srstn                , 
       ce                   => ce                   , 

       -- 
       rd_direct_data_a     => s_direct_data_vector ,                      
       wr_direct_data       => s_direct_data               
);

end netlist;
