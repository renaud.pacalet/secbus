--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mt_ctrl.vhd
--
-- Registers : 
--     R0, R1, R2 : Data Set from Security Ctrl to compute CBC MAC (Crypto_Engine_Int)
--                  with Address as first CBC Block

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

library des_lib;
use des_lib.des_pkg.all;

library fifo_lib;
use fifo_lib.all;

entity mt_ctrl is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- protection input parameters
       p_mb_address            : in  addr_t; -- Master Block Base Address
       p_b_address             : in  addr_t; -- Address of protected region of memory
       p_size                  : in  addr_t; -- Size    of protected region of memory

       p_cache                 : in  std_ulogic;

       -- Request from Security Ctrl/Io _input Ctrl
       rd_mt_cmd               : in  integrity_ctrl_cmd;
       wr_mt_ack               : out integrity_ctrl_ack;

       -- Security context from Security context controller
       rd_ctx_register         : in  ctx_register_read;

       -- From Security controller
       rd_master_ctx           : in  master_ctx_reg_read;

       -- MT Cache Ctrl
       p_direct_mem            : out std_ulogic;
       wr_cache_cmd            : out cache_ctrl_cmd; 
       wr_cache_data           : out data_vector(1 downto 0);
      
       rd_cache_ack            : in  cache_ctrl_ack;
       rd_cache_data           : in  data_vector(1 downto 0);

       -- crypto_engine_int
       wr_crypto_cmd           : out crypto_cmd_out;  
       wr_crypto_data          : out data_vector(11 downto 0);       -- 6 MAC Sets 
 
       rd_crypto_cmd           : in  crypto_cmd_in;                  -- cmdack, rspval
       rd_crypto_data          : in  data_vector(1 downto 0);    

       -- Data from Memory (via Cache)   
       rd_reg_4                : in  data_register_read;  

       -- from Security Ctrl
       rd_reg_0                : in  data_register_read;  
       rd_reg_1                : in  data_register_read;  
       rd_reg_2                : in  data_register_read;  

       -- Req To Security Ctx Ctrl 
       wr_ctx_cmd              : out security_ctx_cmd_out;
       rd_ctx_cmd              : in  security_ctx_cmd_in;

       wr_mt_data              : out pspe_t;

       -- Master Security Context To Security Ctrl                       
       wr_master_update        : out master_ctx_reg_read;
 
       -- Req to IRQ Handler
       wr_irq                  : out irq_out;
       rd_irq_ack              : in std_ulogic
);

end mt_ctrl;

architecture rtl of mt_ctrl is
type init_state is (I_IDLE,
                   I_START_WRITE_NULL_BROTHER,
                   I_WAIT_WRITE_NULL_BROTHER,
                   I_START_READ_BROTHER,
                   I_WAIT_READ_BROTHER,
                   I_START_WRITE_SET,
                   I_WAIT_WRITE_SET,
                   I_START_FIRST_IV_GENERATION,
                   I_START_IV_GENERATION,
                   I_WAIT_IV_GENERATION,
                   I_START_UPDATE_LEAVE_PARENT,
                   I_WAIT_UPDATE_LEAVE_PARENT,
                   I_START_UPDATE_PARENT_M,
                   I_WAIT_UPDATE_PARENT_M,
                   I_START_UPDATE_ROOT_M,
                   I_WAIT_UPDATE_ROOT_M,
                   I_START_WRITE_PSPE,
                   I_WAIT_WRITE_PSPE,
                   I_START_WRITE_MASTER_PSPE, 
                   I_IRQ_SEND);

type check_state is (C_IDLE,
                   C_START_CHECK_REFERENCE_PARENT,       
                   C_CACHE_WAIT_READ_REFERENCE_PARENT,
                   C_MEM_WAIT_READ_REFERENCE_PARENT,
                   C_START_RESTORE_REFERENCE_PARENT_BROTHERS,
                   C_WAIT_RESTORE_REFERENCE_PARENT_BROTHERS,
                   C_START_LAST_IC, 
                   C_WAIT_LAST_IC,    
                   C_IRQ_SEND);

type updt_state is (U_IDLE,
                   U_START_PARENT_UPDATE,
                   U_WAIT_PARENT_UPDATE,
                   U_START_WRITE_NEW_PARENT,
                   U_START_READ_BROTHERS,
                   U_WAIT_READ_BROTHERS,
                   U_CACHE_WAIT_WRITE_NEW_PARENT,
                   U_MEM_WAIT_WRITE_NEW_PARENT,
                   U_START_READ_SLAVE_PSPE,
                   U_WAIT_READ_SLAVE_PSPE,
                   U_START_READ_VICTIM_SET,
                   U_WAIT_READ_VICTIM_HASH_SET,
                   U_START_NEW_HASH_GENERATION,
                   U_WAIT_NEW_HASH_GENERATION,
                   U_START_RESTORE_DIRTY_VICTIM_BROTHERS,
                   U_WAIT_RESTORE_DIRTY_VICTIM_BROTHERS,
                   U_START_READ_REFERENCE_PARENT,
                   U_WAIT_READ_REFERENCE_PARENT,
                   U_START_LAST_IC,
                   U_WAIT_LAST_IC,
                   U_START_UPDATE_ROOT,
                   U_WAIT_UPDATE_ROOT,
                   U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS,
                   U_WAIT_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS,
                   U_START_RESTORE_REFERENCE_PARENT_BROTHERS,
                   U_WAIT_RESTORE_REFERENCE_PARENT_BROTHERS,
                   U_START_WRITE_PSPE,
                   U_WAIT_WRITE_PSPE,
                   U_START_WRITE_MASTER_PSPE,
                   U_IRQ_SEND);

signal s_init           , r_init           : init_state;
signal s_check          , r_check          : check_state;
signal s_updt           , r_updt           : updt_state;
signal s_key            , r_key            : desx_key;     
signal s_addr           , r_addr           : addr_t;
signal s_victim_addr    , r_victim_addr    : addr_t;
signal s_brother_addr   , r_brother_addr   : addr_t;
signal s_parent_addr    , r_parent_addr    : addr_t;
signal s_wr_parent_addr , r_wr_parent_addr : addr_t;
signal s_isMasterTree   , r_isMasterTree   : std_ulogic; 
signal s_index          , r_index          : unsigned(3 downto 0);          
signal s_check_data     , r_check_data     : data_vector(11 downto 0); 
signal s_brother_cpt    , r_brother_cpt    : natural range 0 to LAST_HASH_ARITY;           
signal s_hit_cpt        , r_hit_cpt        : natural range 0 to LAST_HASH_ARITY;           
signal s_isLastLevel    , r_isLastLevel    : boolean;      
signal s_pageLevel      , r_pageLevel      : natural range 0 to 3;           
signal s_mt_pageLevel   , r_mt_pageLevel   : natural range 0 to 3;           
signal s_pspe           , r_pspe           : pspe_t;           
signal s_irq_op         , r_irq_op         : io_status_errc_e;           
signal s_first          , r_first          : Boolean;
signal s_leaves         , r_leaves         : Boolean;

signal s_cry_mac        , r_cry_mac        : data_vector(1 downto 0);       
signal s_ref_mac        , r_ref_mac        : data_vector(1 downto 0); 
signal s_old_mac        , r_old_mac        : data_vector(1 downto 0);
 
signal s_mt             , r_mt             : data_vector(23 downto 0);  --max depth of a mac tree (11 + 1) * 2
signal s_level          , r_level          : natural range 0 to 15; 

signal s_syn_fifo_init 	: std_ulogic;
signal s_syn_fifo_write	: std_ulogic;
signal s_syn_fifo_wdata	: addr_t;                                      
signal s_syn_fifo_read	: std_ulogic;
signal s_syn_fifo_rdata	: addr_t;                                       
signal s_syn_fifo_empty	: std_ulogic;
signal s_syn_fifo_full	: std_ulogic;

signal s_rst_fifo_init 	: std_ulogic;
signal s_rst_fifo_write	: std_ulogic;
signal s_rst_fifo_wdata	: addr_t;                                      
signal s_rst_fifo_read	: std_ulogic;
signal s_rst_fifo_rdata	: addr_t;                                       
signal s_rst_fifo_empty	: std_ulogic;
signal s_rst_fifo_full	: std_ulogic;

--#------
begin

rst_fifo : entity fifo_lib.fifo(rtl)
--Generic (
--	DATA_WIDTH  => ADDR_SIZE;
--	FIFO_DEPTH  => 6 
--);
Port map( 
	clk	    => clk,
	rstn	=> srstn,
	ce   	=> ce,
	init 	=> s_rst_fifo_init,
	write	=> s_rst_fifo_write,
	wdata 	=> s_rst_fifo_wdata,
	read	=> s_rst_fifo_read,
	rdata  	=> s_rst_fifo_rdata,
	empty	=> s_rst_fifo_empty,
	full	=> s_rst_fifo_full	
);

syn_fifo : entity fifo_lib.fifo(rtl)
--Generic (
--	DATA_WIDTH  => ADDR_SIZE;
--	FIFO_DEPTH  => 6 
--);
Port map( 
	clk	    => clk,
	rstn	=> srstn,
	ce   	=> ce,
	init 	=> s_syn_fifo_init,
	write	=> s_syn_fifo_write,
	wdata 	=> s_syn_fifo_wdata,
	read	=> s_syn_fifo_read,
	rdata  	=> s_syn_fifo_rdata,
	empty	=> s_syn_fifo_empty,
	full	=> s_syn_fifo_full	
);

---
pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_init              <= I_IDLE;
              r_check             <= C_IDLE;
              r_updt              <= U_IDLE;
              r_key               <= key_null; 
              r_cry_mac           <= (others => (others => '0'));
              r_ref_mac           <= (others => (others => '0'));
              r_old_mac           <= (others => (others => '0'));
              r_addr              <= (others => '0');
              r_victim_addr       <= (others => '0');
              r_brother_addr      <= (others => '0');
              r_parent_addr       <= (others => '0');
              r_wr_parent_addr    <= (others => '0');
              r_isMasterTree      <= '0';
              r_pageLevel         <= 0;               
              r_mt_pageLevel      <= 0;               
              r_index             <= (others => '0');
              r_check_data        <= (others => (others => '0'));
              r_mt                <= (others => (others => '0'));
              r_brother_cpt       <= 0;               
              r_level             <= 0;               
              r_first             <= False;
              r_leaves            <= False;
              r_isLastLevel       <= False;
              r_pspe              <= pspe_none;

        elsif ce = '1' then
              r_init              <= s_init;
              r_check             <= s_check; 
              r_updt              <= s_updt;
              r_key               <= s_key;
              r_cry_mac           <= s_cry_mac;
              r_ref_mac           <= s_ref_mac;
              r_old_mac           <= s_old_mac;
              r_addr              <= s_addr;
              r_victim_addr       <= s_victim_addr;
              r_brother_addr      <= s_brother_addr;
              r_parent_addr       <= s_parent_addr;
              r_wr_parent_addr    <= s_wr_parent_addr;
              r_isMasterTree      <= s_isMasterTree;
              r_pageLevel         <= s_pageLevel;
              r_mt_pageLevel      <= s_mt_pageLevel;
              r_index             <= s_index;
              r_check_data        <= s_check_data;
              r_mt                <= s_mt;
              r_brother_cpt       <= s_brother_cpt;
              r_level             <= s_level;
              r_hit_cpt           <= s_hit_cpt;        
              r_first             <= s_first;
              r_leaves            <= s_leaves;
              r_isLastLevel       <= s_isLastLevel;
              r_pspe              <= s_pspe;
              r_irq_op            <= s_irq_op; 
        end if;
end if;
end process;

-------
pTransition : process(r_init          , r_check         , r_updt          ,
                      p_cache         , p_mb_address    , p_b_address     , p_size       , 
                      rd_reg_4        , rd_cache_data   , 
                      rd_reg_0        , rd_reg_1        , rd_reg_2        , 
                      rd_master_ctx   , rd_ctx_register ,   
                      rd_crypto_cmd   , rd_cache_ack    , rd_irq_ack      ,  
                      r_parent_addr   , r_brother_addr  , r_wr_parent_addr, r_addr       ,  
                      r_isMasterTree  , r_PageLevel     , r_index         ,   
                      r_cry_mac       , r_ref_mac       , r_old_mac       , 
                      r_mt            , r_level         , r_key           ,
                      r_check_data    , r_brother_cpt   , r_hit_cpt       ,                       
                      r_pspe          , r_irq_op        , r_first         , r_leaves     , 
                      rd_crypto_data  , rd_mt_cmd       , rd_ctx_cmd      , 
                      r_mt_pageLevel  , r_isLastLevel   , r_victim_addr   ,  
                      s_syn_fifo_empty, s_rst_fifo_empty, s_rst_fifo_rdata)

variable v_src               : natural range 0 to 2;
variable v_isMasterTree      : std_ulogic;  
variable v_addr              : unsigned(ADDR_SIZE - 1 downto 0);
variable v_parent_addr       : addr_t;
variable v_arity             : natural range 0 to 6;
variable v_isLastLevel       : boolean;
variable v_pageLevel         : natural;
variable v_cry_mac           : data_vector(1 downto 0);
variable v_mem_mac           : data_vector(1 downto 0);
variable v_mac_matches       : boolean;                   

begin

s_init              <= r_init;
s_check             <= r_check;
s_updt              <= r_updt;
s_key               <= r_key;
s_cry_mac           <= r_cry_mac;
s_ref_mac           <= r_ref_mac;
s_old_mac           <= r_old_mac;
s_addr              <= r_addr;
s_brother_addr      <= r_brother_addr;
s_parent_addr       <= r_parent_addr;
s_wr_parent_addr    <= r_wr_parent_addr;
s_isMasterTree      <= r_isMasterTree;
s_index             <= r_index;
s_check_data        <= r_check_data;
s_brother_cpt       <= r_brother_cpt;
s_hit_cpt           <= r_hit_cpt;
s_isLastLevel       <= r_isLastLevel;
s_victim_addr       <= r_victim_addr;
s_first             <= r_first;
s_leaves            <= r_leaves; 

s_rst_fifo_init     <= '0';	
s_rst_fifo_write    <= '0';	
s_rst_fifo_wdata    <= (others => '0');	                                      
s_rst_fifo_read	    <= '0';

s_syn_fifo_init     <= '0';	
s_syn_fifo_write    <= '0';	
s_syn_fifo_wdata    <= (others => '0');	                                      
s_syn_fifo_read	    <= '0';

s_pageLevel         <= r_pageLevel;
s_mt_pageLevel      <= r_mt_pageLevel;
s_pspe              <= r_pspe;
s_irq_op            <= r_irq_op;

s_mt                <= r_mt;
s_level             <= r_level; 

      -- #-- INITIALIZE
      case r_init is
           when I_IDLE =>
                 
                      if rd_mt_cmd.enable = '1' and rd_mt_cmd.cmd = INITIALIZE then
                                s_hit_cpt       <= 0;
                                s_addr          <= rd_mt_cmd.addr;
                                s_victim_addr   <= rd_mt_cmd.addr;
                                s_check_data    <= (others => (others => '0'));
                                s_first         <= True; 
                                s_key           <= sp_getKeyI(rd_master_ctx.sp);
                                
                                v_src           := to_integer(unsigned(rd_mt_cmd.src));
                                v_isMasterTree  := isMasterTree(rd_mt_cmd.addr, p_mb_address, p_size);
                                
                                s_isMasterTree  <= v_isMasterTree;
                               
                                if v_isMasterTree = '1' then
                                    s_ref_mac(0)      <= rd_master_ctx.pspe(0);
                                    s_ref_mac(1)      <= rd_master_ctx.pspe(1);
                                    s_pageLevel       <= getLevelFromSize(std_ulogic_vector(shift_right(getMMTSize(p_size),2)));
                                    s_brother_addr    <= getMMacTBaseAddress(p_mb_address, p_size); 
                                else                               
                                    s_ref_mac(0)      <= rd_ctx_register.s_pspe(0)(31 downto 3) & "000";                             
                                    s_ref_mac(1)      <= rd_ctx_register.s_pspe(1);
                                    s_pageLevel       <= pspe_getSize(rd_ctx_register.m_pspe);
                                    s_brother_addr    <= pspe_getMSAdd(rd_ctx_register.m_pspe);
                                end if;
                                
                                s_brother_cpt      <= 0;
                                s_init             <= I_START_WRITE_SET;
                 
                      elsif rd_mt_cmd.enable = '1' and rd_mt_cmd.cmd = INITIALIZE_PAGE then
                                s_hit_cpt       <= 0;
                                s_addr          <= rd_mt_cmd.addr;
                                s_victim_addr   <= rd_mt_cmd.addr;
                                s_check_data    <= (others => (others => '0'));
                                s_first         <= True; 
                                s_key           <= sp_getKeyI(rd_master_ctx.sp);
                                
                                v_src           := to_integer(unsigned(rd_mt_cmd.src));
                                v_isMasterTree  := isMasterTree(rd_mt_cmd.addr, p_mb_address, p_size);
 
                                s_isMasterTree  <= v_isMasterTree;
                                
                                if v_isMasterTree = '1' then
                                    s_ref_mac(0)      <= rd_master_ctx.pspe(0);
                                    s_ref_mac(1)      <= rd_master_ctx.pspe(1);
                                    s_pageLevel       <= getLevelFromSize(std_ulogic_vector(shift_right(getMMTSize(p_size),2)));
                                    s_brother_addr    <= getMMacTBaseAddress(p_mb_address, p_size); 
                                else                               
                                    s_ref_mac(0)      <= rd_ctx_register.s_pspe(0)(31 downto 3) & "000";                             
                                    s_ref_mac(1)      <= rd_ctx_register.s_pspe(1);
                                    s_pageLevel       <= pspe_getSize(rd_ctx_register.m_pspe);
                                    s_brother_addr    <= pspe_getMSAdd(rd_ctx_register.m_pspe);
                                end if;
                                
                                s_brother_cpt        <= 0;
                                s_init               <= I_START_WRITE_NULL_BROTHER;
                      end if; 
      
             when I_START_WRITE_NULL_BROTHER =>
                  s_init <= I_WAIT_WRITE_NULL_BROTHER;
      
             when I_WAIT_WRITE_NULL_BROTHER =>
                  if rd_cache_ack.ack = '1' then
                     if r_brother_cpt = LAST_HASH_ARITY - 1 then
                        s_init         <= I_START_WRITE_SET;
                        s_brother_cpt  <= 0;                   
                     else
                        s_brother_cpt  <= r_brother_cpt + 1;
                        v_addr         := unsigned(r_brother_addr) + PSPE_SIZE_BYTES;  
                        s_brother_addr <= std_ulogic_vector(v_addr);
                        s_init         <= I_START_WRITE_NULL_BROTHER; 
                     end if; 
                  end if;
      
              when I_START_READ_BROTHER =>  -- mem read next brother and encrypt actual
                    s_init      <= I_WAIT_READ_BROTHER;
                    s_index     <= to_unsigned(r_brother_cpt, 3)  & '0'; -- index * 2  
      
              when I_WAIT_READ_BROTHER =>    -- wait for cache read brother rsp and cryptoInt rsp
                   if rd_cache_ack.ack = '1' then
      
                       s_check_data(to_integer(r_index))     <= rd_cache_data(0);  
                       s_check_data(to_integer(r_index + 1)) <= rd_cache_data(1);
      
                       if r_brother_cpt = r_hit_cpt then  
                          s_init        <= I_START_UPDATE_ROOT_M;
                       else -- whole set not reached yet 
                          s_brother_cpt  <= r_brother_cpt + 1;
                          s_brother_addr <= std_ulogic_vector(unsigned(r_brother_addr) + PSPE_SIZE_BYTES); -- 8;         
                          s_init         <= I_START_READ_BROTHER;
                       end if;   
                      
                   end if; -- cache rack
            
             when I_START_WRITE_SET =>
                 s_init  <= I_WAIT_WRITE_SET;

             when I_WAIT_WRITE_SET =>
                 if rd_cache_ack.ack = '1'  then
                       if r_brother_cpt = HASH_ARITY - 1 then   
                          s_init        <= I_START_FIRST_IV_GENERATION;
                       else -- whole set not reached yet 
                          s_brother_cpt  <= r_brother_cpt + 1;
                          s_victim_addr  <= std_ulogic_vector(unsigned(r_victim_addr) + PSPE_SIZE_BYTES); -- 8;         
                          s_init         <= I_START_WRITE_SET;
                       end if;   

                 end if;

             when I_START_FIRST_IV_GENERATION =>
                 if rd_crypto_cmd.cmdack = '1' then
                       s_level <= 0;
                       if r_isMasterTree = '1' then
                           v_parent_addr := getMTParentLeavesAddress(std_ulogic_vector(r_addr), p_mb_address, p_size);
                       else                               
                           v_parent_addr := getParentLeavesAddress(std_ulogic_vector(r_addr), rd_ctx_register.m_pspe);
                       end if;
      
                       s_wr_parent_addr  <= v_parent_addr;
                       s_parent_addr     <= v_parent_addr; 

                      s_init <= I_WAIT_IV_GENERATION;
                 end if;
      
             when I_START_IV_GENERATION =>
                 if rd_crypto_cmd.cmdack = '1' then
                      s_level          <= r_level + 1; 
                      s_wr_parent_addr <= getParentAddress(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel);
                      s_init           <= I_WAIT_IV_GENERATION;
                  end if;
      
             when I_WAIT_IV_GENERATION =>
                  if rd_crypto_cmd.rspval = '1' then
                      s_mt(r_level*2)    <= rd_crypto_data(0);  
                      s_mt(r_level*2+ 1) <= rd_crypto_data(1); 
      
                      v_isLastLevel  := isLastLevel(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel); 
                      s_isLastLevel  <= v_isLastLevel;
      
                      if (v_isLastLevel) then
                         s_level          <= 0;
                         s_wr_parent_addr <= r_parent_addr; -- parentLeavesAddr(set_addr)
                         s_init           <= I_START_UPDATE_LEAVE_PARENT; -- compute subtree of second node of last level
                      -- !lastlevel
                      elsif r_wr_parent_addr(4 downto 0) = b"00000" then  -- first node address of a set (32 bytes = 8 words = 4 nodes)) 
                         s_init           <= I_START_IV_GENERATION;
                      else
                          s_level          <= 0;
                          s_wr_parent_addr <= r_parent_addr; -- parentLeavesAddr(set_addr)
                          s_init           <= I_START_UPDATE_LEAVE_PARENT; 
                      end if; -- lastlevel
                  end if; -- crypto_rspval
      
             when I_START_UPDATE_LEAVE_PARENT =>
                  if rd_crypto_cmd.cmdack = '1' then
                     s_init <= I_WAIT_UPDATE_LEAVE_PARENT;  
                  end if;
      
             when I_WAIT_UPDATE_LEAVE_PARENT =>
                  if rd_crypto_cmd.rspval = '1' then
                     s_mt(0) <= rd_crypto_data(0);
                     s_mt(1) <= rd_crypto_data(1);
                     s_init  <= I_START_UPDATE_PARENT_M;  
                  end if;
      
             when I_START_UPDATE_PARENT_M =>
                  if rd_crypto_cmd.cmdack = '1' then
                     s_level <= r_level + 1;
                     s_index <= '0' & getBlockIndex(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel);  
                     s_init  <= I_WAIT_UPDATE_PARENT_M;  
                  end if;
      
             when I_WAIT_UPDATE_PARENT_M =>
                  if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then 
                     -- is it necessary to save mt ? 
                     s_mt(r_level*2)     <= rd_crypto_data(0);
                     s_mt(r_level*2 + 1) <= rd_crypto_data(1);
      
                     v_isLastLevel       := isLastLevel(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel); 

                     if v_isLastLevel then
                         s_check_data(to_integer(r_index*2))     <= r_mt((r_level - 1)*2);  
                         s_check_data(to_integer(r_index*2 + 1)) <= r_mt((r_level - 1)*2 + 1); 

                         if r_index(0) = '1' then -- && r_index mod 2 = 1
                             s_wr_parent_addr <= r_wr_parent_addr(r_wr_parent_addr'left downto 6) & "000000"; -- brother_address ?
                              -- r_index = 1 =>  first  node of last level, no need to read brothers, null value instead
                             if r_index(1) = '1' then -- (index = 3), second node of last level, read only previous (first) brother
                                 s_brother_cpt <= 0;
                                 s_hit_cpt     <= 1;
                                 s_init        <= I_START_READ_BROTHER;
                             elsif r_index(2) = '1' then -- (index = 5), third (last) node of last level, read 2 previous brothers 
                                 s_brother_cpt <= 0;
                                 s_hit_cpt     <= 3;
                                 s_init        <= I_START_READ_BROTHER;
                             else
                                s_init <= I_START_UPDATE_ROOT_M;
                             end if;
                         else
                             v_addr           := unsigned(r_addr) + SIZE_LINE_BYTES;
                             s_addr           <= std_ulogic_vector(v_addr);
                             s_victim_addr    <= std_ulogic_vector(v_addr);
                             s_brother_cpt    <= 0; 
                             s_init           <= I_START_WRITE_SET; 
                         end if;   

                     elsif to_integer(r_index) = HASH_ARITY - 1 then    -- not lastlevel
                         s_wr_parent_addr <= getParentAddress(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel);
                         s_init           <= I_START_UPDATE_PARENT_M;
                     else
                         v_addr         := unsigned(r_addr) + SIZE_LINE_BYTES;
                         s_addr         <= std_ulogic_vector(v_addr);
                         s_victim_addr  <= std_ulogic_vector(v_addr);
                         s_brother_cpt  <= 0; 
                         s_init         <= I_START_WRITE_SET; 
                     end if;
                  end if; -- rspval

             when I_START_UPDATE_ROOT_M =>
                  if rd_crypto_cmd.cmdack = '1' then
                        s_init <= I_WAIT_UPDATE_ROOT_M;
                  end if;
      
             when I_WAIT_UPDATE_ROOT_M =>
                  if rd_crypto_cmd.rspval = '1' then 
                     -- is it necessary to save mt ? 
                     --s_mt(r_level*2)     <= rd_crypto_data(0);
                     --s_mt(r_level*2 + 1) <= rd_crypto_data(1);
      
                     s_cry_mac <= rd_crypto_data; -- for master_ctx_register.pspe
       
                     if r_isMasterTree = '1' then
                        s_init       <= I_START_WRITE_MASTER_PSPE;
                     else -- not masterTree => update slave pspe
                        s_pspe(0)(31 downto 3) <= rd_crypto_data(0)(31 downto 3);
                        s_pspe(0)( 2 downto 1) <= rd_ctx_register.m_pspe(0)(2 downto 1); -- size
                        s_pspe(0)(0)           <= '1'; --rd_ctx_register.s_pspe(0)(0);          -- valid
                        s_pspe(1)              <= rd_crypto_data(1);
                        s_init                 <= I_START_WRITE_PSPE; 
                     end if; -- masterTree
                  end if; -- rspval
      
              when I_START_WRITE_PSPE =>
                   if rd_ctx_cmd.cmdack = '1' then
                      s_init <= I_WAIT_WRITE_PSPE; 
                   end if;
      
              when I_WAIT_WRITE_PSPE => 
                   if rd_ctx_cmd.rspval = '1' then
                      s_init <= I_IDLE;    
                   end if;
      
              when I_START_WRITE_MASTER_PSPE =>  
                   s_init <= I_IDLE;

              when I_IRQ_SEND =>  
                  --if rd_irq_ack = '1' then 
                  --   s_init <= I_IDLE;
                  --end if;
     end case;


      -- #-- CHECK
     case r_check is
           when C_IDLE =>
                      if rd_mt_cmd.enable = '1' and rd_mt_cmd.cmd =  CHECK then
                                 s_addr          <= rd_mt_cmd.addr;
                                 s_check_data    <= (others => (others => '0'));
                                 s_first         <= True; 
                                 s_key           <= sp_getKeyI(rd_master_ctx.sp);
 
                                 v_src           := to_integer(unsigned(rd_mt_cmd.src));
                                 v_isMasterTree  := isMasterTree(rd_mt_cmd.addr, p_mb_address, p_size);
                                 s_isMasterTree  <= v_isMasterTree;

                                if v_isMasterTree = '1' then
                                    s_ref_mac(0)      <= rd_master_ctx.pspe(0);
                                    s_ref_mac(1)      <= rd_master_ctx.pspe(1);
                                    v_parent_addr     := getMTParentLeavesAddress(rd_mt_cmd.addr, p_mb_address, p_size);
                                --  s_mt_pageLevel    <= getLevelFromSize(std_ulogic_vector(shift_right(getMMTSize(p_size),2))); 
                                    s_pageLevel       <= getLevelFromSize(std_ulogic_vector(shift_right(getMMTSize(p_size),2)));
                                    s_brother_addr    <= getMMacTBaseAddress(p_mb_address, p_size); 
                                else                               
                                    s_ref_mac(0)      <= rd_ctx_register.s_pspe(0)(31 downto 3) & "000";                             
                                    s_ref_mac(1)      <= rd_ctx_register.s_pspe(1);
                                    v_parent_addr     := getParentLeavesAddress(rd_mt_cmd.addr, rd_ctx_register.m_pspe);
                                    s_pageLevel       <= pspe_getSize(rd_ctx_register.m_pspe);
                                    s_brother_addr    <= pspe_getMSAdd(rd_ctx_register.m_pspe);
                                end if;
                                
                                s_parent_addr <= v_parent_addr;

                                case v_src is
                                     when 0 =>
                                          s_check_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_0.data;
                                     when 1 =>
                                          s_check_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_1.data;
                                     when 2 =>
                                          s_check_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_2.data;
                                end case;
      
                                s_check      <= C_START_CHECK_REFERENCE_PARENT;
                      end if;   

             when C_START_CHECK_REFERENCE_PARENT =>
                  if (rd_crypto_cmd.cmdack = '1' or not r_first) then
                                if p_cache = '1' then 
                                    s_index <= getBlockIndex(r_parent_addr, p_mb_address, p_size, r_pageLevel) & '0'; -- index * 2 
                                    s_check <= C_CACHE_WAIT_READ_REFERENCE_PARENT;
                                else
                                    s_index <= '0' &  getBlockIndex(r_parent_addr, p_mb_address, p_size, r_pageLevel); 
                                    s_check <= C_MEM_WAIT_READ_REFERENCE_PARENT;
                                end if;
                  end if;
      
             when C_CACHE_WAIT_READ_REFERENCE_PARENT =>
                  if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then 
      
                     if rd_cache_data = rd_crypto_data then
                         if rd_cache_ack.hit = '1' then
                            s_check       <= C_IDLE;
                         else -- match & miss
                            s_brother_cpt                         <= 0; 
                            s_check_data(to_integer(r_index))     <= rd_cache_data(0); -- store parent mac 
                            s_check_data(to_integer(r_index + 1)) <= rd_cache_data(1); 
                            -- get sibling data address
                            s_brother_addr <= getBrotherAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);         
                            s_check        <= C_START_RESTORE_REFERENCE_PARENT_BROTHERS;
                         end if;
                     else -- mismatch
                         s_irq_op  <= LOAD;
                         s_check   <= C_IRQ_SEND;
                     end if;
                  end if;
      
             when C_MEM_WAIT_READ_REFERENCE_PARENT =>
                  if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then
                     v_mem_mac(0) := rd_reg_4.data(word_index(r_parent_addr)); 
                     v_mem_mac(1) := rd_reg_4.data(word_index(r_parent_addr)+1);
                     
      
                     if v_mem_mac = rd_crypto_data then 
                        v_mac_matches := True;
                     else
                        v_mac_matches := False;                               
                     end if;
      
                     v_isLastLevel  := isLastLevel(r_parent_addr, p_mb_address, p_size, r_pageLevel); 
                     if v_isLastLevel then
                          if r_first  then     
                               if v_mac_matches then
                                  if r_index(2) = '1' then   -- index > ARITY - 1 (=3)        #not generic
                                       for i in 0 to SIZE_LINE_WORDS/2 - 1 loop   
                                           s_check_data(i+SIZE_LINE_WORDS) <= rd_reg_4.data(i);  
                                       end loop;
                                       v_addr        := unsigned(line_aligned(r_parent_addr)) - SIZE_LINE_BYTES;  
                                       s_parent_addr <= std_ulogic_vector(v_addr);
                                  else -- index leq ARITY 
                                       for i in 0 to SIZE_LINE_WORDS - 1 loop 
                                           s_check_data(i) <= rd_reg_4.data(i);  
                                       end loop;
                                       v_addr        := unsigned(line_aligned(r_parent_addr)) + SIZE_LINE_BYTES;  
                                       s_parent_addr <= std_ulogic_vector(v_addr);
                                  end if;   
                                  s_first <= False;
                                  s_check <= C_START_CHECK_REFERENCE_PARENT;
      
                               else -- lastlevel && first && !mac_match
                                  s_irq_op  <= LOAD;
                                  s_check   <= C_IRQ_SEND;
                               end if; -- mac_match
                          else -- lastlevel && !first => all last mac set nodes (6) read
                               s_addr <= getSetAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                               if r_index(2) = '1' then   -- index > ARITY - 1 (=3)        #not generic
                                    for i in 0 to SIZE_LINE_WORDS/2 - 1 loop   
                                        s_check_data(i+SIZE_LINE_WORDS) <= rd_reg_4.data(i);  
                                    end loop; 
                               else -- index leq ARITY 
                                    for i in 0 to SIZE_LINE_WORDS - 1 loop 
                                        s_check_data(i) <= rd_reg_4.data(i);  
                                    end loop; 
                               end if;   
                               s_check <= C_START_LAST_IC;
                          end if;
                     else -- not lastlevel
                          if v_mac_matches then
                              s_addr <= getSetAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                              for i in 0 to SIZE_LINE_WORDS - 1 loop 
                                  s_check_data(i) <= rd_reg_4.data(i); -- store parent mac with its brothers 
                              end loop;
                              s_parent_addr <= getParentAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                              s_check       <= C_START_CHECK_REFERENCE_PARENT;
                          else -- !lastlevel && no mac match 
                              s_irq_op  <= LOAD;
                              s_check   <= C_IRQ_SEND;
                          end if;
                     end if; -- last Level
      
                  end if; -- cache ack et cry rspval 
      
            when C_START_RESTORE_REFERENCE_PARENT_BROTHERS =>
                  s_index <= getBlockIndex(r_brother_addr, p_mb_address, p_size, r_pageLevel) & '0'; -- index * 2  
                  s_check <= C_WAIT_RESTORE_REFERENCE_PARENT_BROTHERS;
      
             when C_WAIT_RESTORE_REFERENCE_PARENT_BROTHERS =>
                  if rd_cache_ack.ack = '1' then
                        s_check_data(to_integer(r_index))     <= rd_cache_data(0); 
                        s_check_data(to_integer(r_index + 1)) <= rd_cache_data(1);
       
                        if isLastLevel(r_parent_addr, p_mb_address, p_size, r_pageLevel) then
                           if r_brother_cpt = LAST_HASH_ARITY - 2 then -- 4 
                              s_addr <= getSetAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                              s_check <= C_START_LAST_IC;  
                           else -- whole set not reached yet 
                              s_brother_cpt  <= r_brother_cpt + 1;
                              s_brother_addr <= getBrotherAddress(r_brother_addr, p_mb_address, p_size, r_pageLevel);         
                              s_check        <= C_START_RESTORE_REFERENCE_PARENT_BROTHERS;
                           end if;   
                        else -- !lastLevel
                           if r_brother_cpt = HASH_ARITY - 2 then -- 2   
                              -- whole set restored
                              s_parent_addr <= getParentAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                              s_addr        <= getSetAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                              s_check       <= C_START_CHECK_REFERENCE_PARENT;
                           else -- whole set not reached yet 
                              s_brother_cpt  <= r_brother_cpt + 1;
                              s_brother_addr <= getBrotherAddress(r_brother_addr, p_mb_address, p_size, r_pageLevel);         
                              s_check        <= C_START_RESTORE_REFERENCE_PARENT_BROTHERS;
                           end if;
                        end if; -- isLastLevel 
                  end if; -- cache ack
      
             when C_START_LAST_IC =>
                  if rd_crypto_cmd.cmdack = '1' then
                       s_check <= C_WAIT_LAST_IC;
                  end if;
      
             when C_WAIT_LAST_IC =>
                  if rd_crypto_cmd.rspval = '1' then
                     v_cry_mac := rd_crypto_data; 
       
                     if r_isMasterTree = '0' then
                         v_cry_mac(0) := rd_crypto_data(0)(31 downto 3) & "000"; 
                     end if; 
      
                     if v_cry_mac = r_ref_mac then
                         s_check       <= C_IDLE;
                     else
                         s_irq_op  <= LOAD;
                         s_check   <= C_IRQ_SEND;
                     end if;
                  end if;

              when C_IRQ_SEND =>  
                  --if rd_irq_ack = '1' then 
                  --   s_check <= C_IDLE;
                  --end if;
      end case;

      -- #-- UPDATE
     case r_updt is
           when U_IDLE =>
                      if rd_mt_cmd.enable = '1' and rd_mt_cmd.cmd =  UPDATE then 
                                s_addr          <= rd_mt_cmd.addr;
                                s_check_data    <= (others => (others => '0'));
                                s_first         <= True; 
                                s_key           <= sp_getKeyI(rd_master_ctx.sp);
                                
                                v_src           := to_integer(unsigned(rd_mt_cmd.src));
                                v_isMasterTree  := isMasterTree(rd_mt_cmd.addr, p_mb_address, p_size);
 
                                s_isMasterTree  <= v_isMasterTree;
                                
                                if v_isMasterTree = '1' then
                                    s_ref_mac(0)      <= rd_master_ctx.pspe(0);
                                    s_ref_mac(1)      <= rd_master_ctx.pspe(1);
                                    v_parent_addr     := getMTParentLeavesAddress(rd_mt_cmd.addr, p_mb_address, p_size);
                                    s_mt_pageLevel    <= getLevelFromSize(std_ulogic_vector(shift_right(getMMTSize(p_size),2))); 
                                    s_pageLevel       <= getLevelFromSize(std_ulogic_vector(shift_right(getMMTSize(p_size),2)));
                                    s_brother_addr    <= getMMacTBaseAddress(p_mb_address, p_size); 
                                else                               
                                    s_ref_mac(0)      <= rd_ctx_register.s_pspe(0)(31 downto 3) & "000";                             
                                    s_ref_mac(1)      <= rd_ctx_register.s_pspe(1);
                                    v_parent_addr     := getParentLeavesAddress(rd_mt_cmd.addr, rd_ctx_register.m_pspe);
                                    s_pageLevel       <= pspe_getSize(rd_ctx_register.m_pspe);
                                    s_brother_addr    <= pspe_getMSAdd(rd_ctx_register.m_pspe);
                                end if;
                                
                                s_parent_addr <= v_parent_addr;

                                case v_src is
                                     when 0 =>
                                          s_check_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_0.data;
                                     when 1 =>
                                          s_check_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_1.data;
                                     when 2 =>
                                          s_check_data(SIZE_LINE_WORDS - 1 downto 0) <= rd_reg_2.data;
                                end case;
      
                                s_wr_parent_addr <= v_parent_addr;
      
                                if p_cache = '1' then 
                                    s_updt  <= U_START_PARENT_UPDATE;
                                else
                                    s_leaves <= True; 
                                    s_updt  <= U_START_READ_BROTHERS;
                                end if;
                       end if;

             when U_START_PARENT_UPDATE =>    --compute mac set of leaves (data set)
                  if rd_crypto_cmd.cmdack = '1' then
                       s_updt <= U_WAIT_PARENT_UPDATE;
                  end if;
      
             when U_WAIT_PARENT_UPDATE =>     --keep mac set of nodes, compute parent address of nodes
                  if rd_crypto_cmd.rspval = '1' then
                      s_cry_mac <= rd_crypto_data;
                      s_updt   <= U_START_WRITE_NEW_PARENT;
                  end if;
      
             when U_START_WRITE_NEW_PARENT =>  -- cache_write(r_parent_addr, r_cry_mac)
                 --rst_fifo_init <= '1';
                 --syn_fifo_init <= '1';
                  s_index <= getBlockIndex(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel) & '0'; -- index * 2  
                  if p_cache = '1' then 
                     s_updt <= U_CACHE_WAIT_WRITE_NEW_PARENT;
                     s_rst_fifo_init <= '1';
                     s_syn_fifo_init <= '1';
                  elsif rd_crypto_cmd.cmdack = '1' then
                     s_updt <= U_MEM_WAIT_WRITE_NEW_PARENT;
                  end if;
      
             when U_CACHE_WAIT_WRITE_NEW_PARENT =>
                  if rd_cache_ack.ack = '1' then
                       if rd_cache_ack.full = '1' then
                          s_brother_cpt <= 0;
                          s_hit_cpt     <= 0;
                          s_victim_addr <= rd_cache_ack.victim_addr;
      
                          v_isMasterTree := isMAsterTree(rd_cache_ack.victim_addr, p_mb_address, p_size); 

                          s_isMasterTree <= v_isMasterTree;
        
                          if v_isMasterTree = '1' then
                               s_ref_mac(0)    <= rd_master_ctx.pspe(0);
                               s_ref_mac(1)    <= rd_master_ctx.pspe(1);
                               s_pageLevel     <= r_mt_pageLevel;
                               s_isLastLevel   <= isLastLevel(rd_cache_ack.victim_addr, p_mb_address, p_size, r_mt_pageLevel); 
                               s_brother_addr  <= getSetAddress(rd_cache_ack.victim_addr, p_mb_address, p_size, r_mt_pageLevel);
                               s_updt          <= U_START_READ_VICTIM_SET;
      
                          else -- not isMasterTree
                               if getPSPEAddressFromPageSize(rd_cache_ack.victim_addr,
                                                              pspe_getSize(rd_ctx_register.s_pspe),     
                                                              p_mb_address,
                                                              p_b_address) = rd_ctx_register.s_pspe_addr then    
       
                                    s_ref_mac(0)    <= rd_ctx_register.s_pspe(0)(31 downto 3) & "000";                             
                                    s_ref_mac(1)    <= rd_ctx_register.s_pspe(1);
                                    v_pageLevel     := pspe_getSize(rd_ctx_register.s_pspe);
                                    s_isLastLevel   <= isLastLevel(rd_cache_ack.victim_addr, p_mb_address, p_size, v_pageLevel); 
                                    s_pageLevel     <= v_pageLevel;
                                    s_brother_addr  <= getSetAddress(rd_cache_ack.victim_addr, p_mb_address, p_size, v_pageLevel);
                                    s_updt          <= U_START_READ_VICTIM_SET;                                           
                                else
                                    s_updt          <= U_START_READ_SLAVE_PSPE; 
                                end if; 
      
                          end if; -- isMasterTree
                       else -- not full
                          s_updt           <= U_IDLE;
                       end if; -- full
                  end if; -- cache ack
       
             when U_MEM_WAIT_WRITE_NEW_PARENT => -- cache disabled
                  if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then
                       if isLastLevel(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel) then
                          s_updt   <= U_START_LAST_IC;
                       else
                          s_check_data(to_integer(r_index))     <= r_cry_mac(0);  
                          s_check_data(to_integer(r_index + 1)) <= r_cry_mac(1);   
                          s_wr_parent_addr                      <= getParentAddress(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel);
                          s_updt                                <= U_START_READ_BROTHERS;
                       end if; -- lastlevel
      
                       s_old_mac <= rd_crypto_data;
      
                  end if; -- cache ack & crypto rspval
      
              when U_START_READ_BROTHERS =>
                  if not r_first  or rd_crypto_cmd.cmdack = '1' then
                     s_index <= '0' & getBlockIndex(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel); -- index * 2  
                     s_updt <= U_WAIT_READ_BROTHERS;
                  end if;
      
              when U_WAIT_READ_BROTHERS =>
                  if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then
                     v_mem_mac(0) := rd_reg_4.data(word_index(r_wr_parent_addr)); 
                     v_mem_mac(1) := rd_reg_4.data(word_index(r_wr_parent_addr)+1);
      
                     s_cry_mac    <= rd_crypto_data; 
       
                     if v_mem_mac = r_old_mac then  
                        v_mac_matches := True;
                     else
                        v_mac_matches := False;
                     end if;
      
                     if isLastLevel(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel) then
                          if r_first  then     
                               if v_mac_matches then
                                  if r_index(2) = '1' then   -- index > ARITY - 1 (=3)        #not generic
                                       for i in 0 to SIZE_LINE_WORDS/2 - 1 loop  -- 1 <=> HS_SIZE_IN_WORDS/2  
                                           s_check_data(i+SIZE_LINE_WORDS) <= rd_reg_4.data(i);  
                                       end loop;
                                       v_addr           := unsigned(line_aligned(r_wr_parent_addr)) - SIZE_LINE_BYTES;  
                                       s_wr_parent_addr <= std_ulogic_vector(v_addr);  
                                  else -- index leq ARITY 
                                       for i in 0 to SIZE_LINE_WORDS - 1 loop 
                                           s_check_data(i) <= rd_reg_4.data(i);  
                                       end loop;
                                       v_addr           := unsigned(line_aligned(r_wr_parent_addr)) + SIZE_LINE_BYTES;  
                                       s_wr_parent_addr <= std_ulogic_vector(v_addr);
                                  end if;   
                                  s_first        <= False; 
                                  s_brother_addr <= r_wr_parent_addr; -- saving write address
                                  s_addr         <= getSetAddress(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel);
                                  s_updt         <= U_START_READ_BROTHERS;
      
                               else -- lastlevel && first && !mac_match
                                  s_irq_op  <= LOAD;
                                  s_updt   <= U_IRQ_SEND;
                               end if; -- mac_match
                          else -- lastlevel && !first => second chunk, all last mac set nodes (6) read
                               if r_index(2) = '1' then   -- index > ARITY - 1 (=3)        #not generic
                                    for i in 0 to SIZE_LINE_WORDS/2  - 1 loop  -- 1 <=> HS_SIZE_IN_WORDS/2  
                                        s_check_data(i+SIZE_LINE_WORDS) <= rd_reg_4.data(i);  
                                    end loop; 
                               else -- index leq ARITY 
                                    for i in 0 to SIZE_LINE_WORDS - 1 loop 
                                        s_check_data(i) <= rd_reg_4.data(i);  
                                    end loop; 
                               end if;   
                               s_wr_parent_addr  <= r_brother_addr;
                               s_updt           <= U_START_WRITE_NEW_PARENT;
                          end if;
                     else -- not lastlevel
                          s_addr <= getSetAddress(r_wr_parent_addr, p_mb_address, p_size, r_pageLevel);
                          for i in 0 to SIZE_LINE_WORDS - 1 loop 
                              s_check_data(i) <= rd_reg_4.data(i); -- store parent mac with its brothers 
                          end loop;
      
                          if r_leaves then
                              s_leaves <= False; 
                              s_updt  <= U_START_WRITE_NEW_PARENT;
                          elsif v_mac_matches then
                              s_updt       <= U_START_WRITE_NEW_PARENT;
                          else -- !lastlevel && no mac match 
                              s_irq_op  <= LOAD;
                              s_updt   <= U_IRQ_SEND;
                          end if;
                     end if; -- last Level
                  end if; -- cache ack & cry rspval
      
              when U_START_READ_VICTIM_SET =>  -- mem read next victim brother and encrypt actual
                   if rd_crypto_cmd.cmdack = '1' then
                       s_updt      <= U_WAIT_READ_VICTIM_HASH_SET;
                       s_index      <= to_unsigned(r_brother_cpt, 3)  & '0'; -- index * 2  
                   end if;
      
              when U_WAIT_READ_VICTIM_HASH_SET =>    -- wait for cache read victim brother rsp and cryptoInt rsp
                   if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then
      
                       if rd_cache_ack.hit = '1' then
                          s_hit_cpt <= r_hit_cpt + 1;
                       end if;
      
                       if rd_cache_ack.dirty = '1' then
                          s_rst_fifo_write <= '1';
                          s_rst_fifo_wdata <= r_brother_addr;
      
                          s_syn_fifo_write <= '1';
                          s_syn_fifo_wdata <= r_brother_addr;
                       end if;
      
                       s_cry_mac <= rd_crypto_data;
      
                       s_check_data(to_integer(r_index))     <= rd_cache_data(0); -- store parent mac 
                       s_check_data(to_integer(r_index + 1)) <= rd_cache_data(1);
      
                       if r_isLastLevel then
                          v_arity := LAST_HASH_ARITY;
                       else
                          v_arity := HASH_ARITY;
                       end if;
      
                       if r_brother_cpt = v_arity - 1 then   
                          s_updt        <= U_START_NEW_HASH_GENERATION;
                       else -- whole set not reached yet 
                          s_brother_cpt  <= r_brother_cpt + 1;
                          s_brother_addr <= std_ulogic_vector(unsigned(r_brother_addr) + PSPE_SIZE_BYTES); -- 8;         
                          s_updt        <= U_START_READ_VICTIM_SET;
                       end if;   
                      
                   end if; -- cache rack 
      
              when U_START_NEW_HASH_GENERATION => -- Encipher last brother
                   if rd_crypto_cmd.cmdack = '1' then
                       s_updt  <= U_WAIT_NEW_HASH_GENERATION;
                   end if;
      
              when U_WAIT_NEW_HASH_GENERATION =>   -- keep mac set of victim set
                   if rd_crypto_cmd.rspval = '1' then
                       s_cry_mac <= rd_crypto_data;
      
                       if r_isLastLevel then  -- last level for victim_address
                          v_arity := LAST_HASH_ARITY;
                       else
                          v_arity := HASH_ARITY;
                       end if;
       
                       if r_hit_cpt = v_arity then
                          s_syn_fifo_read <= '1';
                          s_updt         <= U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS;
                       else -- hit_cpt 
                          --keep s_rst_addr from sync_fifo_rdata 
                          s_addr           <= getSetAddress(r_victim_addr, p_mb_address, p_size, r_pageLevel);
                          s_rst_fifo_read  <= '1';
                          s_updt           <= U_START_RESTORE_DIRTY_VICTIM_BROTHERS;
                       end if;  -- not hit_cpt                  
                   end if; -- crypto rspval
      
              when  U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS =>
                    s_updt <= U_WAIT_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS;
      
              when  U_WAIT_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS => 
                    if rd_cache_ack.ack = '1' then
                       if r_isLastLevel then
                          if s_syn_fifo_empty = '1' then
                             if r_isMasterTree = '1' then
                                s_updt       <= U_START_WRITE_MASTER_PSPE;
                             else -- not masterTree => update slave pspe
                                s_pspe(0)(31 downto 3) <= r_cry_mac(0)(31 downto 3);
                                s_pspe(0)( 2 downto 1) <= rd_ctx_register.s_pspe(0)(2 downto 1); -- size
                                s_pspe(0)(0)           <= rd_ctx_register.s_pspe(0)(0);          -- valid
                                s_pspe(1)              <= r_cry_mac(1);
                                s_updt                <= U_START_WRITE_PSPE; 
                             end if; -- masterTree 
                          else -- sync fifo not empty , synchro continue
                             s_syn_fifo_read <= '1';
                             s_updt         <= U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS; 
                          end if; -- sync fifo empty   
                       else -- not lastlevel
                          if s_syn_fifo_empty = '1' then
                             s_wr_parent_addr <= getParentAddress(r_victim_addr, p_mb_address, p_size, r_pageLevel);
                             s_updt          <= U_START_WRITE_NEW_PARENT;  
                          else -- sync fifo not empty , synchro continue
                             s_syn_fifo_read <= '1';
                             s_updt         <= U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS; 
                          end if; -- sync fifo empty
                       end if;  -- lastlevel
                    end if; -- cache ack
      
              when U_START_RESTORE_DIRTY_VICTIM_BROTHERS => 
                    s_index <= getBlockIndex(s_rst_fifo_rdata, p_mb_address, p_size, r_pageLevel) & '0'; -- index * 2  
                    s_updt <= U_WAIT_RESTORE_DIRTY_VICTIM_BROTHERS; 
       
              when U_WAIT_RESTORE_DIRTY_VICTIM_BROTHERS => 
                   if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then
                       s_check_data(to_integer(r_index))     <= rd_cache_data(0); -- store victim brother for computing CBC_MAC
                       s_check_data(to_integer(r_index) + 1) <= rd_cache_data(1);  
      
                       if r_isLastLevel then  
                          if s_rst_fifo_empty = '1' then
                             s_updt         <= U_START_LAST_IC;
                          else
                             s_rst_fifo_read <= '1';
                             s_updt         <= U_START_RESTORE_DIRTY_VICTIM_BROTHERS;
                          end if;                   
                       else
                          if s_rst_fifo_empty = '1' then
                             s_parent_addr  <= getParentAddress(r_victim_addr, p_mb_address, p_size, r_pageLevel);
                             s_updt         <= U_START_READ_REFERENCE_PARENT;
                          else
                             s_rst_fifo_read <= '1';
                             s_updt         <= U_START_RESTORE_DIRTY_VICTIM_BROTHERS;
                          end if;
                       end if; -- last level 
                   end if; -- cache ack & crypto rspval
      
              when U_START_LAST_IC =>
                  if rd_crypto_cmd.cmdack = '1' then
                      s_updt <= U_WAIT_LAST_IC;         
                  end if;
      
              when U_WAIT_LAST_IC =>
                  if rd_crypto_cmd.rspval = '1' then
                     v_cry_mac := rd_crypto_data; -- new mac
      
                     if r_isMasterTree = '0' then
                       v_cry_mac(0) := rd_crypto_data(0)(31 downto 3) & "000"; 
                     end if;

                     if r_ref_mac = v_cry_mac then  
                        if p_cache = '1' then
                              s_syn_fifo_read <= '1';
                              s_updt          <= U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS;
                        else
                              s_check_data(to_integer(r_index))     <= r_cry_mac(0);  
                              s_check_data(to_integer(r_index + 1)) <= r_cry_mac(1);  
                              s_updt                                <= U_START_UPDATE_ROOT;
                        end if;
                     else
                        s_irq_op  <= STORE;
                        s_updt   <= U_IRQ_SEND;
                     end if;
                  end if;

               when U_START_UPDATE_ROOT =>
                  if rd_crypto_cmd.cmdack = '1' then
                      s_updt <= U_WAIT_UPDATE_ROOT;         
                  end if;
      
              when U_WAIT_UPDATE_ROOT =>
                  if rd_crypto_cmd.rspval = '1' then
                      if r_isMasterTree = '1' then
                           s_cry_mac <= rd_crypto_data; -- new mac
                           s_updt    <= U_START_WRITE_MASTER_PSPE;
                      else -- not masterTree => update slave pspe
                           s_pspe(0)(31 downto 3) <= rd_crypto_data(0)(31 downto 3);
                           s_pspe(0)( 2 downto 1) <= rd_ctx_register.s_pspe(0)(2 downto 1); -- size
                           s_pspe(0)(0)           <= rd_ctx_register.s_pspe(0)(0);          -- valid
                           s_pspe(1)              <= rd_crypto_data(1);
                           s_updt                 <= U_START_WRITE_PSPE; 
                      end if; -- masterTree
                   end if;

              when U_START_READ_REFERENCE_PARENT =>
                  if rd_crypto_cmd.cmdack = '1' then
                      s_index        <= getBlockIndex(r_parent_addr, p_mb_address, p_size, r_pageLevel) & '0'; -- index * 2  
                      s_updt        <= U_WAIT_READ_REFERENCE_PARENT;         
                  end if;
       
              when U_WAIT_READ_REFERENCE_PARENT => 
                   if rd_cache_ack.ack = '1' and rd_crypto_cmd.rspval = '1' then
                      --s_cry_mac  <= rd_crypto_data;
      
                      if rd_cache_data = rd_crypto_data then 
                         if rd_cache_ack.hit = '1' then
                             s_syn_fifo_read <= '1';
                             s_updt         <= U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS;
                         else    -- not cache hit
                             s_addr                                <= getSetAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                             s_brother_cpt                         <= 0;
                             s_brother_addr                        <= getBrotherAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);         
                             s_check_data(to_integer(r_index))     <= rd_cache_data(0); -- store parent mac 
                             s_check_data(to_integer(r_index + 1)) <= rd_cache_data(1);
                             s_updt                                <= U_START_RESTORE_REFERENCE_PARENT_BROTHERS;
                         end if; -- cache hit
                      else
                         s_irq_op  <= STORE;
                         s_updt   <= U_IRQ_SEND;
                      end if; -- match(read parent mac, computed mac victim set with retored borthers)
                   end if; -- cache_ack and crypto_rspval
      
              when U_START_RESTORE_REFERENCE_PARENT_BROTHERS =>
                    s_index <= getBlockIndex(r_brother_addr, p_mb_address, p_size, r_pageLevel) & '0'; -- index * 2     
                    s_updt <= U_WAIT_RESTORE_REFERENCE_PARENT_BROTHERS;
      
              when U_WAIT_RESTORE_REFERENCE_PARENT_BROTHERS =>
                    if rd_cache_ack.ack = '1' then
                       s_check_data(to_integer(r_index))     <= rd_cache_data(0); -- store parent mac 
                       s_check_data(to_integer(r_index + 1)) <= rd_cache_data(1);
                      
                       if isLastLevel(r_parent_addr, p_mb_address, p_size, r_pageLevel) then
                          if r_brother_cpt = LAST_HASH_ARITY - 2 then
                               s_updt        <= U_START_LAST_IC; 
                          else
                               s_brother_cpt  <= r_brother_cpt + 1;
                               s_brother_addr <= getBrotherAddress(r_brother_addr, p_mb_address, p_size, r_pageLevel);         
                               s_updt          <= U_START_RESTORE_REFERENCE_PARENT_BROTHERS; 
                          end if;
                       else
                          if r_brother_cpt = HASH_ARITY - 2 then
                               s_parent_addr  <= getParentAddress(r_parent_addr, p_mb_address, p_size, r_pageLevel);
                               s_updt        <= U_START_READ_REFERENCE_PARENT;
                          else
                               s_brother_cpt  <= r_brother_cpt + 1;
                               s_brother_addr <= getBrotherAddress(r_brother_addr, p_mb_address, p_size, r_pageLevel);         
                               s_updt         <= U_START_RESTORE_REFERENCE_PARENT_BROTHERS;
                          end if;
                       end if; -- lastlevel
                    end if; -- cache ack
      
              when U_START_READ_SLAVE_PSPE =>
                   if rd_ctx_cmd.cmdack = '1' then
                        s_updt <= U_WAIT_READ_SLAVE_PSPE; 
                   end if;
      
              when U_WAIT_READ_SLAVE_PSPE => 
                   if rd_ctx_cmd.rspval = '1' and r_check = C_IDLE then
                        s_ref_mac(0)      <= rd_ctx_register.s_pspe(0)(31 downto 3) & "000";                            
                        s_ref_mac(1)      <= rd_ctx_register.s_pspe(1);
                        s_isMasterTree    <= '0';
                        v_pageLevel       := pspe_getSize(rd_ctx_register.s_pspe);
                        s_isLastLevel     <= isLastLevel(r_victim_addr, p_mb_address, p_size, v_pageLevel); 
                        s_pageLevel       <= v_pageLevel;
                        s_brother_addr    <= getSetAddress(r_victim_addr, p_mb_address, p_size, v_pageLevel);
                        s_brother_cpt     <= 0;
                        s_updt            <= U_START_READ_VICTIM_SET;                                
                  end if;
      
              when U_START_WRITE_PSPE =>
                   if rd_ctx_cmd.cmdack = '1' then
                      s_updt <= U_WAIT_WRITE_PSPE; 
                   end if;
      
              when U_WAIT_WRITE_PSPE => 
                   if rd_ctx_cmd.rspval = '1' then
                      s_updt           <= U_IDLE;    
                   end if;
      
              when U_START_WRITE_MASTER_PSPE =>  
                      s_updt           <= U_IDLE; 
      
              when U_IRQ_SEND =>  
                  --if rd_irq_ack = '1' then 
                  --   s_updt <= U_IDLE;
                  --end if; 
      end case;

end process;

------
pGeneration : process(r_init, r_check, r_updt,  
                      p_cache, r_parent_addr, r_addr, r_key, r_brother_addr, r_brother_cpt, r_index, r_level, 
                      r_check_data, r_cry_mac, s_syn_fifo_rdata, s_rst_fifo_rdata, r_victim_addr, rd_ctx_register, 
                      r_pspe, r_irq_op, r_first, r_isLastLevel, r_wr_parent_addr, r_mt)  

variable v_inc : natural range 0 to 4;
begin

wr_crypto_cmd.cmdval      <= '0';  
wr_crypto_cmd.mode        <= CRY_MAC;
wr_crypto_cmd.key         <= key_null;
wr_crypto_cmd.iv          <= (others => '0');
wr_crypto_cmd.size        <= (others => '0');      
wr_crypto_cmd.cipher      <= '0';
wr_crypto_cmd.id_src      <= (others => '0');
wr_crypto_cmd.id_dest     <= (others => '0');
wr_crypto_cmd.direct_data <= '0';                        
wr_crypto_cmd.no_init     <= '0'; 
wr_crypto_cmd.address     <= (others => '0');
wr_crypto_cmd.data_ready  <= '0';

wr_crypto_data            <= (others => (others => '0'));

wr_ctx_cmd.enable         <= '0';
wr_ctx_cmd.mt             <= '0';
wr_ctx_cmd.address        <= (others => '0'); 
wr_ctx_cmd.cmd            <= READ;               

wr_master_update.enable   <= '0'; 
wr_master_update.pspe     <= pspe_none;
wr_master_update.sp       <= sp_none;

wr_mt_ack                 <= ('0','0','0','0');
wr_mt_data                <= pspe_none;

wr_irq.irq                <= '0';
wr_irq.addr               <= (others => '0');
wr_irq.typ                <= NONE;
wr_irq.op                 <= LOAD;

wr_cache_cmd.address      <= (others => '0');
wr_cache_cmd.enable       <= '0';
wr_cache_cmd.cmd          <= READ;
wr_cache_data             <= (others => (others => '0'));

p_direct_mem              <= '0';

-- #-- INIT 
case r_init  is
     when I_IDLE =>
         wr_mt_ack.iack <= '1';

     when I_START_WRITE_NULL_BROTHER =>
          wr_cache_cmd.address    <= r_brother_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= WRITE;      
          wr_cache_data           <= (others => (others => '0'));
          p_direct_mem            <= '1';

    when I_START_READ_BROTHER =>
          wr_cache_cmd.address    <= r_brother_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= READ;

     when I_START_WRITE_SET =>
          wr_cache_cmd.address    <= r_victim_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= WRITE;      
          wr_cache_data           <= (others => (others => '0')); -- init set value = Zero or any other value defined in init settings
          p_direct_mem            <= '1';

     when I_START_FIRST_IV_GENERATION =>
          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.address   <= r_addr;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= X"2";

     when I_START_IV_GENERATION =>
          wr_crypto_cmd.cmdval    <= '1';
          if r_isLastLevel then 
              -- aligned set address (8 nodes), last set contains 6 nodes 
              wr_crypto_cmd.address(r_wr_parent_addr'left downto 8) <= r_wr_parent_addr(r_wr_parent_addr'left downto 8);
              wr_crypto_cmd.address(7 downto 0) <= X"00";
          else
              wr_crypto_cmd.address <= r_wr_parent_addr;
          end if;

          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= X"2"; 

     when I_START_UPDATE_LEAVE_PARENT =>
          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.no_init   <= '1'; 
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.iv        <= r_mt(1) & r_mt(0);  
          wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));     
 
          wr_crypto_data          <= (others => (others => '0')); -- init set value = Zero or any other value defined in init settings

     when I_START_UPDATE_PARENT_M =>
          wr_cache_cmd.address    <= r_wr_parent_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= WRITE;      
          wr_cache_data(0)        <= r_mt(r_level*2);     
          wr_cache_data(1)        <= r_mt(r_level*2 + 1);     
          p_direct_mem            <= '1';

          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.no_init   <= '1'; 
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= X"2";
          wr_crypto_cmd.iv        <= r_mt((r_level+1)*2 + 1) & r_mt((r_level+1)*2);
          wr_crypto_data(0)       <= r_mt(r_level*2);  
          wr_crypto_data(1)       <= r_mt(r_level*2 + 1);

     when I_START_UPDATE_ROOT_M =>
          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.address   <= r_wr_parent_addr;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS+4, 4));      
          wr_crypto_data          <= r_check_data;

     when I_START_WRITE_MASTER_PSPE =>

         wr_master_update.enable   <= '1'; 
         wr_master_update.pspe(0)  <= r_cry_mac(0);
         wr_master_update.pspe(1)  <= r_cry_mac(1);

     when I_START_WRITE_PSPE   =>
          wr_ctx_cmd.enable  <= '1';
          wr_ctx_cmd.mt      <= '1';
          wr_ctx_cmd.address <= rd_ctx_register.s_pspe_addr; 
          wr_ctx_cmd.cmd     <= UPDATE;              
          wr_mt_data         <= r_pspe;

    when I_IRQ_SEND =>
          wr_irq.irq     <= '1';
          wr_irq.addr    <= r_addr;  
          wr_irq.typ     <= HT;
          wr_irq.op      <= r_irq_op;

     when others =>
          null;
end case;
-- #-- CHECK
case r_check  is
     when C_IDLE =>
         wr_mt_ack.cack <= '1';

     when C_START_CHECK_REFERENCE_PARENT => 
          wr_cache_cmd.address      <= r_parent_addr;
          wr_cache_cmd.enable       <= '1';
          wr_cache_cmd.cmd          <= READ;         

          if r_first then
              wr_crypto_cmd.cmdval  <= '1';  
              wr_crypto_cmd.address <= r_addr;
              wr_crypto_cmd.mode    <= CRY_MAC;
              wr_crypto_cmd.key     <= r_key;
              wr_crypto_cmd.size    <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));      
              wr_crypto_data        <= r_check_data;
          end if;

     when C_START_RESTORE_REFERENCE_PARENT_BROTHERS =>
          wr_cache_cmd.address    <= r_brother_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= RESTORE;      

     when C_START_LAST_IC =>
          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.address   <= r_addr;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS+4, 4));      
          wr_crypto_data          <= r_check_data;

    when C_IRQ_SEND =>
          wr_irq.irq     <= '1';
          wr_irq.addr    <= r_addr;  
          wr_irq.typ     <= HT;
          wr_irq.op      <= r_irq_op;

     when others =>
          null;
end case;

-- #-- UPDATE
case r_updt  is
     when U_IDLE =>
         wr_mt_ack.uack     <= '1';
         wr_mt_ack.alap_ack <= '1';

     when U_START_PARENT_UPDATE =>

          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.address   <= r_addr;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));      
          wr_crypto_data          <= r_check_data;  --r_reg_data; @to be verified

    when U_START_WRITE_NEW_PARENT =>
          wr_cache_cmd.address    <= r_wr_parent_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= WRITE;      
          wr_cache_data           <= r_cry_mac;

          if p_cache = '0' then
                v_inc := 0;    
                if r_isLastLevel then    
                  v_inc := 4;
                end if;
                  
                wr_crypto_cmd.cmdval    <= '1';  
                wr_crypto_cmd.address   <= r_addr;
                wr_crypto_cmd.mode      <= CRY_MAC;
                wr_crypto_cmd.key       <= r_key;
                wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS+v_inc, 4));      
                wr_crypto_data          <= r_check_data;
          end if;

    when U_START_READ_BROTHERS =>
          wr_cache_cmd.address    <= r_wr_parent_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= READ;
  
          if r_first then
                wr_crypto_cmd.cmdval    <= '1';  
                wr_crypto_cmd.address   <= r_addr;
                wr_crypto_cmd.mode      <= CRY_MAC;
                wr_crypto_cmd.key       <= r_key;
                wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));      
                wr_crypto_data          <= r_check_data;
          end if;

    when U_START_READ_VICTIM_SET =>
          wr_cache_cmd.address    <= r_brother_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= READ;      

          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= X"2";

          if r_brother_cpt = 0 then
             wr_crypto_cmd.address   <= r_brother_addr;
          else                        
              wr_crypto_cmd.no_init <= '1';
              wr_crypto_cmd.iv      <= r_cry_mac(1) & r_cry_mac(0);
              wr_crypto_data(0)     <= r_check_data(to_integer(r_index));
              wr_crypto_data(1)     <= r_check_data(to_integer(r_index + 1));
          end if;

    when U_START_NEW_HASH_GENERATION =>
          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.no_init   <= '1'; 
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= X"2";
          wr_crypto_cmd.iv        <= r_cry_mac(1) & r_cry_mac(0);
          wr_crypto_data(0)       <= r_check_data(to_integer(r_index));
          wr_crypto_data(1)       <= r_check_data(to_integer(r_index + 1));
 
    when U_START_SYNCHRONIZE_DIRTY_VICTIM_BROTHERS => 
          wr_cache_cmd.address    <= s_syn_fifo_rdata;
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= SYNCHRONIZE;      

    when U_START_RESTORE_DIRTY_VICTIM_BROTHERS => 
          wr_cache_cmd.address    <= s_rst_fifo_rdata; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= RESTORE;

    when U_START_READ_SLAVE_PSPE =>
          wr_ctx_cmd.enable  <= '1';
          wr_ctx_cmd.mt      <= '1';
          wr_ctx_cmd.address <= r_victim_addr; 
          wr_ctx_cmd.cmd     <= READ;

    when U_WAIT_READ_SLAVE_PSPE =>
          wr_mt_ack.alap_ack <= '1';

    when U_START_LAST_IC =>
          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.address   <= r_addr;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS+4, 4));      
          wr_crypto_data          <= r_check_data;


    when U_START_UPDATE_ROOT =>
          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.address   <= r_addr;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS+4, 4));      
          wr_crypto_data          <= r_check_data;

    when U_START_READ_REFERENCE_PARENT => 
          wr_cache_cmd.address    <= r_parent_addr;
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= READ;         

          wr_crypto_cmd.cmdval    <= '1';  
          wr_crypto_cmd.address   <= r_addr;
          wr_crypto_cmd.mode      <= CRY_MAC;
          wr_crypto_cmd.key       <= r_key;
          wr_crypto_cmd.size      <= std_ulogic_vector(to_unsigned(SIZE_LINE_WORDS, 4));      
          wr_crypto_data          <= r_check_data;

    when U_START_RESTORE_REFERENCE_PARENT_BROTHERS =>
          wr_cache_cmd.address    <= r_brother_addr; 
          wr_cache_cmd.enable     <= '1';
          wr_cache_cmd.cmd        <= RESTORE;

    when U_START_WRITE_MASTER_PSPE =>

         wr_master_update.enable   <= '1'; 
         wr_master_update.pspe(0)  <= r_cry_mac(0);
         wr_master_update.pspe(1)  <= r_cry_mac(1);

    when U_START_WRITE_PSPE   =>
          wr_ctx_cmd.enable  <= '1';
          wr_ctx_cmd.mt      <= '1';
          wr_ctx_cmd.address <= rd_ctx_register.s_pspe_addr; 
          wr_ctx_cmd.cmd     <= UPDATE;              
          wr_mt_data         <= r_pspe;

    when U_WAIT_WRITE_PSPE =>
          wr_mt_ack.alap_ack <= '1';

    when U_IRQ_SEND =>
          wr_irq.irq     <= '1';
          wr_irq.addr    <= r_addr;  
          wr_irq.typ     <= HT;
          wr_irq.op      <= r_irq_op;

     when others =>
          null;
end case;

end process;

end rtl;


