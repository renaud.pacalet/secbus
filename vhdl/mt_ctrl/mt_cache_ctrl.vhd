--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mt_cache_ctrl.vhd
-- mt cache : set-associative  
--            write_Back policy
--            pseudo-lru
--            ram block       
--
-- <------------------------------------------ address------------------------- --------------------->
-- ---------------------------------------------------------------------------------------------------
-- |     Tag                                             |         set         | Block               |
-- ---------------------------------------------------------------------------------------------------
-- <--- addr_size - log2(set_size) - log2(block_size) --><-- log2(set_size) --><-- log2(block_size -->

-- Registers : 
--     R4 : from Memory Ctrl
-- Cmd :  
--      000 : read
--      001 : write
--      010 : synchronize
--      011 : restore
--      100 : inval        -- not used
--      101 : immediate    -- not used
-- cache cmd : 0000 : nop; 1000 : inval; 0100 : read; 0010 : write dir; 0001 : write data

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

library caches_lib;
use caches_lib.all;

entity mt_cache_ctrl is
generic(
        WAYS            : natural := 8;
        SETS            : natural := 16;
        BLOCKS          : natural := 2;
        TSH             : natural := 4   -- Threshold = associativity (ways) / 2 
);
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       p_cache                 : in  std_ulogic;
       p_direct_mem            : in  std_ulogic;

       -- Request from Mt Ctrl
       rd_cache_cmd            : in cache_ctrl_cmd; 
       rd_cache_data           : in data_vector(1 downto 0);
      
       -- To Mt Ctrl
       wr_cache_ack            : out cache_ctrl_ack;
       wr_cache_data           : out data_vector(1 downto 0);

       -- Request to Mem Ctrl
       wr_mem_rq               : out memory_ctrl_cmd_out; --  
       rd_mem_rq               : in  memory_ctrl_cmd_in;  -- cmdack, rspval

       wr_mem_data             : out direct_data; -- data(2)                      

       -- Data from Memory    
       rd_reg_4                : in  data_register_read  
);

end mt_cache_ctrl;

architecture rtl of mt_cache_ctrl is


type fsm_state is (IDLE            ,
--                 INVAL_CACHE_HIT , INVAL_CACHE_WRITE ,
                   READ_CACHE_HIT  , 
                   READ_MEM_CMD    , READ_MEM_RSP      ,
                   WRITE_CACHE_HIT , WRITE_CACHE_DATA  , 
                   W_WRITE_MEM_CMD , S_WRITE_MEM_CMD   , 
                   WRITE_MEM_RSP   ,                       
                   SYNC_CACHE_HIT  , 
                   REST_CACHE_HIT  ,
                   M_STORE_CACHE   ,
                   CHECK_FULL      
                  );

signal s_state       , r_state              : fsm_state;

-- ms cache signals
signal s_cache_cmd                          : std_ulogic_vector(3 downto 0); 
signal s_cache_way_in                       : std_ulogic_vector(WAYS - 1 downto 0);
signal s_cache_dirty                        : std_ulogic;
signal s_cache_updTsh                       : std_ulogic;
signal s_cache_incTsh                       : std_ulogic;
signal s_cache_lru                          : std_ulogic;
signal s_cache_wdir                         : std_ulogic;
signal s_cache_wdata                        : std_ulogic;
signal s_cache_addr_in                      : addr_t;
signal s_cache_data_in                      : data_vector(BLOCKS     - 1 downto 0);
signal s_cache_way_out                      : std_ulogic_vector(WAYS - 1 downto 0);
signal s_cache_addr_out                     : addr_t;
signal s_cache_data_out                     : data_vector(BLOCKS     - 1 downto 0); 
signal s_cache_hit                          : std_ulogic;
signal s_cache_dirty_out                    : std_ulogic;
signal s_cache_full                         : std_ulogic;
signal s_ways         , r_ways              : std_ulogic_vector(WAYS - 1 downto 0);

signal s_mt_addr      , r_mt_addr           : addr_t;
signal s_mt_data      , r_mt_data           : data_vector(BLOCKS     - 1 downto 0);
signal s_mem_data     , r_mem_data          : data_vector(BLOCKS     - 1 downto 0);
signal s_mt_ack       , r_mt_ack            : cache_ctrl_ack;

signal s_miss_wr      , r_miss_wr           : std_ulogic;
signal s_direct_mem   , r_direct_mem        : std_ulogic;

------
begin

mt_cache_inst : entity caches_lib.mt_cache 
generic map(
        WAYS            => WAYS, 
        SETS            => SETS,
        BLOCKS          => BLOCKS,
        TSH             => TSH     
)
port map(
        clk             => clk, 
        srstn           => srstn, 
        ce              => ce,
        
        cmd             => s_cache_cmd, 
                        
        updTsh_in       => s_cache_updTsh, 
        incTsh_in       => s_cache_incTsh, 
        dirty_in        => s_cache_dirty,
  
        way_in          => s_cache_way_in,
        addr_in         => s_cache_addr_in,
        data_in         => s_cache_data_in,
        lru_in          => s_cache_lru,
        
        way_out         => s_cache_way_out,
        addr_out        => s_cache_addr_out,
        data_out        => s_cache_data_out,
        dirty_out       => s_cache_dirty_out,
        hit             => s_cache_hit,    
        full            => s_cache_full    
     
);

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state         <= IDLE;
              r_mt_addr       <= (others => '0');
              r_ways          <= (others => '0'); 
              r_mt_data       <= (others => (others => '0'));
              r_mem_data      <= (others => (others => '0'));
              r_mt_ack        <= cache_ctrl_ack_none; 
              r_miss_wr       <= '0'; 
              r_direct_mem    <= '0';     

        elsif ce = '1' then
              r_state         <= s_state; 
              r_mt_addr       <= s_mt_addr;  
              r_ways          <= s_ways;      
              r_mt_data       <= s_mt_data;   
              r_mem_data      <= s_mem_data; 
              r_mt_ack        <= s_mt_ack;    
              r_miss_wr       <= s_miss_wr;       
              r_direct_mem    <= s_direct_mem;     
        end if;
end if;
end process;

 
-- *
p_Transition : process(r_state          , rd_cache_cmd    , rd_cache_data, rd_mem_rq , 
                       s_cache_addr_out , s_cache_data_out, p_cache      , p_direct_mem, 
                       s_cache_dirty_out, s_cache_way_out , s_cache_full ,
                       r_ways           , r_mt_ack        , r_miss_wr    ,
                       r_mt_addr        , r_mt_data       , s_cache_hit  , 
                       r_mem_data       , rd_reg_4        , r_direct_mem)

variable v_mem_data : data_vector(1 downto 0);

begin

s_state          <= r_state;

s_mt_addr        <= r_mt_addr;
s_mt_data        <= r_mt_data;
s_mem_data       <= r_mem_data;
s_ways           <= r_ways;
s_mt_ack         <= r_mt_ack; 
s_miss_wr        <= r_miss_wr;

s_cache_cmd      <= (others => '0');
s_cache_way_in   <= (others => '0');
s_cache_addr_in  <= (others => '0');
s_cache_data_in  <= (others => (others => '0'));
s_cache_wdir     <= '0';
s_cache_wdata    <= '0';
s_cache_dirty    <= '0';
s_cache_updTsh   <= '0';
s_cache_incTsh   <= '0';
s_cache_lru      <= '0';
s_direct_mem     <= r_direct_mem; 

case r_state is
     when IDLE =>
              if rd_cache_cmd.enable = '1' then
                 s_mt_addr    <= rd_cache_cmd.address;
                 s_mt_data    <= rd_cache_data;
                 s_mt_ack     <= cache_ctrl_ack_none;
                 s_miss_wr    <= '0';
                 s_direct_mem <= p_direct_mem;

                  -- cache lookup (read)
                 if p_cache = '1' then
                    s_cache_addr_in <= rd_cache_cmd.address;
                    s_cache_cmd(2)  <= '1';       
                 end if;

                 case rd_cache_cmd.cmd is
--                    when INVALIDATE  =>
--                         s_state <= INVAL_CACHE_HIT;

                      when READ =>
                           if (p_cache = '1') and (p_direct_mem = '0') then
                               s_state   <= READ_CACHE_HIT;
                           else 
                               s_state   <= READ_MEM_CMD;
                           end if;
 
                      when WRITE =>
                           if p_cache = '0' then
                               s_state <= W_WRITE_MEM_CMD;
                           else
                               s_state <= WRITE_CACHE_HIT;
                           end if;

                          --if (p_cache = '1') and (p_direct_mem = '0') then
                          --    s_mem_data <= rd_cache_data;
                          --end if;

                      when SYNCHRONIZE =>
                           s_state <= SYNC_CACHE_HIT;

                      when RESTORE     =>
                           s_state <= REST_CACHE_HIT;

                      when others =>
                           null;
                 end case;
              else
                 s_state <= IDLE;
              end if;

--   when INVAL_CACHE_HIT =>
--       s_mt_ack.hit        <= s_cache_hit;
--
--       if s_cache_hit = '1' then
--          s_ways           <= s_cache_way_out;
--          s_state          <= INVAL_CACHE_WRITE; 
--       else -- 
--          s_state          <= IDLE;
--          s_mt_ack.ack     <= '1';
--       end if;
--
--   when INVAL_CACHE_WRITE =>
--
--       s_cache_addr_in  <= r_mt_addr;
--       s_cache_cmd(3)   <= '1';    -- inval 
--
--       s_cache_updTsh   <= '1';
--       s_cache_incTsh   <= '0';
--       s_mt_ack.ack     <= '1';
--
--       s_state          <= IDLE;

    when READ_CACHE_HIT =>
 
        if s_cache_hit = '1' then
           --* gen
           s_cache_lru    <= '1'; 
           s_mt_ack.ack   <= '1';
           s_mt_ack.hit   <= '1';
           s_mt_ack.dirty <= s_cache_dirty_out;
           s_mem_data     <= s_cache_data_out;
           s_state        <= IDLE;
        else -- 
           s_miss_wr      <= '1';
           s_ways         <= s_cache_way_out;
           s_state        <= READ_MEM_CMD;
        end if;


    when WRITE_CACHE_HIT =>
         s_mt_ack.hit         <= s_cache_hit;
--       s_mt_ack.victim_addr <= s_cache_addr_out;
         s_mt_ack.dirty       <= s_cache_dirty_out;
         s_ways               <= s_cache_way_out;

         if r_direct_mem = '1' then
            s_state <= W_WRITE_MEM_CMD;  
         else 
            s_state <= WRITE_CACHE_DATA;
         end if;
        
    when WRITE_CACHE_DATA  =>
        if r_mt_ack.hit = '0' or r_mt_ack.dirty = '0' then
           s_cache_updTsh <= '1';
           s_cache_incTsh <= '1';
           s_state        <= CHECK_FULL;
        else
           s_mt_ack.ack  <= '1';
           s_state       <= IDLE; 
        end if;

        s_cache_addr_in  <= r_mt_addr;
        s_cache_cmd(1)   <= '1';   --not r_mt_ack.hit; -- write dir 
        s_cache_cmd(0)   <= '1';   -- write data 
        s_cache_dirty    <= '1';
        s_cache_data_in  <= r_mt_data;
        s_cache_way_in   <= r_ways;

    when CHECK_FULL =>
        s_mt_ack.full        <= s_cache_full;
        s_mt_ack.victim_addr <= s_cache_addr_out;
        s_mt_ack.ack         <= '1';
        s_state              <= IDLE;

    when SYNC_CACHE_HIT =>
         s_mt_ack.hit       <= s_cache_hit;
         s_mt_ack.dirty     <= s_cache_dirty_out;
         s_ways             <= s_cache_way_out;
         s_mem_data         <= s_cache_data_out;


         if s_cache_hit = '1' then 
            s_cache_lru     <= '1'; --# 
            if s_cache_dirty_out = '0' then
               s_state      <= IDLE;
               --* gen 
               s_mt_ack.ack <= '1';
               s_mt_ack.hit <= '1';
            else
               s_state <= S_WRITE_MEM_CMD;
            end if;

        -- miss & match R4  => read R4, store in cache
         elsif line_aligned(r_mt_addr) = rd_reg_4.addr then  
             s_mem_data(0) <= rd_reg_4.data(word_index(r_mt_addr));
             s_mem_data(1) <= rd_reg_4.data(word_index(r_mt_addr) + 1);
             s_state       <= M_STORE_CACHE;
               
         -- miss & !match R4  => read mem, store in cache
         else -- miss
            s_miss_wr <= '1';
            s_state   <= READ_MEM_CMD;
         end if;

    when REST_CACHE_HIT =>

        s_ways      <= s_cache_way_out;

        if s_cache_hit = '1' then
           s_mt_ack.hit   <= '1';
           s_mt_ack.dirty <= s_cache_dirty_out;

           if s_cache_dirty_out = '0' then
               --* gen 
               s_cache_lru    <= '1'; --# 
               s_mt_ack.ack   <= '1';
               s_mem_data     <= s_cache_data_out;
               s_state        <= IDLE;
            -- hit & dirty & match R4 => read R4
            elsif line_aligned(r_mt_addr) = rd_reg_4.addr then  
               s_mt_ack.ack  <= '1';
               s_mem_data(0) <= rd_reg_4.data(word_index(r_mt_addr));
               s_mem_data(1) <= rd_reg_4.data(word_index(r_mt_addr) + 1);
               s_state       <= IDLE;
            -- hit & dirty & !match R4 => read mem
            else
               s_state        <= READ_MEM_CMD;
            end if;
        -- miss & match R4  => read R4, store in cache
        elsif line_aligned(r_mt_addr) = rd_reg_4.addr then 
             s_mem_data(0) <= rd_reg_4.data(word_index(r_mt_addr));
             s_mem_data(1) <= rd_reg_4.data(word_index(r_mt_addr) + 1);
             s_state       <= M_STORE_CACHE;

        -- miss & !match R4  => read mem, store in cache
        else -- 
           s_miss_wr  <= '1';    
           s_state    <= READ_MEM_CMD;
        end if;

    when READ_MEM_CMD =>  
        if rd_mem_rq.cmdack = '1' then            
           s_state <= READ_MEM_RSP;
        else 
           s_state <= READ_MEM_CMD;
        end if;

   when READ_MEM_RSP =>  

          if rd_mem_rq.rspval = '1' then
             v_mem_data(0) := rd_reg_4.data(word_index(r_mt_addr));
             v_mem_data(1) := rd_reg_4.data(word_index(r_mt_addr) + 1);

--           if (p_cache = '1') and (r_direct_mem = '0') then
             if r_miss_wr = '1' then
                 s_cache_addr_in   <= r_mt_addr;
                 s_cache_cmd(1)    <= '1'; --r_miss_wr;   -- write dir
                 s_cache_cmd(0)    <= '1'; --r_miss_wr;   -- write data
                 s_cache_dirty     <= '0';
                 s_cache_data_in   <= v_mem_data;
                 s_cache_way_in    <= r_ways;
                 s_mt_ack.dirty    <= '0';  
             end if;

             s_mt_ack.ack          <= '1';
             s_mem_data            <= v_mem_data;
             s_state               <= IDLE;
          end if; --rspval

    when M_STORE_CACHE   =>
             s_cache_addr_in   <= r_mt_addr;
             s_cache_cmd(1)    <= '1'; --r_miss_wr;   -- write dir
             s_cache_cmd(0)    <= '1'; --r_miss_wr;   -- write data
             s_cache_dirty     <= '0';
             s_cache_data_in   <= r_mem_data;
             s_cache_way_in    <= r_ways;
             s_mt_ack.dirty    <= '0';  

             s_mt_ack.ack      <= '1';
             s_state           <= IDLE;

    when S_WRITE_MEM_CMD =>  
          if rd_mem_rq.cmdack = '1' then          
             s_cache_cmd(1)   <= '1'; -- clean 
             s_cache_dirty    <= '0';
             s_cache_addr_in  <= r_mt_addr;
             s_cache_way_in   <= r_ways;
             s_cache_updTsh   <= '1';
             s_cache_incTsh   <= '0';

             s_state          <= WRITE_MEM_RSP;
          end if;

    when W_WRITE_MEM_CMD =>  
          if rd_mem_rq.cmdack = '1' then          
             if (p_cache = '1') and (r_mt_ack.hit='1') then -- direct_mem = 1
                 s_cache_cmd(3)   <= '1'; -- inval 
                 s_cache_addr_in  <= r_mt_addr;
                 s_cache_dirty    <= '0';
                 s_cache_way_in   <= r_ways;

                 if r_mt_ack.dirty = '1' then
                     s_cache_updTsh   <= '1';
                     s_cache_incTsh   <= '0';
                 end if; 
              end if;
              s_state <= WRITE_MEM_RSP;
          end if;

     when WRITE_MEM_RSP => 
            if rd_mem_rq.rspval = '1' then
               s_mt_ack.ack <= '1';
               s_state      <= IDLE;
            end if; --rspval

end case;
end process;

-- generation
wr_cache_ack     <= r_mt_ack;
wr_cache_data    <= r_mem_data;
--

pMem_Generation : process(r_state, r_mt_addr, r_mem_data, r_mt_data) 

begin

wr_mem_rq.cmdval        <= '0';
wr_mem_rq.cmd           <= READ;
wr_mem_rq.start_address <= (others => '0');
wr_mem_rq.plen          <= (others => '0');
wr_mem_rq.id            <= (others => '0'); 
wr_mem_rq.direct_write  <= '0';
 
wr_mem_data.enable      <= '0'; 
wr_mem_data.data        <= (others => (others => '0')); 

case r_state is

       when READ_MEM_CMD =>
            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= READ;
            wr_mem_rq.start_address <= line_aligned(r_mt_addr); 
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(SIZE_LINE_BYTES, PLEN_SIZE));
            wr_mem_rq.id            <= (4 => '1', others => '0'); 

       when S_WRITE_MEM_CMD =>

            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= WRITE;
            wr_mem_rq.start_address <= std_ulogic_vector(r_mt_addr); -- not aligned 
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(BLOCKS*CELL_SIZE, PLEN_SIZE));
            wr_mem_rq.direct_write  <= '1';
 
            wr_mem_data.enable      <= '1';         
            wr_mem_data.data        <= r_mem_data;

       when W_WRITE_MEM_CMD =>

            wr_mem_rq.cmdval        <= '1';
            wr_mem_rq.cmd           <= WRITE;
            wr_mem_rq.start_address <= std_ulogic_vector(r_mt_addr); -- not aligned 
            wr_mem_rq.plen          <= std_ulogic_vector(to_unsigned(BLOCKS*CELL_SIZE, PLEN_SIZE));
            wr_mem_rq.direct_write  <= '1';
 
            wr_mem_data.enable      <= '1';         
            wr_mem_data.data        <= r_mt_data;

       when others => 
            null;

end case;

end process;

end rtl;


