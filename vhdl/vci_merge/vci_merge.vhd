--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- vci_merge.vhd

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

entity vci_merge is
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- Req from  Vci_Split - Target
       p_vci_ini_0_in        : in  vci_i2t;
       p_vci_ini_0_out       : out vci_t2i; 

       -- Req from  Vci_Mem_Ctrl - Target
       p_vci_ini_1_in        : in  vci_i2t;
       p_vci_ini_1_out       : out vci_t2i; 

       -- Req to  Vci External Memory Ctrl - Init
       p_vci_tgt_out         : out vci_i2t;
       p_vci_tgt_in          : in  vci_t2i

);

end vci_merge;

architecture rtl of vci_merge is

type fsm_state is (IDLE, CMD_0, CMD_1, RSP_0, RSP_1);

signal s_state, r_state : fsm_state;

begin

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state <= IDLE;
        elsif ce = '1' then
              r_state <= s_state;
        end if;
end if;
end process;


pTransition : process(r_state, p_vci_ini_0_in, p_vci_ini_1_in, p_vci_tgt_in)

begin

s_state <= r_state;

case r_state is
       when IDLE =>
            -- Init 0 has highest priority
            if p_vci_ini_0_in.cmdval = '1' then  
                    s_state <= CMD_0;
            elsif p_vci_ini_1_in.cmdval = '1' then
                    s_state <= CMD_1;
            else
                    s_state <= IDLE;
            end if;

       when CMD_0 => 
            if (p_vci_ini_0_in.cmdval = '1' and p_vci_ini_0_in.eop = '1' and p_vci_tgt_in.cmdack = '1') then
                    s_state <= RSP_0;
            else
                    s_state <= CMD_0;
            end if;

       when CMD_1 => 
            if (p_vci_ini_1_in.cmdval = '1' and p_vci_ini_1_in.eop = '1' and p_vci_tgt_in.cmdack = '1') then
                    s_state <= RSP_1;
            else
                    s_state <= CMD_1;
            end if;

       when RSP_0 =>
            if (p_vci_tgt_in.rspval = '1' and p_vci_tgt_in.reop = '1' and p_vci_ini_0_in.rspack = '1') then
                   s_state <= IDLE;
            else
                   s_state <= RSP_0;

            end if;

        when RSP_1 =>
            if (p_vci_tgt_in.rspval = '1' and p_vci_tgt_in.reop = '1' and p_vci_ini_1_in.rspack = '1') then
                   s_state <= IDLE;
            else
                   s_state <= RSP_1;

            end if;
end case;      
       
end process;

pGeneration : process(r_state, p_vci_tgt_in, p_vci_ini_0_in, p_vci_ini_1_in)

begin

p_vci_ini_0_out <= vci_t2i_none;
p_vci_ini_1_out <= vci_t2i_none;
p_vci_tgt_out   <= vci_i2t_none;

case r_state is
        when IDLE =>

        when CMD_0 =>

             p_vci_ini_0_out.cmdack <= p_vci_tgt_in.cmdack;
             p_vci_tgt_out          <= p_vci_ini_0_in;

        when CMD_1 =>

             p_vci_ini_1_out.cmdack <= p_vci_tgt_in.cmdack;
             p_vci_tgt_out          <= p_vci_ini_1_in;

        when RSP_0 =>
             
             p_vci_tgt_out.rspack   <= p_vci_ini_0_in.rspack; 
             p_vci_ini_0_out        <= p_vci_tgt_in; 

        when RSP_1 =>
             
             p_vci_tgt_out.rspack   <= p_vci_ini_1_in.rspack; 
             p_vci_ini_1_out        <= p_vci_tgt_in; 

end case;

end process;

end rtl;


