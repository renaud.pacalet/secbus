--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- reg_data.vhd

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.numeric_std.all;

entity reg_data is
port (
       clk                   : in  std_ulogic;
       srstn                 : in  std_ulogic;
       ce                    : in  std_ulogic;

       -- 
       rd_register_write     : in  data_register_write;

       wr_register_read      : out data_register_read 
);
end reg_data;

architecture rtl of reg_data is

signal s_buffer, r_buffer : data_register_read;

begin
----
pTrans : process(r_buffer, rd_register_write)
variable v_mask_data : data_t;

begin

s_buffer <= r_buffer;

    if rd_register_write.enable = '1' then
          if rd_register_write.flush  = '1' then
             s_buffer.addr <= (others => '1');
             s_buffer.data <= (others => (others => '0'));
             s_buffer.be   <= (others => (others => '0'));

          else -- update data
               for i in r_buffer.data'range loop 
                  for j in (CELL_SIZE - 1) downto 0 loop
                      if rd_register_write.be(i)(j) = '1' then
                          v_mask_data((j+1)*8 - 1 downto (j*8)) := (others => '1');                   
                       else
                          v_mask_data((j+1)*8 - 1 downto (j*8)) := (others => '0');                   
                      end if; 
                  end loop;

                  s_buffer.data(i) <= (rd_register_write.data(i) and v_mask_data) or (r_buffer.data(i) and not v_mask_data);
                  s_buffer.be(i)   <= r_buffer.be(i) or rd_register_write.be(i);
               end loop;
               
               s_buffer.addr       <= line_aligned(rd_register_write.addr);

           end if;
    end if;       

end process;
---

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_buffer.data <= (others => (others => '0'));
              r_buffer.be   <= (others => (others => '0'));
              r_buffer.addr <= (others => '0');
     
        elsif ce = '1' then
              r_buffer      <= s_buffer;
        end if;
end if;
end process;

wr_register_read <= r_buffer;

end rtl;

