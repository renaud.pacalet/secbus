--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- pspe_cache.vhd
-- pspe cache : fully-associative  
--              write_through policy
--              pseudo lru
--              ram distributed
-- <---------------------- address ----------------------------->
-- --------------------------------------------------------------
-- |     Tag                             |        block         |
-- --------------------------------------------------------------
-- <--- addr_size - log2(block_size) ---><-- log2(block_size) -->  
--
-- cmd =>  100 : read 
--         010 : write dir
--         001 : write_data 
--
--@todo : implement LRU Policy

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;
use global_lib.utils.all;

--
entity pspe_cache is
generic(
        SETS            : natural := 4;
        BLOCKS          : natural := 2
);
port (
        clk             : in  std_ulogic;
        srstn           : in  std_ulogic;
        ce              : in  std_ulogic;

        cmd             : in  std_ulogic_vector(2 downto 0); -- 100 : read; 010 : write dir; 001 : write_data (write dir & data not exclusive)
                                                      
        set_in          : in  std_ulogic_vector(SETS - 1 downto 0); -- one hot
        addr_in         : in  addr_t; 
        data_in         : in  pspe_t;

        hit             : out std_ulogic;
        data_out        : out pspe_t;
        set_out         : out std_ulogic_vector(SETS - 1 downto 0)  -- one hot
);                                                                   
end pspe_cache;

architecture rtl of pspe_cache is

constant BLOCK_BITS      : natural := log2_up(PSPE_SIZE_BYTES);
constant TAG_BITS        : natural := ADDR_SIZE - BLOCK_BITS; 

type t_set_tag    is array(SETS - 1 downto 0) of unsigned(TAG_BITS - 1 downto 0);
type t_set_block  is array(SETS - 1 downto 0) of pspe_t;

signal s_set_tag  , r_set_tag   : t_set_tag;
signal s_set_block, r_set_block : t_set_block;

signal s_set_in   , r_set_in    : unsigned(SETS      - 1 downto 0);
signal s_valid    , r_valid     : unsigned(SETS      - 1 downto 0);
signal s_lru      , r_lru       : unsigned(SETS      - 1 downto 0);
signal s_set_lru                : unsigned(SETS      - 1 downto 0);
signal s_not_valid              : unsigned(SETS      - 1 downto 0);
signal s_not_lru                : unsigned(SETS      - 1 downto 0);
signal s_set_out                : unsigned(SETS      - 1 downto 0);
signal s_hit_set                : unsigned(SETS      - 1 downto 0);
signal s_victim_set             : unsigned(SETS      - 1 downto 0);
signal s_addr_in  , r_addr_in   : unsigned(ADDR_SIZE - 1 downto 0);

signal s_all_valid              : std_ulogic;  
signal s_hit                    : std_ulogic;                             
signal s_lru_enable             : std_ulogic;                             
signal s_cmd      , r_cmd       : std_ulogic_vector(2 downto 0);          

---#----
begin  

s_addr_in <= unsigned(addr_in);
s_set_in  <= unsigned(set_in);
s_cmd     <= cmd;

genReadHit  : for s in r_valid'range generate
	s_hit_set(s) <= r_valid(s) when unsigned(r_addr_in(addr_in'left downto BLOCK_BITS)) = r_set_tag(s) else 
                           '0';
end generate genReadHit;

s_hit <= r_cmd(2) and or_reduce(s_hit_set);


genWrite : for s in s_set_in'range generate

         -- write Dir
         s_set_tag(s)   <= unsigned(addr_in(addr_in'left downto BLOCK_BITS))  when s_set_in(s) = '1' and cmd(1) = '1' else 
                           r_set_tag(s);     
 
         s_valid(s)     <= '1' when s_set_in(s) = '1' and cmd(1) = '1' else     
                           r_valid(s);

         -- write Data
         s_set_block(s) <= data_in when s_set_in(s) = '1' and cmd(0) = '1' else
                           r_set_block(s);     
end generate genWrite;

-- victim mux selector (valid vector, lru vector)
s_all_valid   <= and_reduce(r_valid);

s_not_valid   <= not r_valid;
s_not_lru     <= not r_lru;

s_lru         <= set_lru(s_set_lru, r_lru);

s_lru_enable  <= s_hit or r_cmd(1) or r_cmd(0); -- (read & hit) | write 

s_set_lru     <= s_hit_set when r_cmd(2) = '1' else -- read
                 r_set_in;                             

-- victim_mux set 
s_victim_set  <= onehot_encoder(s_not_valid) when s_all_valid = '0' else
                 onehot_encoder(s_not_lru);

-- mux set_out
s_set_out     <= s_hit_set when s_hit = '1' else
                 s_victim_set;

-- Outputs
hit           <= s_hit;
set_out       <= std_ulogic_vector(s_set_out);

-- read_mux data out
pData_out : process(s_hit_set, r_set_block)
variable v_data : pspe_t;
begin

v_data := pspe_none;

for s in s_hit_set'range loop
	if s_hit_set(s) = '1' then
		v_data := r_set_block(s);
		exit;
	end if;
end loop;

data_out <= v_data;

end process;

pClk : process(clk)
begin

if rising_edge(clk) then
   if srstn = '0' then
         r_set_in    <= (others => '0');
         r_lru       <= (others => '0');
         r_addr_in   <= (others => '0');
         r_set_tag   <= (others => (others => '0'));
         r_set_block <= (others => pspe_none);
         r_valid     <= (others => '0');
         r_cmd       <= (others => '0');

   elsif ce = '1' then
         assert and_reduce(r_lru) = '0' 
         report "At least one block must be old !!!" severity failure; 

         if s_lru_enable = '1' then
            r_lru    <= s_lru;
         end if;
         r_set_in    <= s_set_in;
         r_addr_in   <= s_addr_in;
         r_valid     <= s_valid;
         r_set_tag   <= s_set_tag;
         r_set_block <= s_set_block;
         r_cmd       <= s_cmd;
   end if;
end if;
end process;

end rtl;


