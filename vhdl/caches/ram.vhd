--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- ram.vhd
-- Write first (Transparent mode)

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;

entity ram is
generic(
	ADDR_SIZE : natural := 10;
    DATA_SIZE : natural := 32
);
port
(
	p_clk	: in  std_ulogic;
	p_cs 	: in  std_ulogic;
	p_addr	: in  std_ulogic_vector(ADDR_SIZE - 1 downto 0);
	p_wdata	: in  std_ulogic_vector(DATA_SIZE - 1 downto 0);
	p_we 	: in  std_ulogic;
	p_rdata	: out std_ulogic_vector(DATA_SIZE - 1 downto 0));
end ram;

architecture rtl of ram is
type memArrayType is array (0 to 2**p_addr'length - 1) of std_ulogic_vector(p_wdata'range);

signal mem : memArrayType ;
signal r_addr : std_ulogic_vector(p_addr'range); 

--attribute ram_style : string;
--attribute ram_style of mem : signal is "block";

begin
        
pClk: process (p_clk)
begin
   if rising_edge(p_clk) then
      if p_cs = '1' then
    	if p_we = '1' then
           mem(to_integer(unsigned(p_addr))) <= p_wdata;
        end if;

        r_addr <= p_addr;       
      end if;
   end if;
end process ;

p_rdata <= mem(to_integer(unsigned(r_addr)));

end rtl;


 
