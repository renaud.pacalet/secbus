--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mt_cache.vhd
-- ms cache : set-associative  
--            write_Back policy
--            pseudo-lru
--            ram block         
--
-- Dir  Ram block => Depth : sets, Width : (Valid + Dirty + Tag) 
-- Data Ram block => Depth : sets, Width : 64 (DATA_SIZE * BLOCKS)
-- <------------------------------------------ address------------------------- --------------------->
-- ---------------------------------------------------------------------------------------------------
-- |     Tag                                             |         set         | Block               |
-- ---------------------------------------------------------------------------------------------------
-- <--- addr_size - log2(set_size) - log2(block_size) --><-- log2(set_size) --><-- log2(block_size -->
--
-- cmd => 0000: nop  
--        1000: inval      
--        0100: read  
--        0010: write Dir 
--        0001: write Data

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;
use global_lib.utils.all;


entity mt_cache is
generic(
        WAYS            : natural := 4;
        SETS            : natural := 16; 
        BLOCKS          : natural := 2;    
        TSH             : natural := 2  -- associativity/2  
);
port (
        clk             : in  std_ulogic;
        srstn           : in  std_ulogic;
        ce              : in  std_ulogic;
        
        cmd             : in  std_ulogic_vector(3 downto 0);                                           
 
        dirty_in        : in  std_ulogic;
        way_in          : in  std_ulogic_vector(WAYS - 1 downto 0);
        addr_in         : in  addr_t;                                                 
        data_in         : in  data_vector(BLOCKS     - 1 downto 0);      
        updTsh_in       : in std_ulogic;
        incTsh_in       : in std_ulogic;
        lru_in          : in std_ulogic;
       
        way_out         : out std_ulogic_vector(WAYS - 1 downto 0); 
        addr_out        : out addr_t;                                    
        data_out        : out data_vector(BLOCKS     - 1 downto 0);      
        dirty_out       : out std_ulogic;
        hit             : out std_ulogic;
        full            : out std_ulogic
);
end mt_cache;


architecture rtl of mt_cache is
 
constant SET_BITS        : natural := log2_up(SETS);
constant BLOCK_BITS      : natural := log2_up(BLOCKS*CELL_SIZE);
constant TAG_BITS        : natural := ADDR_SIZE - (SET_BITS + BLOCK_BITS);   

type t_way_tag      is array (WAYS - 1 downto 0) of std_ulogic_vector(TAG_BITS + 1 downto 0); -- valid+dirty+tag
type t_set_tag      is array (SETS - 1 downto 0) of t_way_tag;
type t_way_data     is array (WAYS - 1 downto 0) of std_ulogic_vector(BLOCKS*DATA_SIZE - 1 downto 0); 
type t_set_dirty    is array (SETS - 1 downto 0) of std_ulogic_vector(WAYS - 1 downto 0);
type t_set_valid    is array (SETS - 1 downto 0) of std_ulogic_vector(WAYS - 1 downto 0);
type t_tsh          is array (SETS - 1 downto 0) of integer; --natural; -- range 0 to TSH;

type t_set_vector   is array (SETS - 1 downto 0) of  unsigned(WAYS - 1 downto 0);

signal s_tag_out               : t_way_tag;                    -- data output from directory
signal s_data_in               : t_way_data;                   
signal s_data_out              : t_way_data;                   
signal s_wen_dir               : unsigned(WAYS - 1 downto 0);
signal s_wen_data              : unsigned(WAYS - 1 downto 0);
signal r_lru_vector            : t_set_vector;
signal s_lru_vector            : unsigned(WAYS - 1 downto 0);  
signal s_hit_ways              : unsigned(WAYS - 1 downto 0);
signal s_miss_ways             : unsigned(WAYS - 1 downto 0);  
signal s_victim_ways           : unsigned(WAYS - 1 downto 0);  
signal s_all_valid             : std_ulogic;                    
signal s_or_old_and_clean      : std_ulogic;                    
signal s_or_old_and_dirty      : std_ulogic;                    
signal s_lru_ways              : unsigned(WAYS - 1 downto 0);  
signal s_lru_enable            : std_ulogic;                    
signal s_hit                   : std_ulogic;
signal s_not_valid_set         : unsigned(WAYS - 1 downto 0);  
signal s_not_dirty_set         : unsigned(WAYS - 1 downto 0);  
signal s_valid_set             : unsigned(WAYS - 1 downto 0);  
signal s_dirty_set             : unsigned(WAYS - 1 downto 0);  
signal s_old_and_clean_set     : unsigned(WAYS - 1 downto 0);  
signal s_old_and_dirty_set     : unsigned(WAYS - 1 downto 0);  
signal s_tag_rd, r_tag_rd      : std_ulogic_vector(TAG_BITS - 1 downto 0);
signal s_tag_wr                : std_ulogic_vector(TAG_BITS + 1 downto 0);
signal s_cmd   , r_cmd         : std_ulogic_vector(3 downto 0);
--signal r_way_in                : unsigned(WAYS - 1 downto 0);

signal s_set,   r_set          : natural range 0 to SETS - 1;

signal s_victim_addr           : addr_t;
signal s_hit_data              : data_vector(BLOCKS - 1 downto 0); 
signal s_dirty_out             : std_ulogic;
signal s_full                  : std_ulogic;

signal s_tsh,  r_tsh           : t_tsh;

function notAllRecent(v : t_set_vector) return boolean is
begin
      for i in v'range loop
          if and_reduce(v(i)) = '1' then 
             return false;
          end if;
      end loop;

      return true;
end function;
-----------
begin  

s_tag_rd <= addr_in(addr_in'left downto (SET_BITS + BLOCK_BITS)); -- valid (<=> read  dir) & tag
s_tag_wr <= (cmd(1) or not cmd(3)) & dirty_in & addr_in(addr_in'left downto (SET_BITS + BLOCK_BITS)); -- valid & dirty & tag
 
s_set    <= to_integer(unsigned(addr_in(SET_BITS + BLOCK_BITS - 1 downto BLOCK_BITS))) when cmd(2) = '1' else
            r_set;

s_cmd    <= cmd;

genramDir : for w in 0 to WAYS - 1 generate
begin

	s_hit_ways(w) <= s_tag_out(w)(TAG_BITS+1) when (r_tag_rd = s_tag_out(w)(TAG_BITS - 1 downto 0)) else '0';

	-- valid victim way
--	s_valid_set(w) <= s_tag_out(w)(TAG_BITS+1);
--  ram block is uninitialized, data including valid bit is undefined 
	s_valid_set(w) <= '1' when s_tag_out(w)(TAG_BITS+1) = '1' else '0';
	s_dirty_set(w) <= s_tag_out(w)(TAG_BITS);

	s_wen_dir(w)   <= way_in(w) and (cmd(1) or cmd(3)); -- write dir/inval      
 
    ram_tag : entity work.ram
	generic map (
		ADDR_SIZE   => SET_BITS, 
		DATA_SIZE   => TAG_BITS + 2
	) 
        port map (
		p_clk       => clk,
		p_cs        => ce,
		p_addr      => addr_in(SET_BITS + BLOCK_BITS - 1 downto BLOCK_BITS), -- set
		p_wdata     => s_tag_wr,
		p_we        => s_wen_dir(w),   
		p_rdata     => s_tag_out(w)   
         );
end generate genramDir;

genramData : for w in 0 to WAYS - 1 generate
begin

	s_wen_data(w) <= way_in(w) and cmd(0);
    s_data_in(w)  <= data_in(1) & data_in(0);
 
    ram_data : entity work.ram
    generic map (
    	ADDR_SIZE   => SET_BITS,         
    	DATA_SIZE   => DATA_SIZE*BLOCKS  
    ) 
    port map (
    	p_clk       => clk,
    	p_cs        => ce,
    	p_addr      => addr_in(SET_BITS + BLOCK_BITS - 1 downto BLOCK_BITS), -- set
        p_wdata     => s_data_in(w), 
    	p_we        => s_wen_data(w),        
    	p_rdata     => s_data_out(w)            
    );

end generate genramData;

-- mux lru 
s_lru_ways              <= s_hit_ways when r_cmd(2) = '1' else -- read
                           unsigned(way_in);                             
--                         r_way_in;                             

s_lru_vector            <= set_lru(s_lru_ways, r_lru_vector(r_set));

s_old_and_clean_set     <= not (r_lru_vector(r_set) or s_dirty_set);   -- miss_way = (old and clean)
s_or_old_and_clean      <= or_reduce(s_old_and_clean_set);    

s_old_and_dirty_set     <= not r_lru_vector(r_set) and s_dirty_set;    -- victim way = (old and dirty)
s_or_old_and_dirty      <= or_reduce(s_old_and_dirty_set);    

--s_lru_enable            <= s_hit or r_cmd(1) or r_cmd(0); -- (read & hit) | write  
s_lru_enable            <= (lru_in and s_hit) or cmd(0); -- (read & hit) | write  

s_not_valid_set         <= not s_valid_set;

s_all_valid             <= and_reduce(s_valid_set);    
s_not_dirty_set         <= not s_dirty_set; 

s_hit                   <= r_cmd(2) and or_reduce(s_hit_ways);                 

-- choose one entry in case of miss
s_miss_ways             <= onehot_encoder(s_not_valid_set)     when s_all_valid        = '0' else -- at least, one entry free
                           onehot_encoder(s_old_and_clean_set) when s_or_old_and_clean = '1' else -- at least, one entry old and clean (not dirty) 
                           onehot_encoder(s_not_dirty_set);                                       -- the first clean entry since they're all recent

s_victim_ways           <= onehot_encoder(s_old_and_dirty_set) when s_or_old_and_dirty = '1' else -- at least, one dirty entry is old
                           onehot_encoder(s_dirty_set);                                            -- All dirty entries recent, choose the first one
--
pUpdTsh : process(updTsh_in, incTsh_in, r_set, r_tsh)
begin

s_tsh <= r_tsh;

if updTsh_in = '1' then
   if incTsh_in = '1' then
     s_tsh(r_set) <= r_tsh(r_set) + 1;
   else
     s_tsh(r_set) <= r_tsh(r_set) - 1;
   end if;
end if;
end process;

pFull : process(r_set, r_tsh)
begin

s_full <= '0';

if r_tsh(r_set) = TSH then -- or >=
   s_full <= '1';
end if;

end process;

-------
pclk : process(clk)
begin

if rising_edge(clk) then
    if(srstn = '0') then
        r_lru_vector   <= (others => (others => '0'));
        r_tag_rd       <= (others => '0');
--      r_way_in       <= (others => '0');
        r_cmd          <= (others => '0');
        r_set          <= 0;
        r_tsh          <= (others => 0);

    elsif ce = '1' then

        assert notAllRecent(r_lru_vector)
        report "At least one line must be recent !!!" severity failure;

        if s_lru_enable = '1' then
            	r_lru_vector(r_set) <= s_lru_vector;
        end if;
        r_tag_rd       <= s_tag_rd;  
--      r_way_in       <= unsigned(way_in); 	
        r_cmd          <= s_cmd;
        r_set          <= s_set;
        r_tsh          <= s_tsh;
     end if;
end if;

end process;
-----
-- mux data out
pHitData_out : process(s_hit_ways, s_data_out, s_tag_out)
begin

s_hit_data  <= (others => (others => '0'));
s_dirty_out <= '0';

for w in 0 to WAYS - 1 loop
	if s_hit_ways(w) = '1' then
        for i in 0 to BLOCKS - 1 loop
		     s_hit_data(i) <= s_data_out(w)(DATA_SIZE*(i+1) - 1 downto DATA_SIZE*i);
        end loop;

        s_dirty_out <= s_tag_out(w)(TAG_BITS);                  
		exit;
	end if;
end loop;

end process;

pVictim_out : process(s_victim_ways, s_tag_out, r_set)
begin

s_victim_addr <= (others => '0');

for w in 0 to WAYS - 1 loop
	if s_victim_ways(w) = '1' then
                s_victim_addr(s_victim_addr'left downto (SET_BITS + BLOCK_BITS)) <= s_tag_out(w)(TAG_BITS - 1 downto 0);
                s_victim_addr(SET_BITS + BLOCK_BITS - 1 downto BLOCK_BITS)       <= std_ulogic_vector(to_unsigned((r_set), SET_BITS));
		exit;
	end if;
end loop;

end process;

hit       <= s_hit;
way_out   <= std_ulogic_vector(s_hit_ways)    when s_hit = '1' else
             std_ulogic_vector(s_miss_ways);           

addr_out  <= s_victim_addr;
dirty_out <= s_dirty_out;
data_out  <= s_hit_data;
full      <= s_full;

end rtl;


