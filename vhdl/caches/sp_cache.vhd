--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- sp_cache.vhd
-- sp cache : set-associative  
--            write_through policy
--            pseudo-lru
--            ram block      
--
-- Ram block => Depth : sets, Width : 32, Ram block number : (SP_SIZE_WORDS/2) x ways
-- @to do : reduce number of ram blocks 
-- <------------------------------------------ address------------------------- --------------------->
-- ---------------------------------------------------------------------------------------------------
-- |     Tag                                             |         set         | Block               |
-- ---------------------------------------------------------------------------------------------------
-- <--- addr_size - log2(set_size) - log2(block_size) --><-- log2(set_size) --><-- log2(block_size -->
--
-- cmd =>  100 : read 
--         010 : write dir
--         001 : write_data 

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;
use global_lib.utils.all;

entity sp_cache is
generic(
        WAYS            : natural := 2;
        SETS            : natural := 4;
        BLOCKS          : natural := 8   
);
port (
        clk             : in  std_ulogic;
        srstn           : in  std_ulogic;
        ce              : in  std_ulogic;
        
        cmd             : in  std_ulogic_vector(2 downto 0);                                           
 
        way_in          : in  std_ulogic_vector(WAYS - 1 downto 0);
        addr_in         : in  addr_t;                                                 
        data_in         : in  data_vector(BLOCKS - 1 downto 0);      
        
        way_out         : out std_ulogic_vector(WAYS - 1 downto 0); 
        data_out        : out data_vector(BLOCKS - 1 downto 0);      
        hit             : out std_ulogic
      
);
end sp_cache;

architecture rtl of sp_cache is

constant SET_BITS        : natural := log2_up(SETS);
constant BLOCK_BITS      : natural := log2_up(BLOCKS*CELL_SIZE);
constant TAG_BITS        : natural := ADDR_SIZE - (SET_BITS + BLOCK_BITS); -- valid + tag 

type t_way_tag      is array (WAYS - 1 downto 0) of  std_ulogic_vector(TAG_BITS downto 0);
type t_way_data     is array (WAYS - 1 downto 0) of  data_vector(BLOCKS - 1 downto 0);
type t_set_vector   is array (SETS - 1 downto 0) of  unsigned(WAYS - 1 downto 0);

function notAllRecent(v : t_set_vector) return boolean is
begin
     for i in v'range loop
         if and_reduce(v(i)) = '1' then 
            return false;
         end if;
     end loop;

     return true;
end function;

signal s_tag_out           : t_way_tag;                    -- data output from directory
signal s_data_out          : t_way_data;                   
signal r_lru_vector        : t_set_vector;
signal s_lru_vector        : unsigned(WAYS - 1 downto 0);  -- output from set_lru, in0 : s_lru_ways, in1 : r_lru_vector(set) 
signal s_wen_dir           : unsigned(WAYS - 1 downto 0);
signal s_wen_data          : unsigned(WAYS - 1 downto 0);
signal s_way_out           : unsigned(WAYS - 1 downto 0);
signal s_hit_ways          : unsigned(WAYS - 1 downto 0);
signal s_victim_ways       : unsigned(WAYS - 1 downto 0);  -- output from mux : in0 : s_valid_victim_ways, in1 : s_lru_victim_ways, sel : s_all_valid
signal s_all_valid         : std_logic;                    -- all entries are valid for address set.
signal s_lru_ways          : unsigned(WAYS - 1 downto 0);  -- output from mux : in0 :way_in, in1 : hit_ways, sel : cmd(read,update) 
signal s_lru_enable        : std_logic;                    -- set lru when READ or UPDATE
signal s_hit               : std_logic;
signal s_not_valid_set     : unsigned(WAYS - 1 downto 0);   -- input of valid prio_1hot_encoder for a given set;
signal s_valid_set         : unsigned(WAYS - 1 downto 0);   -- input of valid prio_1hot_encoder for a given set;
signal s_not_lru_set       : unsigned(WAYS - 1 downto 0);   -- input of lru   prio_1hot_encoder for a given set;
signal s_tag_rd, r_tag_rd  : std_ulogic_vector(TAG_BITS downto 0);
signal s_tag_wr            : std_ulogic_vector(TAG_BITS downto 0);
signal s_cmd   , r_cmd     : std_ulogic_vector(2 downto 0);
signal r_way_in            : unsigned(WAYS - 1 downto 0);

signal s_set,   r_set      : natural range 0 to SETS- 1;


begin  

s_tag_rd <= cmd(2) & addr_in(addr_in'left downto (SET_BITS + BLOCK_BITS)); -- valid (<=> read  dir) & tag
s_tag_wr <= cmd(1) & addr_in(addr_in'left downto (SET_BITS + BLOCK_BITS)); -- valid (<=> write dir) & tag
s_set    <= to_integer(unsigned(addr_in(SET_BITS + BLOCK_BITS - 1 downto BLOCK_BITS)));
s_cmd    <= cmd;

genramDir : for w in 0 to WAYS - 1 generate
begin

	s_hit_ways(w) <= s_tag_out(w)(TAG_BITS) when r_tag_rd = s_tag_out(w) else '0';

	-- valid victim way
	s_valid_set(w) <= s_tag_out(w)(TAG_BITS);

	s_wen_dir(w) <= way_in(w) and cmd(1); -- write dir      
 
    ram_tag : entity work.ram
	generic map (
		ADDR_SIZE   => SET_BITS, 
		DATA_SIZE   => TAG_BITS + 1
	) 
        port map (
		p_clk       => clk,
		p_cs        => ce,
		p_addr      => addr_in(SET_BITS + BLOCK_BITS - 1 downto BLOCK_BITS), -- set
		p_wdata     => s_tag_wr,
		p_we        => s_wen_dir(w),   
		p_rdata     => s_tag_out(w)   
         );
end generate genramDir;


-- mux lru
s_lru_ways      <= s_hit_ways when r_cmd(2) = '1' else -- read
                   r_way_in;                             

s_lru_vector    <= set_lru(s_lru_ways, r_lru_vector(r_set));

s_not_lru_set   <= not r_lru_vector(r_set);

--s_lru_enable    <= (r_cmd(2) and s_hit) or r_cmd(1) or r_cmd(0); -- (read & hit) | write 
s_lru_enable    <= s_hit or r_cmd(1) or r_cmd(0); -- (read & hit) | write 

s_not_valid_set <= not s_valid_set;

s_all_valid     <= and_reduce(s_valid_set);    

s_hit           <= r_cmd(2) and or_reduce(s_hit_ways);                 

-- mux victim_ways
s_victim_ways <= onehot_encoder(s_not_valid_set) when s_all_valid = '0' else
                 onehot_encoder(s_not_lru_set);

s_way_out     <= s_hit_ways when s_hit = '1' else
                 s_victim_ways; 
------
genramData : for w in 0 to WAYS - 1 generate
begin

	s_wen_data(w) <= way_in(w) and cmd(0);

        genRamWord : for i in 0 to BLOCKS - 1 generate

             ram_data : entity work.ram
             generic map (
             	ADDR_SIZE   => SET_BITS,         
             	DATA_SIZE   => DATA_SIZE 
             ) 
             port map (
             	p_clk       => clk,
             	p_cs        => ce,
             	p_addr      => addr_in(SET_BITS + BLOCK_BITS - 1 downto BLOCK_BITS), -- set
             	p_wdata     => data_in(i),          
             	p_we        => s_wen_data(w),        
             	p_rdata     => s_data_out(w)(i)            
             );
        end generate genRamWord;

end generate genramData;

-------
pclk : process(clk)
begin

if rising_edge(clk) then
    if(srstn = '0') then
        r_lru_vector <= (others => (others => '0'));
        r_tag_rd     <= (others => '0');
        r_way_in     <= (others => '0');
        r_cmd        <= (others => '0');
        r_set        <= 0;

     elsif ce = '1' then

        assert notAllRecent(r_lru_vector)
        report "At least one line must be old !!!" severity failure;

        if s_lru_enable = '1' then
            	r_lru_vector(r_set) <= s_lru_vector;
        end if;
        
        r_tag_rd     <= s_tag_rd;  
        r_way_in     <= unsigned(way_in); 	
        r_cmd        <= s_cmd;
        r_set        <= s_set;
     end if;
end if;

end process;
-----
hit     <= s_hit;

way_out <= std_ulogic_vector(s_way_out);           

-- mux data out
pData_out : process(s_hit_ways, s_data_out)
begin

data_out <= (others => (others => '0'));

for w in 0 to WAYS - 1 loop
	if s_hit_ways(w) = '1' then
        for i in 0 to BLOCKS - 1 loop
		    data_out(i) <= s_data_out(w)(i);
        end loop;
		exit;
	end if;
end loop;

end process;

end rtl;

-- ### eviction policy ###
--
--
--   ----------------------------
--   ¡                          ¡
--   ¡ ----------               ¡
--   ¡ ¡------  ¡               ¡
--   ¡ ¡¡    ¡  ¡    ...     ¡  ¡
--   ¡ ¡¡  --------------------------   --------------------------    
--   ¡ ¡¡  ¡     valid vector       ¡   ¡     lru vector         ¡    
--   ¡ ¡¡  --------------------------   --------------------------   
--   ¡ ¡¡            ¡                            ¡                   
--   ¡ ¡¡  --------------------------   --------------------------       
--   ¡ ¡¡  ¡ not valid vector       ¡   ¡  not lru vector        ¡      
--   ¡ ¡¡  --------------------------   --------------------------      
--   ¡ ¡¡            ¡                            ¡                    
--   ¡ ¡¡  --------------------------   --------------------------     
--   ¡ ¡¡  ¡ one hot valid vector   ¡   ¡ one hot lru vector     ¡        
--   ¡ ¡¡  --------------------------   --------------------------       
--   ¡ ¡¡            ¡                            ¡          
--   ¡ ¡¡            ------------     -------------     
--   ¡ ¡¡                       ¡     ¡
--   ----            ------------------------------
--    ¡   All_valid  ¡                            ¡
--    ---------------¡ 0         MUX             1¡
--                   ------------------------------     
--                                ¡
--                   ------------------------------
--                   ¡    one hot victim ways     ¡
--                   ------------------------------
-- 
