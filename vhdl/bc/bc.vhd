--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- @brief Block cipher, based on DESX and capable of enciphering and deciphering in ECB or CBC modes of operation.
--
-- The BC module computes sequences of DESX enciphering or deciphering. The DESX engine computes 1, 2, 4, 8 or 16 DES rounds per clock cycle. This is set by
-- the L2N (log2 of rounds per clock cycle) generic parameter. Each time a DESX computation completes, the BUSY signal is de-asserted, indicating that the DO
-- output carries a valid result. Then, the BUSY and DO outputs remain unchanged until a new DESX computation starts. The DSI input (data strobe in)
-- indicates a new input block on DI and, if FIRST is also asserted, a new initialization vector on IV. DSI is ignored during computations. If DSI is asserted
-- and BUSY is de-asserted on a rising edge of CLK, DI (and IV if FIRST is also asserted) are sampled, BUSY is asserted and a new computation starts. DO
-- changes only when BUSY is deasserted: during a computation the previous output result remains available on DO. An internal register DI_PREV stores the DI
-- value on every rising edge of CLK with SRSTN=1, CE=1 and DSI=1 (used for CBC decryption).
--
-- Behaviour:
-- Whith SRSTN and CE asserted (MODE=E: encryption and MODE=D: decryption):
-- FIRST   DSI  MODE  BUSY  Description
--   X      X    X     1  A computation is already running. Inputs are ignored.
--   X      0    X     0  Do nothing, maintain BUSY and DO.
--   1      1    E     0  Assert BUSY and start computing DESX(KEY, DI xor IV) in L2N clock cycles. When done, put result on DO and deassert BUSY.
--   0      1    E     0  Assert BUSY and start computing DESX(KEY, DI xor DO) in L2N clock cycles. When done, put result on DO and deassert BUSY.
--   1      1    D     0  Assert BUSY and start computing DESX-1(KEY, DI) in L2N clock cycles. When done, put result xor IV on DO and deassert BUSY.
--   0      1    D     0  Assert BUSY and start computing DESX-1(KEY, DI) in L2N clock cycles. When done, put result xor DI_PREV on DO and deassert BUSY.
--
-- Primary inputs:
-- - CLK: BC is is synchronous on rising edge of CLK.
-- - SRSTN: synchronous, active low, reset. All internal registers are re-initialized to zero when SRSTN is low on a rising edge of CLK.
-- - CE: active high chip enable. All internal registers are frozen on rising edges of CLK for which CE is low and SRSTN is high. 
-- - FIRST: when asserted whith DSI on a rising edge of CLK, indicates that DI is the first input block of a CBC chain.
-- - DSI: data strobe in: when asserted on a rising edge of CLK, indicates that DI carries a valid input block of a CBC chain. Ignored during a computation.
-- - IV: initialization vector used during the first computation of a CBC chain.
-- - DI: input block.
-- - KEY: 3-fold DESX secret key. DESX(KEY, M) = KEY.K2 XOR DES(KEY.K, M XOR KEY.K1). DESX-1(KEY, M) = KEY.K1 XOR DES-1(KEY.K, M XOR KEY.K2).
-- - MODE: encrypt or decrypt.
--
-- Primary outputs:
-- - BUSY: when asserted indicates an on-going computation.
-- - DO: data output.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

library des_lib;
use des_lib.des_pkg.all;

entity bc is

  generic(l2n: natural range 0 to 4 := 0);
  port(clk:   in  std_ulogic;
       srstn: in  std_ulogic;
       ce:  in  std_ulogic;
       first: in  std_ulogic;
       dsi:   in  std_ulogic;
       iv:  in  des_word_64;
       di:  in  des_word_64;
       key:   in  desx_key;
       mode:  in  encryption_mode;
       busy:  out std_ulogic;
       do:  out des_word_64);

end entity bc;

architecture rtl of bc is
  signal di_prev, do_local: des_word_64;
  signal key_local: desx_key; 
  signal busy_local: std_ulogic;
begin
 
  process (clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        di_prev <= (others => '0');
      elsif ce = '1' and dsi = '1' and busy_local = '0' then
        di_prev <= di;
      end if;
    end if;
  end process;

  process (first, key, do_local, iv, mode, di_prev)
  begin
    key_local.k <= key.k;
    key_local.k2 <= key.k2;
    if first = '1' then 
      key_local.k1 <= iv xor key.k1;
    elsif mode = encrypt then
      key_local.k1 <= do_local xor key.k1;
    else
      key_local.k1 <= di_prev xor key.k1;
    end if;
  end process;

  idesx : entity work.desx(rtl)
    generic map(l2n => l2n)
    port map(clk => clk,
             ce => ce,
             srstn => srstn,
             dsi => dsi,
             di => di,
             key => key_local,
             mode => mode,
             busy => busy_local,
             do => do_local);

  busy <= busy_local;
  do <= do_local;
end architecture rtl;
