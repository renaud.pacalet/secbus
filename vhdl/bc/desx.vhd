--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- @brief DESX block cipher with configurable number of DES rounds per clock cycle

library ieee;
use ieee.std_logic_1164.all;

library des_lib;
use des_lib.des_pkg.all;

entity desx is

  generic(l2n: natural range 0 to 4 := 0);
  port(clk, ce, srstn, dsi: in std_ulogic;
       di:   in des_word_64;
       key:  in desx_key;
       mode: in encryption_mode;
       busy: out std_ulogic;
       do:   out des_word_64);

end entity desx;

architecture rtl of desx is
  signal lr, kr: des_word_64;
  signal cd: des_word_56;
  signal mode_local: encryption_mode;
  signal busy_local: std_ulogic;
begin

  do <= kr xor fp(lr(33 to 64) & lr(1 to 32));

  busy <= busy_local;

  process(clk)
    variable cnt: natural range 0 to 16;
    variable lrv: des_word_64;
    variable cdv: des_word_56;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        lr <= (others => '0');
        kr <= (others => '0');
        cd <= (others => '0');
        busy_local <= '0';
        mode_local <= encrypt;
        cnt := 0;
      elsif ce = '1' then
        if busy_local = '0' then
          if dsi = '1' then
            if mode = encrypt then
              lr <= ip(di xor key.k1);
              cd <= ls(pc1_56(key.k));
              kr <= key.k2;
            else
              lr <= ip(di xor key.k2);
              cd <= pc1_56(key.k);
              kr <= key.k1;
            end if;
            cnt := 1;
            busy_local <= '1';
            mode_local <= mode;
          end if;
        elsif cnt /= 0 then
          lrv := lr;
          cdv := cd;
          for i in 1 to 2**l2n loop
            lrv := lrv(33 to 64) & (lrv(1 to 32) xor f(lrv(33 to 64), pc2(cdv)));
            if mode_local = encrypt then
              if cnt = 1 or cnt = 8 or cnt = 15 then
                cdv := ls(cdv);
              else
                cdv := ls(ls(cdv));
              end if;
            else
              if cnt = 1 or cnt = 8 or cnt = 15 then
                cdv := rs(cdv);
              else
                cdv := rs(rs(cdv));
              end if;
            end if;
            if cnt = 16 then
              busy_local <= '0';
              cnt := 0;
            else
              cnt := cnt + 1;
            end if;
            lr <= lrv;
            cd <= cdv;
          end loop;
        end if;
      end if;
    end if;
  end process;

end architecture rtl;
