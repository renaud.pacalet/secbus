--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- io_input_ctrl.vhd

-- Registers : 
--      R0 : from security Ctrl
--      R0 : to security Ctx Ctrl (update Context) 
--      R3 : to security Ctrl     (general purpose)
--
-- @Note : be field in security command structure not used, suppressed.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

entity io_input_ctrl is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- protection input parameters
       p_mb_address            : in  addr_t; -- Master Block Base Address
       p_size                  : in  addr_t; -- Size of protected region of memory
       p_b_address             : in  addr_t; -- Base address of protection region of memory

       -- Security context from security context controller
       rd_ctx_register         : in  ctx_register_read;

       -- Data from Security Ctrl
       rd_reg_0                : in  data_register_read;  
     
       -- Req from IO registers
       rd_io_handler           : in  io_block;

       -- Flag From Security Controller   
       sc_nbusy                : in  std_ulogic;

       -- Req to Mac Tree Ctrl
       mt_cmd                  : out integrity_ctrl_cmd;
       mt_ack                  : in  integrity_ctrl_ack;

       ms_ack                  : in  integrity_ctrl_ack;

       -- Data to Security Context Ctrl 
       wr_reg_0                : out data_register_write;

       -- Data to Security Ctrl 
       wr_reg_3                : out data_register_write;

       -- Rsp to Vci Io Target 
       io_handler_data_out     : out data_out ;  -- 8 words (256 bits)
       io_ack                  : out std_ulogic; -- ack to io reg
      
       busy                    : out std_ulogic; -- flag to status reg;

       -- Req to Security Ctx Ctrl
       wr_ctx_cmd              : out security_ctx_cmd_out;
       rd_ctx_cmd              : in  security_ctx_cmd_in;

       -- Req to Security Ctrl
       wr_sec_cmd              : out security_rq_out;
       rd_sec_cmd              : in  security_rq_in;

       -- Req to IRQ Handler
       wr_irq                  : out irq_out;
       rd_irq_ack              : in  std_ulogic  
);

end io_input_ctrl;

architecture rtl of io_input_ctrl is

type fsm_state is (IDLE           , 
                   MT_START_INIT  , MT_START_CONT  ,  -- START_INIT, START_CONTINUE 
                   MT_WAIT_INIT   ,                   -- WAIT_INIT,  
                   SC_IO_CMD_READ , SC_IO_RSP_READ ,  -- IO_READ, WIO_READ
                   SC_IO_CMD_WRITE, SC_IO_RSP_WRITE,  -- IO_WRITE, WIO_WRITE  
                   CTX_CMD_READ   , CTX_RSP_READ   , 
                   CTX_CMD_UPDATE , CTX_RSP_UPDATE , 
                   IO_REG_WRITE   ,                   -- READ_IO 
                -- FLUSH_REG      ,                   -- 
                   IRQ_SEND);                         -- IRQ

signal s_state        , r_state         : fsm_state;
signal s_state_save   , r_state_save    : fsm_state;
signal s_addr         , r_addr          : addr_t;
signal s_is_mb        , r_is_mb         : std_ulogic;
signal s_wdata        , r_wdata         : data_vector(SIZE_LINE_WORDS - 1 downto 0);
signal s_all_ack                        : std_ulogic;

begin

s_all_ack     <= mt_ack.uack and mt_ack.cack and mt_ack.alap_ack and mt_ack.iack and sc_nbusy and ms_ack.uack and ms_ack.cack;

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state       <= IDLE;
              r_state_save  <= IDLE;
              r_is_mb       <= '0';
              r_addr        <= (others => '0');
              r_wdata       <= (others => (others => '0'));
              
        elsif ce = '1' then
              r_state         <= s_state;
              r_state_save    <= s_state_save;
              r_is_mb         <= s_is_mb;    
              r_addr          <= s_addr;         
              r_wdata         <= s_wdata;        
        end if;
end if;
end process;

pTransition : process(r_state, r_state_save, r_addr, r_wdata, r_is_mb, rd_io_handler, rd_irq_ack,  
                      p_mb_address, p_b_address, p_size, rd_ctx_register, mt_ack, rd_sec_cmd, 
                      rd_ctx_cmd, s_all_ack, sc_nbusy)   

variable v_is_mb : std_ulogic;
variable v_mmac_addr : addr_t;
variable v_pspe_addr : addr_t;

begin

s_state         <= r_state;
s_state_save    <= r_state_save;
s_is_mb         <= r_is_mb;
s_addr          <= r_addr;
s_wdata         <= r_wdata;
       
case r_state is
       when IDLE =>
           if s_all_ack = '1' then
               s_addr  <= rd_io_handler.address;
               -- is addr  PSPE/SP ?
               v_mmac_addr :=  getMMacTBaseAddress(p_mb_address, p_size);
               v_is_mb := inclusion(rd_io_handler.address,  
		                    p_mb_address,
		                    v_mmac_addr); 
                                    
               s_is_mb <= v_is_mb;

               case rd_io_handler.cmd is
                   when INIT  =>
                        if v_is_mb = '1' then 
                           s_state      <= MT_START_INIT;     
                        else
                           s_state      <= CTX_CMD_READ;
                           s_state_save <= MT_START_INIT;
                        end if;

                   when CONTINUE =>
                        if v_is_mb = '1' then
                           s_state      <= MT_START_CONT;      
                        else 
                           s_state      <= CTX_CMD_READ;
                           s_state_save <= MT_START_CONT;
                        end if;               
 
                   when STORE =>
                        s_wdata     <= rd_io_handler.data;
                        s_state     <= IO_REG_WRITE; --FLUSH_REG;

                   when LOAD  =>
                        v_pspe_addr := getPSPEAddressFromPageSize(rd_io_handler.address, 
                                                                 pspe_getSize(rd_ctx_register.m_pspe), 
                                                                 p_mb_address,
                                                                 p_b_address);

                        if (v_is_mb = '1') or (rd_ctx_register.page_addr = v_pspe_addr) then 
                            s_state      <= SC_IO_CMD_READ;
                        else
                            s_state      <= CTX_CMD_READ;
                            s_state_save <= SC_IO_CMD_READ;                     
                        end if;

                   when OTHERS =>  -- NONE
                        s_state <= IDLE;
               end case;

           end if; -- mt all ack 

       when MT_START_INIT =>           
           if mt_ack.cack = '1' then 
               s_state <= MT_WAIT_INIT;
           end if;

       when MT_START_CONT =>                
           if mt_ack.cack = '1' then 
              s_state <= MT_WAIT_INIT;
           end if; 

       when MT_WAIT_INIT =>
           if mt_ack.iack = '1' then
              s_state <= IRQ_SEND;
           end if;

       when SC_IO_CMD_READ =>                  
           if rd_sec_cmd.cmdack = '1' then
              s_state <= SC_IO_RSP_READ;   
           end if;

       when SC_IO_RSP_READ =>                   
            if rd_sec_cmd.rspval = '1' then
               s_state <= IRQ_SEND;     
            end if;

       when IO_REG_WRITE =>             
            if sc_nbusy = '1' then      
                if r_is_mb = '1' then

                    s_state <= CTX_CMD_UPDATE;

                elsif (rd_ctx_register.page_addr = getPSPEAddressFromPageSize(r_addr, 
                                                                              pspe_getSize(rd_ctx_register.m_pspe), 
                                                                              p_mb_address,
                                                                              p_b_address)) then

                    s_state      <= SC_IO_CMD_WRITE;
                else
                    s_state      <= CTX_CMD_READ;
                    s_state_save <= SC_IO_CMD_WRITE;                     
                end if;
            end if;
       when SC_IO_CMD_WRITE =>    
            if rd_sec_cmd.cmdack = '1' then
               s_state <= SC_IO_RSP_WRITE; 
            end if;

       when SC_IO_RSP_WRITE =>  
            if rd_sec_cmd.rspval = '1' then
                  s_state <= IRQ_SEND; 
            end if;

       when CTX_CMD_READ => 
            if (rd_ctx_cmd.cmdack = '1') and (s_all_ack = '1') then -- uack and sc_nbusy in systemc
                   s_state <= CTX_RSP_READ;    
            end if;

       when CTX_RSP_READ =>
            if (rd_ctx_cmd.rspval = '1') then
                   s_state <= r_state_save;    
            end if;

       when CTX_CMD_UPDATE =>
           if (rd_ctx_cmd.cmdack = '1') then
               s_state <= CTX_RSP_UPDATE;
           end if;

       when CTX_RSP_UPDATE =>
           if (rd_ctx_cmd.rspval = '1') then
               s_state <= IRQ_SEND;
           end if;

       when IRQ_SEND =>  
            if rd_irq_ack = '1' then 
               s_state <= IDLE;
            end if; 
end case;

end process;


pGeneration : process(r_state, r_addr, r_is_mb, r_wdata, rd_reg_0, s_all_ack, mt_ack)

begin

wr_sec_cmd.cmdval       <= '0';

wr_ctx_cmd.enable       <= '0'; 
wr_ctx_cmd.cmd          <= READ;
wr_ctx_cmd.mt           <= '0';
wr_ctx_cmd.address      <= (others => '0');

wr_sec_cmd              <= security_rq_out_none;

mt_cmd.enable           <= '0';
mt_cmd.addr             <= (others => '0');
mt_cmd.cmd              <= CHECK;
mt_cmd.src              <= (others => '0');  

wr_reg_0.enable    <= '0';
wr_reg_0.addr      <= (others => '0');
wr_reg_0.flush     <= '0';
wr_reg_0.data      <= (others => (others => '0'));
wr_reg_0.be        <= (others => (others => '0'));

wr_reg_3.enable    <= '0';
wr_reg_3.addr      <= (others => '0');
wr_reg_3.flush     <= '0';
wr_reg_3.data      <= (others => (others => '0'));
wr_reg_3.be        <= (others => (others => '0'));

io_handler_data_out.enable <= '0';
io_handler_data_out.data   <= (others => (others => '0'));

wr_irq.irq              <= '0';
wr_irq.addr             <= (others => '0');
wr_irq.typ              <= NONE;
wr_irq.op               <= LOAD;

busy                    <= '1';
io_ack                  <= '0';

case r_state is
       when IDLE => 
            busy  <= not s_all_ack;

       when MT_START_INIT =>
            mt_cmd.enable  <= mt_ack.cack;
            mt_cmd.addr    <= r_addr;
            mt_cmd.cmd     <= INITIALIZE_PAGE;

       when MT_START_CONT =>
            mt_cmd.enable  <= mt_ack.cack;
            mt_cmd.addr    <= r_addr;
            mt_cmd.cmd     <= INITIALIZE;

       when MT_WAIT_INIT =>
            null;

       when SC_IO_CMD_READ =>
            wr_sec_cmd.cmdval  <= '1';
            wr_sec_cmd.cmd     <= READ;
            wr_sec_cmd.mb      <= r_is_mb;
            wr_sec_cmd.address <= (others => '0');
            wr_sec_cmd.address <= line_aligned(r_addr);

       when SC_IO_RSP_READ =>
            null;

       when IO_REG_WRITE =>
            null;            

       when SC_IO_CMD_WRITE =>
            wr_sec_cmd.cmdval     <= '1';
            wr_sec_cmd.cmd        <= WRITE;

            wr_sec_cmd.mb         <= r_is_mb;
            wr_sec_cmd.address    <= line_aligned(r_addr);

            wr_reg_3.enable       <= '1';
            wr_reg_3.flush        <= '0';
            wr_reg_3.addr         <= r_addr;  
            wr_reg_3.data         <= r_wdata; 
            wr_reg_3.be           <= (others => (others => '1'));

       when SC_IO_RSP_WRITE =>
            null;
 
       when CTX_CMD_READ =>  
            wr_ctx_cmd.enable  <= s_all_ack; 
            wr_ctx_cmd.cmd     <= READ;
            wr_ctx_cmd.address <= r_addr;

       when CTX_RSP_READ =>
            null;

       when CTX_CMD_UPDATE =>  
            wr_ctx_cmd.enable  <= '1'; 
            wr_ctx_cmd.cmd     <= UPDATE;
            wr_ctx_cmd.address <= r_addr;

            wr_reg_0.enable    <= '1';
            wr_reg_0.flush     <= '0';
            wr_reg_0.addr      <= r_addr;  
            wr_reg_0.data      <= r_wdata; 
            wr_reg_0.be        <= (others => (others => '1'));

       when CTX_RSP_UPDATE =>
            null;

       when IRQ_SEND =>
            wr_irq.irq     <= '1';
            wr_irq.addr    <= r_addr;
            wr_irq.typ     <= NONE;
            wr_irq.op      <= STORE;

            io_ack                     <= '1';  
            io_handler_data_out.data   <= rd_reg_0.data;           
            io_handler_data_out.enable <= '1';
end case;

end process;

end rtl;


