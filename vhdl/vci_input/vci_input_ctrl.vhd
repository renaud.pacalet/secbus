--
-- SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
-- Copyright (C) - Telecom Paris
-- Contacts: contact-secbus@telecom-paris.fr
--
-- This file must be used under the terms of the CeCILL.
-- This source file is licensed as described in the file COPYING, which
-- you should have received as part of this distribution. The terms
-- are also available at
-- http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- vci_input_ctrl.vhd
--
-- Registers : 
--     R0 : from security Ctrl
--     R3 : to security Ctrl

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.numeric_std.all;
use global_lib.global.all;

entity vci_input_ctrl is
port (
       clk                     : in  std_ulogic;
       srstn                   : in  std_ulogic;
       ce                      : in  std_ulogic;

       -- Security context from security context controller
       rd_ctx_register         : in  ctx_register_read;

       -- Data from Security Ctrl
       rd_reg_0                : in  data_register_read;  
     
       -- protection input parameters
       p_mb_address            : in  addr_t; -- Master Block Base Address
       p_size                  : in  addr_t; -- Size of protected region of memory
       p_b_address             : in  addr_t; -- Base address of protection region of memory
        
       -- Acknowledges from Mt Ctrl
       mt_ack                  : in  integrity_ctrl_ack;
       ms_ack                  : in  integrity_ctrl_ack;

       -- Flag from Security Controller   
       sc_nbusy                : in  std_ulogic;

       -- Data to Security Ctrl
       wr_reg_3                : out data_register_write;

       -- Req from Vci Split      
       p_vci_ini_in            : in  vci_i2t;
       p_vci_ini_out           : out vci_t2i;

       -- Req to Security Ctx Ctrl
       wr_ctx_cmd              : out security_ctx_cmd_out;
       rd_ctx_cmd              : in  security_ctx_cmd_in;

       -- Req to Security Ctrl
       wr_sec_cmd              : out security_rq_out;
       rd_sec_cmd              : in  security_rq_in

);

end vci_input_ctrl;

architecture rtl of vci_input_ctrl is

type fsm_state is (IDLE        , 
                   SC_CMD_READ , SC_RSP_READ   ,    -- CMD_READ, WREAD 
                   SC_CMD_WRITE, SC_RSP_WRITE  ,    -- START_WRITE, WWRITE 
                   INI_RSP_READ, INI_RSP_WRITE ,    -- RREAD
                   REG_WRITE   , REG_WRITE_LAST,    -- CMD_WRITE 
                   FLUSH_REG   ,                    -- FLUSH_REGISTER_0
                   CTX_CMD_READ, CTX_RSP_READ);     -- READ_CTX, WAIT_CTX 

signal s_state        , r_state         : fsm_state;
signal s_state_save   , r_state_save    : fsm_state;
signal s_cnt          , r_cnt           : unsigned(plen_size - CELL_BITS - 1 downto 0); 
signal s_cnt_reached                    : std_ulogic;              
signal s_wr_wrd_index , r_wr_wrd_index  : natural range 0 to SIZE_LINE_WORDS; 
signal s_rd_wrd_index , r_rd_wrd_index  : unsigned(WORD_BITS - 1 downto 0); -- word range is 0 to (SIZE_LINE_WORDS - 1) to handle wrap bursts
signal s_addr         , r_addr          : addr_t;
signal s_start_addr   , r_start_addr    : addr_t;
signal s_wdata        , r_wdata         : data_t;
signal s_be           , r_be            : be_t;
signal s_srcid        , r_srcid         : srcid_t;
signal s_trdid        , r_trdid         : trdid_t;
signal s_pktid        , r_pktid         : pktid_t;
signal s_pending_write, r_pending_write : std_ulogic;

signal s_all_ack                        : std_ulogic;

begin

s_all_ack     <= mt_ack.uack and mt_ack.cack and mt_ack.alap_ack and mt_ack.iack and sc_nbusy and  ms_ack.uack and ms_ack.cack;

s_cnt_reached <= '1' when r_cnt = 0 else
                 '0';

pClk : process(clk)
begin

if rising_edge(clk) then
        if srstn = '0' then
              r_state         <= IDLE;
              r_state_save    <= IDLE;
              r_cnt           <= (others => '0');
              r_wr_wrd_index  <= 0;
              r_rd_wrd_index  <= (others => '0'); 
              r_addr          <= (others => '0');
              r_start_addr    <= (others => '0');
              r_wdata         <= (others => '0');
              r_be            <= (others => '0');
              r_srcid         <= (others => '0');
              r_trdid         <= (others => '0');
              r_pktid         <= (others => '0');
              r_pending_write <= '0';

        elsif ce = '1' then
              r_state         <= s_state;
              r_state_save    <= s_state_save;
              r_cnt           <= s_cnt;          
              r_wr_wrd_index  <= s_wr_wrd_index;    
              r_rd_wrd_index  <= s_rd_wrd_index;    
              r_addr          <= s_addr;         
              r_start_addr    <= s_start_addr;         
              r_wdata         <= s_wdata;        
              r_be            <= s_be;           
              r_srcid         <= s_srcid;        
              r_trdid         <= s_trdid;        
              r_pktid         <= s_pktid;        
              r_pending_write <= s_pending_write;
        end if;
end if;
end process;

pTransition : process(r_state, r_state_save, r_addr, r_start_addr, r_srcid, r_trdid, r_pktid, r_cnt, r_wr_wrd_index, 
                      r_wdata, r_be, r_pending_write, p_vci_ini_in, rd_ctx_register, sc_nbusy, r_rd_wrd_index,
                      p_mb_address, p_b_address, rd_sec_cmd, rd_ctx_cmd, s_cnt_reached, s_all_ack) 

begin

s_state         <= r_state;
s_state_save    <= r_state_save;
s_addr          <= r_addr;
s_start_addr    <= r_start_addr;
s_srcid         <= r_srcid;
s_trdid         <= r_trdid;
s_pktid         <= r_pktid;
s_cnt           <= r_cnt;
s_wr_wrd_index  <= r_wr_wrd_index;
s_rd_wrd_index  <= r_rd_wrd_index;
s_pending_write <= r_pending_write;
s_wdata         <= r_wdata;
s_be            <= r_be;
       
case r_state is
       when IDLE =>
            if s_all_ack = '1' and p_vci_ini_in.cmdval = '1' then 
              s_addr         <= p_vci_ini_in.addr;
              s_start_addr   <= p_vci_ini_in.addr;
              s_srcid        <= p_vci_ini_in.srcid;
              s_trdid        <= p_vci_ini_in.trdid;
              s_pktid        <= p_vci_ini_in.pktid;

              -- word counter : plen expressed in bytes => divide by cellsize
              -- @todo : cells_count_s = ((p_vci_initiator.plen.read() + 
	          -- _ctz(p_vci_initiator.be.read(), vci_param::B) + 
              --  vci_param::B - 1)/vci_param::B) - 1;

              s_cnt   <= unsigned(p_vci_ini_in.plen(PLEN_SIZE - 1 downto CELL_BITS)) - 1;
                           
              if p_vci_ini_in.cmd = VCI_READ then
                 if rd_ctx_register.page_addr = getPSPEAddressFromPageSize(p_vci_ini_in.addr, 
                                                                           pspe_getSize(rd_ctx_register.m_pspe), 
                                                                           p_mb_address,
                                                                           p_b_address)  then
                    s_state      <= SC_CMD_READ; 
                 else
                    s_state      <= CTX_CMD_READ;
                    s_state_save <= SC_CMD_READ;
                 end if;

              elsif p_vci_ini_in.cmd = VCI_WRITE then
                 s_pending_write <= '0';
                 s_wdata         <= p_vci_ini_in.wdata;
                 s_be            <= p_vci_ini_in.be;

                 s_wr_wrd_index  <= word_index(p_vci_ini_in.addr);
                 s_state         <= FLUSH_REG;

                 if p_vci_ini_in.eop = '1' then -- single flit burst
                    s_state_save <= REG_WRITE_LAST;
                 else
                    s_state_save <= REG_WRITE;
                 end if;

              --else -@todo : handle bad vci command 
              end if;
            
           end if; -- cmdval
 
       when SC_CMD_READ =>      
            if rd_sec_cmd.cmdack = '1' then
               s_state <= SC_RSP_READ;   
            end if;

       when SC_RSP_READ =>     
            if rd_sec_cmd.rspval = '1' then
               s_rd_wrd_index <= to_unsigned(word_index(r_addr), WORD_BITS);  
               s_state        <= INI_RSP_READ;     
            end if;

       when INI_RSP_READ =>   
            if p_vci_ini_in.rspack = '1' then
               if (s_cnt_reached = '1')  then
                  s_state <= IDLE;
               else
                  s_cnt       <= r_cnt - 1;
                  -- @todo : handle burst_type = WRAP
                  --         r_wr_wrd_index range : 0 to SIZE_LINE_WORDS - 1           
                  s_rd_wrd_index <= r_rd_wrd_index + 1;
                  s_state        <= INI_RSP_READ;
               end if;
            end if;     

       when REG_WRITE =>     
            if p_vci_ini_in.cmdval = '1' then
                s_wdata        <= p_vci_ini_in.wdata;
                s_be           <= p_vci_ini_in.be;
                s_addr         <= p_vci_ini_in.addr;
                s_wr_wrd_index <= word_index(p_vci_ini_in.addr);

                if line_aligned(r_start_addr) /= line_aligned(p_vci_ini_in.addr) then --  
                   s_pending_write <= '1';
                   s_start_addr    <= p_vci_ini_in.addr;
                   s_state         <= SC_CMD_WRITE;
                   if p_vci_ini_in.eop = '1' then
                      s_state_save <= REG_WRITE_LAST;
                   else
                      s_state_save <= REG_WRITE;
                   end if; 
                else
                   if p_vci_ini_in.eop = '1' then 
                          s_state <= REG_WRITE_LAST;
                   else 
                          s_state <= REG_WRITE;
                   end if;
                end if;

            end if;

       when  REG_WRITE_LAST =>
            -- write last data and enable write register 
            s_state <= SC_CMD_WRITE; 

       when SC_CMD_WRITE =>    
            if rd_sec_cmd.cmdack = '1' then
               s_state <= SC_RSP_WRITE; 
            end if;

       when SC_RSP_WRITE =>  
            if rd_sec_cmd.rspval = '1' then
               if r_pending_write = '1'then
                  s_state         <= FLUSH_REG;
                  s_pending_write <= '0';
               else
                  s_state <= INI_RSP_WRITE;
               end if;
            end if;

       when INI_RSP_WRITE => 
            if p_vci_ini_in.rspack = '1' then
                s_state <= IDLE;
            end if;

       when CTX_CMD_READ =>  
            if (rd_ctx_cmd.cmdack = '1') and (s_all_ack = '1') then 
               s_state  <= CTX_RSP_READ;    
            end if;

       when CTX_RSP_READ =>  
            if (rd_ctx_cmd.rspval = '1') then
               s_state <= r_state_save;    -- SC_CMD_READ or REG_WRITE
            end if;

       when FLUSH_REG =>
           if sc_nbusy = '1' then
                 if rd_ctx_register.page_addr = getPSPEAddressFromPageSize(r_addr, 
                                                                           pspe_getSize(rd_ctx_register.m_pspe), 
                                                                           p_mb_address,
                                                                           p_b_address)  then
                      s_state <= r_state_save;
                 else
                      s_state <= CTX_CMD_READ;
                 end if;
            end if;
                      
end case;

end process;

pGeneration : process(r_state, r_addr, r_wdata, r_be, s_cnt_reached, r_wr_wrd_index, r_rd_wrd_index, rd_reg_0,
                      r_trdid, r_pktid, r_srcid, r_start_addr, sc_nbusy, s_all_ack) 

begin

p_vci_ini_out     <= vci_t2i_none;

wr_ctx_cmd.enable  <= '0'; 
wr_ctx_cmd.cmd     <= READ;
wr_ctx_cmd.mt      <= '0';
wr_ctx_cmd.address <= (others => '0');

wr_sec_cmd         <= security_rq_out_none;

wr_reg_3.enable    <= '0';
wr_reg_3.flush     <= '0';

wr_reg_3.addr      <= (others => '0');
wr_reg_3.data      <= (others => (others => '0'));
wr_reg_3.be        <= (others => (others => '0'));

case r_state is
       when IDLE =>  
            p_vci_ini_out.cmdack       <= s_all_ack; 
 
       when SC_CMD_READ =>
            wr_sec_cmd.cmdval          <= '1';
            wr_sec_cmd.cmd             <= READ;
            wr_sec_cmd.mb              <= '0';
            wr_sec_cmd.address         <= line_aligned(r_addr);
 
       when SC_RSP_READ =>
            null;
         
       when INI_RSP_READ =>
            p_vci_ini_out.rspval       <= '1';
            p_vci_ini_out.rerror       <= (others => '0');
            p_vci_ini_out.reop         <= s_cnt_reached;
            p_vci_ini_out.rdata        <= rd_reg_0.data(to_integer(r_rd_wrd_index));
            p_vci_ini_out.rsrcid       <= r_srcid;
            p_vci_ini_out.rtrdid       <= r_trdid;
            p_vci_ini_out.rpktid       <= r_pktid;

       when FLUSH_REG =>  
            wr_reg_3.enable            <= sc_nbusy;     
            wr_reg_3.flush             <= '1';

       when REG_WRITE =>
            p_vci_ini_out.cmdack          <= '1';

            wr_reg_3.enable               <= '1';         
            wr_reg_3.addr                 <= r_start_addr; 
            wr_reg_3.data(r_wr_wrd_index) <= r_wdata;
            wr_reg_3.be(r_wr_wrd_index)   <= r_be;
           
       when REG_WRITE_LAST =>
            -- vci eop 
            wr_reg_3.enable               <= '1';
            wr_reg_3.addr                 <= r_start_addr;  -- r_addr in systemC
            wr_reg_3.data(r_wr_wrd_index) <= r_wdata;
            wr_reg_3.be(r_wr_wrd_index)   <= r_be;

       when INI_RSP_WRITE =>
            p_vci_ini_out.rspval       <= '1';
            p_vci_ini_out.rdata        <= (others => '0');
            p_vci_ini_out.rerror       <= (others => '0');
            p_vci_ini_out.reop         <= '1';
            p_vci_ini_out.rsrcid       <= r_srcid;
            p_vci_ini_out.rtrdid       <= r_trdid;
            p_vci_ini_out.rpktid       <= r_pktid;

       when SC_CMD_WRITE =>
            wr_sec_cmd.cmdval          <= '1';
            wr_sec_cmd.cmd             <= WRITE;
            wr_sec_cmd.mb              <= '0';
            wr_sec_cmd.address         <= r_start_addr;  

       when SC_RSP_WRITE =>
            null;
 
       when CTX_CMD_READ =>  
            wr_ctx_cmd.enable          <= s_all_ack; 
            wr_ctx_cmd.cmd             <= READ;
            wr_ctx_cmd.address         <= r_addr;

       when CTX_RSP_READ  =>
            null;

end case;

end process;

end rtl;


