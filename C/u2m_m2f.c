/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

/*
 * Read <nbytes> bytes at address <addr> in target memory and store them in file
 * <file>. Use u2m_read (32-bits words). Supports addresses that are not aligned
 * on a 4-bytes boundary and number of bytes that are not multiples of 4:
 * discard first and/or last bytes if needed.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ftdi.h>
#include "u2m_lib.h"

int main(int argc, char **argv) {
  uint32_t addr;
  uint8_t rdata[4];
  int i, nbytes, discard_first, discard_last;
  int s;
  FILE *fd;
  struct ftdi_context *ftdi;

  if(argc != 4) {
    fprintf(stderr, "usage: %s <addr> <nbytes> <file>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  addr = strtoul(argv[1], NULL, 0);
  nbytes = strtoul(argv[2], NULL, 0);
  if(nbytes < 1) {
    fprintf(stderr, "%s: invalid number of bytes (%d)\n", argv[0], nbytes);
    exit(EXIT_FAILURE);
  }
  fd = fopen(argv[3], "w");
  if(fd == NULL) {
    fprintf(stderr, "%s: file %s not found or read-only\n", argv[0], argv[3]);
    return EXIT_FAILURE;
  }
  discard_first = addr % 4;
  addr -= discard_first;
  nbytes += discard_first;
  discard_last = (nbytes % 4) ? 4 - nbytes % 4 : 0;
  nbytes += discard_last;
#ifdef DEBUG
  fprintf(stderr, "addr=%08x, nbytes=%d, discard_first=%d, discard_last=%d\n", addr, nbytes, discard_first, discard_last);
#endif
  ftdi = u2m_open();
  for(i = 0; i < nbytes / 4; i++) {
    s = u2m_read(ftdi, addr, (uint32_t *)rdata);
    if(s == EXIT_SUCCESS) {
      if(i == 0) {
        fwrite(rdata + discard_first, 1, 4 - discard_first, fd);
      }
      else if(i == nbytes / 4 - 1) {
        fwrite(rdata, 1, 4 - discard_last, fd);
      }
      else {
        fwrite(rdata, 1, 4, fd);
      }
    }
    else {
      break;
    }
    addr += 4;
  }
  fclose(fd);
  u2m_close(ftdi);
  return s;
}

