/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

/*
 * Write 32-bits word <wdata> at address <addr> in target memory with byte
 * enable <be>. Two least significant bits of <addr> are ignored.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ftdi.h>
#include "u2m_lib.h"

int main(int argc, char **argv) {
  uint32_t addr, wdata, be;
  struct ftdi_context *ftdi;

  if(argc != 4) {
    fprintf(stderr, "usage: %s <addr> <data> <be>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  addr = strtoul(argv[1], NULL, 0);
  wdata = strtoul(argv[2], NULL, 0);
  be = strtoul(argv[3], NULL, 0);
  if(be > 0xf) {
    fprintf(stderr, "%s: invalid byte enable: 0x%08" PRIx32 "\n", argv[0], be);
    exit(EXIT_FAILURE);
  }
  ftdi = u2m_open();
  u2m_write(ftdi, addr, wdata, be);
  u2m_close(ftdi);
  return EXIT_SUCCESS;
}

