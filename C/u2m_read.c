/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

/*
 * Read one 32-bits word at address <addr> in target memory and print read
 * value. Two least significant bits of <addr> are ignored.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ftdi.h>
#include "u2m_lib.h"

int main(int argc, char **argv) {
  uint32_t addr, rdata;
  struct ftdi_context *ftdi;

  if(argc != 2) {
    fprintf(stderr, "usage: %s <addr>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  addr = strtoul(argv[1], NULL, 0);
  ftdi = u2m_open();
  if(u2m_read(ftdi, addr, &rdata) == EXIT_SUCCESS) {
    fprintf(stdout, "0x%08" PRIx32 ": 0x%08" PRIx32 "\n", addr, rdata);
  }
  else {
    fprintf(stdout, "0x%08" PRIx32 ": ERROR\n", addr);
  }
  u2m_close(ftdi);
  return EXIT_SUCCESS;
}

