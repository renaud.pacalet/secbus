/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

/*
 * Load file <file> in memory at address <addr>. Write 4-bytes at a time using
 * u2m_write. Supports addresses that are not aligned on a 4-bytes boundary and
 * file sizes that are not multiples of 4 bytes: use byte enable (be) parameter
 * of u2m_write to write right number of byte during first and last calls to
 * u2m_write.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ftdi.h>
#include "u2m_lib.h"

int main(int argc, char **argv) {
  uint32_t addr, wdata, be;
  int s, skip_first;
  FILE *fd;
  struct ftdi_context *ftdi;

  if(argc != 3) {
    fprintf(stderr, "usage: %s <addr> <file>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  addr = strtoul(argv[1], NULL, 0);
  fd = fopen(argv[2], "r");
  if(fd == NULL) {
    fprintf(stderr, "%s: file %s not found\n", argv[0], argv[2]);
    return EXIT_FAILURE;
  }
  ftdi = u2m_open();
  be = UINT32_C(0xf);
  skip_first = addr % 4;
  addr -= skip_first;
  while((s = fread(&wdata, 1, 4 - skip_first, fd))) {
    be = (1 << s) - 1;
    be <<= skip_first;
    wdata <<= skip_first * 8;
#ifdef DEBUG
    if(skip_first)
      fprintf(stderr, "addr=%08x, skip_first=%d, be=%1x, wdata=%08x, (s=%d)\n", addr, skip_first, be, wdata, s);
#endif
    skip_first = 0;
    s = u2m_write(ftdi, addr, wdata, be);
    if(s != EXIT_SUCCESS)
      break;
    addr += 4;
  }
  fclose(fd);
  u2m_close(ftdi);
  return s;
}

