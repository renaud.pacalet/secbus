/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

/*
 * Store the content of the data file at the beginning of four 4kB memory pages
 * starting at base_address (see below). Then, enter an infinite loop where a
 * textual menu is presented to the user asking to pick one of the four pages
 * (or to exit). Read the chosen page and print it. Wait for a keystroke and
 * loop.
 *

 *
 * Usage: player INPUTFILE
 *
 *         INPUTFILE  Name of data file
 *
 * Example:
 *
 * $ player data.txt
 * Loading data in memory, please wait...
 *
 * Play from:
 * 1. Unprotected page
 * 2. Encrypted page
 * 3. Integrity-checked page
 * 4. Encrypted and integrity-checked page
 *
 * 0. Quit
 *  >> 1
 * ...
 *
 * Press any key to continue...
 */

#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>

#define PAGE_SIZE 4096UL
#define ADDR_MASK (PAGE_SIZE - 1)

#define PAGE1_ADDR 0x4fffc000
#define PAGE2_ADDR 0x4fffd000
#define PAGE3_ADDR 0x4fffe000
#define PAGE4_ADDR 0x4ffff000

off_t phys_addr[4] = {PAGE1_ADDR, PAGE2_ADDR, PAGE3_ADDR, PAGE4_ADDR};
void *virt_base[4], *virt_addr[4];
int size;
char *page_labels[4] = {"Unprotected", "Encrypted", "Integrity-checked", "Encrypted and integrity-checked"};
char translate[256];

int text_read(char *fname, char *buffer);
void play(int p);

int main(int argc, char **argv) {
  int p, r, stop;
#ifdef DEVMEM
  int fd;
#endif
  struct status {
    int code;
    int mem_opened;
    int mapped;
    int written;
  };
  struct status st = {-1, 0, 0, 0};
  static struct termios oldt, newt;

  while(1) {
    if(argc != 2) {
      fprintf(stderr, "usage: %s <file>\n", argv[0]);
      break;
    }
#ifdef DEVMEM
    fd = open("/dev/mem", O_RDWR | O_SYNC);
    if(fd == -1) {
      fprintf(stderr, "cannot open /dev/mem\n");
      break;
    }
    st.mem_opened = 1;
#endif
    for(p = 0; p < 4; p++) {
#ifdef DEVMEM
      virt_base[p] = mmap(0, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, phys_addr[p] & ~ADDR_MASK);
      if(virt_base[p] == (void *) -1) {
        fprintf(stderr, "cannot map memory\n");
        break;
      }
      virt_addr[p] = (void *)((int) virt_base[p] + (phys_addr[p] & ADDR_MASK));
#else
      virt_base[p] = malloc(PAGE_SIZE);
      if(virt_base[p] == NULL) {
        fprintf(stderr, "cannot allocate memory\n");
        break;
      }
      virt_addr[p] = virt_base[p];
#endif
      st.mapped += 1;
    }
    if(p != 4) {
      break;
    }
    size = text_read(argv[1], virt_addr[0]);
    st.written = 1;
    if(size < 0) {
      fprintf(stderr, "decryption failed\n");
      break;
    }
    for(p = 1; p < 4; p++) {
      memcpy(virt_addr[p], virt_addr[0], size);
      st.written += 1;
    }
    stop = 0;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON);          
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    for(int i = 0; i < 256; i++) {
      if(i >= 0x80) {
        translate[i] = '?';
      } else {
        translate[i] = (char)(i);
      }
    }
    while(!stop) {
      fprintf(stdout, "\033[2J\033[1G");
      fprintf(stdout, "\nPlay from:\n");
      for(p = 0; p < 4; p++) {
        fprintf(stdout, "%d. %s page\n", p + 1, page_labels[p]);
      }
      fprintf(stdout, "\n0. Quit\n");
      fprintf(stdout, " >>  ");
      r = fgetc(stdin);
      p = -1;
      switch(r) {
        case '0': stop = 1;
                  break;
        case '1': p = 0;
                  break;
        case '2': p = 1;
                  break;
        case '3': p = 2;
                  break;
        case '4': p = 3;
                  break;
        default: break;
      }
	  if(p >= 0 && p <= 3) {
		  play(p);
	  }
    }
    st.code = 0;
    break;
  }
  fprintf(stdout, "\033[2J\033[1G");
  fprintf(stdout, "Bye...\n");
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
#ifdef DEVMEM
  if(st.mem_opened) {
    close(fd);
  }
#endif
  for(p = 0; p < st.written; p++) {
    memset(virt_base[p], 0, size);
  }
  for(p = 0; p < st.mapped; p++) {
#ifdef DEVMEM
    if(munmap(virt_base[p], PAGE_SIZE) == -1) {
      fprintf(stderr, "cannot unmap memory\n");
      st.code = -1;
    }
#else
    free(virt_base[p]);
#endif
  }
  return st.code;
}

void play(int p) {
  unsigned char *v = (unsigned char *)(virt_addr[p]);

  fprintf(stdout, "\033[2J\033[1G");
  fprintf(stdout, "Playing from %s page...\n\n", page_labels[p]);
  for(int i = 0; i < size; i++, v++) {
    putchar(translate[*v]);
  }
  fprintf(stdout, "\nPress any key to continue...");
  fgetc(stdin);
}

int text_read(char *fname, char *buffer) {
  size_t len = 0;
  FILE *fi;
  struct stat stat_fi;
  struct status {
    int code;
    int file_opened;
    int buffer_written;
  };
  struct status st = {-1, 0, 0};

  while(1) {
    fi = fopen(fname, "r");
    if(fi == NULL) {
      fprintf(stderr, "%s: file not found\n", fname);
      break;
    }
    st.file_opened = 1;
    if(stat(fname, &stat_fi)) {
      fprintf(stderr, "%s: cannot stat file\n", fname);
      break;
    }
    len = (size_t) stat_fi.st_size;
    if(len == 0) {
      fprintf(stderr, "%s: file empty\n", fname);
      break;
    }
    if(len > PAGE_SIZE) {
      fprintf(stderr, "maximum plaintext size (%lu) exceeded (%zu)\n", PAGE_SIZE, len);
      break;
    }
    if(fread(buffer, 1, len, fi) != len) {
      fprintf(stderr, "could not read text from file %s\n", fname);
      break;
    }
    fclose(fi);
    st.file_opened = 0;
    st.buffer_written = 1;
    st.code = len;
    break;
  }
  if(st.buffer_written && st.code < 0) {
    memset(buffer, 0, len);
  }
  if(st.file_opened) {
    fclose(fi);
  }
  return st.code;
}

// vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
