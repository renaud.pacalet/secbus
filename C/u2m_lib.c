/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

/*
 * Basic I/O routines for UART2MAXILITE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <ftdi.h>

/* FTDI / FT232R */
#define VENDOR        0x403
#define PRODUCT       0x6001
/* Line parameters */
#define BAUDRATE      115200
#define BITS_PER_CHAR BITS_8
#define PARITY        NONE
#define STOP_BITS     STOP_BIT_1
/* UART2MAXILITE command bytes */
#define MEM_READ      (0<<4)
#define MEM_WRITE     (1<<4)
#define REG_READ      (2<<4)
#define REG_WRITE     (3<<4)
/* UART2MAXILITE exit status bytes */
#define OKAY          0
#define SLVERR        2
#define DECERR        3
/* Number of retries when reading characters from UART */
#define READ_RETRY    1000

/* Sleep duration between to attempts to read characters from UART */
const struct timespec ms10_s = {0, 10000};
const struct timespec *ms10 = &ms10_s;

/* Initialize and open FTDI device. Exit with EXIT_FAILURE status if fail to
 * find, initialize and open device. */
struct ftdi_context *u2m_open(void) {
  struct ftdi_context *ftdi;
  struct ftdi_device_list *devlist;
  int ndev, status;

  if ((ftdi = ftdi_new()) == 0)
  {
    fprintf(stderr, "u2m_open error: ftdi_new failed\n");
    exit(EXIT_FAILURE);
  }
  ftdi_set_interface(ftdi, INTERFACE_ANY);
  ndev = ftdi_usb_find_all(ftdi, &devlist, VENDOR, PRODUCT);
  if (ndev < 0) {
    fprintf(stderr, "u2m_open error: %d (idVendor=%x, idProduct=%x)\n", ndev, VENDOR, PRODUCT);
    exit(EXIT_FAILURE);
  }
  if (ndev == 0) {
    fprintf(stderr, "u2m_open error - no FTDI devices found (idVendor=%x, idProduct=%x)\n", VENDOR, PRODUCT);
    exit(EXIT_FAILURE);
  }
  if (ndev > 1) {
    fprintf(stderr, "u2m_open error - more than one FTDI devices found: %d (idVendor=%x, idProduct=%x)\n", ndev, VENDOR, PRODUCT);
    exit(EXIT_FAILURE);
  }
  status = ftdi_usb_open_dev(ftdi, devlist[0].dev);
  if(status != 0) {
    fprintf(stderr, "u2m_open error - could not open FTDI device: %d (%s)\n", status, ftdi_get_error_string(ftdi));
    exit(EXIT_FAILURE);
  }
  status = ftdi_set_baudrate(ftdi, BAUDRATE);
  if (status < 0)
  {
    fprintf(stderr, "u2m_open error - could not set baudrate: %d (%s)\n", status, ftdi_get_error_string(ftdi));
    exit(EXIT_FAILURE);
  }
  status = ftdi_set_line_property(ftdi, BITS_PER_CHAR, STOP_BITS, PARITY);
  if (status < 0)
  {
    fprintf(stderr, "u2m_open error - could not set line parameters: %d (%s)\n", status, ftdi_get_error_string(ftdi));
    exit(EXIT_FAILURE);
  }
  return ftdi;
}

/* Close FTDI device. */
void u2m_close(struct ftdi_context *ftdi) {
  ftdi_usb_close(ftdi);
  ftdi_free(ftdi);
}

/* Read one char from FTDI device. Wait 10 micro-seconds minimum between
 * retries. Retry at most READ_RETRY times. Read char stored in rdata on
 * success, undefined on failure. Return EXIT_SUCCESS on success, EXIT_FAILURE
 * on failure. */
int u2m_getc(struct ftdi_context *ftdi, uint8_t *rdata) {
  int i, status;

  for(i = 0; i < READ_RETRY; i++) {
    status = ftdi_read_data(ftdi, rdata, 1);
    if(status != 0)
      break;
    nanosleep(ms10, NULL);
  }
  if (status != 1)
  {
    fprintf(stderr, "u2m_getc error - could not read char from FTDI device: %d (%s)\n", status, ftdi_get_error_string(ftdi));
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/* Read one 32-bits word at address <addr> in target memory and store read word
 * in <rdata>. Send sequence of commands to FTDI device:
 * - reg_write register a0 (a1, a2, a3) with addr[7..0] (addr[15..8]...)
 * - mem_read
 * - reg_read register d0 (d1, d2, d3)
 * and read 5 characters using u2m_getc (status of mem_read plus the 4 read
 * bytes).
 */
int u2m_read(struct ftdi_context *ftdi, uint32_t addr, uint32_t *rdata) {
  uint8_t  buf[13];
  int i, status;

  for(i = 0; i < 4; i++) {
    buf[2 * i] = REG_WRITE | i;
    buf[2 * i + 1] = (addr >> 8 * i) & 0xff;
  }
  buf[8] = MEM_READ;
  for(i = 0; i < 4; i++) {
    buf[9 + i] = REG_READ | (i + 4);
  }
  status = ftdi_write_data(ftdi, buf, 13);
  if (status != 13)
  {
    fprintf(stderr, "u2m_read error - could not send command to FTDI device: %d (%s)\n", status, ftdi_get_error_string(ftdi));
    return status;
  }
  status = u2m_getc(ftdi, buf);
  if(status != EXIT_SUCCESS)
    return status;
  if(buf[0] != OKAY) {
    fprintf(stderr, "u2m_read error - AXI response not OKAY: %d\n", buf[0]);
    return buf[0];
  }
  *rdata = 0;
  for(i = 1; i < 5; i++) {
    status = u2m_getc(ftdi, buf + i);
    if(status != EXIT_SUCCESS)
      return status;
  }
  for(i = 4; i > 0; i--) {
    *rdata = (*rdata << 8) | buf[i];
  }
  return EXIT_SUCCESS;
}

/* Write 32-bits word <wdata> at address <addr> in target memory with byte
 * enable <be>. Send sequence of commands to FTDI device:
 * - reg_write register a0 (a1, a2, a3) with addr[7..0] (addr[15..8]...)
 * - reg_write register d0 (d1, d2, d3) with wdata[7..0] (wdata[15..8]...)
 * - mem_write
 * and read mem_write status character using u2m_getc.
 */
int u2m_write(struct ftdi_context *ftdi, uint32_t addr, uint32_t wdata, uint32_t be) {
  uint8_t  buf[17];
  int i, status;

  for(i = 0; i < 4; i++) {
    buf[2 * i] = REG_WRITE | i;
    buf[2 * i + 1] = (addr >> 8 * i) & 0xff;
  }
  for(i = 0; i < 4; i++) {
    buf[8 + 2 * i] = REG_WRITE | (i + 4);
    buf[8 + 2 * i + 1] = (wdata >> 8 * i) & 0xff;
  }
  buf[16] = MEM_WRITE | (be & 0xf);
  status = ftdi_write_data(ftdi, buf, 17);
  if (status != 17)
  {
    fprintf(stderr, "u2m_write error - could not send command to FTDI device: %d (%s)\n", status, ftdi_get_error_string(ftdi));
    return status;
  }
  status = u2m_getc(ftdi, buf);
  if(status != EXIT_SUCCESS)
    return status;
  if(buf[0] != OKAY) {
    fprintf(stderr, "u2m_write error - AXI response not OKAY: %d\n", buf[0]);
    return buf[0];
  }
  return EXIT_SUCCESS;
}

