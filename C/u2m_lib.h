/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <inttypes.h>
#include <ftdi.h>

struct ftdi_context *u2m_open(void);
void u2m_close(struct ftdi_context *ftdi);
int u2m_getc(struct ftdi_context *ftdi, uint8_t *rdata);
int u2m_read(struct ftdi_context *ftdi, uint32_t addr, uint32_t *rdata);
int u2m_write(struct ftdi_context *ftdi, uint32_t addr, uint32_t wdata, uint8_t be);
