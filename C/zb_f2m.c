/*
 * SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
 * Copyright (C) - Telecom Paris
 * Contacts: contact-secbus@telecom-paris.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#define PAGE_SIZE 4096UL
#define ADDR_MASK (PAGE_SIZE - 1)

int main(int argc, char **argv) {
  FILE *fp;
  int fd, n;
  off_t phys_addr;
  void *virt_base, *virt_addr;

  if(argc != 3) {
    fprintf(stderr, "usage: %s <file> <addr>\n", argv[0]);
    exit(1);
  }
  fp = fopen(argv[1], "r");
  if(fp == NULL) {
    fprintf(stderr, "%s: file not found\n", argv[1]);
    exit(1);
  }
  phys_addr = strtoull(argv[2], NULL, 0);
  fd = open("/dev/mem", O_RDWR | O_SYNC);
  if(fd == -1) {
    fprintf(stderr, "cannot open /dev/mem\n");
    exit(1);
  }
  virt_base = mmap(0, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, phys_addr & ~ADDR_MASK);
  if(virt_base == (void *) -1) {
    fprintf(stderr, "cannot map memory\n");
    exit(1);
  }
  virt_addr = (void *)((int) virt_base + ((int) phys_addr & ADDR_MASK));
  n = fread(virt_addr, 1, 4096, fp);
  fprintf(stdout, "wrote %d bytes\n", n);
  fclose(fp);
  if(munmap(virt_base, PAGE_SIZE) == -1) {
    fprintf(stderr, "cannot unmap memory\n");
    close(fd);
    exit(1);
  }
  return 0;
}
