<!--
SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
Copyright (C) - Telecom Paris
Contacts: contact-secbus@telecom-paris.fr

This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution. The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
-->

# SecBus, a hardware / software architecture protecting the external memories of an SoC

SecBus is a hardware / software architecture that protects a System-on-a-Chip (SoC) against tampering with its external memories. Thanks to a Hardware Security Module (HSM) SecBus allows enciphering and integrity checking of sensitive memory regions.

Please visit the [Wiki] for more information.

[Wiki]: https://gitlab.telecom-paristech.fr/renaud.pacalet/secbus/wikis/home
