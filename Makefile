#
# SecBus ( https://secbus.telecom-paris.fr/ ) - This file is part of SecBus
# Copyright (C) - Telecom Paris
# Contacts: contact-secbus@telecom-paris.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

# Default goal
.DEFAULT_GOAL			:= help

# Compute the project's root directory from the path of this Makefile.
ROOTDIR					:= $(realpath $(strip $(dir $(lastword $(MAKEFILE_LIST)))))
SCRIPTSDIR				:= $(ROOTDIR)/scripts
BUILDDIRS				:=

# Configuration variables
include .config
BUILDDIRS				+= $(O)

# Verbosity
$(V).SILENT:
ifeq ($(V),)
OUTPUT := > /dev/null 2>&1
endif

# Backup directory $(1) as $(dir $(1)).YYYY-mm-dd.HH:MM:SS.NN, based on its
# last modification time
define BACKUP
if [ -r $(1) ]; then \
	ts="`stat -c%y $(1)`"; \
	ts="`date -d"$$ts" +%Y-%m-%d.%H:%M:%S.%N`"; \
	mv "$(1)" "$(1).$$ts"; \
	printf '[BACKUP]      %s -> %s.%s\n' "$(1)" "$(1)" "$$ts"; \
fi
endef

# Cross-compiler
export ARCH				:= arm
export CROSS_COMPILE	:= arm-linux-gnueabihf-
GCC_VERSION	 := $(shell $(CROSS_COMPILE)gcc -dumpversion)
ifneq ($(GCC_VERSION),$(SUPPORTED_GCC_VERSION))
$(warning WARNING: cross-compiler version ($(GCC_VERSION)) is not the expected one ($(SUPPORTED_GCC_VERSION)))
endif

# Help
.PHONY: help

define HELP_message
usage: make [GOAL] [VARIABLE=VALUE...]

GOAL:
  help                                                        Help
  syn                                                         Synthesis
  fsbl-orig fsbl-new fsbl-build fsbl                          FSBL
  u-boot-orig u-boot-new u-boot-config u-boot-build u-boot    Das U-Boot
  boot                                                        Zynq boot image
  linux-config linux-build linux                              Linux kernel
  dt-orig dt-new dt-build dt                                  Device tree
  rootfs-config rootfs-build rootfs                           Root file system
  demo                                                        Demonstration files
  all                                                         Complete build
  clean                                                       Clean

See the commented .config file for detailed explanations about the tunable make
variables and their role. The behaviour of this Makefile can be tuned either by
editing the XXX variable definition in .config or by setting the XXX variable
on the command line (make GOAL XXX=VALUE). The later takes precedence over the
former.
endef
export HELP_message

help:
	@printf '%s\n' "$$HELP_message"

# Synthesis
.PHONY: syn

TOP				:= demo
VIVADO			:= vivado
VIVADOSCRIPT	:= $(SCRIPTSDIR)/$(TOP).syn.tcl
VIVADOFLAGS		:= -mode batch -notrace -source $(VIVADOSCRIPT) -tclargs $(FREQUENCY) $(if $(CHIPSCOPE),1,0)
VHDLSRCS		:= $(addprefix $(ROOTDIR)/vhdl/,global/numeric_std.vhd des/des_pkg.vhd global/global.vhd global/utils.vhd axi/axi_pkg.vhd arbiters/direct_data_arbiter.vhd arbiters/irq_arbiter.vhd arbiters/mt_arbiter.vhd fifo/fifo.vhd mt_ctrl/mt_ctrl.vhd caches/ram.vhd caches/mt_cache.vhd mt_ctrl/mt_cache_ctrl.vhd arbiters/crypto_int_arbiter.vhd caches/ms_cache.vhd ms_ctrl/sr_ff.vhd ms_ctrl/ms_ctrl.vhd bc/desx.vhd bc/bc.vhd crypto/cryptoInt.vhd crypto/cryptoConf.vhd arbiters/ctx_arbiter.vhd io_input/io_input_ctrl.vhd arbiters/sc_arbiter.vhd register/reg_data.vhd arbiters/reg_arbiter.vhd caches/pspe_cache.vhd caches/sp_cache.vhd sec_ctx/security_ctx_ctrl.vhd sec_ctrl/security_ctrl.vhd arbiters/mem_arbiter.vhd mem_ctrl/vci_mem_ctrl.vhd vci_io_target/vci_io_target.vhd vci_input/vci_input_ctrl.vhd vci_merge/vci_merge.vhd vci_split/vci_split.vhd vci_secbus/vci_secbus.vhd axi_vci/axi4lite_2_vci.vhd axi_vci/axi4_2_vci.vhd axi_vci/vci_2_axi4.vhd vci_secbus/axi_secbus_wrapper.vhd demo/axi_monitor.vhd uart2maxilite/bitfield_pkg.vhd uart2maxilite/uart2maxilite.vhd axi_secbus_bridge/axi_secbus_bridge.vhd demo/dbgmux.vhd global/debouncer.vhd uart2maxilite/uart2maxilite_pkg.vhd demo/demo.vhd)
SYSDEF			:= $(SYNDIR)/$(TOP).sysdef
BITSTREAM		:= $(SYNDIR)/$(TOP).bit

syn: $(BITSTREAM) $(SYSDEF)
	
$(BITSTREAM): $(VIVADOSCRIPT) $(VHDLSRCS)
	$(if $(CHIPSCOPE),$(error ERROR: CHIPSCOPE=$(CHIPSCOPE) chipscope not supported yet),)
	$(call BACKUP,$(SYNDIR))
	mkdir -p $(SYNDIR)
	@printf '[VIVADO]      $(SYNDIR)\n'
	cd $(SYNDIR) && $(VIVADO) $(VIVADOFLAGS) $(OUTPUT)

$(SYSDEF): | $(BITSTREAM)

# First Stage Boot Loader (FSBL)
.PHONY: fsbl-orig fsbl-new fsbl-build fsbl

BUILDDIRS		+= $(FSBLDIR)
HSI				:= hsi
HSIFSBLPATCH	:= $(SCRIPTSDIR)/fsbl.patch
HSIFSBLSCRIPT	:= $(SCRIPTSDIR)/fsbl.hsi.tcl
HSIFSBLFLAGS	:= -mode batch -quiet -nojournal -nolog -notrace -source $(HSIFSBLSCRIPT) -tclargs $(SYSDEF) $(FSBLDIR)/orig

fsbl: fsbl-build
fsbl-build: $(FSBLDIR)/new/executable.elf
fsbl-new: $(FSBLDIR)/new/.done
fsbl-orig: $(FSBLDIR)/orig/.done

$(FSBLDIR)/new/executable.elf: $(FSBLDIR)/new/.done
	@printf '[MAKE]        $(@D)\n'
	$(MAKE) -j1 -C $(@D) $(OUTPUT)
	
$(FSBLDIR)/new/.done: $(HSIFSBLPATCH) $(FSBLDIR)/orig/.done
	@printf '[PATCH]       $(@D)\n'
	rm -rf $(@D)
	cp -r $(FSBLDIR)/orig $(@D)
	cd $(FSBLDIR) && patch -p1 -d new < $< $(OUTPUT)
	touch $@

$(FSBLDIR)/orig/.done: $(SYSDEF) $(HSIFSBLSCRIPT) | $(FSBLDIR)
	@printf '[HSI]         $(@D)\n'
	rm -rf $(@D)
	cd $(O) && $(HSI) $(HSIFSBLFLAGS) $(OUTPUT)
	touch $@

# Das U-Boot
.PHONY: u-boot-orig u-boot-new u-boot-config u-boot-build u-boot

UBOOTPATCH		:= $(SCRIPTSDIR)/u-boot.patch
MKIMAGE			:= $(UBOOTDIR)/build/tools/mkimage

u-boot: u-boot-build
u-boot-build: $(UBOOTDIR)/build/u-boot $(UBOOTDIR)/build/tools/mkimage
u-boot-config: $(UBOOTDIR)/build/.config
u-boot-new: $(UBOOTDIR)/new/.done
u-boot-orig: $(UBOOTDIR)/orig/.done

$(UBOOTDIR)/build/u-boot: $(UBOOTDIR)/build/.config
	@printf '[MAKE]        $(<D)\n'
	$(MAKE) -j$(J) -C $(<D) $(OUTPUT)

$(MKIMAGE): | $(UBOOTDIR)/build/u-boot

$(UBOOTDIR)/build/.config: $(UBOOTDIR)/new/.done
	@printf '[CONFIG]      $(@D)\n'
	rm -rf $(@D)
	$(MAKE) -C $(<D) O=$(@D) secbus_zynq_zed_defconfig $(OUTPUT)

$(UBOOTDIR)/new/.done: $(UBOOTPATCH) $(UBOOTDIR)/orig/.done
	@printf '[PATCH]       $(@D)\n'
	rm -rf $(@D)
	cp -rf $(UBOOTDIR)/orig $(@D)
	cd $(UBOOTDIR) && patch -p1 -d new < $< $(OUTPUT)
	touch $@

$(UBOOTDIR)/orig/.done:
	@if [ ! -d $(XLNXUBOOT) ]; then \
		printf 'ERROR: Clone of Xilinx U-Boot git repository not found.\n' && \
		printf '       please set XLNXUBOOT to the correct path:\n' && \
		printf '       make uboot O=$(O) XLNXUBOOT=<some-path>\n' && \
		exit 1; \
	fi
	@printf '[GIT]         $(@D)\n'
	rm -rf $(@D)
	mkdir -p $(@D)
	git -C $(XLNXUBOOT) archive --format=tar $(UBOOTGITTAG) | (cd $(@D) && tar xf -) $(OUTPUT)
	touch $@

# Boot image
.PHONY: boot

BUILDDIRS		+= $(SDCARDDIR) $(BOOTDIR)
BOOTGEN			:= bootgen
BOOTGENFLAGS	:= -w -image
BOOTGENSCRIPT	:= $(SCRIPTSDIR)/boot.bif

boot: $(SDCARDDIR)/boot.bin

$(SDCARDDIR)/boot.bin: $(BOOTDIR)/boot.bif $(BOOTDIR)/fsbl.elf $(BOOTDIR)/bitstream.bit $(BOOTDIR)/u-boot.elf | $(SDCARDDIR)
	@printf '[BOOTGEN]     $@\n'
	cd $(BOOTDIR) && $(BOOTGEN) $(BOOTGENFLAGS) $< -o $@ $(OUTPUT)

$(BOOTDIR)/boot.bif: $(BOOTGENSCRIPT)
$(BOOTDIR)/fsbl.elf: $(FSBLDIR)/new/executable.elf
$(BOOTDIR)/bitstream.bit: $(BITSTREAM)
$(BOOTDIR)/u-boot.elf: $(UBOOTDIR)/build/u-boot

$(BOOTDIR)/boot.bif $(BOOTDIR)/fsbl.elf $(BOOTDIR)/bitstream.bit $(BOOTDIR)/u-boot.elf: | $(BOOTDIR)
	@printf '[CP]          $< -> $@\n'
	cp -f $< $@

# Linux
.PHONY: linux-config linux-build linux

DTC				:= $(LINUXDIR)/scripts/dtc/dtc
MKIMAGEFLAGS	:= -A arm -O linux -C none -T kernel -a $(LINUXLOADADDR) -e $(LINUXLOADADDR)

linux: linux-build
linux-build: $(SDCARDDIR)/uImage
linux-config: $(LINUXDIR)/.config

$(SDCARDDIR)/uImage: $(LINUXDIR)/arch/arm/boot/zImage | $(SDCARDDIR) $(MKIMAGE)
	@printf '[MKIMAGE]     $@\n'
	$(MKIMAGE) $(MKIMAGEFLAGS) -d $< $@ $(OUTPUT)

$(LINUXDIR)/arch/arm/boot/zImage: $(LINUXDIR)/.config
	@printf '[MAKE]        $(LINUXDIR)\n'
	$(MAKE) -j$(J) -C $(LINUXDIR) $(OUTPUT)

$(DTC): | $(LINUXDIR)/arch/arm/boot/zImage

$(LINUXDIR)/.config:
	@if [ ! -d $(XLNXLINUX) ]; then \
		printf 'ERROR: Clone of Xilinx Linux git repository not found.\n' && \
		printf '       please set XLNXLINUX to the correct path:\n' && \
		printf '       make linux O=$(O) XLNXLINUX=<some-path>\n' && \
		exit 1; \
	fi
	$(call BACKUP,$(LINUXDIR))
	rm -rf $(@D)
	@printf '[GIT]         $@\n'
	git -C $(XLNXLINUX) checkout $(LINUXGITTAG) $(OUTPUT)
	@printf '[CONFIG]      $(@D)\n'
	$(MAKE) -j$(J) -C $(XLNXLINUX) O=$(@D) xilinx_zynq_defconfig $(OUTPUT)

# Device Tree
.PHONY: dt-orig dt-new dt-build dt

BUILDDIRS		+= $(DTDIR)
HSI				:= hsi
HSIDTSSCRIPT	:= $(SCRIPTSDIR)/dts.hsi.tcl
HSIDTSPATCH		:= $(SCRIPTSDIR)/dts.patch
HSIDTSFLAGS		:= -mode batch -quiet -notrace -nojournal -nolog -source $(HSIDTSSCRIPT) -tclargs $(SYSDEF) $(XLNXDTS) $(DTDIR)/orig
DTCFLAGS		:= -I dts -O dtb

dt: dt-build
dt-build: $(SDCARDDIR)/devicetree.dtb
dt-new: $(DTDIR)/new/.done
dt-orig: $(DTDIR)/orig/.done

$(SDCARDDIR)/devicetree.dtb: $(DTDIR)/new/.done | $(DTC) $(SDCARDDIR)
	@printf '[DTC]         $@\n'
	$(DTC) $(DTCFLAGS) -i $(DTDIR)/new -o $@ $(DTDIR)/new/system-top.dts $(OUTPUT)

$(DTDIR)/new/.done: $(HSIDTSPATCH) $(DTDIR)/orig/.done
	@printf '[PATCH]       $(@D)\n'
	rm -rf $(@D)
	cp -r $(DTDIR)/orig $(@D)
	cd $(DTDIR) && patch -p1 -d new < $< $(OUTPUT)
	touch $@

$(DTDIR)/orig/.done: $(SYSDEF) $(HSIDTSSCRIPT) | $(DTDIR)
	@if [ ! -d $(XLNXDTS) ]; then \
		printf 'ERROR: Clone of Xilinx device tree sources git repository not found.\n' && \
		printf '       please set XLNXDTS to the correct path:\n' && \
		printf '       make dt O=$(O) XLNXDTS=<some-path>\n' && \
		exit 1; \
	fi
	@printf '[GIT]         $(@D)\n'
	rm -rf $(@D)
	git -C $(XLNXDTS) checkout $(DTSGITTAG) $(OUTPUT)
	@printf '[HSI]         $(@D)\n'
	cd $(O) && $(HSI) $(HSIDTSFLAGS) $(OUTPUT)
	touch $@

# Root filesystem
.PHONY: rootfs-config rootfs-build rootfs

BUILDDIRS		+= $(ROOTFSDIR)
ROOTFSCONFIGDIR	:= $(ROOTDIR)/buildroot
ROOTFSCONFIG	:= $(shell find $(ROOTFSCONFIGDIR) -type f)
ROOTFSOVERLAY	:= $(filter $(ROOTFSCONFIGDIR)/overlay/%,$(ROOTFSCONFIG))
ROOTFSCONFIG	:= $(filter-out $(ROOTFSCONFIGDIR)/overlay/%,$(ROOTFSCONFIG))

rootfs: rootfs-build
rootfs-build: $(SDCARDDIR)/uramdisk.image.gz
rootfs-config: $(ROOTFSDIR)/.config

$(SDCARDDIR)/uramdisk.image.gz: $(ROOTFSDIR)/images/rootfs.cpio.uboot | $(SDCARDDIR)
	@printf '[CP]          $< -> $@\n'
	cp -f $< $@

$(ROOTFSDIR)/images/rootfs.cpio.uboot: $(ROOTFSDIR)/.config $(ROOTFSOVERLAY)
	@printf '[MAKE]        $(ROOTFSDIR)\n'
	export PATH=/bin:/usr/bin && \
	$(MAKE) -C $(ROOTFSDIR) BR2_EXTERNAL=$(ROOTFSCONFIGDIR) $(OUTPUT)

$(ROOTFSDIR)/.config: $(ROOTFSCONFIG)
	@if [ ! -d $(BUILDROOT) ]; then \
		printf 'ERROR: Clone of Buildroot git repository not found.\n' && \
		printf '       please set BUILDROOT to the correct path:\n' && \
		printf '       make rootfs O=$(O) BUILDROOT=<some-path>\n' && \
		exit 1; \
	fi
	$(call BACKUP,$(ROOTFSDIR))
	@printf '[GIT]         $(BUILDROOT)\n'
	git -C $(BUILDROOT) checkout $(BUILDROOTGITTAG) $(OUTPUT)
	@printf '[CONFIG]      $(@D)\n'
	$(MAKE) -C $(BUILDROOT) O=$(ROOTFSDIR) BR2_EXTERNAL=$(ROOTFSCONFIGDIR) secbus_defconfig $(OUTPUT)

# Demonstration host and target applications
.PHONY: demo

BUILDDIRS			+= $(DEMODIR) $(SDCARDDIR) $(SDCARDDIR)/host

DEMOHOSTEXECS		:= $(addprefix $(SDCARDDIR)/host/,u2m_read u2m_write u2m_f2m u2m_m2f)
DEMOHOSTOBJS		:= $(patsubst $(SDCARDDIR)/host/%,$(DEMODIR)/%.o,$(DEMOHOSTEXECS)) $(DEMODIR)/u2m_lib.o
DEMOHOSTDATA		:=
DEMOHOSTFILES		:= $(DEMOHOSTEXECS) $(DEMOHOSTDATA)

DEMOTARGETEXECS		:= $(addprefix $(SDCARDDIR)/,player zb_m2f zb_f2m)
DEMOTARGETOBJS		:= $(patsubst $(SDCARDDIR)/%,$(DEMODIR)/%.o,$(DEMOTARGETEXECS))
DEMOTARGETDATA		:= $(addprefix $(SDCARDDIR)/,data.txt uEnv.txt perf.sh)
DEMOTARGETFILES		:= $(DEMOTARGETEXECS) $(DEMOTARGETDATA)

demo: $(DEMOHOSTFILES) $(DEMOTARGETFILES)

$(DEMOHOSTEXECS): LDLIBS += -lftdi
$(DEMOHOSTEXECS): $(DEMODIR)/u2m_lib.o
$(DEMOHOSTEXECS): $(SDCARDDIR)/host/%: $(DEMODIR)/%.o | $(SDCARDDIR)/host
	@printf '[HOSTCC]      $< -> $@\n'
	$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS) $(OUTPUT)

$(DEMOHOSTOBJS): CFLAGS += -Wall -O3 -I$(ROOTDIR)/C
$(DEMOHOSTOBJS): $(ROOTDIR)/C/u2m_lib.h
$(DEMOHOSTOBJS): $(DEMODIR)/%.o: $(ROOTDIR)/C/%.c | $(DEMODIR)
	@printf '[CC]          $< -> $@\n'
	$(CC) $(CFLAGS) -c $< -o $@ $(OUTPUT)

$(DEMOHOSTDATA): | $(SDCARDDIR)/host
	@printf '[CP]          $< -> $@\n'
	cp -f $< $@

$(DEMOTARGETEXECS): CC := $(CROSS_COMPILE)gcc
$(DEMOTARGETEXECS): $(SDCARDDIR)/%: $(DEMODIR)/%.o | $(SDCARDDIR)
	@printf '[LD]          $< -> $@\n'
	$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS) $(OUTPUT)

$(DEMOTARGETOBJS): CFLAGS += -Wall -O3 -D DEVMEM
$(DEMOTARGETOBJS): $(DEMODIR)/%.o: $(ROOTDIR)/C/%.c | $(DEMODIR)
	@printf '[CC]          $< -> $@\n'
	$(CC) $(CFLAGS) -c $< -o $@ $(OUTPUT)

$(SDCARDDIR)/data.txt: $(ROOTDIR)/data/data.txt
$(SDCARDDIR)/uEnv.txt: $(SCRIPTSDIR)/uEnv.txt
$(SDCARDDIR)/perf.sh: $(SCRIPTSDIR)/perf.sh
$(DEMOTARGETDATA): | $(SDCARDDIR)
	@printf '[CP]          $< -> $@\n'
	cp -f $< $@

# $(O and subdirectories
$(sort $(BUILDDIRS)):
	@printf '[MKDIR]       $@\n'
	mkdir -p $@

.PHONY: all

all: syn fsbl u-boot boot linux dt rootfs demo

.PHONY: clean

clean:
	if [ -d "$(O)" ]; then \
		read -p 'Do you really want to delete $(O)? (Y/N) ' response; \
		if [ "$$response" = "Y" -o "$$response" = "y" ]; then \
			printf 'rm -rf $(O)\n'; \
			rm -rf $(O); \
		else \
			printf 'cancelled\n'; \
		fi; \
	fi

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
